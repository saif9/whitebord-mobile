In total 2 packages have been updated using patch-package.

1. Disable \n from textinput to control the jumping cursor.
2. react-native+0.63.0: It is just for fixing a bug in this react native version. Bug details can be found here: https://github.com/facebook/react-native/issues/29279
2. react-native-pell-rich-editor+1.8.0: This patch solves a problem in which cursor disappears on the focused editor without calling the blur event. Also reduced the padding of rich editor.