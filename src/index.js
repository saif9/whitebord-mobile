import React from 'react';
import {
  StatusBar, Platform, LogBox, Text, TextInput
} from 'react-native';
import { Provider } from 'react-redux';
import allSettled from 'promise.allsettled';
import Toast from 'react-native-toast-message';
import AppNavigator from './scripts/navigation/AppNavigator';
import store from './scripts/state';
import { mixpanel } from './scripts/tracking';

/* Suppress warnings on ReactNativeFiberHostComponent,
   RCTLayoutAnimationGroup for keyboard avoidance in TaskList,
   and ImmutableStateInvariantMiddleware */
LogBox.ignoreLogs([
  'You can now directly use the ref instead',
  'Overriding previous layout animation with new one before the first began',
  'ImmutableStateInvariantMiddleware',
]);

allSettled.shim();

// initialize Mixpanel for user event reporting
mixpanel.init();

// system preferences allows users to change text size for their phone
// this overrides the preferred text size so the app functions normal
Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;

TextInput.defaultProps = TextInput.defaultProps || {};
TextInput.defaultProps.allowFontScaling = false;

const App = () => {
  return (
    <Provider store={store}>
      {Platform.OS === 'ios' && <StatusBar barStyle="dark-content" />}
      <AppNavigator />
      <Toast ref={(ref) => Toast.setRef(ref)} />
    </Provider>
  );
};

export default App;
