import storage from '@react-native-firebase/storage';
import auth from '@react-native-firebase/auth';
import ImagePicker from 'react-native-image-crop-picker';
import perf from '@react-native-firebase/perf';

const uploadFile = async (pathToFile, mimeType) => {
  const trace = await perf().startTrace('upload_file'); // measures how long upload takes
  // create bucket storage reference to not yet existing image
  const me = auth().currentUser;
  const onlinePath = `/users/${me.uid}/${new Date().getTime()}.jpeg`;
  const reference = storage().ref(onlinePath);
  await reference.putString(pathToFile, 'base64', { contentType: 'image/jpeg' }); // uploading image in base64 format
  const url = await reference.getDownloadURL();

  trace.stop(); // stops trace
  return url;
};

export const startImageUpload = async () => {
  const res = await ImagePicker.openPicker({
    mediaType: 'photo',
    compressImageMaxWidth: 300,
    compressImageMaxHeight: 300,
    forceJpg: true,
    cropping: false,
    writeTempFile: false,
    includeBase64: true
  });
  const url = await uploadFile(res.data, res.mime);
  return url;
};
