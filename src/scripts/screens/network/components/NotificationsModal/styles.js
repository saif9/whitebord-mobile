import { StyleSheet } from 'react-native';
import { font } from '../../../../../constants';
import { wp } from '../../../../utils';

const inActiveColor = '#7979A1';
const borderColor = '#DEDDFF';
export default StyleSheet.create({
  modal: {
    marginHorizontal: 0,
    marginBottom: 0,
    flex: 1,
    backgroundColor: 'white',
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  page: {
    flex: 1,
    marginTop: wp(3)
  },
  xButton: {
    alignSelf: 'flex-end',
    padding: wp(3),
    marginTop: wp(2),
  },
  header: {
    marginBottom: wp(4),
  },
  headerText: {
    color: '#434279',
    fontFamily: font.semibold,
    fontSize: 20,
    textAlign: 'center'
  },
  item: {
    borderColor,
    borderBottomWidth: 1,
    padding: 15,
  },
  nameText: {
    fontFamily: font.bold,
    fontSize: 14,
  },
  simpleText: {
    fontFamily: font.regular,
    fontSize: 14,
    maxWidth: wp(80),
  },
  dateText: {
    color: inActiveColor,
    fontFamily: font.regular,
    fontSize: 12,
    marginTop: 4
  },
  avatar: {
    marginRight: 10,
    height: 40
  },
});
