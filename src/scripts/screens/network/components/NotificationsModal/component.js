import React, { useEffect, useState } from 'react';
import {
  Text, View, TouchableOpacity, FlatList
} from 'react-native';
import Modal from 'react-native-modal';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';
import UserAvatar from '../../../../components/UserAvatar';
import styles from './styles';
import X from '../../../../../assets/icons/x.svg';
import { notificationsSelectors, setNewNotification } from '../../../../state/slices/publicProfiles/publicProfileSlice';
import { removeHTMLTags } from '../../../../utils';

const getMessage = (type, data) => {
  switch (type) {
    case 'invite.create':
      return 'wants you to join their network.';
    case 'invite.accept':
      return 'accepted your invitation.';
    case 'invite.decline':
      return 'declined your invitation.';
    case 'invite.cancel':
      return 'canceled the invitation.';
    case 'share.start.projects':
      return `shared project: '${data.name}' with you.`;
    case 'share.stop.projects':
      return `removed you from project: '${data.name}'`;
    case 'share.decline.projects':
      return `is removed from project: '${data.name}'`;
    case 'share.start.whiteboardItems':
      return `shared task: '${removeHTMLTags(data.name)}' with you.`;
    case 'share.stop.whiteboardItems':
      return `removed you from task: '${removeHTMLTags(data.name)}'`;
    case 'share.decline.whiteboardItems':
      return `is removed from task: '${removeHTMLTags(data.name)}'`;
    case 'task.complete':
      return `marked task: '${removeHTMLTags(data.name)}' completed.`;
    default:
      return 'added or removed you.';
  }
};

const NotificationItem = ({ item, lastTime }) => {
  const {
    user: { displayName, photoURL }, createdAt, type, data
  } = item;
  const toHighlight = moment(createdAt).isAfter(moment(lastTime));
  return (
    <View style={[styles.item, { backgroundColor: toHighlight ? '#ABA9FF80' : undefined }]}>
      <View style={{ flexDirection: 'row' }}>
        <UserAvatar style={styles.avatar} size={40} name={`${displayName || ''}`} src={photoURL} />
        <View>
          <Text style={styles.simpleText}>
            <Text style={styles.nameText}>{displayName}</Text>
            {' '}
            {getMessage(type, data)}
            {' '}
          </Text>
          <Text style={styles.dateText}>{moment(createdAt).fromNow()}</Text>
        </View>
      </View>
    </View>
  );
};

const NotificationsModal = ({ isOpen, setOpen }) => {
  const [lastTime, setLastTime] = useState(new Date().toISOString());

  const notifications = useSelector(notificationsSelectors.selectAll)
    .sort((a, b) => moment(b.createdAt).diff(moment(a.createdAt)));

  const dispatch = useDispatch();
  useEffect(() => {
    (async () => {
      const lastSeen = await AsyncStorage.getItem('lastTime') || new Date().toISOString();
      setLastTime(lastSeen);
      AsyncStorage.setItem('lastTime', new Date().toISOString());
    })();
    dispatch(setNewNotification(false));
  }, []);
  return (
    <Modal
      animationType="slide"
      coverScreen
      visible={isOpen}
      style={styles.modal}
    >
      <View style={styles.container}>
        <View style={styles.page}>
          <TouchableOpacity onPress={() => { setOpen(false); }} style={styles.xButton}><X height={15} width={15} /></TouchableOpacity>
          <View style={styles.header}>
            <Text style={styles.headerText}>Notifications</Text>
          </View>
          <FlatList
            data={notifications}
            keyExtractor={(item, index) => item.id + index}
            renderItem={({ item }) => (<NotificationItem item={item} lastTime={lastTime} />)}
          />
        </View>
      </View>
    </Modal>
  );
};

export default NotificationsModal;
