import React, { useState } from 'react';
import {
  Text, View, FlatList, TouchableOpacity
} from 'react-native';
import { useSelector } from 'react-redux';
import UserAvatar from '../../../../components/UserAvatar';
import { publicProfilesSelectors } from '../../../../state/slices/publicProfiles/publicProfileSlice';
import ProfileModal from '../ProfileModal';
import styles from './styles';

const UserItem = ({ currentProfile, setCurrentProfile }) => {
  const {
    firstName, lastName, photoURL, location, jobTitle
  } = currentProfile;
  return (
    <View style={styles.card}>
      <TouchableOpacity style={styles.item} onPress={() => { setCurrentProfile(currentProfile); }}>
        <UserAvatar size={80} name={`${firstName || ''} ${lastName || ''}`} src={photoURL} />
        <Text style={styles.nameText}>{`${firstName || ''} ${lastName || ''}`}</Text>
        <Text style={styles.infoText}>{jobTitle || 'No Job Title'}</Text>
        <Text style={styles.infoText}>{location || 'No Location'}</Text>
      </TouchableOpacity>
    </View>
  );
};

const Empty = ({ setOpen }) => (
  <View style={styles.empty}>
    <Text style={styles.emptyText}>Looks like you don’t have any connections yet.</Text>
    <TouchableOpacity onPress={() => { setOpen(true); }} style={styles.inviteBtn}>
      <Text style={styles.inviteText}>Send Invites</Text>
    </TouchableOpacity>
  </View>
);

const ViewTab = ({ setOpen, searchText }) => {
  const publicProfiles = useSelector(publicProfilesSelectors.selectAll)
    .sort((a, b) => a.firstName - b.firstName)
    .filter(({ firstName, lastName }) => {
      // eslint-disable-next-line prefer-template
      return searchText ? (firstName + ' ' + lastName).toLowerCase()?.startsWith(searchText.toLowerCase()) : true;
    });
  const [currentProfile, setCurrentProfile] = useState();
  return (
    <View style={styles.container}>
      {currentProfile && <ProfileModal currentProfile={currentProfile} setCurrentProfile={setCurrentProfile} />}
      <View style={styles.page}>
        <FlatList
          data={publicProfiles}
          keyExtractor={(item, index) => item + index}
          renderItem={({ item }) => (<UserItem currentProfile={item} setCurrentProfile={setCurrentProfile} />)}
          numColumns={2}
          ListEmptyComponent={() => <Empty setOpen={setOpen} />}
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
        />
      </View>
    </View>
  );
};

export default ViewTab;
