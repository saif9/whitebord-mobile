import { StyleSheet } from 'react-native';
import { font } from '../../../../../constants';
import { wp } from '../../../../utils';

const bluClr = '#6574FF';
const inActiveColor = '#7979A1';
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F8F9FF',
    borderTopWidth: 1,
    borderColor: '#DEDDFF'
  },
  page: {
    marginHorizontal: wp(2),
    marginTop: wp(2)
  },
  nameText: {
    fontFamily: font.semibold,
    fontSize: 14,
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 3,
    color: '#434279'
  },
  card: {
    backgroundColor: '#fff',
    borderRadius: 5,
    marginHorizontal: '5%',
    marginVertical: '3%',
    width: '40%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 5,
  },
  item: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
  },
  infoText: {
    color: inActiveColor,
    fontFamily: font.regular,
    fontSize: 10,
    textAlign: 'center',
    textTransform: 'uppercase'
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 80 / 2,
  },
  empty: {
    borderStyle: 'dashed',
    borderColor: '#DEDDFF',
    borderRadius: 4,
    borderWidth: 2,
    alignItems: 'center',
    margin: wp(5),
    padding: wp(5)
  },
  emptyText: {
    paddingBottom: wp(4),
    fontFamily: font.regular,
    fontSize: 14,
    color: inActiveColor
  },
  inviteBtn: {
    backgroundColor: bluClr,
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderRadius: 20,
    marginBottom: 5
  },
  inviteText: {
    fontFamily: font.regular,
    fontSize: 16,
    color: '#fff'
  },
});
