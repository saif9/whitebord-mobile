import React from 'react';
import {
  Text, View, SectionList, TouchableOpacity
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import styles from './styles';
import InvitationSent from '../../../../../assets/icons/invitation_sent.svg';
import {
  invitesSentSelectors, invitesReceivedSelectors, removeInvite, acceptInvite
} from '../../../../state/slices/publicProfiles/publicProfileSlice';
import UserAvatar from '../../../../components/UserAvatar/component';

const NO_CONTENT = 'no-content';

const SentItem = ({ item }) => {
  const { to: { email }, createdAt } = item;
  const dispatch = useDispatch();
  return (
    <View style={styles.item}>
      <View style={{ flexDirection: 'row' }}>
        <View style={styles.avatar}><InvitationSent /></View>
        <View>
          <Text style={styles.simpleInviteText}>
            Invitation sent to
            {' '}
            <Text style={styles.nameText}>{email}</Text>
          </Text>
          <Text style={styles.dateText}>{moment(createdAt).fromNow()}</Text>
        </View>
      </View>
      <View style={styles.btnContainer}>
        <TouchableOpacity style={styles.declineBtn} onPress={() => dispatch(removeInvite(item))}>
          <Text style={styles.declineText}>Cancel</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const ReceivedItem = ({ item }) => {
  const {
    from: {
      uid, firstName, lastName, photoURL
    }, createdAt
  } = item;
  const dispatch = useDispatch();
  return (
    <View style={styles.item}>
      <View style={{ flexDirection: 'row' }}>
        <UserAvatar style={styles.avatar} size={40} name={`${firstName || ''} ${lastName || ''}`} src={photoURL} />
        <View>
          <Text style={styles.simpleText}>
            <Text style={styles.nameText}>{`${firstName} ${lastName}`}</Text>
            {' '}
            wants you to join their network.
            {' '}
          </Text>
          <Text style={styles.dateText}>{moment(createdAt).fromNow()}</Text>
        </View>
      </View>
      <View style={styles.btnContainer}>
        <TouchableOpacity style={styles.declineBtn} onPress={() => dispatch(removeInvite(item))}>
          <Text style={styles.declineText}>Decline</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.acceptBtn} onPress={() => dispatch(acceptInvite(uid))}>
          <Text style={styles.acceptText}>Accept</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const NoContent = () => (
  <View style={styles.noContent}>
    <Text style={styles.noContentText}>No Invites</Text>
  </View>
);

const ManageTab = ({ searchText }) => {
  const invitesSent = useSelector(invitesSentSelectors.selectAll);
  const invitesReceived = useSelector(invitesReceivedSelectors.selectAll);
  let invitesSentData = [NO_CONTENT];
  let invitesReceivedData = [NO_CONTENT];
  if (invitesSent.length > 0) { // We are searching user on the basis of email and sorting datewise
    invitesSentData = invitesSent.filter(({ to: { email } }) => email.toLowerCase().startsWith(searchText.toLowerCase()))
      .sort((a, b) => moment(b.createdAt).diff(moment(a.createdAt)));
  }
  if (invitesReceived.length > 0) { // We are searching user on the basis of firstName and lastName and sorting datewise
    invitesReceivedData = invitesReceived.filter(({ from: { firstName, lastName } }) => {
      // eslint-disable-next-line prefer-template
      return searchText ? (firstName + ' ' + lastName).toLowerCase()?.startsWith(searchText.toLowerCase()) : true;
    }).sort((a, b) => moment(b.createdAt).diff(moment(a.createdAt)));
  }
  const DATA = [
    {
      title: 'Received',
      data: invitesReceivedData,
    },
    {
      title: 'Pending',
      data: invitesSentData,
    },
  ];
  return (
    <View style={styles.container}>
      <View style={styles.page}>
        <SectionList
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          sections={DATA}
          stickySectionHeadersEnabled={false}
          keyExtractor={(item, index) => item.id + index}
          renderItem={({ item, section: { title } }) => {
            if (item === NO_CONTENT) return <NoContent />;
            return title === 'Pending' ? <SentItem item={item} /> : <ReceivedItem item={item} />;
          }}
          renderSectionHeader={({ section: { title } }) => (
            <View style={styles.header}><Text style={styles.headerText}>{title}</Text></View>
          )}
        />
      </View>
    </View>
  );
};

export default ManageTab;
