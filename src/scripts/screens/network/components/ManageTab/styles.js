import { StyleSheet } from 'react-native';
import { font } from '../../../../../constants';
import { hp, wp } from '../../../../utils';

const inActiveColor = '#7979A1';
const borderColor = '#DEDDFF';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  page: {
    marginHorizontal: wp(3),
    marginTop: wp(3)
  },
  header: {
    borderColor,
    borderBottomWidth: 1,
    marginVertical: 8,
  },
  headerText: {
    fontFamily: font.regular,
    fontSize: 15,
    paddingVertical: 4,
    color: inActiveColor
  },
  item: {
    backgroundColor: '#F8F9FF',
    borderColor,
    borderWidth: 1,
    borderRadius: 10,
    padding: 15,
    marginVertical: 8
  },
  nameText: {
    fontFamily: font.bold,
    fontSize: 14,
  },
  simpleText: {
    fontFamily: font.regular,
    fontSize: 14,
    maxWidth: wp(80),
  },
  simpleInviteText: {
    fontFamily: font.regular,
    fontSize: 14,
  },
  dateText: {
    color: inActiveColor,
    fontFamily: font.regular,
    fontSize: 10,
    marginTop: 4
  },
  avatar: {
    marginRight: 10,
    height: 40,
  },
  btnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: 10
  },
  acceptBtn: {
    backgroundColor: '#ABA9FF',
    borderRadius: 5,
    paddingVertical: 5,
    paddingHorizontal: 20
  },
  declineBtn: {
    paddingVertical: 5,
    paddingHorizontal: 10
  },
  acceptText: {
    color: '#fff',
    fontFamily: font.regular,
    fontSize: 14,
  },
  declineText: {
    color: inActiveColor,
    fontFamily: font.regular,
    fontSize: 14,
  },
  noContent: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: hp(2),
  },
  noContentText: {
    color: inActiveColor,
    fontFamily: font.regular,
    fontSize: 14,
  },
});
