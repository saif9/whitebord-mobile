import { StyleSheet } from 'react-native';
import { font } from '../../../../../constants';
import { wp } from '../../../../utils';

export default StyleSheet.create({
  modal: {
    marginHorizontal: 0,
    marginBottom: 0,
    marginTop: 25,
    flex: 1,
    backgroundColor: 'white',
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  page: {
    marginHorizontal: wp(3),
    marginTop: wp(3)
  },
  xButton: {
    alignItems: 'flex-end',
    margin: wp(3),
  },
  header: {
    marginVertical: 0,
  },
  headerText: {
    color: '#434279',
    fontFamily: font.semibold,
    fontSize: 20,
    textAlign: 'center'
  },
  subHeadingText: {
    marginTop: wp(8),
    marginBottom: wp(3),
    color: '#9D9DBA',
    fontFamily: font.semibold,
    fontSize: 14,
    textAlign: 'center'
  },
  descriptionText: {
    margin: wp(3),
    color: '#434279',
    fontFamily: font.semibold,
    fontSize: 16,
  },
  input: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: wp(3),
    padding: wp(2),
    backgroundColor: '#F8F9FF',
    borderColor: '#DEDDFF',
    borderWidth: 1,
    borderRadius: 5,
  },
  inviteBtnInActive: {
    marginTop: wp(8),
    backgroundColor: '#7979A1',
    borderRadius: 40
  },
  inviteBtnActive: {
    marginTop: wp(8),
    backgroundColor: '#6574FF',
    borderRadius: 40
  },
  inviteText: {
    paddingVertical: wp(4),
    paddingHorizontal: wp(20),
    color: '#fff',
    fontFamily: font.semibold,
    fontSize: 15,
  },
  emailView: {
    alignSelf: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: wp(1),
    paddingHorizontal: wp(1),
    paddingVertical: wp(1),
    backgroundColor: '#fff',
    borderColor: '#DEDDFF',
    borderWidth: 1,
    borderRadius: 10,
  },
  emailText: {
    marginHorizontal: wp(2),
    color: '#434279',
    fontFamily: font.regular,
    fontSize: 14,
  },
  xEmailButton: {
    marginHorizontal: wp(1),
  }
});
