import React, { useRef, useState } from 'react';
import {
  Text, View, TouchableOpacity, TextInput, ActivityIndicator
} from 'react-native';
import Modal from 'react-native-modal';
import { useDispatch } from 'react-redux';
import styles from './styles';
import X from '../../../../../assets/icons/x.svg';
import { checkEmail } from '../../../../state/slices/publicProfiles/api';
import { createInvites } from '../../../../state/slices/publicProfiles/publicProfileSlice';
import { emailPattern } from '../../../../../constants';
import { trackCollaboratorInvitation } from '../../../../tracking';

const EmailView = ({ vEmail, onDelete }) => (
  <View>
    <View style={styles.emailView}>
      <Text style={styles.emailText}>{vEmail.email}</Text>
      <TouchableOpacity style={styles.xEmailButton} onPress={() => onDelete(vEmail.email)}><X /></TouchableOpacity>
    </View>
  </View>
);

const InviteModal = ({ isOpen, setOpen }) => {
  const [emails, setEmails] = useState([]);
  const [loading, setLoading] = useState(false);
  const [eText, setEText] = useState('');
  const inputRef = useRef();
  const dispatch = useDispatch();
  const onDelete = (email) => {
    setEmails(emails.filter((vEmail) => vEmail.email !== email));
  };

  const addEmail = async (email) => {
    inputRef.current.clear();
    setEText('');
    if (emailPattern.test(email)) {
      setEmails([...emails, { email, uid: '' }]);
      setLoading(true);
      const { success, uid } = await checkEmail(email);
      const prevEmails = emails.filter((vEmail) => vEmail.email !== email);
      if (success) {
        setEmails([...prevEmails, { email, uid }]);
      } else {
        setEmails([...prevEmails]);
      }
      setLoading(false);
    }
  };

  const onPressInvite = async () => {
    if (eText.trim().length > 0) {
      await addEmail(eText.trim().toLowerCase());
    }
    let updatedEmails = [];
    setEmails((mails) => {
      updatedEmails = mails;
      return mails;
    });
    if (updatedEmails.length > 0) {
      setLoading(true);
      await dispatch(createInvites(updatedEmails));
      trackCollaboratorInvitation();
      setLoading(false);
      setOpen(false);
    }
  };

  return (
    <Modal
      animationType="slide"
      coverScreen
      visible={isOpen}
      style={styles.modal}
    >
      <View style={styles.container}>
        <View style={styles.page}>
          <TouchableOpacity onPress={() => { setOpen(false); }} style={styles.xButton}><X height={15} width={15} /></TouchableOpacity>
          <View style={styles.header}>
            <Text style={styles.headerText}>Invite Collaborators</Text>
          </View>
          <Text style={styles.subHeadingText}>Invite collaborators to join your network.</Text>
          <Text style={styles.descriptionText}>Enter their email(s):</Text>
          <View style={styles.input}>
            {emails.map((vEmail) => <EmailView key={vEmail.email} vEmail={vEmail} onDelete={onDelete} />)}
            <TextInput ref={inputRef}
              keyboardType="email-address"
              placeholder="Add Email..."
              autoFocus
              value={eText}
              blurOnSubmit={false}
              onSubmitEditing={() => {
                addEmail(eText.trim().toLowerCase());
              }}
              style={{ height: 40 }}
              onChangeText={(text) => {
                setEText(text);
                if (text.endsWith(' ')) { addEmail(text.trim().toLowerCase()); }
              }}
            />
          </View>
          <View style={{ alignItems: 'center' }}>
            <TouchableOpacity style={emails.length > 0 ? styles.inviteBtnActive : styles.inviteBtnInActive}
              onPress={onPressInvite}
            >
              {loading ? <ActivityIndicator style={styles.inviteText} color="#fff" />
                : <Text style={styles.inviteText}>Invite</Text>}
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default InviteModal;
