import { StyleSheet } from 'react-native';
import { font } from '../../../../../../constants';
import { wp } from '../../../../../utils';

export default StyleSheet.create({
  modal: {
    marginHorizontal: 0,
    marginBottom: 0,
    flex: 1,
    backgroundColor: 'white',
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  page: {
    flex: 1,
    marginTop: wp(3)
  },
  xButton: {
    alignItems: 'flex-end',
    margin: wp(3),
  },
  header: {
    marginBottom: wp(4),
  },
  headerText: {
    color: '#434279',
    fontFamily: font.semibold,
    fontSize: 20,
    textAlign: 'center'
  },
  item: {
    margin: 15
  },
  labelText: {
    marginLeft: 15,
    marginVertical: 3,
    color: '#9D9DBA',
    fontFamily: font.regular,
    fontSize: 12,
  },
  avatarContainer: {
    width: 130,
    height: 130,
    borderRadius: 130 / 2,
    alignSelf: 'center',
    overflow: 'hidden'
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 130 / 2,
  },
  inputContainer: {
    backgroundColor: '#F8F9FF',
    borderColor: '#DEDDFF',
    borderRadius: 40,
    borderWidth: 1,
    height: 35,
    paddingHorizontal: '4%'
  },
  editPic: {
    marginTop: -43,
    width: 120,
    height: 40,
    backgroundColor: '#C4C4C490',
    alignSelf: 'center',
    justifyContent: 'center'
  },
  editPicText: {
    color: '#434279',
    alignSelf: 'center',
    fontFamily: font.bold,
    fontSize: 14,
  },
  btnActive: {
    marginTop: '10%',
    backgroundColor: '#6574FF',
    borderRadius: 40,
    width: '50%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnText: {
    paddingVertical: '6%',
    color: '#F4F5F4',
    fontFamily: font.semibold,
    fontSize: 15,
  }
});
