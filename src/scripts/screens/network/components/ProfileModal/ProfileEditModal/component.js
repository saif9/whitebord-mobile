import React, { useState } from 'react';
import {
  Text, View, TouchableOpacity, TextInput, ActivityIndicator
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useDispatch } from 'react-redux';
import styles from './styles';
import X from '../../../../../../assets/icons/x.svg';
import { updateMyProfile } from '../../../../../state/slices/publicProfiles/publicProfileSlice';
import { startImageUpload } from '../../../../../imageUploader';
import UserAvatar from '../../../../../components/UserAvatar';

const inActiveColor = '#7979A1';
const Input = ({
  label, value, setValue, disabled
}) => (
  <View style={{ margin: '2%' }}>
    <Text style={styles.labelText}>{label}</Text>
    <View style={styles.inputContainer}>
      <TextInput style={{ flex: 1, color: disabled ? inActiveColor : undefined }}
        editable={!disabled}
        value={value}
        onChangeText={setValue}
      />
    </View>
  </View>
);

const ProfileEditModal = ({ currentProfile, setCurrentProfile }) => {
  const dispatch = useDispatch();
  const [profile, setProfile] = useState(currentProfile);
  const [loading, setLoading] = useState(false);
  const {
    firstName, lastName, email, photoURL, location, jobTitle
  } = profile;
  const updateProfile = (field, value) => {
    setProfile({ ...profile, [field]: value });
  };
  const chooseImage = async () => {
    try {
      setLoading(true);
      const url = await startImageUpload();
      updateProfile('photoURL', url);
    } catch (e) {
      console.log(e.message);
    } finally {
      setLoading(false);
    }
  };
  return (
    <View style={styles.container}>
      <View style={styles.page}>
        <TouchableOpacity onPress={() => { setCurrentProfile(undefined); }} style={styles.xButton}>
          <X height={15} width={15} />
        </TouchableOpacity>
        <View style={styles.header}>
          <Text style={styles.headerText}>Edit Your Profile</Text>
        </View>
        <KeyboardAwareScrollView>
          <View style={styles.item}>
            <View style={styles.avatarContainer}>
              <UserAvatar size={130} name={`${firstName || ''} ${lastName || ''}`} src={photoURL} />
              <TouchableOpacity style={styles.editPic}
                onPress={chooseImage}
              >
                <Text style={styles.editPicText}>edit picture</Text>
              </TouchableOpacity>
            </View>
            <View>
              <Input label="First Name" value={firstName} setValue={(val) => updateProfile('firstName', val)} />
              <Input label="Last Name" value={lastName} setValue={(val) => updateProfile('lastName', val)} />
              <Input label="Email" value={email} disabled />
              <Input label="Location" value={location} setValue={(val) => updateProfile('location', val)} />
              <Input label="Job Title" value={jobTitle} setValue={(val) => updateProfile('jobTitle', val)} />
            </View>
            <TouchableOpacity style={styles.btnActive}
              onPress={async () => {
                setLoading(true);
                await dispatch(updateMyProfile(profile));
                setLoading(false);
                setCurrentProfile(undefined);
              }}
            >
              {loading ? <ActivityIndicator style={styles.btnText} color="#fff" />
                : <Text style={styles.btnText}>Save Changes</Text> }

            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </View>
    </View>
  );
};

export default ProfileEditModal;
