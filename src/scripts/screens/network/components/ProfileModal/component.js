import React, { useState } from 'react';
import {
  Text, View, TouchableOpacity, Linking
} from 'react-native';
import Modal from 'react-native-modal';
import { useDispatch } from 'react-redux';
import UserAvatar from '../../../../components/UserAvatar';
import styles from './styles';
import X from '../../../../../assets/icons/x.svg';
import ProfileEditModal from './ProfileEditModal';
import { removeProfile } from '../../../../state/slices/publicProfiles/publicProfileSlice';

const UserItem = ({
  currentProfile, edit, setEditMode, setCurrentProfile
}) => {
  const {
    id, firstName, lastName, email, photoURL, location, jobTitle
  } = currentProfile;
  const dispatch = useDispatch();
  return (
    <View style={styles.container}>
      <View style={styles.page}>
        <TouchableOpacity onPress={() => { setCurrentProfile(undefined); }} style={styles.xButton}>
          <X height={15} width={15} />
        </TouchableOpacity>
        <View style={styles.item}>
          <UserAvatar size={120} name={`${firstName || ''} ${lastName || ''}`} src={photoURL} />
          <Text style={styles.nameText}>{`${firstName} ${lastName}`}</Text>
          <Text style={styles.infoText}>{`${jobTitle || 'No Job Title'} | ${location || 'No Location'}`}</Text>
          <Text style={styles.emailText}>{email}</Text>
          {!edit && (
          <TouchableOpacity style={styles.btnActive} onPress={() => { Linking.openURL(`mailto:${email}`); }}>
            <Text style={styles.btnText}>Send a message</Text>
          </TouchableOpacity>
          )}
          {edit ? (
            <TouchableOpacity style={styles.btnEdit} onPress={() => { setEditMode(true); }}>
              <Text style={styles.inactiveBtnText}>Edit Profile</Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity style={styles.btnRemoveColab}
              onPress={() => {
                setCurrentProfile(undefined);
                dispatch(removeProfile(id));
              }}
            >
              <Text style={styles.inactiveBtnText}>Remove collaborator</Text>
            </TouchableOpacity>
          )}

        </View>
      </View>
    </View>
  );
};

const ProfileModal = ({ currentProfile, setCurrentProfile, edit }) => {
  const [editMode, setEditMode] = useState(false);
  return (
    <Modal
      animationIn="bounceIn"
      animationOut="bounceOut"
      visible={!!currentProfile}
      onBackdropPress={() => {
        setCurrentProfile(undefined);
      }}
      style={editMode ? styles.fullModal : styles.modal}
    >
      {editMode ? <ProfileEditModal currentProfile={currentProfile} setCurrentProfile={setCurrentProfile} />
        : <UserItem currentProfile={currentProfile} edit={edit} setEditMode={setEditMode} setCurrentProfile={setCurrentProfile} />}

    </Modal>
  );
};

export default ProfileModal;
