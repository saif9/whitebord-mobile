import { StyleSheet } from 'react-native';
import { font } from '../../../../../constants';

const inActiveColor = '#7979A1';
export default StyleSheet.create({
  modal: {
    margin: 0,
    backgroundColor: '#000000aa',
  },
  fullModal: {
    marginHorizontal: 0,
    marginBottom: 0,
    flex: 1,
    backgroundColor: 'white',
  },
  container: {
    backgroundColor: 'white',
    width: '80%',
    alignSelf: 'center',
    borderRadius: 20,
    padding: 15
  },
  page: {
  },
  xButton: {
    alignItems: 'flex-end',
  },
  nameText: {
    fontFamily: font.semibold,
    fontSize: 20,
    textAlign: 'center',
    marginVertical: '4%',
    color: '#434279'
  },
  item: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  infoText: {
    marginVertical: 4,
    color: inActiveColor,
    fontFamily: font.semibold,
    fontSize: 14,
    textAlign: 'center',
    textTransform: 'uppercase'
  },
  emailText: {
    color: inActiveColor,
    fontFamily: font.regular,
    fontSize: 14,
    textAlign: 'center',
  },
  avatar: {
    width: 120,
    height: 120,
    borderRadius: 120 / 2,
  },
  btnActive: {
    marginTop: '10%',
    backgroundColor: '#6574FF',
    borderRadius: 40,
    width: '60%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnRemoveColab: {
    marginVertical: '4%',
    backgroundColor: '#F8F9FF',
    borderColor: '#DEDDFF',
    borderRadius: 40,
    borderWidth: 1,
    width: '60%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnEdit: {
    margin: '10%',
    paddingVertical: '2%',
    backgroundColor: '#F8F9FF',
    borderColor: '#DEDDFF',
    borderRadius: 40,
    borderWidth: 1,
    width: '60%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnText: {
    paddingVertical: '4%',
    color: '#fff',
    fontFamily: font.semibold,
    fontSize: 15,
  },
  inactiveBtnText: {
    paddingVertical: '4%',
    color: inActiveColor,
    fontFamily: font.semibold,
    fontSize: 15,
  },
});
