import React, { useState } from 'react';
import {
  Text, TextInput, View, TouchableOpacity
} from 'react-native';
import { useSelector } from 'react-redux';
import styles from './styles';
import NotificationIcon from '../../../assets/icons/notification.svg';
import SearchIcon from '../../../assets/icons/search.svg';
import {
  InviteModal, ManageTab, ViewTab, NotificationsModal, ProfileModal
} from './components';
import UserAvatar from '../../components/UserAvatar';
import EditModal from '../../components/modals/EditModal';
import UniversalPlus from '../../components/UniversalPlus';

const NetworkScreen = ({ navigation }) => {
  const [secondTab, setSecondTab] = useState(false);
  const [isOpen, setOpen] = useState(false);
  const [isNotiOpen, setNotiOpen] = useState(false);
  const [currentProfile, setCurrentProfile] = useState();
  const myProfile = useSelector((state) => state.publicProfiles.myProfile);
  const [searchText, setSearchText] = useState('');
  const { photoURL, firstName, lastName } = myProfile;
  const showIndicator = useSelector(
    (state) => state.publicProfiles.newNotification,
  );

  return (
    <>
      <EditModal />
      <View style={styles.container}>
        {isOpen && <InviteModal isOpen={isOpen} setOpen={setOpen} />}
        {isNotiOpen && <NotificationsModal isOpen={isNotiOpen} setOpen={setNotiOpen} />}
        {currentProfile && <ProfileModal currentProfile={currentProfile} setCurrentProfile={setCurrentProfile} edit />}
        <View style={styles.networkPage}>
          <View style={styles.networkHeader}>
            <Text style={styles.newtworkHeaderText}>My Network</Text>
            <View style={styles.networkHeaderGroup}>
              <TouchableOpacity onPress={() => setNotiOpen(true)}>
                <View style={showIndicator ? styles.notiIconContainerActive : styles.notiIconContainerInActive}>
                  <NotificationIcon height={20} style={showIndicator ? styles.notiIconActive : styles.notiIconInActive} />
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ paddingLeft: 10 }} onPress={() => setCurrentProfile(myProfile)}>
                <UserAvatar size={40} name={`${firstName || ''} ${lastName || ''}`} src={photoURL} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.searchContainer}>
            <SearchIcon />
            <TextInput style={styles.searchInput} value={searchText} onChangeText={setSearchText} />
          </View>
          <View style={styles.tabBar}>
            <TouchableOpacity onPress={() => setSecondTab(false)} style={secondTab ? styles.tab : styles.tabSelected}>
              <Text style={secondTab ? styles.tabText : styles.tabTextSelected}>Manage</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => setSecondTab(true)} style={secondTab ? styles.tabSelected : styles.tab}>
              <Text style={secondTab ? styles.tabTextSelected : styles.tabText}>View</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { setOpen(true); }} style={styles.inviteBtn}>
              <Text style={styles.inviteText}>Send Invites</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flex: 1 }}>
          {
          secondTab ? <ViewTab setOpen={setOpen} searchText={searchText} /> : <ManageTab searchText={searchText} />
        }
        </View>
      </View>
      <UniversalPlus />
    </>
  );
};

export default NetworkScreen;
