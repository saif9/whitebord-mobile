import { StyleSheet } from 'react-native';
import { font } from '../../../constants';
import { wp } from '../../utils';

const bluClr = '#6574FF';
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  networkPage: {
    marginHorizontal: wp(5),
  },
  networkHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  networkHeaderGroup: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  newtworkHeaderText: {
    color: '#434279',
    marginRight: 5,
    fontFamily: font.semibold,
    fontSize: 20,
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: wp(90),
    backgroundColor: '#F8F9FF',
    height: 35,
    borderRadius: 20,
    paddingHorizontal: 4
  },
  searchInput: {
    flex: 1,
    paddingHorizontal: 9,
  },
  tabBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 5,
  },
  tab: {
    padding: 8,
    borderColor: '#fff',
    borderBottomWidth: 2
  },
  tabSelected: {
    padding: 8,
    borderColor: bluClr,
    borderBottomWidth: 2
  },
  tabText: {
    fontFamily: font.regular,
    fontSize: 16,
  },
  tabTextSelected: {
    fontFamily: font.regular,
    fontSize: 16,
    color: bluClr
  },
  inviteBtn: {
    backgroundColor: bluClr,
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderRadius: 20,
    marginBottom: 5
  },
  inviteText: {
    fontFamily: font.regular,
    fontSize: 16,
    color: '#fff'
  },
  notiIconContainerActive: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 30 / 2,
    backgroundColor: '#ABA9FF',
    marginLeft: 10,
  },
  notiIconContainerInActive: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 30 / 2,
    backgroundColor: '#F8F9FF',
    marginLeft: 10,
  },
  notiIconActive: {
    color: '#fff',
  },
  notiIconInActive: {
    color: '#ABA9FF',
  },
});
