import { StyleSheet } from 'react-native';
import { font } from '../../../constants';
import { wp } from '../../utils';

export default StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    marginHorizontal: wp(5),
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  addSection: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    marginVertical: 1,
  },
  arrow: {
    marginRight: 10,
  },
  addSectionText: {
    fontFamily: font.regular,
    fontSize: 16,
    color: '#434279',
  },
  headerText: {
    fontFamily: font.semibold,
    color: '#434279',
    fontSize: 20,
  },
});
