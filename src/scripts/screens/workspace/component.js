import React, {
  useState, useCallback, useRef, useEffect
} from 'react';
import {
  Text, TextInput, TouchableWithoutFeedback, View, Keyboard, KeyboardAvoidingView
} from 'react-native';
import DraggableFlatList from 'react-native-draggable-flatlist';
import { useSelector, useDispatch } from 'react-redux';
import styles from './styles';
import EditModal from '../../components/modals/EditModal';
import UniversalPlus from '../../components/UniversalPlus';
import Section from './components/Section';
import NewSection from '../../../assets/icons/new_section.svg';
import InboxButton from '../../components/InboxButton';
import { memoAllSections } from '../../state/slices/sections/selectors';
import { createSection } from '../../state/slices/sections/sectionsSlice';
import { upsertUser } from '../../state/slices/user/userSlice';

const WorkspaceScreen = ({ navigation }) => {
  const [text, setText] = useState('');
  const [newId, setNewId] = useState('');
  const [sortedIds, setSortedIds] = useState([]);
  const dispatch = useDispatch();
  const sections = useSelector(memoAllSections);
  // sorting here again to reduce the drag n drop lag that was causing flickering
  sections.sort((secA, secB) => sortedIds.indexOf(secA.id) - sortedIds.indexOf(secB.id));
  const newSectionInputRef = useRef();

  useEffect(() => {
    setSortedIds(sections.map((sec) => sec.id));
  }, [sections]);

  const generateSection = () => {
    const secId = createSection({ name: text }, dispatch);
    setText('');
    setNewId(secId);
    setSortedIds([...sortedIds, secId]);
    newSectionInputRef.current.clear();
  };
  const renderItem = useCallback(
    ({
      item, drag, isActive
    }) => (
      item
        ? (
          <Section
            key={item.id}
            id={item.id}
            navigation={navigation}
            onSectionLongPress={drag}
            isSectionActive={isActive}
            newlyCreated={newId && item.id === newId}
          />
        ) : <View />
    ),
    [newId]
  );

  const renderFooter = () => (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <View style={styles.addSection}>
        <NewSection marginLeft={20} marginRight={10} />
        <TextInput
          placeholder="Add Section"
          placeholderTextColor="#6574FF"
          onChangeText={setText}
          defaultValue={text}
          onSubmitEditing={generateSection}
          style={styles.addSectionText}
          ref={newSectionInputRef}
        />
      </View>
    </TouchableWithoutFeedback>
  );
  return (
    <>
      <EditModal />
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding"
        keyboardVerticalOffset={80}
      >
        <View style={styles.header}>
          <Text style={styles.headerText}>Workspace</Text>
          <InboxButton navigation={navigation} />
        </View>
        <DraggableFlatList
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          listKey="section-list"
          data={sections}
          renderItem={renderItem}
          keyExtractor={(item, _index) => `draggable-section-${item.id}`}
          onDragEnd={({ data }) => {
            const ids = data.map((sec) => sec.id);
            setSortedIds(ids);
            dispatch(upsertUser({ updates: { sections: ids } }));
          }}
          ListFooterComponent={renderFooter()}
        />
      </KeyboardAvoidingView>
      <UniversalPlus />

    </>
  );
};

export default WorkspaceScreen;
