import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  sectionContainer: {
    borderBottomColor: '#E5E5EA',
    borderBottomWidth: 0.5,
  },
});
