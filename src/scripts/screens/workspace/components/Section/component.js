import React, {
  useState, useCallback, useMemo, useEffect
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { View } from 'react-native';
import DraggableFlatList from 'react-native-draggable-flatlist';
import Project from '../Project';
import AccordionListItem from '../AccordianListItem';
import ProjectEditModal from '../../../../components/modals/ProjectEditModal';
import styles from './styles';
import { getSharedSectionId } from '../../../../utils';
import { deleteSection, upsertSection } from '../../../../state/slices/sections/sectionsSlice';
import { memoSectionById } from '../../../../state/slices/sections/selectors';

const Section = ({
  id, navigation, onSectionLongPress, isSectionActive, newlyCreated
}) => {
  const dispatch = useDispatch();
  const [editMode, setEditMode] = useState(false);
  const [createProjectModal, setCreateProjectModal] = useState(false);
  const selectSection = useMemo(memoSectionById, []);
  const [sortedIds, setSortedIds] = useState([]);
  const { name, projects } = useSelector((state) => selectSection(state, id));
  // sorting here to reduce the drag n drop lag that causing flickering
  projects.sort((projA, projB) => sortedIds.indexOf(projA) - sortedIds.indexOf(projB));

  const swipeToDelete = () => {
    dispatch(deleteSection({ updates: { id } }));
  };

  const updateSectionName = (sectionName) => {
    dispatch(upsertSection({ updates: { id, name: sectionName } }));
    setEditMode(false);
  };

  useEffect(() => {
    setSortedIds(projects);
  }, [projects]);

  const isSharedSectionEmpty = id === getSharedSectionId() && projects.length === 0;
  const swipeoutBtns = [
    {
      text: 'edit',
      color: 'white',
      backgroundColor: '#626DFF',
      onPress() { setEditMode(true); }
    },
    {
      text: 'delete',
      color: 'white',
      backgroundColor: '#FA8072',
      onPress() { swipeToDelete(); }
    },
  ];

  const renderItem = useCallback(
    ({
      item, index, drag, isActive
    }) => {
      return (
        <Project
          key={item}
          id={item}
          sectionId={id}
          navigation={navigation}
          onLongPress={drag}
          isActive={isActive}
        />

      );
    },
    []
  );

  return (
    isSharedSectionEmpty || !projects
      ? <View />
      : (
        <View style={styles.sectionContainer}>
          {/* For Creating New Project */}
          <ProjectEditModal
            editProject={createProjectModal}
            setEditProject={setCreateProjectModal}
            sectionId={id}
            projectId={null}
          />
          <AccordionListItem
            title={name}
            swipeoutBtns={id === getSharedSectionId() ? [] : swipeoutBtns}
            editMode={editMode}
            setShowCreateProject={setCreateProjectModal}
            updateSectionName={updateSectionName}
            onLongPress={onSectionLongPress}
            isActive={isSectionActive}
            newlyCreated={newlyCreated}
          >
            <DraggableFlatList
              keyboardDismissMode="on-drag"
              data={projects}
              renderItem={renderItem}
              keyExtractor={(item, _index) => `draggable-project-${item}`}
              onDragEnd={({ data }) => {
                setSortedIds(data);
                dispatch(upsertSection({ updates: { id, projects: data } }));
              }}
            />
          </AccordionListItem>
        </View>
      )
  );
};

export default Section;
