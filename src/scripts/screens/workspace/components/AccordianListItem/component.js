import React, { useState, useRef, useEffect } from 'react';
import {
  View,
  Text,
  TextInput,
  Animated,
  Easing,
  Pressable,
  TouchableOpacity
} from 'react-native';
import RightSwipeable from '../../../../components/RightSwipeable';
import styles from './styles';
import Arrow from '../../../../../assets/icons/workspace_arrow.svg';
import Plus from '../../../../../assets/icons/section_plus.svg';
import Section from '../../../../../assets/icons/section.svg';

const AccordionListItem = ({
  children,
  editMode,
  title,
  setShowCreateProject,
  swipeoutBtns,
  updateSectionName,
  onLongPress,
  isActive,
  newlyCreated
}) => {
  const [open, setOpen] = useState(false);
  const [swipeableOpen, setSwipeableOpen] = useState(false);
  const [sectionName, setSectionName] = useState(title);
  const animatedController = useRef(new Animated.Value(0)).current;
  const [bodySectionHeight, setBodySectionHeight] = useState(100);

  useEffect(() => {
    if (newlyCreated) toggleListItem();
  }, [newlyCreated]);

  const bodyHeight = animatedController.interpolate({
    inputRange: [0, 1],
    outputRange: [0, bodySectionHeight],
  });

  const arrowAngle = animatedController.interpolate({
    inputRange: [0, 1],
    outputRange: ['0rad', `${Math.PI / 2}rad`],
  });

  const toggleListItem = () => {
    Animated.timing(animatedController, {
      duration: 300,
      toValue: open ? 0 : 1,
      useNativeDriver: false,
      easing: Easing.bezier(0.4, 0.0, 0.2, 1),
    }).start();
    setOpen(!open);
  };

  const CreateNewProject = () => {
    return (
      !swipeableOpen
        ? (
          <TouchableOpacity style={styles.plusButton} onPress={() => setShowCreateProject(true)}>
            <Plus />
          </TouchableOpacity>
        ) : null
    );
  };

  const AccordianListWrappperStyle = {
    transform: isActive ? [{ scale: 1.03 }] : null,
  };

  return (
    <View style={AccordianListWrappperStyle}>
      {swipeoutBtns
        ? (
          <RightSwipeable
            swipeoutBtns={swipeoutBtns}
            padding={23}
            setSwipeableOpen={setSwipeableOpen}
          >
            <Pressable
              onPressIn={() => setSwipeableOpen(true)}
              onPressOut={() => setSwipeableOpen(false)}
            >
              <View style={styles.titleContainer}>
                <Pressable style={styles.touchToToggle}
                  onPress={toggleListItem}
                  onLongPress={onLongPress}
                >
                  <View style={styles.nameContainer}>
                    <Section marginLeft={20} marginRight={10} />
                    {!editMode
                      ? <Text style={styles.sectionTitle}>{sectionName}</Text>
                      : (
                        <View>
                          <TextInput
                            style={styles.sectionTitle}
                            onChangeText={setSectionName}
                            value={sectionName}
                            onEndEditing={() => updateSectionName(sectionName)}
                            onSubmitEditing={() => updateSectionName(sectionName)}
                            selectTextOnFocus
                            autoFocus
                            multiline
                          />
                        </View>
                      )}
                    <Animated.View style={{ transform: [{ rotateZ: arrowAngle }] }}>
                      <Arrow style={styles.arrowIcon} />
                    </Animated.View>
                  </View>
                </Pressable>
                {swipeoutBtns && swipeoutBtns.length > 0 && open ? <CreateNewProject /> : <View />}
              </View>
            </Pressable>

          </RightSwipeable>
        )
        : (
          <TouchableOpacity onPress={toggleListItem}>
            <View style={styles.nameContainer}>
              <Animated.View style={{ transform: [{ rotateZ: arrowAngle }] }}>
                <Arrow style={styles.arrowIcon} />
              </Animated.View>
              <Text style={styles.sectionTitle}>{title}</Text>
            </View>
          </TouchableOpacity>
        )}

      <Animated.View style={[styles.bodyBackground, { height: bodyHeight }]}>
        <View
          style={styles.bodyContainer}
          onLayout={(event) => setBodySectionHeight(event.nativeEvent.layout.height)}
        >
          {children}
        </View>
      </Animated.View>
    </View>

  );
};
export default AccordionListItem;
