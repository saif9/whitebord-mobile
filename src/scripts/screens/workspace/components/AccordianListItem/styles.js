import { StyleSheet } from 'react-native';
import { font } from '../../../../../constants';

export default StyleSheet.create({
  bodyBackground: {
    overflow: 'hidden',
  },
  touchToToggle: {
    width: '50%',
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 70,
  },
  nameContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
  },
  bodyContainer: {
    bottom: 0,
    position: 'absolute',
    width: '100%%',
  },
  sectionTitle: {
    fontFamily: font.medium,
    fontSize: 16,
    color: '#7979A1',
  },
  plusButton: {
    padding: 10,
    paddingRight: 30,
  },
  arrowIcon: {
    marginHorizontal: 10,
  }
});
