import React, { useState } from 'react';
import {
  Text, View, TouchableHighlight
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import auth from '@react-native-firebase/auth';
import ProjectEditModal from '../../../../components/modals/ProjectEditModal';
import { font } from '../../../../../constants';
import styles from './styles';
import RightSwipeable from '../../../../components/RightSwipeable';
import SharedIcon from '../../../../../assets/icons/shared.svg';
import { deleteProject, projectsSelectors } from '../../../../state/slices/projects/projectsSlice';

const Project = ({
  id, sectionId, navigation, onLongPress, isActive
}) => {
  const dispatch = useDispatch();
  const { uid } = auth().currentUser;
  const projectState = useSelector((state) => projectsSelectors.selectById(state, id));
  const [editProject, setEditProject] = useState(false);
  const isSharedProject = Object.keys(projectState?.roles || []).length > 1;

  const projectDeadline = projectState?.deadline
    ? moment(projectState?.deadline).format('MMM D')
    : '';

  const isOwner = projectState?.roles[uid] === 'owner';

  const swipeToDelete = () => {
    dispatch(deleteProject({ updates: { id } }));
  };

  const swipeoutBtns = [
    {
      text: 'edit',
      color: 'white',
      backgroundColor: '#626DFF',
      onPress() { setEditProject(true); }
    },
    {
      text: isOwner ? 'delete' : 'remove',
      color: 'white',
      backgroundColor: '#FA8072',
      onPress() { swipeToDelete(); }
    }
  ];

  return (
    !projectState
      ? <View />
      : (
        <RightSwipeable swipeoutBtns={swipeoutBtns} padding={12}>

          {editProject && (
            <ProjectEditModal
              projectId={id}
              sectionId={sectionId}
              editProject={editProject}
              setEditProject={setEditProject}
            />
          )}
          <View style={isSharedProject ? { ...styles.listItem, marginLeft: 0 } : styles.listItem}>
            {isSharedProject && <SharedIcon marginLeft={15} />}
            <View style={styles.projectContainer}>
              <View style={{
                ...styles.projectTitle,
                transform: isActive ? [{ scale: 1.2 }] : null,
                paddingLeft: isActive ? 40 : 11
              }}
              >
                <TouchableHighlight
                  underlayColor="transparent"
                  onPress={() => navigation.navigate('TaskList',
                    { projectId: id, sectionId })}
                  onLongPress={onLongPress}
                >
                  <View style={styles.projectText}>
                    <Text style={{
                      ...styles.projectFont,
                      fontFamily: isActive ? font.bold : font.medium
                    }}
                    >
                      {projectState.name}
                    </Text>
                    <Text style={styles.deadline}>
                      {projectDeadline}
                    </Text>
                  </View>
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </RightSwipeable>
      )
  );
};

export default Project;
