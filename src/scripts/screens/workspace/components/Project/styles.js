import { StyleSheet } from 'react-native';
import { font } from '../../../../../constants';

export default StyleSheet.create({
  listItem: {
    flexDirection: 'row',
    marginLeft: 40,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  projectContainer: {
    flexDirection: 'row',
    paddingVertical: 15,
    borderTopWidth: 0.5,
    borderColor: '#E5E5EA',
    alignItems: 'center',
  },
  projectFont: {
    fontFamily: font.medium,
    fontSize: 16,
    color: '#434279'
  },
  projectTitle: {
    flex: 3,
  },
  projectEdit: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  deadline: {
    fontFamily: font.medium,
    color: '#6574FF',
    fontSize: 11,
    marginRight: 20,
  },
  projectText: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
