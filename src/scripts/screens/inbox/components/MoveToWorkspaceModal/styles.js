import { StyleSheet } from 'react-native';
import { font } from '../../../../../constants';

export default StyleSheet.create({
  modal: {
    marginHorizontal: 10,
    marginVertical: 30,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderColor: '#DEDDFF',
    paddingRight: 20,
  },
  headerText: {
    fontFamily: font.bold,
    fontSize: 20,
    color: '#434279',
  },
  modalText: {
    marginTop: 40,
    marginBottom: 40,
  },
  moveButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 60,
  },
  moveButton: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 10,
    backgroundColor: '#2F57E9',
  },
  moveButtonText: {
    color: 'white',
    fontFamily: font.semibold,
    fontSize: 15,
  },
  sectionsContainer: {
    height: '82%',
  }
});
