import React, { useState } from 'react';
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Modal,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './styles';
import DisplaySections from '../../../../components/workspace/DisplaySections';
import X from '../../../../../assets/icons/x.svg';
import { projectsSelectors } from '../../../../state/slices/projects/projectsSlice';
import { moveToWorkspace } from '../../../../state/slices/whiteboardItems/whiteboardItemsSlice';

const MoveToWorkspaceModal = ({
  modalVisible, setModalVisible, move
}) => {
  const [selectedProjectId, setSelectedProjectId] = useState('');

  const allProjects = useSelector(projectsSelectors.selectEntities);

  const dispatch = useDispatch();
  const moveTasks = () => {
    setModalVisible(false);
    dispatch(moveToWorkspace({ updates: { taskIds: move, projectId: selectedProjectId } }));
  };

  return (
    <Modal animationType="slide" visible={modalVisible}>
      <SafeAreaView style={styles.modal}>
        {/* Header */}
        <View style={styles.header}>
          <Text style={styles.headerText}> Select Destination Project </Text>
          <TouchableOpacity onPress={() => setModalVisible(false)}>
            <X />
          </TouchableOpacity>
        </View>
        {/* Sections & Projects */}
        <KeyboardAwareScrollView style={styles.sectionsContainer}>
          <DisplaySections
            selectedProjectId={selectedProjectId}
            setSelectedProjectId={setSelectedProjectId}
          />
        </KeyboardAwareScrollView>
        {/* Move Button */}
        {!allProjects[selectedProjectId]
          ? <View />
          : (
            <View style={styles.moveButtonContainer}>
              <TouchableOpacity style={styles.moveButton} onPress={() => moveTasks()}>
                <Text style={styles.moveButtonText}>
                  {`Move to "${allProjects[selectedProjectId].name}"`}
                </Text>
              </TouchableOpacity>
            </View>
          )}
      </SafeAreaView>
    </Modal>
  );
};

export default MoveToWorkspaceModal;
