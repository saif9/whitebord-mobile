import React, { useState, useEffect } from 'react';
import {
  TextInput, View
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import CheckBox from '@react-native-community/checkbox';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import styles from './styles';
import RightSwipeable from '../../../../components/RightSwipeable';
import { setModal, toggleModal, toggleEditMode } from '../../../../state/actions/modal';
import { canTaskBeDeleted, removeHTMLTags } from '../../../../utils';
import { deleteWhiteboardItem, upsertWhiteboardItem, whiteboardItemsSelectors } from '../../../../state/slices/whiteboardItems/whiteboardItemsSlice';
import { INBOX_ID } from '../../../../../constants';

const QuickTask = ({
  id, showCheckbox = true, move, setMove, setModalVisible, newTaskFieldRef
}) => {
  const taskState = useSelector((state) => whiteboardItemsSelectors.selectById(state, id));
  const [text, setText] = useState(taskState?.name);

  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const dispatch = useDispatch();

  const canDelete = canTaskBeDeleted(taskState);

  useEffect(() => {
    setText(taskState?.name);
  }, [taskState?.name]);

  const onKeyPress = ({ nativeEvent: { key: value } }) => {
    if (!text && value === 'Backspace') {
      dispatch(deleteWhiteboardItem({ updates: { id } }));
      newTaskFieldRef?.current.focus();
    }
  };

  const swipeoutBtns = [
    {
      text: 'Edit',
      color: 'white',
      backgroundColor: '#626DFF',
      onPress() { swipeToEdit(); }
    },
    {
      text: canDelete ? 'Delete' : 'Remove',
      color: 'white',
      backgroundColor: '#FA8072',
      onPress() { swipeToDelete(); }
    }
  ];

  const swipeToDelete = () => {
    dispatch(deleteWhiteboardItem({ updates: { id } }));
  };

  const swipeToEdit = () => {
    toggleModal(true, dispatch);
    setModal({
      ...taskState, projectId: INBOX_ID, mode: 'edit'
    }, dispatch);
    toggleEditMode(true, dispatch);
  };

  const moveTasks = (isChecked) => {
    setToggleCheckBox(isChecked);
    if (isChecked) setMove([...move, id]);
    else setMove(move.filter((e) => e !== id));
  };

  const updateWhiteboardItemName = () => {
    dispatch(upsertWhiteboardItem({ updates: { id, name: text } }));
  };

  return (
    !taskState
      ? <View />
      : (
        <View style={styles.taskContainer}>
          <RightSwipeable swipeoutBtns={swipeoutBtns} padding={8}>
            <View style={styles.taskContent}>
              {showCheckbox
                && (
                  <CheckBox
                    disabled={false}
                    value={toggleCheckBox}
                    onValueChange={(isChecked) => {
                      moveTasks(isChecked);
                      ReactNativeHapticFeedback.trigger('selection');
                    }}
                    style={styles.checkbox}
                    boxType="square"
                    onCheckColor="white"
                    onFillColor="#9D9DBA"
                    tintColor="#9D9DBA"
                    onTintColor="#9D9DBA"
                    animationDuration={0}
                  />
                )}
              <TextInput
                style={styles.taskEdit}
                value={removeHTMLTags(text)}
                onChangeText={setText}
                onEndEditing={updateWhiteboardItemName}
                onKeyPress={onKeyPress}
                multiline
              />
            </View>
          </RightSwipeable>
        </View>
      )
  );
};

export default QuickTask;
