import { StyleSheet } from 'react-native';
import { font } from '../../../../../constants';

export default StyleSheet.create({
  taskContainer: {
    marginHorizontal: 30,
    borderBottomColor: '#E5E5EA',
    borderBottomWidth: 0.5,
  },
  taskContent: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    minHeight: 45,
  },
  checkbox: {
    width: 14,
    height: 14,
    marginHorizontal: 10,
  },
  taskEdit: {
    flex: 1,
    paddingBottom: 5,
    alignSelf: 'center',
    fontFamily: font.regular,
    fontSize: 16,
    color: '#434279',
  },
  deadline: {
    fontFamily: font.medium,
    color: '#6574FF',
    fontSize: 11,
  }
});
