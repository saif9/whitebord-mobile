import React, { useState, useRef } from 'react';
import {
  Text, TextInput, TouchableOpacity, View, SafeAreaView, Keyboard
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useSelector, useDispatch } from 'react-redux';
import { useKeyboard } from '@react-native-community/hooks';
import styles from './styles';
import { QuickTask, MoveToWorkspaceModal } from './components';
import ArrowBack from '../../../assets/icons/chevron_left.svg';
import { memoAllInboxTasks } from '../../state/slices/quickTasks/selectors';
import { upsertWhiteboardItem } from '../../state/slices/whiteboardItems/whiteboardItemsSlice';
import { createQuickTask } from '../../state/slices/quickTasks/quickTasks';

const InboxScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const [text, setText] = useState('');
  const combinedTasks = useSelector(memoAllInboxTasks);
  const [select, setSelect] = useState(false);
  const [move, setMove] = useState([]); // tasks to move to a project
  const [modalVisible, setModalVisible] = useState(false);
  const keyboard = useKeyboard();
  const newTaskFieldRef = useRef();
  const scrollView = useRef();

  const AddToInbox = () => {
    if (text) {
      scrollView.current.scrollToEnd();
      const newQuickTask = createQuickTask({ name: text });
      if (newQuickTask) { dispatch(upsertWhiteboardItem({ updates: newQuickTask })); }
      setText('');
      newTaskFieldRef.current.focus();
    }
  };

  const handlePress = () => {
    if (!keyboard.keyboardShown) {
      // if not showing, toggle select
      setSelect(!select);
    } else {
      // if showing, dismiss keyboard
      Keyboard.dismiss();
    }
  };

  const getHeaderButtonText = () => {
    if (keyboard.keyboardShown) return 'done';
    return !select ? 'select' : 'cancel';
  };

  return (
    <>
      <View style={styles.topHeaderSpacing} />
      <SafeAreaView style={styles.header}>
        <View style={styles.leftHeader}>
          <TouchableOpacity
            style={styles.arrowBack}
            onPress={navigation.goBack}
          >
            <ArrowBack />
          </TouchableOpacity>
          <Text style={styles.headerText}> Inbox </Text>
        </View>
        <TouchableOpacity onPress={handlePress}>
          <Text style={styles.selectButton}>
            {getHeaderButtonText()}
          </Text>
        </TouchableOpacity>
      </SafeAreaView>
      <KeyboardAwareScrollView
        keyboardOpeningTime={Number.MAX_SAFE_INTEGER}
        keyboardDismissMode="on-drag"
        enableResetScrollToCoords={false}
        style={styles.container}
        ref={scrollView}
      >
        {combinedTasks.map((id, idx) => (
          <QuickTask
            id={id}
            key={`${id + idx}`}
            showCheckbox={select}
            type="quicktask"
            move={move}
            setMove={setMove}
            setModalVisible={setModalVisible}
          />
        ))}
        {!select ? (
          <View style={styles.AddToInbox}>
            <TextInput
              ref={newTaskFieldRef}
              placeholder="tap to add a task..."
              placeholderTextColor="#6574FF"
              onChangeText={setText}
              defaultValue={text}
              style={styles.taskFont}
              blurOnSubmit={false}
              onSubmitEditing={AddToInbox}
            />
          </View>
        )

          : (
            <View style={styles.AddToWorkspaceContainer}>
              <TouchableOpacity
                style={styles.AddToWorkspaceButton}
                onPress={() => setModalVisible(true)}
              >
                <Text style={styles.AddToWorkspaceText}> Add to Workspace </Text>
              </TouchableOpacity>
            </View>
          )}
        <MoveToWorkspaceModal
          modalVisible={modalVisible}
          setModalVisible={setModalVisible}
          move={move}
        />
      </KeyboardAwareScrollView>
    </>
  );
};

export default InboxScreen;
