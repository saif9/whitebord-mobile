import { connect } from 'react-redux';

import InboxScreen from './component';

const mapStateToProps = (_state) => {
  return {};
};

const mapDispatchToProps = (_dispatch) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InboxScreen);
