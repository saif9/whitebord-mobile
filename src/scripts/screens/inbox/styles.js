import { StyleSheet } from 'react-native';
import { font } from '../../../constants';

export default StyleSheet.create({
  container: {
    paddingTop: 10,
    color: 'black',
    flex: 1,
    backgroundColor: 'white',
  },
  selectButton: {
    color: '#6574FF',
    textAlign: 'right',
    paddingRight: 30,
  },
  taskFont: {
    fontFamily: font.regular,
    fontSize: 16,
    color: '#9d9dba',
    paddingVertical: 10,
  },
  AddToInbox: {
    width: '85%',
    alignSelf: 'center',
    paddingVertical: 5,
    borderColor: '#E5E5EA',
    marginBottom: 50,
  },
  AddToWorkspaceContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 30,
    marginBottom: 50
  },
  AddToWorkspaceButton: {
    width: 180,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 10,
    backgroundColor: '#2F57E9',
  },
  AddToWorkspaceText: {
    color: 'white',
    fontFamily: font.semibold,
    fontSize: 15,
  },
  header: {
    justifyContent: 'space-between',
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
  },
  leftHeader: {
    flexDirection: 'row',
    marginLeft: 10,
  },
  arrowBack: {
    padding: 10,
  },
  headerText: {
    fontFamily: font.semibold,
    fontSize: 20,
    color: '#434279',
  },
  topHeaderSpacing: {
    height: 10,
    backgroundColor: 'white'
  }
});
