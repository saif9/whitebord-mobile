import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  View, Text, TouchableOpacity, Pressable
} from 'react-native';
import moment from 'moment';
import { FlatList } from 'react-native-gesture-handler';
import * as Progress from 'react-native-progress';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import Arrow from '../../../assets/icons/arrow.svg';
import { setModal, toggleModal } from '../../state/actions/modal';
import styles from './styles';
import { TaskSubList } from './TaskSubList';
import { removeHTMLTags } from '../../utils';
import AdaptableCheckBox from '../../components/AdaptableCheckBox';
import { INBOX_ID } from '../../../constants';
import { projectsSelectors } from '../../state/slices/projects/projectsSlice';
import { upsertWhiteboardItem } from '../../state/slices/whiteboardItems/whiteboardItemsSlice';
import { upsertException } from '../../state/slices/exceptions/exceptionsSlice';

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const weekdays = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];

const formatDueDate = (date) => {
  const d = new Date(moment(date).startOf('day'));

  const today = new Date(moment(new Date()).startOf('day'));
  const future = new Date(
    today.getFullYear(),
    today.getMonth(),
    today.getDate() + 7,
  );

  const past = new Date(
    today.getFullYear(),
    today.getMonth(),
    today.getDate() - 7,
  );

  if (d < future && d > past) {
    if (moment(d).isSame(today, 'day')) {
      return 'Due Today';
    } else if (
      moment(d).isSame(
        new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1),
        'day',
      )
    ) {
      return 'Due Tomorrow';
    } else if (
      moment(d).isSame(
        new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1),
        'day',
      )
    ) {
      return 'Due Yesterday';
    } else {
      return `Due ${weekdays[d.getDay()]}`;
    }
  } else if (d.getFullYear() === today.getFullYear()) {
    return `Due ${months[d.getMonth()]} ${d.getDate().toString()}`;
  } else {
    return `Due ${(
      d.getMonth() + 1
    ).toString()}/${d.getDate()}/${d.getFullYear()}`;
  }
};

export default ({ date, items }) => {
  const dispatch = useDispatch();
  const projects = useSelector(projectsSelectors.selectEntities);

  const [viewCompleted, setViewCompleted] = useState(false);
  const completedTasks = items.filter((task) => task.completed);
  const incompleteTasks = items.filter((task) => !task.completed);

  const toggleItem = (item, newValue) => {
    if (item.isRecurrenceItem) {
      dispatch(upsertException({ updates: { id: item.id, completed: newValue } }));
    } else {
      dispatch(upsertWhiteboardItem({ updates: { id: item.id, completed: newValue } }));
    }
  };

  const openTaskModal = (task) => {
    toggleModal(true, dispatch);
    const parentProjectName = task?.projectId === INBOX_ID
      ? 'Inbox'
      : projects[task?.projectId]?.name;
    const item = task.itemToEdit ? task.itemToEdit : task;
    setModal({ ...item, parentProjectName, mode: 'edit' }, dispatch);
  };

  const taskTextStyle = (item) => {
    return moment(item.scheduledDate).isBefore(new Date(), 'days')
      ? { ...styles.listText, color: 'red' }
      : styles.listText;
  };

  const renderItem = ({ item, drag, isActive }) => (
    <View style={styles.item}>
      <AdaptableCheckBox
        type={item.isRecurrenceItem ? item.type : undefined}
        value={item.completed}
        onValueChange={(newValue) => {
          ReactNativeHapticFeedback.trigger('selection');
          toggleItem(item, newValue);
        }}
      />
      <TouchableOpacity onPress={() => openTaskModal(item)} onLongPress={drag}>
        <Text style={taskTextStyle(item)}>{removeHTMLTags(item.name)}</Text>
      </TouchableOpacity>
    </View>
  );

  return (
    <>
      <View style={styles.tasksSection}>
        <View style={styles.dueList}>
          <Text style={styles.subHeader}>{formatDueDate(date)}</Text>
          <View style={styles.completion}>
            <Text style={styles.completionText}>
              {completedTasks.length}
              /
              {completedTasks.length + incompleteTasks.length}
              {' '}
              Completed
            </Text>
            <Progress.Bar
              color="#ABA9FF"
              unfilledColor="#F8F9FF"
              progress={
                completedTasks.length
                  / (completedTasks.length + incompleteTasks.length) || 0
              }
              borderRadius={10}
              width={120}
              height={10}
            />
          </View>
        </View>

        <View style={styles.checklist}>
          {items.length > 0 ? (
            <FlatList
              keyboardDismissMode="on-drag"
              ListHeaderComponent={() => (
                <View>
                  <TaskSubList
                    listKey="incomplete-tasks"
                    data={incompleteTasks}
                    renderItem={renderItem}
                  />
                  <Pressable
                    style={styles.viewCompleted}
                    onPress={() => setViewCompleted((completed) => !completed)}
                  >
                    <View
                      style={{
                        ...styles.viewCompletedArrow,
                        transform: [
                          { rotateZ: viewCompleted ? Math.PI / 2 : 0 },
                        ],
                      }}
                    >
                      <Arrow />
                    </View>
                    <Text style={styles.viewCompletedText}>View Completed</Text>
                  </Pressable>

                  {viewCompleted ? (
                    <TaskSubList
                      listKey="completed-tasks"
                      data={completedTasks}
                      renderItem={renderItem}
                    />
                  ) : (
                    <View />
                  )}
                </View>
              )}
            />
          ) : (
            <Text style={styles.nothingDue}>No tasks due today!</Text>
          )}
        </View>
      </View>
    </>
  );
};
