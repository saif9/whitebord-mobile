import React, { useEffect, useState } from 'react';
import {
  View,
  Pressable,
  Text,
  ScrollView,
} from 'react-native';
import moment from 'moment';
import styles from './styles';
import DashboardEvent from '../../components/home/dashboardEvent';

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const weekdays = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];

const formatDate = (date) => {
  const d = new Date(moment(date).startOf('day'));

  const today = new Date(moment(new Date()).startOf('day'));
  const future = new Date(
    today.getFullYear(),
    today.getMonth(),
    today.getDate() + 7,
  );

  const past = new Date(
    today.getFullYear(),
    today.getMonth(),
    today.getDate() - 7,
  );

  if (d < future && d > past) {
    if (moment(d).isSame(today, 'day')) {
      return 'today';
    } else if (
      moment(d).isSame(
        new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1),
        'day',
      )
    ) {
      return 'tomorrow';
    } else if (
      moment(d).isSame(
        new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1),
        'day',
      )
    ) {
      return 'yesterday';
    } else {
      return weekdays[d.getDay()];
    }
  } else if (d.getFullYear() === today.getFullYear()) {
    return `${months[d.getMonth()]} ${d.getDate().toString()}`;
  } else {
    return `${(d.getMonth() + 1).toString()}/${d.getDate()}/${d.getFullYear()}`;
  }
};

const formatNothingDue = (date, isPast) => {
  return !isPast
    ? `No upcoming events for ${formatDate(date)}!`
    : `No past events for ${formatDate(date)}!`;
};

const filterEvents = (events, date, showPastTodayEvents) => {
  return events.filter((item) => {
    if (!moment(date).isSame(new Date(), 'day')) return true;

    if (!showPastTodayEvents) {
      return item.allDay || item.endDate >= new Date();
    } else {
      return !item.allDay && item.endDate < new Date();
    }
  });
};

function EventsSection({ items, date }) {
  const [showPastTodayEvents, setShowPastTodayEvents] = useState(false);

  useEffect(() => {
    if (showPastTodayEvents) {
      setShowPastTodayEvents(false);
    }
  }, [date]);

  return (
    <View style={styles.eventsSection}>
      <View style={styles.upcomingHeader}>
        <Text style={styles.subHeader}>
          {moment(date).isBefore(new Date(), 'day') || showPastTodayEvents
            ? 'Past Events'
            : 'Upcoming Events'}
        </Text>
        {moment(date).isSame(new Date(), 'day') && (
          <Pressable
            onPress={() => setShowPastTodayEvents(!showPastTodayEvents)}
          >
            <Text style={styles.eventSwitcherText}>
              {!showPastTodayEvents ? 'show past' : 'show upcoming'}
            </Text>
          </Pressable>
        )}
      </View>

      {
        // calLoading ? (
        //   <View
        //     style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        //   >
        //     <ActivityIndicator />
        //   </View>
        // ) :
        (
          <ScrollView>
            {filterEvents(items, date, showPastTodayEvents).length === 0 ? (
              <Text style={styles.nothingDue}>
                {formatNothingDue(
                  date,
                  moment(date).isBefore(new Date(), 'day') || showPastTodayEvents,
                )}
              </Text>
            ) : (
              filterEvents(items, date, showPastTodayEvents).map((item) => (
                <DashboardEvent key={item.id} item={item} />
              ))
            )}
          </ScrollView>
        )
      }
    </View>
  );
}

export default EventsSection;
