import React from 'react';
import { FlatList } from 'react-native';

export const TaskSubList = ({ listKey, data, renderItem }) => {
  return (
    <FlatList
      listKey={listKey}
      data={data}
      renderItem={renderItem}
      keyExtractor={(item) => item.id}
      keyboardDismissMode="on-drag"
    />
  );
};
