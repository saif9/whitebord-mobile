import React, { useState } from 'react';
import {
  View, Text, Pressable
} from 'react-native';
import moment from 'moment';
import styles from './styles';
import SettingsIcon from '../../../assets/icons/settings.svg';
import InboxButton from '../../components/InboxButton';
import Ellipses from '../../../assets/icons/ellipses.svg';
import DTPickerModal from '../../components/modals/DTPickerModal';
import MenuButton from '../../components/MenuButton';

function Header({
  navigation, displayMonth, date, setDate
}) {
  const [displayDTModal, setDisplayDTModal] = useState(false);

  return (
    <View style={styles.homeHeader}>
      <View style={styles.homeHeaderGroup}>
        <Text style={styles.homeHeaderText}>{displayMonth}</Text>
        <Pressable onPress={() => setDisplayDTModal(true)}>
          <Ellipses />
          <DTPickerModal
            visible={displayDTModal}
            dismissModal={() => setDisplayDTModal(false)}
            mode="date"
            value={moment(date).toDate()}
            hideButtons
            onChange={(newDate) => setDate(moment(newDate).startOf('day'))}
          />
        </Pressable>
      </View>
      <View style={styles.navButtons}>
        <MenuButton onPress={() => navigation.navigate('User')}>
          <SettingsIcon />
        </MenuButton>
        <InboxButton navigation={navigation} />
      </View>
    </View>
  );
}

export default Header;
