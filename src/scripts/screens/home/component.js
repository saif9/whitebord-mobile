/* eslint-disable no-nested-ternary */
import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { View } from 'react-native';

import moment from 'moment';
import Header from './Header';
import HorizCal from '../../components/home/horizCal/component';

import TasksSection from './TasksSection';
import EditModal from '../../components/modals/EditModal';

import styles from './styles';
import {
  calendarsSelectors,
  fetchEvents,
} from '../../state/slices/calendars/calendarSlice';
import { setFilter } from '../../state/slices/filters/filterSlice';
import {
  generateRecurrentItems
} from '../../state/slices/recurrentItems/recurrentItemsSlice';
import EventsSection from './EventsSection';
import UniversalPlus from '../../components/UniversalPlus';
import { memoAllRecItems, memoFormatAllEvents, memoFormatAllHomeTasks } from '../../state/slices/whiteboardItems/selectors';

const HomeScreen = ({ navigation }) => {
  const [date, setDate] = useState(moment(new Date()));
  const [displayMonth, setDisplayMonth] = useState(
    moment(date).format('MMMM YYYY'),
  );
  const dispatch = useDispatch();

  const calendars = useSelector(calendarsSelectors.selectAll);

  useEffect(() => {
    const initFilter = {
      habit: true,
      task: true,
      event: true,
    };
    calendars.forEach((c) => {
      initFilter[c.id] = true;
    });
    dispatch(setFilter(initFilter));
  }, [dispatch]);

  const recItems = useSelector(memoAllRecItems);
  useEffect(() => {
    if (date) {
      const dateRange = {
        timeMinString: moment(date)
          .startOf('month')
          .add(1, 'minute')
          .toISOString(),
        timeMaxString: moment(date).endOf('month').toISOString(),
      };
      dispatch(generateRecurrentItems({ dateRange }));
    }
  }, [recItems.length]);

  useEffect(() => {
    if (date) {
      const dateRange = {
        timeMinString: moment(date)
          .startOf('day')
          .add(1, 'minute')
          .toISOString(),
        timeMaxString: moment(date).endOf('day').toISOString(),
      };
      dispatch(fetchEvents(dateRange));
      dispatch(generateRecurrentItems({ dateRange }));
    }
  }, [date]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => setDate(moment()),);
    return unsubscribe;
  }, [navigation]);

  const formatAllEvents = useSelector((state) => memoFormatAllEvents(state, date));
  const formatAllTasks = useSelector((state) => memoFormatAllHomeTasks(state, date));

  return (
    <>
      <EditModal />
      <View style={styles.container}>
        <Header
          navigation={navigation}
          displayMonth={displayMonth}
          date={date}
          setDate={setDate}
        />
        <HorizCal
          date={date}
          setDate={setDate}
          displayMonth={displayMonth}
          setDisplayMonth={setDisplayMonth}
        />
        <EventsSection
          items={formatAllEvents}
          date={date}
        />
        <TasksSection date={date} items={formatAllTasks} />
      </View>
      <UniversalPlus />
    </>
  );
};

export default HomeScreen;
