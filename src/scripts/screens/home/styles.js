import { StyleSheet } from 'react-native';
import { font } from '../../../constants';
import { wp } from '../../utils';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  item: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  upcomingHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  dueList: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  checkbox: {
    width: 18,
    height: 18,
    marginRight: 10,
    marginLeft: 1,
    paddingRight: 300,
    zIndex: 1,
  },
  checklist: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 3,
    borderColor: '#DEDDFF',
    padding: 10,
    flex: 1,
    marginBottom: 10,
  },
  completion: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
  },
  completionText: {
    color: '#6574FF',
    fontFamily: font.bold,
    marginRight: 5,
    fontSize: 12,
  },
  eventsSection: {
    flex: 1,
    marginHorizontal: wp(5),
  },
  eventSwitcherText: {
    color: '#7979A1',
    fontFamily: font.semibold,
    fontSize: 12,
    marginRight: 5,
  },
  subHeader: {
    color: '#434279',
    marginVertical: 10,
    fontFamily: font.semibold,
    fontSize: 17,
  },
  listText: {
    fontFamily: font.regular,
    color: '#434279',
    zIndex: 0,
    fontSize: 15,
  },
  homeHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: wp(5),
  },
  homeHeaderGroup: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  homeHeaderText: {
    color: '#434279',
    marginRight: 5,
    fontFamily: font.semibold,
    fontSize: 20,
  },
  navButtons: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  navButton: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 100,
    backgroundColor: '#F8F9FF',
  },
  settings: {
    marginRight: 10,
  },
  tasksSection: {
    flex: 1.2,
    marginHorizontal: wp(5),
  },
  inbox: {
    marginLeft: 10,
  },
  nothingDue: {
    fontFamily: font.regular,
    fontSize: 15,
  },
  viewCompleted: {
    flexDirection: 'row',
    width: 128,
    justifyContent: 'space-between',
    marginLeft: 7,
  },
  viewCompletedArrow: {
    justifyContent: 'center',
  },
  viewCompletedText: {
    color: '#434279',
    fontFamily: font.semibold,
  },
});
