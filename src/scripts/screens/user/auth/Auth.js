import React, { useState } from 'react';
import { View, SafeAreaView } from 'react-native';
import Login from './login';
import Signup from './signup';
// import Google from './Google';

const Auth = ({ route: { params: { login } } }) => {
  const [loginPage, setLoginPage] = useState(login || 0);

  return (
    <View style={{ flex: 1, backgroundColor: 'white' }}>
      <SafeAreaView>
        {loginPage
          ? <Login setLoginPage={setLoginPage} />
          : <Signup setLoginPage={setLoginPage} />}
        {/* <Google /> */}
      </SafeAreaView>
    </View>
  );
};

export default Auth;
