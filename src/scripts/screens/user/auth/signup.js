/* eslint-disable camelcase */
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import {
  Text, TextInput, View, TouchableOpacity
} from 'react-native';
import auth from '@react-native-firebase/auth';
import { Google } from './Google';
import Facebook from './Facebook';
import { Microsoft } from './Microsoft';
import styles from '../styles';
import { trackSignUp } from '../../../tracking';

const Signup = ({ setLoginPage }) => {
  const dispatch = useDispatch();
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [signupErr, setSignupErr] = useState(null);
  const [validate, setValidate] = useState({
    no_fn: false,
    no_ln: false,
    invalid_email: false,
    weakPwd: false
  });

  const signup = async () => {
    try {
      await auth().createUserWithEmailAndPassword(email, password);
      await auth().currentUser.updateProfile({ displayName: `${firstName} ${lastName}` });
      await auth().currentUser.sendEmailVerification();
      trackSignUp({ user: auth().currentUser });
      dispatch({ type: 'SIGN_IN' });
    } catch (err) {
      setSignupErr(err);
    }
  };

  const handleSubmit = (e) => {
    let noFN = false;
    let noLN = false;
    let invalidEmail = false;
    let weakPwd = false;

    noFN = firstName.length === 0;

    noLN = lastName.length === 0;

    invalidEmail = !email.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);

    weakPwd = !password.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/gm);
    setValidate({
      noFN, noLN, invalidEmail, weakPwd
    });

    if (!noFN && !noLN && !invalidEmail && !weakPwd) {
      signup();
    }
  };

  const displayErrorView = (msg) => {
    return (
      <View style={{ alignSelf: 'flex-start' }}>
        <Text style={{ color: 'red' }}>
          {msg}
        </Text>
      </View>
    );
  };

  return (
    <View style={styles.authContainer}>
      <Text style={styles.userWelcome}>SIGN UP</Text>
      <View style={styles.authInfoStack}>
        <View>
          <View style={styles.infoStrip}>
            <TextInput
              id="firstName"
              textContentType="givenName"
              value={firstName}
              onChangeText={setFirstName}
              placeholder="First name"
              placeholderTextColor="#434C73"
              style={styles.infoText}
            />
          </View>
          {validate.noFN ? displayErrorView('This field cannot be blank.') : null}
          <View style={styles.infoStrip}>
            <TextInput
              id="lastName"
              textContentType="familyName"
              value={lastName}
              onChangeText={setLastName}
              placeholder="Last name"
              placeholderTextColor="#434C73"
              style={styles.infoText}
            />
          </View>
          {validate.no_ln ? displayErrorView('This field cannot be blank.') : null}
          <View style={styles.infoStrip}>
            <TextInput
              autoCapitalize="none"
              id="email"
              textContentType="emailAddress"
              value={email}
              onChangeText={setEmail}
              placeholder="Email*"
              placeholderTextColor="#434C73"
              style={styles.infoText}
            />
          </View>
          {validate.invalid_email ? displayErrorView('Please provide a valid email.') : null}
          <View style={styles.infoStrip}>
            <TextInput
              autoCapitalize="none"
              id="password"
              secureTextEntry
              textContentType="newPassword"
              value={password}
              onChangeText={setPassword}
              placeholder="Password*"
              placeholderTextColor="#434C73"
              style={styles.infoText}
            />
          </View>
          {validate.weakPwd ? displayErrorView('Password must contain at least:\n8 characters\n1 uppercase letter\n1 lowercase letter\n1 number') : null}

        </View>
        <View>
          <TouchableOpacity
            style={styles.actionButton}
            onPress={handleSubmit}
          >
            <Text style={styles.actionText}>Sign up</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ ...styles.actionButton, backgroundColor: '#FDCA29' }}
            onPress={() => setLoginPage((prevState) => !prevState)}
          >
            <Text style={{ ...styles.actionText, color: 'black' }}>Login</Text>
          </TouchableOpacity>
        </View>
        {signupErr ? displayErrorView(signupErr) : null}
      </View>
      <View style={styles.authButtonsView}>
        <Facebook />
        <Google />
        <Microsoft />
      </View>
    </View>
  );
};
export default Signup;
