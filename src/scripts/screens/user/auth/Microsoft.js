import React, { useState } from 'react';
import {
  Text, TouchableOpacity
} from 'react-native';

import auth from '@react-native-firebase/auth';
import { useDispatch } from 'react-redux';

import AzureAuth from 'react-native-azure-auth';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import styles from '../styles';
import MicrosoftLogo from '../../../../assets/icons/microsoft_logo.svg';
import { backendRoot } from '../../../../constants';
import { trackLogin } from '../../../tracking';

const TOKEN_URL = `${backendRoot}/getCustomToken`;
const azureAuth = new AzureAuth({
  clientId: '5bd8ddd0-c55e-46bf-bc89-fadcbef8e288',
});
const MScope = 'openid profile User.Read offline_access Calendars.ReadWrite';
export const microsoftSignout = async () => {
  try {
    await AsyncStorage.removeItem('microsoftUserId');
  } catch (e) {
    console.log(e);
  }
};

export const clearMicrosoftToken = async () => {
  await azureAuth.auth.clearPersistenCache();
  await azureAuth.webAuth.clearSession();
};

export const getMicrosoftToken = async () => {
  try {
    const microsoftUserId = await AsyncStorage.getItem('microsoftUserId');
    if (microsoftUserId) {
      let tokens = await azureAuth.auth.acquireTokenSilent({ scope: MScope, userId: microsoftUserId });
      if (!tokens) {
        tokens = await azureAuth.webAuth.authorize({ scope: MScope });
      }
      // const info = await azureAuth.auth.msGraphRequest({ token: tokens.accessToken, path: '/me' });
      return tokens.accessToken;
    }
  } catch (error) {
    console.log(error);
  }
  return null;
};
const onMicrosoftButtonPress = async (linking) => {
  const tokens = await azureAuth.webAuth.authorize({ scope: MScope });
  const res = await axios({
    method: 'post',
    url: TOKEN_URL,
    headers: {
      Authorization: `Bearer ${tokens.accessToken}`
    }
  });
  // return auth().signInWithMicrosoft();
  if (linking) {
    return AsyncStorage.setItem('microsoftUserId', tokens.userId);
  }
  await AsyncStorage.setItem('microsoftUserId', tokens.userId);
  return auth().signInWithCustomToken(res.data.custom_token);
};

export const Microsoft = ({ linking }) => {
  const dispatch = useDispatch();
  const [loginErr, setLoginErr] = useState(null);

  const authErrHandler = () => {
    // "auth/user-not-found" exception is verbose/confusing, thus use this custom text
    return (
      null // <View className="error"><Text>{"Microsoft Login Unsucessful"}</Text></View>
    );
  };

  const onButtonPress = () => onMicrosoftButtonPress(linking).then((loginData) => {
    dispatch({ type: `${linking ? 'LINKED' : 'SIGN_IN'}` });
    const user = {
      ...JSON.parse(JSON.stringify(loginData.user)),
      network: 'Microsoft',
    };
    trackLogin({ user });
    if (linking) linking();
  })
    .catch((err) => {
      setLoginErr(err);
    });

  return (
    <>
      {
        linking ? (
          <TouchableOpacity style={styles.button} onPress={onButtonPress}>
            <Text style={styles.buttonText}> Link Microsoft </Text>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity onPress={onButtonPress}>
            <MicrosoftLogo width={45} height={45} marginRight={20} />
          </TouchableOpacity>
        )
      }
      {loginErr && authErrHandler()}
    </>
  );
};
