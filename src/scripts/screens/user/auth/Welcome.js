import React from 'react';
import { TouchableOpacity, Text, SafeAreaView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import WhiteboardIcon from '../../../../assets/icons/whiteboard.svg';
import styles from '../styles';

const Welcome = ({ navigation: { navigate } }) => {
  return (
    <LinearGradient
      colors={['#6bb6ff', '#DEDDFF']}
      style={styles.welcomeBackground}
      locations={[0.50, 1.00]}
    >
      <SafeAreaView style={styles.welcomeView}>
        <WhiteboardIcon />
        <Text style={styles.whiteboardText}>Whiteboard</Text>
        <Text style={styles.sloganText}>A New Way to Plan</Text>
        <TouchableOpacity
          style={{ ...styles.welcomeButton, backgroundColor: 'white' }}
          onPress={() => navigate('Auth', { login: false })}
        >
          <Text style={{ ...styles.actionText, color: 'black' }}>Sign up</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.welcomeButton}
          onPress={() => navigate('Auth', { login: true })}
        >
          <Text style={styles.actionText}>Login</Text>
        </TouchableOpacity>
      </SafeAreaView>
    </LinearGradient>
  );
};

export default Welcome;
