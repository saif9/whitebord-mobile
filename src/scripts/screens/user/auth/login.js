import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import {
  Text, TextInput, View, TouchableOpacity
} from 'react-native';
import auth from '@react-native-firebase/auth';
import styles from '../styles';
import { Google } from './Google';
import Facebook from './Facebook';
import { Microsoft } from './Microsoft';
import { trackLogin } from '../../../tracking';

const Login = ({ setLoginPage }) => {
  const dispatch = useDispatch();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loginErr, setLoginErr] = useState(null);

  const signin = () => {
    auth().signInWithEmailAndPassword(email, password)
      .then(() => {
        dispatch({ type: 'SIGN_IN' });
        trackLogin({ user: auth().currentUser });
      })
      .catch((err) => {
        console.log(err);
        setLoginErr(err);
      });
  };

  const authErrHandler = () => {
    // "auth/user-not-found" exception is verbose/confusing, thus use this custom text
    if (loginErr.code === 'auth/user-not-found') {
      return (
        <View className="error">
          <Text>
            No user is using this email address. Please sign up first!
          </Text>
        </View>
      );
    }
    return (
      <View className="error"><Text>{loginErr?.message}</Text></View>
    );
  };

  return (
    <>
      <View style={styles.authContainer}>
        <Text style={styles.userWelcome}>LOG IN</Text>
        <View style={styles.authInfoStack}>
          <View style={styles.infoStrip}>
            <TextInput
              autoCapitalize="none"
              id="email"
              textContentType="emailAddress"
              value={email}
              onChangeText={setEmail}
              placeholder="Email"
              placeholderTextColor="#434C73"
              style={styles.infoText}
            />
          </View>
          <View style={styles.infoStrip}>
            <TextInput
              autoCapitalize="none"
              id="password"
              secureTextEntry
              textContentType="newPassword"
              value={password}
              onChangeText={setPassword}
              placeholder="Password"
              placeholderTextColor="#434C73"
              style={styles.infoText}
              onSubmitEditing={signin}
            />
          </View>
          <View>
            <TouchableOpacity
              style={styles.actionButton}
              onPress={signin}
            >
              <Text style={{ ...styles.actionText }}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ ...styles.actionButton, backgroundColor: '#FDCA29' }}
              onPress={() => setLoginPage((prevState) => !prevState)}
            >
              <Text style={{ ...styles.actionText, color: 'black' }}>Sign up</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      {loginErr ? authErrHandler() : <Text />}
      <View style={styles.authButtonsView}>
        <Facebook />
        <Google />
        <Microsoft />
      </View>
    </>
  );
};

export default Login;
