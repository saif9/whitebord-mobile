import React, { useState } from 'react';
import {
  Text, TouchableOpacity,
} from 'react-native';
import { LoginManager, Settings, AccessToken } from 'react-native-fbsdk-next';

import auth from '@react-native-firebase/auth';
import { useDispatch } from 'react-redux';
import styles from '../styles';
import FacebookLogo from '../../../../assets/icons/facebook_logo.svg';
import { trackLogin } from '../../../tracking';

Settings.initializeSDK();

const onFacebookButtonPress = async (linking) => {
  // Attempt login with permission
  const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);

  if (result.isCancelled) {
    throw new Error('User cancelled the login process');
  }
  const data = await AccessToken.getCurrentAccessToken();

  if (!data) {
    throw new Error('Something went wrong obtaining access token');
  }
  const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);
  if (linking && auth().currentUser) {
    return auth().currentUser.linkWithCredential(facebookCredential);
  }
  return auth().signInWithCredential(facebookCredential);
};

export default ({ linking }) => {
  const dispatch = useDispatch();
  const [loginErr, setLoginErr] = useState(null);

  const authErrHandler = () => {
    console.log(loginErr);
    // "auth/user-not-found" exception is verbose/confusing, thus use this custom text
    return (
      null // <View className="error"><Text>Facebook Login Unsucessful</Text></View>
    );
  };

  const onButtonPress = () => onFacebookButtonPress(linking)
    .then((loginData) => {
      dispatch({ type: `${linking ? 'LINKED' : 'SIGN_IN'}` });
      const user = {
        ...JSON.parse(JSON.stringify(loginData.user)),
        network: 'Facebook',
      };
      trackLogin({ user });
      if (linking) linking();
    })
    .catch((err) => {
      setLoginErr(err);
    });

  return (
    <>
      {
        linking ? (
          <TouchableOpacity style={styles.button} onPress={onButtonPress}>
            <Text style={styles.buttonText}> Link Facebook </Text>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity onPress={onButtonPress}>
            <FacebookLogo width={45} height={45} />
          </TouchableOpacity>
        )
      }
      {loginErr ? authErrHandler() : <Text />}
    </>
  );
};
