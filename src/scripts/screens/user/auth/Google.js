import React, { useState } from 'react';
import {
  Text, TouchableOpacity,
} from 'react-native';
import { GoogleSignin } from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth';
import { useDispatch } from 'react-redux';

import styles from '../styles';
import GoogleLogo from '../../../../assets/icons/google_logo.svg';
import { trackLogin } from '../../../tracking';

GoogleSignin.configure({ scopes: ['https://www.googleapis.com/auth/calendar'] });

const signOut = async () => {
  try {
    // await GoogleSignin.revokeAccess();
    await GoogleSignin.signOut();// Remember to remove the user from your app's state as well
  } catch (error) {
    console.error(error);
  }
};

export const clearGoogleToken = async () => {
  await GoogleSignin.revokeAccess();
};

export const getGoogleToken = async () => {
  try {
    await GoogleSignin.signInSilently();
    const tokens = await GoogleSignin.getTokens();
    return tokens.accessToken;
  } catch (error) {
    console.log(error);
  }
  return null;
};

const onGoogleButtonPress = async (linking) => {
  await signOut();
  const { idToken } = await GoogleSignin.signIn();
  const googleCredential = auth.GoogleAuthProvider.credential(idToken);
  if (linking && auth().currentUser) {
    return auth().currentUser.linkWithCredential(googleCredential);
  }
  return auth().signInWithCredential(googleCredential);
};

export const Google = ({ linking }) => {
  const dispatch = useDispatch();
  const [loginErr, setLoginErr] = useState(null);

  const authErrHandler = () => {
    // "auth/user-not-found" exception is verbose/confusing, thus use this custom text
    return (
      null // <View className="error"><Text>{"Google Login Unsucessful"}</Text></View>
    );
  };

  const onButtonPress = () => onGoogleButtonPress(linking).then((loginData) => {
    dispatch({ type: `${linking ? 'LINKED' : 'SIGN_IN'}` });
    const user = {
      ...JSON.parse(JSON.stringify(loginData.user)),
      network: 'Google',
    };
    trackLogin({ user });
    if (linking) linking();
  })
    .catch((err) => {
      if (err.message?.includes('ERROR_PROVIDER_ALREADY_LINKED')) linking();
      setLoginErr(err);
    });

  return (
    <>
      {
        linking ? (
          <TouchableOpacity style={styles.button} onPress={onButtonPress}>
            <Text style={styles.buttonText}> Link Google </Text>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity onPress={onButtonPress}>
            <GoogleLogo width={80} height={80} marginRight={30} />
          </TouchableOpacity>
        )
      }

      {loginErr && authErrHandler()}
    </>
  );
};
