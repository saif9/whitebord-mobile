import { StyleSheet } from 'react-native';
import { font, layout } from '../../../constants';

const { height, width } = layout.window;
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 15,
    paddingHorizontal: 30,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: font.semibold,
    color: '#434279',
    fontSize: 20,
  },
  arrowBack: {
    marginRight: 15,
    height: 24,
    width: 24,
  },
  sectionHeader: {
    color: '#434279',
    fontSize: 16,
    fontFamily: font.semibold,
    marginVertical: 15,
  },
  subSection: {
    marginLeft: 20,
  },
  text: {
    fontFamily: font.regular,
    fontSize: 14,
    color: '#01004A',
    marginVertical: 5,
  },
  button: {
    width: 140,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#5879ED',
    marginBottom: 20,
  },
  buttonText: {
    alignSelf: 'center',
    fontFamily: font.regular,
    fontSize: 12,
    padding: 5,
    color: '#2F57E9',
  },
  signoutButton: {
    alignSelf: 'center',
    height: 40,
    width: 150,
    marginTop: 50,
    backgroundColor: '#2F57E9',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginVertical: 5,
  },
  signoutText: {
    fontSize: 15,
    fontFamily: font.semibold,
    color: 'white',
  },
  userWelcome: {
    fontFamily: font.bold,
    fontSize: 16,
    color: '#8093F1',
    marginLeft: 20,
    marginTop: 30,
    marginBottom: 10,
  },
  actionButton: {
    height: 40,
    width: 140,
    backgroundColor: '#6574ff',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    marginVertical: 7,
  },
  actionText: {
    fontFamily: font.semibold,
    fontSize: 15,
    color: 'white',
  },
  infoStrip: {
    flexDirection: 'row',
    backgroundColor: '#F1F2F9',
    borderRadius: 10,
    paddingHorizontal: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 7,
    height: 35,
  },
  infoText: {
    fontFamily: font.medium,
    fontSize: 15,
    color: '#424555',
    width: '100%',
  },
  authContainer: {
    marginHorizontal: 10,
  },
  authInfoStack: {
    alignItems: 'center',
    marginHorizontal: 20,
    flexDirection: 'column',
  },
  welcomeBackground: {
    flex: 1,
    paddingTop: height / 10,
  },
  welcomeView: {
    alignItems: 'center',
  },
  whiteboardText: {
    fontFamily: font.extrabold,
    fontSize: 40,
    color: 'rgba(73, 75, 197, 1)',
    marginTop: height / 40,
  },
  sloganText: {
    fontFamily: font.bold,
    fontSize: 23,
    color: 'white',
    marginBottom: height / 20,
  },
  welcomeButton: {
    height: 50,
    width: width / 2,
    backgroundColor: '#6574ff',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    marginVertical: 10,
  },
  authButtonsView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginLeft: 30,
  },
});
