import React, { useEffect, useState } from 'react';
import {
  Text,
  TouchableOpacity,
  ScrollView,
  View,
  Linking,
} from 'react-native';
import auth from '@react-native-firebase/auth';
import moment from 'moment';
import { useDispatch } from 'react-redux';
import styles from './styles';
import ArrowBack from '../../../assets/icons/chevron_left.svg';
import { clearGoogleToken, Google } from './auth/Google';
import Facebook from './auth/Facebook';
import { Microsoft, getMicrosoftToken } from './auth/Microsoft';
import { fetchEvents, removeAllCalendars, removeAllEvents } from '../../state/slices/calendars/calendarSlice';
import { ActionTypes } from '../../state/actions/types';
import { FEEDBACK_URL } from '../../../constants';

const UserScreen = ({ navigation }) => {
  const [providers, setProviders] = useState(undefined);
  const [update, setUpdate] = useState(0);
  const dispatch = useDispatch();

  const logout = () => {
    auth()
      .signOut()
      .then(() => {
        setTimeout(() => {
          dispatch({
            type: ActionTypes.USER_LOGOUT,
            payload: undefined,
          });
        }, 1000);
      });
  };

  useEffect(() => {
    const getProviders = async () => {
      if (auth().currentUser) {
        const provs = await auth().fetchSignInMethodsForEmail(
          auth().currentUser.email,
        );
        if (await getMicrosoftToken()) provs.push('microsoft.com');
        setProviders(provs);
      }
    };
    getProviders();
  }, [update]);

  const refreshCalendars = () => {
    const dateRange = {
      timeMinString: moment(new Date())
        .startOf('month')
        .add(1, 'minute')
        .toISOString(),
      timeMaxString: moment(new Date()).endOf('month').toISOString(),
    };
    dispatch(fetchEvents(dateRange));
  };

  const clearCalendars = async () => {
    try {
      clearGoogleToken();
      // clearMicrosoftToken(); // It is required but will only work on main firebase account
      dispatch(removeAllCalendars());
      dispatch(removeAllEvents());
    } catch (e) {
      console.log(e.message);
    }
  };

  return (
    <ScrollView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={navigation.goBack}>
          <ArrowBack style={styles.arrowBack} />
        </TouchableOpacity>
        <Text style={styles.headerText}> Settings </Text>
      </View>
      <Text style={styles.sectionHeader}> Account Details </Text>
      <View style={styles.subSection}>
        <Text style={styles.text}>
          {`Name: ${auth().currentUser.displayName}`}
        </Text>
        <Text style={styles.text}>
          {`Email: ${auth().currentUser.email}`}
        </Text>
      </View>

      <Text style={styles.sectionHeader}> Give Us Feedback </Text>
      <View style={styles.subSection}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => { Linking.openURL(FEEDBACK_URL,); }}
        >
          <Text style={styles.buttonText}> Feedback Form </Text>
        </TouchableOpacity>
      </View>

      <Text style={styles.sectionHeader}> Calendar Integrations </Text>
      <View style={styles.subSection}>
        {<Google linking={refreshCalendars} />}
        {<Microsoft linking={refreshCalendars} />}
        {providers && providers.indexOf('facebook.com') === providers.length && (
        <Facebook linking={() => setUpdate((val) => val + 1)} />
        )}
        <TouchableOpacity style={styles.button} onPress={clearCalendars}>
          <Text style={styles.buttonText}> Clear Calendar Data </Text>
        </TouchableOpacity>
      </View>

      <TouchableOpacity style={styles.signoutButton} onPress={logout}>
        <Text style={styles.signoutText}>Sign Out</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

export default UserScreen;
