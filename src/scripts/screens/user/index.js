import { connect } from 'react-redux';
import UserScreen from './component';

const mapStateToProps = (_state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserScreen);
