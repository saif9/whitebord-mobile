import React, { useState, useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import { View } from 'react-native';
import { CalTypes } from './constants';
import {
  Day, ThreeDay, FiveDay, Week, Month, CalHeader, ModeModal
} from './components';
import EditModal from '../../components/modals/EditModal';
import DTPickerModal from '../../components/modals/DTPickerModal';
import UniversalPlus from '../../components/UniversalPlus';
import styles from './styles';
import { calendarsSelectors, fetchEvents } from '../../state/slices/calendars/calendarSlice';
import FilterComponent from './components/whiteboardItems/FilterComponent/component';
import { generateRecurrentItems } from '../../state/slices/recurrentItems/recurrentItemsSlice';
import {
  memoAllDayItems, memoAllEvents, memoAllSmallItems
} from '../../state/slices/whiteboardItems/selectors';

const CalendarScreen = ({ navigation }) => {
  const [currCal, setCurrCal] = useState(CalTypes.DAY);
  const [modeModalVisible, setModeModalVisible] = useState(false);
  const [isFilterModalVisible, setFilterModalVisible] = useState(false);
  const [displayDTModal, setDisplayDTModal] = useState(false);
  const [date, setDate] = useState(moment());
  const [calRange, setCalRange] = useState({
    timeMinString: moment(date).startOf('day').add(1, 'minute')
      .toISOString(),
    timeMaxString: moment(date).endOf('day').toISOString()
  });
  const dispatch = useDispatch();
  const weekViewRef = useRef();

  // Allows for smoother UI in Month component
  const [nextDate, setNextDate] = useState(moment());

  useEffect(() => {
    // calRange is the range of calendar dates which is currently visible in UI.
    // If User changes the calendar mode or changes the dates by swiping, the calendar range will get updated.
    dispatch(fetchEvents(calRange));
    dispatch(generateRecurrentItems({ dateRange: calRange }));
  }, [calRange]);

  const calendars = useSelector(calendarsSelectors.selectAll);

  const allEvents = useSelector(memoAllEvents);
  const allDayItems = useSelector(memoAllDayItems);
  const calItems = useSelector(memoAllSmallItems);
  const monthItems = allEvents;
  const calLoading = useSelector((state) => state.calendars.loading);

  const openDTPickerModal = () => {
    setDisplayDTModal(true);
  };

  const goToDate = (toDate) => {
    onChangeDate(moment(toDate).add(-1, 'day'), 1, 'day');
    // weekViewRef.current.goToDate(moment(toDate));
  };

  const onChangeDate = (oldDate, number, period) => {
    const newDate = moment(oldDate).add(number, period);
    let fDate = null;
    let lDate = null;
    if (number < 0) {
      fDate = moment(oldDate).add(number, period);
      lDate = moment(oldDate);
    } else {
      fDate = moment(newDate);
      lDate = moment(newDate).add(number, period);
    }
    setCalRange({
      timeMinString: moment(fDate).startOf('day').toISOString(),
      timeMaxString: moment(lDate).startOf('day').subtract(1, 'm').toISOString()
    });
    setDate(moment(newDate));
  };

  const onChangeMonth = (newDate) => {
    setCalRange({
      timeMinString: moment(newDate).startOf('month').toISOString(),
      timeMaxString: moment(newDate).endOf('month').toISOString()
    });
    setNextDate(newDate);
  };

  const changeMode = () => {
    setModeModalVisible(!modeModalVisible);
  };

  const onSetCurrCal = (mode, d) => {
    setCurrCal(mode);
    // setDate does not set a newly selected date fast enough to use the new date, so d is used to pass the new date
    const actualDate = d ?? date;
    let minDate = moment(actualDate).startOf('d');
    let maxDate = moment(actualDate);
    switch (mode) {
      case CalTypes.DAY:
        maxDate = moment(actualDate).add(1, 'd').startOf('d').subtract(1, 'm');
        break;
      case CalTypes.THREEDAY:
        maxDate = moment(actualDate).add(3, 'd').startOf('d').subtract(1, 'm');
        break;
      case CalTypes.FIVEDAY:
        maxDate = moment(actualDate).add(5, 'd').startOf('d').subtract(1, 'm');
        break;
      case CalTypes.WEEK:
        minDate = moment(actualDate).startOf('w');
        maxDate = moment(actualDate).endOf('w');
        break;
      case CalTypes.MONTH:
        return;
      default:
        break;
    }
    setCalRange({ timeMinString: minDate.toISOString(), timeMaxString: maxDate.toISOString() });
  };

  const renderCal = (mode) => {
    switch (mode) {
      case CalTypes.DAY:
        return (
          <Day
            weekViewRef={weekViewRef}
            allDayEvents={allDayItems}
            events={calItems}
            date={date}
            onChangeDate={onChangeDate}
          />
        );
      case CalTypes.THREEDAY:
        return (
          <ThreeDay
            allDayEvents={allDayItems}
            events={calItems}
            date={date}
            onChangeDate={onChangeDate}
          />
        );
      case CalTypes.FIVEDAY:
        return (
          <FiveDay
            allDayEvents={allDayItems}
            events={calItems}
            date={date}
            onChangeDate={onChangeDate}
          />
        );
      case CalTypes.WEEK:
        return (
          <Week
            allDayEvents={allDayItems}
            events={calItems}
            date={date}
            onChangeDate={onChangeDate}
          />
        );
      case CalTypes.MONTH:
        return (
          <Month
            allDayEvents={allDayItems}
            events={monthItems}
            date={date}
            onChangeMonth={onChangeMonth}
            setDate={goToDate}
            setCurrCal={onSetCurrCal}
          />
        );
      default:
        return <View />;
    }
  };

  return (
    <>
      <EditModal />
      <DTPickerModal
        visible={displayDTModal}
        dismissModal={() => setDisplayDTModal(false)}
        mode="date"
        value={new Date(date)}
        hideButtons
        onChange={(newDate) => {
          onChangeDate(moment(newDate), 0, 'day');
          onSetCurrCal(CalTypes.DAY, newDate);
          weekViewRef?.current?.goToDate(moment(newDate));
        }}
      />
      <View style={styles.container}>
        <CalHeader
          date={currCal === CalTypes.MONTH ? nextDate : date} // account for date changes in Month mode
          openDTPickerModal={openDTPickerModal}
          changeMode={changeMode}
          navigation={navigation}
          setFilterModalVisible={setFilterModalVisible}
          calLoading={calLoading}
        />
        <ModeModal isVisible={modeModalVisible} mode={currCal} setCurrCal={onSetCurrCal} changeMode={changeMode} />
        {renderCal(currCal)}
        <UniversalPlus />
        <FilterComponent isModalVisible={isFilterModalVisible}
          setFilterModalVisible={setFilterModalVisible}
          calendars={calendars}
        />
      </View>
    </>
  );
};

export default CalendarScreen;
