export const CalTypes = {
  DAY: 'DAY',
  WEEK: 'WEEK',
  MONTH: 'MONTH',
  THREEDAY: 'THREEDAY',
  FIVEDAY: 'FIVEDAY'
};

export const CallbackTypes = {
  DRAG_POSITION: 'DRAG_POSITION',
  EVENT_RESCHEDULE: 'EVENT_RESCHEDULE',
  TO_ACTIVITYBOX: 'TO_ACTIVITYBOX'
};

export const CalArray = [
  { text: 'Day', id: CalTypes.DAY },
  { text: '3-Day', id: CalTypes.THREEDAY },
  { text: '5-Day', id: CalTypes.FIVEDAY },
  { text: 'Week', id: CalTypes.WEEK },
  { text: 'Month', id: CalTypes.MONTH }
];
