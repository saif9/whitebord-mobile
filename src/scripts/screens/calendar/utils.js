import { CONTAINER_HEIGHT } from '@whiteboard-llc/react-native-week-view';
import moment from 'moment';

const HEADER_HEIGHT = 120;

/**
 * Determine day based on gesture movement in X direction
 * @param {string} startDate original start date of task
 * @param {number} transX over course of drag gesture
 * @param {number} daySize number of days shown in view
 */
export const calculateTaskDay = (startDate, transX, daySize) => {
  return moment(startDate).add(Math.floor(((transX * daySize) / 350) + 0.5), 'days');
};

/**
 * Format ISO string given date and hour
 * @param {*} day in format acceptable for momentjs
 * @param {number} eventHour can be a decimal number
 */
export const formatDateTime = (day, eventHour) => (
  moment(day)
    .set('hour', eventHour)
    .set('minute', 60 * (eventHour % 1))
    .set('second', 0)
    .set('millisecond', 0)
    .toISOString()
);

/**
 * Calculate hour where given Y location is on screen.
 * May need further refinement to work with many phone sizes.
 * Precise to the half hour.
 * @param {number} taskLocation Current task location relative to phone
 * @param {number} calendarScroll Current scroll position in calendar ScrollView
 */
export const calculateTaskHour = (taskLocation, calendarScroll) => {
  const currentLocation = taskLocation + calendarScroll - HEADER_HEIGHT;
  const hoursInDisplay = 12;
  const hour = ((currentLocation * hoursInDisplay) - 100) / (CONTAINER_HEIGHT);
  return Math.floor(2 * hour) / 2;
};

/**
 * Determine date and time as result of drag gesture
 * @param {number} finalPosition absolute Y w.r.t. WeekView ScrollView
 * @param {number} currentScroll scroll position of WeekView
 * @param {string} dragTaskStartDate original date of task
 * @param {number} transX over course of drag gesture
 * @param {number} daySize number of days shown in view
 * @returns date in ISO string format
 */
export const calculateScheduledDate = (
  finalPosition, currentScroll, dragTaskStartDate, transX, daySize
) => {
  const eventHour = calculateTaskHour(finalPosition - currentScroll + 130, currentScroll);
  const eventDay = calculateTaskDay(dragTaskStartDate, transX, daySize);
  return formatDateTime(eventDay, eventHour);
};
