import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    color: 'black',
    flex: 1,
    backgroundColor: 'white',
  },
});
