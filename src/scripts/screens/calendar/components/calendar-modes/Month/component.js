import React from 'react';
import moment from 'moment';
import { CalendarList } from 'react-native-calendars';
import { ScrollView } from 'react-native';
import { DayComponent } from '../../whiteboardItems';
import { MonthHeader } from '../../headers';
import { CalTypes } from '../../../constants';
import { font } from '../../../../../../constants';
import { removeHTMLTags } from '../../../../../utils';

const Month = ({
  events, date, onChangeMonth, setCurrCal, setDate
}) => {
  /**
   * Called when number of visible months change (either 1 or 2)
   * Receives array of visible months
   * If length is 1, then set date to singleton's month
   */
  const handleMonthChange = (currDate) => (months) => {
    if (months.length !== 1) return;

    const { month, year } = months[0];
    onChangeMonth(moment(currDate).set('month', month - 1).set('year', year));
  };

  /**
   * Convert WeekView events into Wix markings
   * @param {Object[]} eventList list of events in WeekView format
   * @returns {Object} JSON mapping dates to list of events
   */
  const mapEvents = (eventList) => {
    return eventList.map((e) => {
      const startDate = moment(e.startDate);
      const endDate = moment(e.endDate);
      const diff = moment(e.endDate).add(e.calId ? -1 : 0, 'm').diff(moment(e.startDate), 'day');
      const isSingleDay = diff === 0 || e.calId;
      const sortOrder = isSingleDay ? -1 : diff;
      return {
        key: e.id,
        name: removeHTMLTags(e.name),
        color: e.color,
        type: e.type,
        startDate,
        endDate,
        isSingleDay,
        sortOrder,
        calId: e.calId
      };
    })
      .reduce((obj, e) => {
        let currentDate = moment(e.startDate);
        const endDate = moment(e.endDate).add(e.calId ? -1 : 0, 'm');
        let eves = {};
        while (currentDate <= endDate) {
          const fDate = currentDate.format('YYYY-MM-DD');
          eves = {
            ...obj,
            ...eves,
            [fDate]: {
              events: [...(obj[fDate]?.events || []), e].sort((a, b) => b.sortOrder - a.sortOrder)
            }
          };
          currentDate = moment(currentDate).add(1, 'days');
        }
        return eves;
      }, {});
  };

  const onDayPress = (day) => {
    setCurrCal(CalTypes.DAY);
    setDate(moment(day.dateString));
  };

  return (
    <ScrollView>
      <CalendarList
        horizontal
        pagingEnabled
        markedDates={mapEvents(events)}
        hideExtraDays={false}
        current={date.format('YYYY-MM-DD')}
        onVisibleMonthsChange={handleMonthChange(date)}
        customHeader={MonthHeader}
        dayComponent={DayComponent}
        calendarHeight={650}
        onDayPress={onDayPress}
        textDayFontFamily={font.light}
        textMonthFontFamily={font.light}
        textDayHeaderFontFamily={font.light}
      />
    </ScrollView>
  );
};

export default Month;
