import React, { useState } from 'react';
import { Dimensions } from 'react-native';
import WeekView from '@whiteboard-llc/react-native-week-view';
import { useDispatch, useSelector } from 'react-redux';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withSpring,
} from 'react-native-reanimated';
import moment from 'moment';

import { cloneDeep } from 'lodash';
import { EventComponent, FloatingComponent } from '../../whiteboardItems';
import { ActivityBox } from '../../activitybox';
import { CallbackTypes } from '../../../constants';
import { calculateTaskHour, formatDateTime } from '../../../utils';
import { newEvent } from '../constants';
import styles from '../../styles';
import { setModal, toggleModal } from '../../../../../state/actions/modal';
import { memoActivityItems } from '../../../../../state/slices/whiteboardItems/selectors';
import { upsertWhiteboardItem } from '../../../../../state/slices/whiteboardItems/whiteboardItemsSlice';
import { upsertException } from '../../../../../state/slices/exceptions/exceptionsSlice';
import { hp } from '../../../../../utils';

const Day = ({
  allDayEvents, events, date, onChangeDate, weekViewRef
}) => {
  const [currentScroll, setCurrentScroll] = useState(0);
  const dispatch = useDispatch();

  // Values of floating component, driven by drag gesture in some EventComponent
  const FloatingDayComponent = FloatingComponent(true);
  const dragTaskData = useSharedValue(null);
  const dragX = useSharedValue(0);
  const dragY = useSharedValue(0);

  const isActivityBoxOpen = useSharedValue(false);
  const activityBoxTop = useSharedValue(0);

  const calculateActivityBoxTop = (height) => {
    // total height minus box height and height of TabBar
    // the 50 is an estimate of the bottom inset safe area
    const top = hp(100) - height - hp(10) - 50;
    return top;
  };

  /**
   * Mirror tasks in "Due Today" of dashboard excluding overdue tasks
   */

  const activityItems = useSelector((state) => memoActivityItems(state, date));

  const onEventPress = (data) => {
    // Google events include a key called "summary"
    if (!data.summary) {
      const item = data.itemToEdit ? data.itemToEdit : data;
      toggleModal(true, dispatch);
      setModal({ ...item, mode: 'edit' }, dispatch);
    } else {
      toggleModal(true, dispatch);
      setModal({ ...data, mode: 'edit', editDisabled: true }, dispatch);
    }
  };

  // Track scroll position of ScrollView
  const handleScroll = ({ nativeEvent }) => {
    setCurrentScroll(nativeEvent.contentOffset.y);
  };

  const onGridClick = (_pressEvent, startHour, myDate) => {
    toggleModal(true, dispatch);
    const datetime = moment(myDate).add(startHour, 'hours');
    const startDate = moment(datetime);
    const endDate = datetime.clone().add(1, 'hours');

    setModal(
      {
        ...newEvent,
        startDate,
        endDate,
        type: 'event',
      },
      dispatch,
    );
  };

  const onUpdateItem = (id, dragTaskLocation) => {
    const eventHour = calculateTaskHour(dragTaskLocation - 67.5, currentScroll);
    const item = activityItems.find((it) => it.id === id);

    const fields = {
      allDay: false,
      duration: item.duration || 1,
      scheduledDate: formatDateTime(date, eventHour),
    };
    if (item.isRecurrenceItem) {
      dispatch(upsertException({ updates: { id, ...fields, date: fields.scheduledDate } }));
    } else dispatch(upsertWhiteboardItem({ updates: { id, ...fields } }));
  };

  /**
   * Handle callbacks from EventComponent
   * @param {Object} callback
   * @param {string} callback.type Specifies intent
   * @param {any} callback.payload Data to be utilized
   */
  const sendCallback = (callback) => {
    try {
      switch (callback.type) {
        // Just update dragging component data
        case CallbackTypes.DRAG_POSITION: {
          // prevent needless reassignments
          const { event, x, y } = callback.payload;
          if (dragX.value === 0) dragTaskData.value = cloneDeep(event);
          dragX.value = x;
          dragY.value = y;
          break;
        }

        // Update item to new scheduled location or activity box
        case CallbackTypes.EVENT_RESCHEDULE: {
          const { finalPosition, id, toActivityBox } = callback.payload;

          // No drag occurred
          if (!dragTaskData.value) return;

          // Prevent putting event into activity box
          if (
            dragTaskData.value.type === 'event'
            && isActivityBoxOpen.value
            && toActivityBox
          ) { return; }

          // if activitybox is open and is dropped there, then update data to indicate that
          const fields = isActivityBoxOpen.value && toActivityBox
            ? {
              allDay: true,
              scheduledDate: moment(date).format('YYYY-MM-DD'),
            }
            : {
              scheduledDate: formatDateTime(
                date,
                calculateTaskHour(
                  finalPosition - currentScroll + 130,
                  currentScroll,
                ),
              ),
            };
          if (dragTaskData.value.isRecurrenceItem) {
            dispatch(
              upsertException({ updates: { id, ...fields, date: fields.scheduledDate } }),
            ).then(() => {
              setTimeout(() => {
                dragTaskData.value = null;
                dragX.value = 0;
                dragY.value = 0;
              }, 600);
            });
          } else {
            dispatch(upsertWhiteboardItem({ updates: { id, ...fields } })).then(() => {
              setTimeout(() => {
                dragTaskData.value = null;
                dragX.value = 0;
                dragY.value = 0;
              }, 600);
            });
          }

          break;
        }

        default: {
          break;
        }
      }
    } catch (e) {
      console.log(e);
    }
  };

  /**
   * Stylings of FloatingDayComponent
   * Initial position based on data
   * Translate based on incoming drag callbacks
   */
  const floatingComponentStyle = useAnimatedStyle(() => ({
    opacity: dragTaskData.value ? 1 : 0,
    zIndex: 20,
    left: dragTaskData.value ? dragTaskData.value.left + 61 : 0,
    top: dragTaskData.value ? dragTaskData.value.top - currentScroll + 135 : 0,
    height: dragTaskData.value ? dragTaskData.value.duration * 75 : 0,
    transform: [
      { translateX: dragX.value },
      { translateY: dragY.value },
      { scale: withSpring(dragTaskData.value ? 1.025 : 1) },
    ],
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: withSpring(dragTaskData.value ? 0.25 : 0),
    shadowRadius: 5,
    elevation: 1,
  }));
  return (
    <>
      <WeekView
        ref={(ref) => {
          weekViewRef.current = ref;
        }}
        events={events}
        allDayEvents={allDayEvents}
        selectedDate={moment(date)}
        numberOfDays={1}
        onEventPress={onEventPress}
        sendCallback={sendCallback}
        onGridClick={onGridClick}
        onSwipeNext={() => onChangeDate(date, 1, 'day')}
        onSwipePrev={() => onChangeDate(date, -1, 'day')}
        headerStyle={styles.header}
        headerTextStyle={styles.headerText}
        hourTextStyle={styles.hourText}
        eventContainerStyle={styles.eventContainer}
        formatDateHeader="ddd+"
        hoursInDisplay={12}
        startHour={8}
        EventComponent={EventComponent(true, activityBoxTop)}
        handleScroll={handleScroll}
      />
      <Animated.View
        style={[
          styles.floatingWrapper,
          { width: Dimensions.get('window').width - 76 },
          floatingComponentStyle,
        ]}
      >
        <FloatingDayComponent event={dragTaskData} />
      </Animated.View>
      <ActivityBox
        activityItems={activityItems}
        onUpdateItem={onUpdateItem}
        isActivityBoxOpen={isActivityBoxOpen}
        onHeightUpdated={(height) => {
          activityBoxTop.value = calculateActivityBoxTop(height);
        }}
      />
    </>
  );
};

export default Day;
