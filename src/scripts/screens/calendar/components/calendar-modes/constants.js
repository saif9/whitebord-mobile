/**
 * startDate and endDate must be provided
 */

export const newEvent = {
  name: '',
  color: '#fa8072',
  completed: false,
  id: null,
  mode: 'create',
  type: 'event',
  editing: true,
};
