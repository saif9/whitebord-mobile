import React, { useState } from 'react';
import { Dimensions } from 'react-native';
import { useDispatch } from 'react-redux';
import Animated, { useAnimatedStyle, useSharedValue, withSpring } from 'react-native-reanimated';
import WeekView from '@whiteboard-llc/react-native-week-view';
import moment from 'moment';
import { cloneDeep } from 'lodash';
import { CallbackTypes } from '../../../constants';
import { calculateScheduledDate } from '../../../utils';
import { EventComponent, FloatingComponent } from '../../whiteboardItems';
import { newEvent } from '../constants';
import styles from '../../styles';
import { setModal, toggleModal } from '../../../../../state/actions/modal';
import { upsertWhiteboardItem } from '../../../../../state/slices/whiteboardItems/whiteboardItemsSlice';
import { upsertException } from '../../../../../state/slices/exceptions/exceptionsSlice';

const FiveDay = ({
  events, date, onChangeDate, allDayEvents
}) => {
  const dispatch = useDispatch();
  const [currentScroll, setCurrentScroll] = useState(0);

  // Values of floating component, driven by drag gesture in some EventComponent
  const FloatingMultidayComponent = FloatingComponent(false);
  const dragTaskData = useSharedValue(null);
  const dragTaskStartDate = useSharedValue('');
  const dragTaskWeekday = useSharedValue(0);
  const dragX = useSharedValue(0);
  const dragY = useSharedValue(0);

  const onEventPress = (data) => {
    // Google events include a key called "summary"
    if (!data.summary) {
      const item = data.itemToEdit ? data.itemToEdit : data;
      toggleModal(true, dispatch);
      setModal({ ...item, mode: 'edit' }, dispatch);
    } else {
      toggleModal(true, dispatch);
      setModal({ ...data, mode: 'edit', editDisabled: true }, dispatch);
    }
  };

  const onGridClick = (_pressEvent, startHour, myDate) => {
    toggleModal(true, dispatch);
    const datetime = moment(myDate).add(startHour, 'hours');
    const startDate = moment(datetime);
    const endDate = datetime.clone().add(1, 'hours');

    setModal({
      ...newEvent,
      startDate,
      endDate,
      type: 'event'
    }, dispatch);
  };

  // Track scroll position of ScrollView
  const handleScroll = ({ nativeEvent }) => {
    setCurrentScroll(nativeEvent.contentOffset.y);
  };

  /**
   * Handle callbacks from EventComponent
   * @param {Object} callback
   * @param {string} callback.type Specifies intent
   * @param {any} callback.payload Data to be utilized
   */
  const sendCallback = (callback) => {
    try {
      switch (callback.type) {
        // Just update dragging component data
        case CallbackTypes.DRAG_POSITION: {
          // prevent needless reassignments
          const { event, x, y } = callback.payload;
          if (dragX.value === 0) {
            dragTaskData.value = cloneDeep(event);
            dragTaskStartDate.value = moment(event.startDate).format('YYYY-MM-DD');
            dragTaskWeekday.value = moment(event.startDate).startOf('day').diff(moment(date).startOf('day'), 'days');
          }
          dragX.value = x;
          dragY.value = y;
          break;
        }

        // Update item to new scheduled location
        case CallbackTypes.EVENT_RESCHEDULE: {
          const { finalPosition, transX, id } = callback.payload;
          const scheduledDate = calculateScheduledDate(finalPosition, currentScroll, dragTaskStartDate.value, transX, 5);
          if (dragTaskData.value.isRecurrenceItem) {
            dispatch(upsertException({ updates: { id, scheduledDate, date: scheduledDate } })).then(() => {
              setTimeout(() => {
                dragTaskData.value = null;
                dragX.value = 0;
                dragY.value = 0;
              }, 600);
            });
          } else {
            dispatch(upsertWhiteboardItem({ updates: { id, scheduledDate } })).then(() => {
              setTimeout(() => {
                dragTaskData.value = null;
                dragX.value = 0;
                dragY.value = 0;
              }, 600);
            });
          }
          break;
        }

        default: {
          break;
        }
      }
    } catch (e) { console.log(e); }
  };

  /**
   * Stylings of FloatingDayComponent
   * Initial position based on data
   * Translate based on incoming drag callbacks
   */
  const animatedStyle = useAnimatedStyle(() => ({
    opacity: dragTaskData.value ? 1 : 0,
    zIndex: 20,
    left: dragTaskData.value ? dragTaskData.value.left + (70 * dragTaskWeekday.value) + 61 : 0,
    top: dragTaskData.value ? dragTaskData.value.top - currentScroll + 132 : 0,
    height: dragTaskData.value ? dragTaskData.value.duration * 70 : 0,
    transform: [
      { translateX: dragX.value },
      { translateY: dragY.value },
      { scale: withSpring(dragTaskData.value ? 1.025 : 1) }
    ],
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: withSpring(dragTaskData.value ? 0.25 : 0),
    shadowRadius: 5,
    elevation: 1
  }));

  return (
    <>
      <WeekView
        allDayEvents={allDayEvents}
        events={events}
        selectedDate={moment(date)}
        numberOfDays={5}
        onEventPress={onEventPress}
        onGridClick={onGridClick}
        onSwipeNext={() => onChangeDate(date, 5, 'days')}
        onSwipePrev={() => onChangeDate(date, -5, 'days')}
        headerStyle={styles.header}
        headerTextStyle={styles.headerText}
        hourTextStyle={styles.hourText}
        eventContainerStyle={styles.eventContainer}
        formatDateHeader="ddd+"
        hoursInDisplay={12}
        startHour={8}
        EventComponent={EventComponent(false)}
        sendCallback={sendCallback}
        handleScroll={handleScroll}
      />
      <Animated.View
        style={[{
          position: 'absolute',
          width: (Dimensions.get('window').width - 61) / 5,
          flex: 1,
        }, animatedStyle]}
      >
        <FloatingMultidayComponent event={dragTaskData} />
      </Animated.View>
    </>
  );
};

export default FiveDay;
