import React from 'react';
import {
  View, Text, Dimensions, FlatList, Pressable
} from 'react-native';
import Animated, {
  Easing, useAnimatedStyle, withTiming
} from 'react-native-reanimated';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import { font } from '../../../../../../constants';
import { CalArray } from '../../../constants';

import styles from './styles';

const ModeModal = ({
  isVisible, mode, setCurrCal, changeMode
}) => {
  const { width } = Dimensions.get('window');

  const animatedStyle = useAnimatedStyle(() => ({
    height: withTiming(isVisible ? 200 : 0, { duration: 250, easing: Easing.inOut(Easing.ease) }),
    paddingVertical: withTiming(isVisible ? 10 : 0, { duration: 250, easing: Easing.inOut(Easing.ease) }),
    opacity: withTiming(isVisible ? 1 : 0, { duration: 250, easing: Easing.inOut(Easing.ease) }),
  }));

  const modeItem = ({ item }) => {
    const isSelected = item.id === mode;
    return (
      <Pressable
        style={[styles.modeItem, { backgroundColor: isSelected ? '#deddff' : 'white' }]}
        onPress={() => { setCurrCal(item.id); changeMode(); }}
      >
        <Text style={{ fontFamily: font.medium, color: isSelected ? '#2F57E9' : 'black' }}>{item.text}</Text>
        {isSelected && <FontAwesome5Icon name="check" color="#2F57E9" />}
      </Pressable>
    );
  };

  return (
    <Animated.View style={[styles.container, { width: width / 2 }, animatedStyle]}>
      <View style={styles.header}>
        <Text style={styles.headerText}>Calendar Mode</Text>
      </View>
      <FlatList data={CalArray} renderItem={modeItem} keyboardDismissMode="on-drag" />
    </Animated.View>
  );
};

export default ModeModal;
