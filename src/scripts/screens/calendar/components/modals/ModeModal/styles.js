import { StyleSheet } from 'react-native';
import { font } from '../../../../../../constants';

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    right: 15,
    top: 70,
    zIndex: 5,
    backgroundColor: 'white',
    flexDirection: 'column',
    textAlign: 'center',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.35,
    shadowRadius: 10,
    elevation: 1,
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ccc',
    borderBottomWidth: 0.5,
    paddingBottom: 10
  },
  headerText: {
    fontFamily: font.semibold,
  },
  modeItem: {
    padding: 7,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15
  }
});

export default styles;
