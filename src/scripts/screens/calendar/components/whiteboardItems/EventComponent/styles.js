import { StyleSheet } from 'react-native';
import { font } from '../../../../../../constants';

const styles = StyleSheet.create({
  name: {
    color: 'black',
    textAlign: 'left',
    flex: 1,
    flexWrap: 'wrap',
    fontSize: 15,
    fontFamily: font.regular,
  },
  eventBody: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    borderRadius: 5,
    minHeight: 11,
  },
  topBar: {
    height: 5,
    width: '100%',
  },
  contentContainer: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  checkbox: {
    width: 15,
    height: 15,
  },
});

export default styles;
