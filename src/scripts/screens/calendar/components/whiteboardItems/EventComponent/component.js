import React, { useState, useEffect } from 'react';
import { Text, View } from 'react-native';
import Animated, {
  useSharedValue,
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useDerivedValue,
  withTiming,
  runOnJS,
  withSpring,
} from 'react-native-reanimated';
import { LongPressGestureHandler } from 'react-native-gesture-handler';
import { useDispatch } from 'react-redux';

import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { CallbackTypes } from '../../../constants';
import styles from './styles';

import { taskBodyColor, taskAccentColor, habitBodyColor } from '../../styles';
import {
  EVENT_COLORS,
  EVENT_COLORS_W_BODY,
} from '../../../../../../constants/event_colors';
import { removeHTMLTags } from '../../../../../utils';
import AdaptableCheckBox from '../../../../../components/AdaptableCheckBox';
import { upsertWhiteboardItem } from '../../../../../state/slices/whiteboardItems/whiteboardItemsSlice';
import { upsertException } from '../../../../../state/slices/exceptions/exceptionsSlice';

const EventComponent = (isDay, activityBoxTop) => ({
  event,
  position,
  sendCallback,
}) => {
  const [isCompleted, setIsCompleted] = useState(event.completed);
  const [reset] = useState(0);
  const durationPadding = 0;
  const dispatch = useDispatch();

  const accentColor = event.type === 'task' || event.type === 'habit'
    ? taskAccentColor
    : event.color ?? EVENT_COLORS.GREEN;
  const bodyColor = (() => {
    switch (event.type) {
      case 'task':
        return taskBodyColor;
      case 'habit':
        return habitBodyColor;
      default:
        return EVENT_COLORS_W_BODY[accentColor]?.body;
    }
  }).call();

  // Animation variables to track drag movement
  const translateX = useSharedValue(0);
  const translateY = useSharedValue(0);
  const startX = useSharedValue(0);
  const startY = useSharedValue(0);
  const isGestureActive = useSharedValue(false);

  const updateEvent = (completed) => {
    if (event.isRecurrenceItem) {
      dispatch(upsertException({ updates: { id: event.id, completed } }));
    } else {
      dispatch(upsertWhiteboardItem({ updates: { id: event.id, completed } }));
    }
    setIsCompleted(completed); // don't wait for firebase feedback
    ReactNativeHapticFeedback.trigger('selection');
  };

  useEffect(() => {
    translateX.value = 0;
    translateY.value = 0;
    startX.value = 0;
    startY.value = 0;
    isGestureActive.value = withTiming(false, 175);
  }, [reset]);
  /**
   * Send updated drag data to parent
   * @param {number} x translateX
   * @param {number} y translateY
   */
  const updatePosition = (x, y) => {
    if (x !== 0 || y !== 0) {
      sendCallback({
        type: CallbackTypes.DRAG_POSITION,
        payload: { x, y, event: { ...event, ...position } },
      });
    }
  };

  /**
   * Triggers when translateX or translateY change, like useEffect
   * Will call updatePosition() on JS (normal) thread
   */
  useDerivedValue(() => {
    runOnJS(updatePosition)(translateX.value, translateY.value);
  }, [translateX.value, translateY.value]);

  /**
   * Inform parent that this event will be rescheduled
   * Send necessary info for Redux action
   * @param {number} positionTop Original position of event in ScrollView
   * @param {number} transY How much the event moved in drag gesture
   */
  const callbackReschedule = (positionTop, transX, transY, toActivityBox) => {
    sendCallback({
      type: CallbackTypes.EVENT_RESCHEDULE,
      payload: {
        id: event.id,
        finalPosition: positionTop + transY,
        transX,
        toActivityBox,
      },
    });
  };

  // Handle start, during, and end of drag gesture
  const onGestureEvent = useAnimatedGestureHandler({
    /**
     * Update animation values based on gesture data
     * Dependent if this gesture was started (see above)
     */
    onActive: ({ absoluteX, absoluteY }) => {
      if (!isGestureActive.value) {
        startX.value = absoluteX;
        startY.value = absoluteY;
        isGestureActive.value = true;
      }
      translateX.value = absoluteX - startX.value;
      translateY.value = absoluteY - startY.value;
    },
    /**
     * Determine if event was placed in calendar or activity box
     * Call appropriate method in JS thread for each case
     * Reset animation info
     */
    onEnd: ({ absoluteY }) => {
      // if isDay, then check drop height and the top of the activityBox to see if being dropped there
      if (
        isDay === true
        && isGestureActive.value
        && absoluteY > activityBoxTop.value
      ) {
        runOnJS(callbackReschedule)(
          position.top,
          translateX.value,
          translateY.value,
          true,
        );
      } else {
        runOnJS(callbackReschedule)(
          position.top,
          translateX.value,
          translateY.value,
          false,
        );
      }
    },
  });

  const gestureMovement = useDerivedValue(() => {
    return Math.sqrt(translateX.value ** 2 + translateY.value ** 2);
  }, [translateX.value, translateY.value]);

  /**
   * Styling focused on allowing smooth animation at beginning of gesture
   * Will create shadow on long press
   * Become invisible after gesture starts, but will translate to follow floating component
   */
  const animatedStyle = useAnimatedStyle(() => ({
    opacity: withTiming(gestureMovement.value > 0 ? 0 : 1, 175),
    transform: [
      { translateX: translateX.value },
      { translateY: translateY.value },
      { scale: withSpring(isGestureActive.value ? 1.025 : 1) },
    ],
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: withSpring(isGestureActive.value ? 0.35 : 0),
    shadowRadius: 10,
    elevation: 1,
  }));

  const containerStyle = event.type === 'event'
    ? { backgroundColor: bodyColor }
    : {
      backgroundColor: bodyColor,
    };

  const getAdditionalStyling = () => {
    if (event.allDay || event.duration < 0.5) {
      return {};
    } else {
      return { paddingTop: 5 };
    }
  };

  return (
    <LongPressGestureHandler onGestureEvent={onGestureEvent}>
      <Animated.View style={[styles.eventBody, animatedStyle, containerStyle]}>
        <View style={[styles.contentContainer, getAdditionalStyling()]}>
          {!isDay || event.type === 'event' ? (
            <View style={{ width: isDay ? 20 : 5 }} />
          ) : (
            <View
              style={{
                width: 35,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <AdaptableCheckBox
                type={event.isRecurrenceItem ? event.type : undefined}
                value={isCompleted}
                onValueChange={updateEvent}
                duration={event.duration}
              />
            </View>
          )}
          <Text
            style={[
              styles.name,
              {
                paddingVertical: durationPadding,
                fontSize: event.duration < 0.42 ? 10 : 15,
                textDecorationLine: isCompleted ? 'line-through' : 'none',
              },
            ]}
          >
            {removeHTMLTags(event.name)}
          </Text>
        </View>
      </Animated.View>
    </LongPressGestureHandler>
  );
};

export default EventComponent;
