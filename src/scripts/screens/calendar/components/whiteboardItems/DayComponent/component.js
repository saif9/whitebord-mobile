import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';
import styles from './styles';
import { wp } from '../../../../../utils';
import { taskBodyColor, taskAccentColor, habitBodyColor } from '../../styles';
import {
  EVENT_COLORS,
  EVENT_COLORS_W_BODY,
} from '../../../../../../constants/event_colors';

/**
 * Class-based in order for library to access PropTypes
 * otherwise events won't be passed through
 */
class DayComponent extends React.Component {
  render() {
    const disabled = this.props.state === 'disabled';
    return (
      <TouchableOpacity onPress={() => this.props.onPress(this.props.date)}>
        {/**
         * Width of this view needs to be better generalized
         * across different phone views
         */}
        <View style={[styles.container, { width: wp(14.28) }]}>
          <Text style={{ color: disabled ? '#6d6d6d' : '#000' }}>
            {String(this.props.children)}
          </Text>
          {this.props.marking.events?.map((e, i) => {
            const isStartDate = moment(this.props.date.dateString).isSame(
              moment(e.startDate),
              'day',
            );
            const isSunday = moment(this.props.date.dateString).isoWeekday() === 7;
            // eslint does not like the template string for some reason
            const accentColor = e.type === 'task' || e.type === 'habit'
              ? taskAccentColor
              : e.color ?? EVENT_COLORS.GREEN;

            const backgroundColor = e.type === 'event'
              ? e.color.concat('BB')
              : (() => {
                switch (e.type) {
                  case 'task':
                    return taskBodyColor;
                  case 'habit':
                    return habitBodyColor;
                  default:
                    return EVENT_COLORS_W_BODY[accentColor]?.body;
                }
              }).call();

            const style = e.isSingleDay
              ? styles.event
              : {
                width: isStartDate ? '90%' : '100%',
                marginTop: 2,
                paddingLeft: isStartDate ? 3 : 0,
              };
            // eslint-disable-next-line no-nested-ternary
            const name = e.isSingleDay
              ? e.name
              : isStartDate || isSunday
                ? e.name
                : ' ';
            return (
              <View
                key={e.key}
                style={[style, { backgroundColor }]}
              >
                <Text numberOfLines={1} style={styles.eventText}>
                  {name}
                </Text>
              </View>
            );
          })}
        </View>
      </TouchableOpacity>
    );
  }
}

DayComponent.propTypes = {
  children: PropTypes.number,
  // eslint-disable-next-line react/forbid-prop-types
  marking: PropTypes.object,
  onPress: PropTypes.func,
  state: PropTypes.string,
};

DayComponent.defaultProps = {
  children: 0,
  marking: {},
  onPress: () => null,
  state: '',
};

export default DayComponent;
