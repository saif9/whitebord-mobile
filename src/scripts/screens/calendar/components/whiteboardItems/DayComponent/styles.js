import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    // widths needs to be better generalized across different screen sizes
    width: 56,
    height: 101,
    borderWidth: 1,
    borderColor: '#e5e5e5',
    borderRadius: 2,
    alignItems: 'center',
    marginBottom: -15,
    backgroundColor: 'white',
    overflow: 'hidden'
  },
  event: {
    width: '90%',
    marginTop: 2,
    paddingLeft: 3,
    paddingRight: 3,
  },
  task: {
    width: '90%',
    marginTop: 2,
    paddingLeft: 3,
    paddingRight: 3,
  },
  eventText: {
    fontSize: 8,
  }
});

export default styles;
