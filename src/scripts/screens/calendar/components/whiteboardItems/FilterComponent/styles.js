import { StyleSheet } from 'react-native';
import { font } from '../../../../../../constants';
import { wp } from '../../../../../utils';

import { taskAccentColor } from '../../styles';

const styles = StyleSheet.create({
  name: {
    marginHorizontal: 4,
    color: taskAccentColor,
    textAlign: 'left',
    flex: 1,
    flexWrap: 'wrap',
    fontSize: 15,
    fontFamily: font.regular,
  },
  eventBody: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    borderRadius: 5,
  },
  topBar: {
    height: 5,
    width: '100%',
  },
  contentContainer: {
    flexDirection: 'row',
  },
  checkbox: {
    transform: [{ scaleX: 0.8 }, { scaleY: 0.8 }]
  },
  cancelText: {
    color: '#ABA9FF',
    fontFamily: font.medium,
  },
  editSaveText: {
    color: '#6574FF',
    fontFamily: font.medium,
  },
  headerText: {
    color: '#434279',
    fontFamily: font.semibold,
    fontSize: 16,
    paddingVertical: wp(2)
  },
  subHeaderText: {
    color: '#434279',
    fontFamily: font.semibold,
    fontSize: 14,
    paddingVertical: wp(3)
  },
  optionText: {
    color: '#434279',
    fontFamily: font.regular,
    fontSize: 14,
    marginTop: -2,
    paddingLeft: wp(2)
  },
  calOptionText: {
    color: '#434279',
    fontFamily: font.regular,
    fontSize: 14,
    marginTop: -5,
    paddingLeft: wp(5)
  },
  hairLine: {
    width: wp(85),
    alignSelf: 'center',
    backgroundColor: '#DEDDFF',
    height: 1,
    marginVertical: wp(5)
  },
});

export default styles;
