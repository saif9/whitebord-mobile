import React from 'react';
import CheckBox from '@react-native-community/checkbox';

import Modal from 'react-native-modal';
import {
  SafeAreaView, ScrollView, Switch, Text, View
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { wp } from '../../../../../utils';
import styles from './styles';
import { setFilter } from '../../../../../state/slices/filters/filterSlice';
import { trackCalendarFilters } from '../../../../../tracking';

const FilterComponent = ({
  isModalVisible, setFilterModalVisible, calendars
}) => {
  const dispatch = useDispatch();
  const { filter } = useSelector((state) => state.calFilter);
  return (

    <Modal isVisible={isModalVisible} style={{ margin: 0 }}>
      <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: wp(5), }}>
          <Text style={styles.cancelText}
            onPress={() => {
              const reset = {};
              Object.keys(filter).forEach((v) => { reset[v] = true; });
              dispatch(setFilter(reset));
            }}
          >
            reset
          </Text>
          <Text style={styles.editSaveText} onPress={() => { setFilterModalVisible(false); }}>save</Text>
        </View>
        <View style={{ alignSelf: 'center' }}>
          <Text style={styles.headerText}>Calendar Options</Text>
        </View>
        <ScrollView>
          <View style={{ paddingHorizontal: wp(7) }}>
            <View>
              <Text style={styles.subHeaderText}>Filter:</Text>
              {Object.entries(filter).map(([key, value], i) => (i < 3 ? (
                <View key={key} style={{ flexDirection: 'row', alignItems: 'center', marginVertical: wp(1) }}>
                  <Switch
                    style={{ transform: [{ scaleX: 0.7 }, { scaleY: 0.6 }] }}
                    trackColor={{ false: '#767577', true: '#6574FF' }}
                    onValueChange={(val) => {
                      dispatch(setFilter({ ...filter, [key]: val }));
                      trackCalendarFilters({ filter: `${key.charAt(0).toUpperCase() + key.slice(1)}s` });
                    }}
                    value={value}
                  />
                  <Text style={styles.optionText}>{`${key.charAt(0).toUpperCase() + key.slice(1)}s`}</Text>
                </View>
              ) : null))}

            </View>
            <View style={styles.hairLine} />
            <View>
              <Text style={styles.subHeaderText}>Manage Calendars:</Text>
              {calendars.map(({ id, name }) => (
                <View key={id}
                  style={{
                    flexDirection: 'row', alignItems: 'center', marginVertical: wp(2)
                  }}
                >
                  <CheckBox
                    style={styles.checkbox}
                    onValueChange={(val) => dispatch(setFilter({ ...filter, [id]: val }))}
                    value={filter[id] !== undefined ? filter[id] : true}
                    boxType="square"
                    onCheckColor="white"
                    onFillColor="#6574FF"
                    tintColor="#6574FF"
                    onTintColor="#6574FF"
                    animationDuration={0}
                  />
                  <Text style={styles.calOptionText}>{`${name}`}</Text>
                </View>
              ))}

            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </Modal>
  );
};

export default FilterComponent;
