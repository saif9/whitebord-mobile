/* eslint-disable no-nested-ternary */
import React from 'react';
import AnimateableText from 'react-native-animateable-text';

import Animated, {
  useAnimatedProps,
  useAnimatedStyle,
} from 'react-native-reanimated';
import AdaptableCheckBox from '../../../../../components/AdaptableCheckBox';
import styles from './styles';

import { taskBodyColor, taskAccentColor, habitBodyColor } from '../../styles';
import {
  EVENT_COLORS,
  EVENT_COLORS_W_BODY,
} from '../../../../../../constants/event_colors';

const AnimatedCheckbox = Animated.createAnimatedComponent(AdaptableCheckBox);

const FloatingComponent = (isDay) => ({ event }) => {
  /**
   * All these hooks allow conditional styling and data to component
   * useState is impractical as it would cause a rerender and cancel drag gesture
   */

  const textStyle = useAnimatedStyle(() => ({
    paddingVertical: event.value?.duration <= 0.5 ? 5 : 8,
    textDecorationLine: event.value?.completed ? 'line-through' : 'none',
  }));

  const viewStyle = useAnimatedStyle(() => ({
    paddingTop: event.value?.duration <= 0.5 ? 9 : 12,
    opacity: event.value?.type === 'task' ? 1 : 0,
  }));

  const text = useAnimatedProps(() => ({
    text: event.value?.name ? event.value?.name.replace(/<(.|\n)*?>/g, '') : '',
  }));

  const checkBoxProps = useAnimatedProps(() => ({
    value: event.value?.completed,
    type: event.value?.type,
    onValueChanged: () => {},
  }));

  const containerStyle = useAnimatedStyle(() => {
    const accentColor = event.value?.type === 'task'
      ? taskAccentColor
      : event.value?.color ?? EVENT_COLORS.GREEN;

    const bodyColor = (() => {
      switch (event.value?.type) {
        case 'task':
          return taskBodyColor;
        case 'habit':
          return habitBodyColor;
        default:
          return EVENT_COLORS_W_BODY[accentColor]?.body;
      }
    }).call();

    return event.value?.type === 'event'
      ? { backgroundColor: bodyColor, borderWidth: 0 }
      : {
        backgroundColor: bodyColor,
      };
  });

  const textPadding = useAnimatedStyle(() => ({ paddingLeft: isDay ? 7 : 0 }));

  return (
    <Animated.View style={[styles.eventBody, containerStyle]}>
      <Animated.View style={[styles.contentContainer, textPadding]}>
        {isDay && (
          <Animated.View style={[styles.checkboxContainer, viewStyle]}>
            <AnimatedCheckbox animatedProps={checkBoxProps} />
          </Animated.View>
        )}
        <AnimateableText
          style={[styles.name, textStyle]}
          animatedProps={text}
        />
      </Animated.View>
    </Animated.View>
  );
};

export default FloatingComponent;
