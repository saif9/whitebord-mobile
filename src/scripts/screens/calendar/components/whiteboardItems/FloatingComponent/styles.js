import { StyleSheet } from 'react-native';
import { font } from '../../../../../../constants';

const styles = StyleSheet.create({
  name: {
    marginHorizontal: 4,
    color: 'black',
    textAlign: 'left',
    flex: 1,
    flexWrap: 'wrap',
    fontSize: 15,
    fontFamily: font.regular,
  },
  eventBody: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    borderRadius: 5,
  },
  topBar: {
    height: 5,
    width: '100%',
  },
  contentContainer: {
    flexDirection: 'row',
  },
  checkboxContainer: {
    height: 20,
    width: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  checkbox: {
    width: 15,
    height: 15,
  },
});

export default styles;
