import { StyleSheet } from 'react-native';
import { font } from '../../../../constants';

export const taskBodyColor = '#F8EABA';
export const taskAccentColor = '#E9BF2F';
export const eventBodyColor = '#BAF8D5';
export const eventAccentColor = '#45C2D2';
export const habitBodyColor = '#BAC7F8';
export const habitAccentColor = '#2F57E9';

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#fff',
    borderTopWidth: 0,
    borderLeftWidth: 1,
    borderRightWidth: 0,
    borderColor: '#e5e5e5',
    // borderBottomWidth: 2,
    // borderBottomColor: '#DEDDFF',
    paddingBottom: 10,
  },
  headerText: {
    textTransform: 'uppercase',
    fontSize: 11,
  },
  hourText: {
    fontFamily: font.regular,
  },
  floatingWrapper: {
    position: 'absolute',
    flex: 1,
  },
  eventContainer: {
    borderBottomColor: 'white',
    borderBottomWidth: 1,
  },
});

export default styles;
