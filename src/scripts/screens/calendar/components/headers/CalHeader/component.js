import React from 'react';
import {
  View, Text, ActivityIndicator
} from 'react-native';
import moment from 'moment';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import InboxButton from '../../../../../components/InboxButton';
import ThreeDots from '../../../../../../assets/icons/threeDots.svg';
import Ellipses from '../../../../../../assets/icons/ellipses.svg';
import ModeIcon from '../../../../../../assets/icons/menu_rect.svg';
import styles from './styles';
import MenuButton from '../../../../../components/MenuButton';

const CalHeader = ({
  date, openDTPickerModal, changeMode, navigation, setFilterModalVisible, calLoading
}) => {
  return (
    <View style={styles.mainContainer}>
      <View style={styles.titleContainer}>
        <View style={styles.filterContainer}>
          <Text style={styles.title}>
            {moment(date).format('MMMM YYYY')}
          </Text>
          <TouchableWithoutFeedback onPress={openDTPickerModal}>
            <Ellipses />
          </TouchableWithoutFeedback>
        </View>
        {calLoading && <ActivityIndicator /> }
        <View style={styles.buttons}>
          <MenuButton onPress={changeMode}>
            <ModeIcon />
          </MenuButton>
          <MenuButton onPress={() => setFilterModalVisible(true)}>
            <View style={{ transform: [{ rotate: '90deg' }] }}>
              <ThreeDots height={18} width={18} />
            </View>
          </MenuButton>
          <InboxButton navigation={navigation} />
        </View>
      </View>
      <View style={styles.dateContainer} />
    </View>
  );
};

export default CalHeader;
