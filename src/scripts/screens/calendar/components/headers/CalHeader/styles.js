import { StyleSheet } from 'react-native';
import { font } from '../../../../../../constants';
import { wp } from '../../../../../utils';

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: 'white'
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: wp(5),
  },
  filterContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  dateContainer: {
    paddingLeft: 17
  },
  todayButton: {
    borderColor: 'grey',
    borderWidth: 0.5,
    borderRadius: 5,
    paddingVertical: 8,
    paddingHorizontal: 12,
  },
  todayText: {
    fontFamily: font.semibold
  },
  title: {
    color: '#434279',
    fontFamily: font.semibold,
    fontSize: 20,
    marginRight: 5,
  },
  monthText: {
    fontFamily: font.medium,
  },
  buttonWrapper: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 100,
    backgroundColor: '#F8F9FF',
  },
  settings: {
    marginLeft: 10,
  },
  buttons: {
    alignItems: 'center', flexDirection: 'row'
  },
  eye: {
    fontSize: 15
  }
});

export default styles;
