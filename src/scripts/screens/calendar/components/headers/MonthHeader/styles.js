import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  monthHeader: {
    marginBottom: -50
  },
  monthHeaderWrapper: {
    flex: 0,
    paddingBottom: 30
  },
});

export default styles;
