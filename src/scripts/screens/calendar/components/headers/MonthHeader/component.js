import React, { Component } from 'react';
import moment from 'moment';
import { View } from 'react-native';
import { Header } from '@whiteboard-llc/react-native-week-view';

import calendarStyles from '../../styles';
import styles from './styles';

/**
 * Needs to be a class-based component as it is passed
 * into CalendarList from wix/react-native-calendars,
 * where they attach a reference to this component
 */
class MonthHeader extends Component {
  render() {
    return (
      <View
        style={styles.monthHeaderWrapper}
      >
        <Header
          style={[calendarStyles.header, styles.monthHeader]}
          textStyle={calendarStyles.headerText}
          formatDate="ddd-"
          numberOfDays={7}
          initialDate={moment().format('YYYY-MM-DD')}
        />
      </View>
    );
  }
}

export default MonthHeader;
