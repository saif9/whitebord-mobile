import React from 'react';
import {
  TouchableOpacity,
  View,
  SafeAreaView,
  Text,
  ScrollView,
} from 'react-native';
import Modal from 'react-native-modal';
import EditModal from '../../../../../components/modals/EditModal';
import ActivityItem from '../ActivityItem';
import styles from './styles';
import UniversalPlus from '../../../../../components/UniversalPlus';
import { hp } from '../../../../../utils';

function ActivityModal({ isOpen, closeModal, activityItems }) {
  return (
    <Modal
      animationType="slide"
      coverScreen
      visible={isOpen}
      style={styles.modal}
    >
      <EditModal />
      <SafeAreaView style={styles.modalView}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 20,
          }}
        >
          <Text style={{ color: 'white' }}>X</Text>
          <Text style={styles.activityBoxText}>Activity Box</Text>
          <TouchableOpacity onPress={closeModal}>
            <Text style={{ color: '#6574FF' }}>X</Text>
          </TouchableOpacity>
        </View>
        <ScrollView>
          {activityItems.map((item) => (
            <ActivityItem event={item} key={item.id} />
          ))}
        </ScrollView>
      </SafeAreaView>
      <UniversalPlus bottomMargin={{ marginBottom: hp(10) }} />
    </Modal>
  );
}

export default ActivityModal;
