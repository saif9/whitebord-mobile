import { StyleSheet } from 'react-native';
import { font } from '../../../../../../constants';

export default StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 20,
  },
  dismissText: { color: '#6574FF' },
  disabledDismissText: { color: 'white' },
  modal: {
    marginHorizontal: 0,
    marginBottom: 0,
    marginTop: 25,
    flex: 1,
    backgroundColor: 'white',
  },
  modalView: {
    flex: 1,
    alignSelf: 'center',
    backgroundColor: 'white',
    width: '100%',
    borderRadius: 20,
    borderStyle: 'solid',
    borderColor: '#DEDDFF',
    borderWidth: 0,
  },
  modalContent: {
    flex: 1,
    marginHorizontal: 10,
  },
  activityBoxText: {
    fontFamily: font.semibold,
    fontSize: 18,
  },
});
