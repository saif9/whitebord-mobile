import { StyleSheet } from 'react-native';
import { font } from '../../../../../../constants';

const styles = StyleSheet.create({
  eventContainer: {
    borderRadius: 3,
    borderWidth: 1
  },
  name: {
    fontFamily: font.regular,
    textAlign: 'left',
    flex: 1,
    flexWrap: 'wrap',
    fontSize: 16,
  },
  eventBody: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
  },
  contentContainer: {
    flexDirection: 'row',
    flex: 1,
    paddingVertical: 5,
    backgroundColor: 'white'
  },
  checkboxContainer: {
    height: 22.5,
    width: 22.5,
    paddingHorizontal: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  checkbox: {
    width: 18.5,
    height: 18.5
  }
});

export default styles;
