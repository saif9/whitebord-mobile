import React from 'react';
import { View, Text } from 'react-native';
import Animated, {
  useSharedValue,
  useAnimatedGestureHandler,
  useAnimatedStyle,
  withTiming,
  Easing,
  runOnJS,
  withSpring,
} from 'react-native-reanimated';
import { State, LongPressGestureHandler } from 'react-native-gesture-handler';

import { useDispatch } from 'react-redux';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { setModal, toggleModal } from '../../../../../state/actions/modal';
import styles from './styles';
import { removeHTMLTags, hp } from '../../../../../utils';
import AdaptableCheckBox from '../../../../../components/AdaptableCheckBox';
import { upsertWhiteboardItem } from '../../../../../state/slices/whiteboardItems/whiteboardItemsSlice';
import { upsertException } from '../../../../../state/slices/exceptions/exceptionsSlice';

const ACTIVITY_BOX_TOP = hp(70);

const ActivityItem = ({ event, setIsMovingItem, onUpdateItem }) => {
  const dispatch = useDispatch();

  // Animation values
  const isGestureActive = useSharedValue(false);
  const translateX = useSharedValue(0);
  const translateY = useSharedValue(0);
  const startX = useSharedValue(0);
  const startY = useSharedValue(0);

  const updateEvent = async (completed) => {
    if (event.isRecurrenceItem) {
      dispatch(upsertException({ updates: { id: event.id, completed } }));
    } else {
      dispatch(upsertWhiteboardItem({ updates: { id: event.id, completed } }));
    }
    ReactNativeHapticFeedback.trigger('selection');
  };

  /**
   * Delay to allow smooth return to original position
   * @param {boolean} bool applied to setIsMovingItem()
   * @param {number} delay (ms)
   */
  const delayedSetIsMovingItem = (bool, delay) => {
    if (setIsMovingItem === undefined) return;

    setTimeout(() => {
      isGestureActive.value = false;
    }, delay / 2);
    setTimeout(() => setIsMovingItem(bool), delay);
  };

  const onGestureEvent = useAnimatedGestureHandler({
    /**
     * Update animation values based on gesture data
     */
    onActive: ({ absoluteX, absoluteY }) => {
      if (setIsMovingItem === undefined) return;

      if (!isGestureActive.value) {
        startX.value = absoluteX;
        startY.value = absoluteY;
        runOnJS(setIsMovingItem)(true);
        isGestureActive.value = true;
      }
      translateX.value = absoluteX - startX.value;
      translateY.value = absoluteY - startY.value;
    },
    /**
     * Determine if event was placed in calendar or should return to activity box
     * Call appropriate method in JS thread for each case
     */
    onEnd: ({ absoluteY }) => {
      if (setIsMovingItem === undefined) return;

      if (isGestureActive.value && absoluteY < ACTIVITY_BOX_TOP) {
        runOnJS(onUpdateItem)(event.id, absoluteY);
      } else {
        translateX.value = withTiming(0, {
          duration: 350,
          easing: Easing.inOut(Easing.ease),
        });
        translateY.value = withTiming(0, {
          duration: 350,
          easing: Easing.inOut(Easing.ease),
        });
      }
      runOnJS(delayedSetIsMovingItem)(false, 350);
    },
  });

  /**
   * Translate to follow drag
   * Rest to give animated floating effect to component.
   */
  const animatedStyle = useAnimatedStyle(() => ({
    zIndex: isGestureActive.value ? 5 : 1,
    transform: [
      { translateX: translateX.value },
      { translateY: translateY.value },
      { scale: withSpring(isGestureActive.value ? 1.025 : 1) },
    ],
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: withSpring(isGestureActive.value ? 0.35 : 0),
    shadowRadius: 10,
    elevation: 1,
  }));

  const handleStateChange = ({ nativeEvent }) => {
    if (nativeEvent.state === State.ACTIVE) ReactNativeHapticFeedback.trigger('impactHeavy');
    if (nativeEvent.state === State.END) ReactNativeHapticFeedback.trigger('impactLight');
  };

  const onTextPress = () => {
    const item = event.itemToEdit ? event.itemToEdit : event;
    toggleModal(true, dispatch);
    setModal({ ...item, mode: 'edit' }, dispatch);
  };

  return (
    <LongPressGestureHandler
      onGestureEvent={onGestureEvent}
      onHandlerStateChange={handleStateChange}
    >
      <Animated.View style={[styles.contentContainer, animatedStyle]}>
        <View style={styles.checkboxContainer}>
          <AdaptableCheckBox
            type={event.isRecurrenceItem ? event.type : undefined}
            value={event.completed}
            onValueChange={updateEvent}
          />
        </View>
        <Text
          style={[
            styles.name,
            { textDecorationLine: event.completed ? 'line-through' : 'none' },
          ]}
          onPress={onTextPress}
        >
          {removeHTMLTags(event.name)}
        </Text>
      </Animated.View>
    </LongPressGestureHandler>
  );
};

export default ActivityItem;
