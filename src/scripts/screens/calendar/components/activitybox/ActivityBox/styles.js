import { StyleSheet } from 'react-native';
import { font } from '../../../../../../constants';

const styles = StyleSheet.create({
  activityBoxWrapper: {
    backgroundColor: 'white',
    shadowOffset: { height: -2 },
    shadowRadius: 10,
    shadowOpacity: 0.25,
    paddingVertical: 5,
  },
  activityBoxButton: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 15,
  },
  chevron: {
    fontSize: 15,
    color: '#A0A4A8',
  },
  chevronContainer: { flexDirection: 'row', alignItems: 'center' },
  numberOfTasks: {
    fontFamily: font.medium,
    fontSize: 16,
    color: '#7C7E80',
    paddingLeft: 7.5,
  },
  showAll: {
    fontFamily: font.medium,
    fontSize: 12,
    color: '#7C7E80',
    paddingRight: 7.5,
  },
  topBarContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
  },
});

export default styles;
