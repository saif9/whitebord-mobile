import React, { useState } from 'react';
import {
  View,
  Text,
  Pressable,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Animated, {
  useDerivedValue,
  withTiming,
  useAnimatedStyle,
  Easing,
} from 'react-native-reanimated';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import GestureRecognizer from 'react-native-swipe-gestures';
import styles from './styles';
import ActivityItem from '../ActivityItem';
import ActivityModal from '../ActivityModal/component';
import { hp } from '../../../../../utils';

const AnimatedIcon = Animated.createAnimatedComponent(FontAwesome5Icon);
// relative heights for activity box
const minHeight = Math.ceil(hp(14));
const maxHeight = Math.ceil(hp(33));

const ActivityBox = ({
  activityItems,
  onUpdateItem,
  isActivityBoxOpen,
  onHeightUpdated,
}) => {
  const [isMovingItem, setIsMovingItem] = useState(false);
  const [boxHeight, setBoxHeight] = useState(0);
  const [modalOpen, setModalOpen] = useState(false);

  const activityBoxStyle = useAnimatedStyle(() => ({
    height: withTiming(isActivityBoxOpen.value ? boxHeight : 0, {
      duration: 250,
      easing: Easing.inOut(Easing.ease),
    }),
  }));

  const rotation = useDerivedValue(() => {
    return withTiming(isActivityBoxOpen.value ? 0 : 180, {
      duration: 250,
      easing: Easing.inOut(Easing.ease),
    });
  });

  const chevronStyle = useAnimatedStyle(() => ({
    transform: [{ rotateX: `${rotation.value}deg` }],
  }));

  const updateBoxHeight = (height) => {
    setBoxHeight(height);
    onHeightUpdated(height);
  };

  const onActivityBoxPressed = () => {
    const newValue = !isActivityBoxOpen.value;
    isActivityBoxOpen.value = newValue;

    const newHeight = newValue ? minHeight : 0;
    updateBoxHeight(newHeight);
  };

  return (
    <>
      <ActivityModal
        isOpen={modalOpen}
        closeModal={() => setModalOpen(false)}
        activityItems={activityItems}
      />
      <View style={styles.activityBoxWrapper}>
        <Pressable
          style={styles.activityBoxButton}
          onPress={onActivityBoxPressed}
        >
          <View style={styles.topBarContainer}>
            <View style={styles.chevronContainer}>
              <AnimatedIcon
                name="chevron-down"
                color="#4a4957"
                style={[styles.chevron, chevronStyle]}
              />
              <Text style={styles.numberOfTasks}>
                {activityItems.length}
                {' '}
                Task
                {activityItems.length !== 1 && 's'}
              </Text>
            </View>
            {boxHeight !== 0 && (
              <TouchableOpacity onPress={() => setModalOpen(true)}>
                <Text style={styles.showAll}>expand</Text>
              </TouchableOpacity>
            )}
          </View>
        </Pressable>
        <Animated.View style={activityBoxStyle}>
          {boxHeight !== minHeight ? (
            <ScrollView
              style={{ overflow: isMovingItem ? 'visible' : 'hidden' }}
              scrollEventThrottle={1}
              onScroll={(e) => {
                if (e.nativeEvent.contentOffset.y < -40) {
                  updateBoxHeight(minHeight);
                }
              }}
              keyboardDismissMode="on-drag"
            >
              {activityItems.map((item) => (
                <ActivityItem
                  event={item}
                  key={item.id}
                  setIsMovingItem={setIsMovingItem}
                  onUpdateItem={onUpdateItem}
                />
              ))}
            </ScrollView>
          ) : (
            <GestureRecognizer
              onSwipeUp={() => {
                updateBoxHeight(maxHeight);
              }}
              onSwipeDown={() => {
                isActivityBoxOpen.value = !isActivityBoxOpen.value;
                updateBoxHeight(0);
              }}
              config={{
                velocityThreshold: 0,
                directionalOffsetThreshold: 40,
              }}
              style={{ overflow: isMovingItem ? 'visible' : 'scroll', flex: 1 }}
              keyboardDismissMode="on-drag"
            >
              <View>
                {activityItems.map((item) => (
                  <ActivityItem
                    event={item}
                    key={item.id}
                    setIsMovingItem={setIsMovingItem}
                    onUpdateItem={onUpdateItem}
                  />
                ))}
              </View>
            </GestureRecognizer>
          )}
        </Animated.View>
      </View>
    </>
  );
};

export default ActivityBox;
