import React, { useCallback, useState } from 'react';
import {
  Text, Pressable, View
} from 'react-native';
import { useDispatch } from 'react-redux';
import moment from 'moment';
import auth from '@react-native-firebase/auth';
import SharedIcon from '../../../../../assets/icons/shared.svg';
import styles from './styles';
import RightSwipeable from '../../../../components/RightSwipeable';
import { deleteProject } from '../../../../state/slices/projects/projectsSlice';
import ProjectEditModal from '../../../../components/modals/ProjectEditModal';
import { wp } from '../../../../utils';

const Project = ({
  item, onDrag, sectionId, navigation, isActive
}) => {
  const {
    id, name, roles, deadline
  } = item;
  const dispatch = useDispatch();
  const { uid } = auth().currentUser;
  const [editProject, setEditProject] = useState(false);
  const isSharedProject = Object.keys(roles || []).length > 1;

  const projectDeadline = deadline
    ? moment(deadline).format('MMM D') : '';

  const isOwner = roles[uid] === 'owner';

  const swipeToDelete = useCallback(() => {
    dispatch(deleteProject({ updates: { id } }));
  }, [id]);

  const gotoTaskList = useCallback(() => {
    navigation.navigate('TaskList', { projectId: id, sectionId });
  }, [sectionId]);

  const swipeoutBtns = [
    {
      text: 'edit',
      color: 'white',
      backgroundColor: '#626DFF',
      onPress: () => setEditProject(true)
    },
    {
      text: isOwner ? 'delete' : 'remove',
      color: 'white',
      backgroundColor: '#FA8072',
      onPress: swipeToDelete
    }
  ];
  return (
    <RightSwipeable swipeoutBtns={swipeoutBtns} padding={wp(3)}>
      {editProject && (
      <ProjectEditModal
        projectId={id}
        sectionId={sectionId}
        editProject={editProject}
        setEditProject={setEditProject}
      />
      )}
      <View style={styles.separatorStyle} />
      <Pressable
        style={[styles.projectContainer, { backgroundColor: isActive ? 'yellow' : '#fff' }]}
        onPress={gotoTaskList}
        onLongPress={onDrag}
      >
        <View style={styles.projectNameContainer}>
          {isSharedProject ? <SharedIcon /> : <View width={24} />}
          <Text style={styles.projectText}>{name}</Text>
        </View>
        <Text style={styles.deadline}>
          {projectDeadline}
        </Text>
      </Pressable>
    </RightSwipeable>
  );
};

export default Project;
