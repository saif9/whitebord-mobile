import { StyleSheet } from 'react-native';
import { font } from '../../../../../constants/index';
import { wp } from '../../../../utils';

export default StyleSheet.create({
  projectText: {
    fontFamily: font.medium,
    fontSize: 16,
    color: '#434279',
    paddingHorizontal: wp(2),
  },
  projectContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    paddingHorizontal: wp(5),
    paddingVertical: wp(3.5)
  },
  separatorStyle: {
    borderTopColor: '#E5E5EA',
    borderTopWidth: 0.5,
    marginLeft: wp(10),
  },
  projectNameContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  deadline: {
    fontFamily: font.medium,
    color: '#6574FF',
    fontSize: 11,
  },
});
