import React, { useCallback, useEffect, useState } from 'react';
import {
  Text, Pressable, View, TextInput
} from 'react-native';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import { useDispatch } from 'react-redux';
import SectionIcon from '../../../../../assets/icons/section.svg';
import Plus from '../../../../../assets/icons/section_plus.svg';
import { getSharedSectionId, wp } from '../../../../utils';
import styles from './styles';
import RightSwipeable from '../../../../components/RightSwipeable';
import { deleteSection, upsertSection } from '../../../../state/slices/sections/sectionsSlice';
import ProjectEditModal from '../../../../components/modals/ProjectEditModal';

const Section = ({
  item, onDrag, toggleCollapse, collapseState, isActive
}) => {
  const { id, name, projects } = item;
  const dispatch = useDispatch();
  const [editMode, setEditMode] = useState(false);
  const [createProjectModal, setCreateProjectModal] = useState(false);
  const [sectionName, setSectionName] = useState('');
  useEffect(() => {
    setSectionName(name);
  }, [name]);
  const isSharedSection = id === getSharedSectionId();
  if (isSharedSection && projects.length === 0) return <View />;
  const swipeToDelete = useCallback(() => {
    dispatch(deleteSection({ updates: { id } }));
  });
  const updateSectionName = useCallback(() => {
    dispatch(upsertSection({ updates: { id, name: sectionName } }));
    setEditMode(false);
  }, [sectionName]);
  const swipeoutBtns = isSharedSection ? [] : [
    {
      text: 'edit',
      color: 'white',
      backgroundColor: '#626DFF',
      onPress: () => setEditMode(true)
    },
    {
      text: 'delete',
      color: 'white',
      backgroundColor: '#FA8072',
      onPress: swipeToDelete
    },
  ];
  return (
    <RightSwipeable swipeoutBtns={swipeoutBtns} padding={wp(3)}>
      <ProjectEditModal
        editProject={createProjectModal}
        setEditProject={setCreateProjectModal}
        sectionId={id}
        projectId={null}
      />
      <Pressable
        style={[styles.sectionContainer, { backgroundColor: isActive ? 'yellow' : '#fff' }]}
        onPress={() => toggleCollapse(id)}
        onLongPress={onDrag}
      >
        <View style={styles.sectionNameContainer}>
          <SectionIcon />
          {
            editMode ? (
              <View>
                <TextInput
                  style={styles.sectionText}
                  onChangeText={setSectionName}
                  value={sectionName}
                  onEndEditing={updateSectionName}
                  onSubmitEditing={updateSectionName}
                  selectTextOnFocus
                  autoFocus
                  multiline
                />
              </View>
            ) : <Text style={styles.sectionText}>{sectionName}</Text>
          }
          <FontAwesome5Icon
            size={wp(3)}
            color="#434279"
            name={collapseState || isActive ? 'chevron-right' : 'chevron-down'}
          />
        </View>
        {collapseState || isSharedSection ? <View /> : (
          <Pressable onPress={() => setCreateProjectModal(true)}
            hitSlop={{ // Increases the touch aread without resizing the views
              left: 10, top: 10, right: 10, bottom: 10
            }}
          >
            <Plus />
          </Pressable>
        )}
      </Pressable>
    </RightSwipeable>
  );
};

export default Section;
