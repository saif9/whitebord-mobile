import { StyleSheet } from 'react-native';
import { font } from '../../../../../constants/index';
import { wp } from '../../../../utils';

export default StyleSheet.create({
  sectionText: {
    fontFamily: font.regular,
    fontSize: 16,
    color: '#434279',
    paddingHorizontal: wp(2),
  },
  sectionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    borderTopColor: '#E5E5EA',
    borderTopWidth: 0.5,
    paddingHorizontal: wp(5),
    paddingVertical: wp(5)
  },
  sectionNameContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
});
