import React, {
  useState, useRef, useCallback, useEffect
} from 'react';
import {
  Text, TextInput, TouchableWithoutFeedback, View, Keyboard, KeyboardAvoidingView
} from 'react-native';
import DraggableFlatList from 'react-native-draggable-flatlist';
import { useSelector, useDispatch } from 'react-redux';
import isEqual from 'lodash.isequal';
import styles from './styles';
import EditModal from '../../components/modals/EditModal';
import UniversalPlus from '../../components/UniversalPlus';
import NewSection from '../../../assets/icons/new_section.svg';
import InboxButton from '../../components/InboxButton';
import { memoWorkspaceList } from '../../state/slices/sections/selectors';
import { createSection, manageItemIdInSection, upsertSection } from '../../state/slices/sections/sectionsSlice';
import Section from './components/Section';
import Project from './components/Project';
import { upsertUser } from '../../state/slices/user/userSlice';
import { getSharedSectionId } from '../../utils';

let sectionId = '';

const WorkspaceScreen = ({ navigation }) => {
  const [text, setText] = useState('');
  const [sortedIds, setSortedIds] = useState([]);
  const sortedState = useRef(false); // useRef won't wait for rerender and we will have immediate updated result
  const dispatch = useDispatch();
  const newSectionInputRef = useRef();
  const [dragItem, setDragItem] = useState(undefined);
  const [collapseIds, setCollapseIds] = useState({});
  // sortedIds is used to remove a small delay on firestore side.
  // workingList is our main list containing all sections, collapsed sections and projects details.
  const workingList = useSelector((state) => memoWorkspaceList(state, collapseIds));

  useEffect(() => { // This hook is waiting for sorted list to get updated in firestore before and then it will allow changes from firestore.
    // It is just used to reduce a small delay when list is sorted which was causing items to Jump before settle to new state.
    if (sortedState.current) {
      const prevOrderState = sortedIds.map((item) => item.id);
      const newOrderState = workingList.map((item) => item.id);
      if (isEqual(prevOrderState, newOrderState)) {
        setSortedIds(workingList);
        sortedState.current = false;
      }
    } else { setSortedIds(workingList); }
  }, [workingList]);

  const generateSection = () => {
    newSectionInputRef.current.clear();
    createSection({ name: text }, dispatch);
    setText('');
  };

  const collapseAll = () => {
    const colObj = {};
    workingList.forEach((item) => {
      if (isSection(item)) colObj[item.id] = true;
    });
    setCollapseIds(colObj);
  };

  const onDragBegin = (index) => {
    const item = workingList[index];
    setDragItem(item);
  };

  const onDragEnd = ({ data }) => {
    if (isSection(dragItem)) { // sort sections based on new postions
      const ids = data.filter((item) => isSection(item)).map((item) => item.id);
      dispatch(upsertUser({ updates: { sections: ids } }));
    } else { // reassign project to different position
      let currentSection;
      let toBreak = false;
      let projects = [];
      if (data[0]?.id === dragItem.id) return; // projects cannot be assigned to row zero so avoid it.
      data.every((item) => { // calculating the parent section where the dragItem is dropped.
        if (isSection(item)) {
          if (toBreak) return false;
          currentSection = item;
          projects = [];
        } else {
          projects = [...projects, item.id];
          if (dragItem.id === item.id) toBreak = true;
        }
        return true;
      });
      if (currentSection) {
        if (!currentSection.projects?.includes(dragItem.id)) {
          const fromSection = data.find((item) => item?.projects && item.projects.includes(dragItem.id));
          const secId = fromSection?.id || getSharedSectionId();
          dispatch(manageItemIdInSection({ updates: { sectionId: secId, projectId: dragItem.id, isDeleted: true } }));
        }
        dispatch(upsertSection({ updates: { id: currentSection.id, projects } }));
      }
    }
    setDragItem(undefined);
    setSortedIds(data);
    sortedState.current = true;
  };

  const toggleCollapse = (id) => {
    setCollapseIds({ ...collapseIds, [id]: !collapseIds[id] });
  };

  const isSection = useCallback((item) => {
    return !!item.projects;
  }, []);

  const renderItem = ({ item, drag, isActive }) => {
    if (isSection(item)) {
      sectionId = item.id;
      return (
        <Section item={item}
          onDrag={() => {
            collapseAll();
            setTimeout(drag, 100);
          }}
          isActive={isActive}
          collapseState={collapseIds[item.id]}
          toggleCollapse={toggleCollapse}
        />
      );
    } else {
      return (
        <Project item={item}
          onDrag={() => {
            setCollapseIds({});
            setTimeout(drag, 100);
          }}
          isActive={isActive}
          sectionId={sectionId}
          navigation={navigation}
        />
      );
    }
  };

  const renderFooter = () => (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <View style={styles.addSection}>
        <NewSection marginLeft={20} marginRight={10} />
        <TextInput
          placeholder="Add Section"
          placeholderTextColor="#6574FF"
          onChangeText={setText}
          onSubmitEditing={generateSection}
          style={styles.addSectionText}
          ref={newSectionInputRef}
        />
      </View>
    </TouchableWithoutFeedback>
  );
  return (
    <>
      <EditModal />
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding"
        keyboardVerticalOffset={80}
      >
        <View style={styles.header}>
          <Text style={styles.headerText}>Workspace</Text>
          <InboxButton navigation={navigation} />
        </View>
        <DraggableFlatList
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          listKey="item-list"
          data={sortedIds}
          renderItem={renderItem}
          keyExtractor={(item, _index) => `draggable-item-${item.id}`}
          onDragBegin={onDragBegin}
          onDragEnd={onDragEnd}
          ListFooterComponent={renderFooter()}
        />
      </KeyboardAvoidingView>
      <UniversalPlus />

    </>
  );
};

export default WorkspaceScreen;
