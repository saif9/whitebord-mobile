import { StyleSheet } from 'react-native';
import { font } from '../../../constants';
import { wp } from '../../utils';

export default StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: wp(5),
    marginBottom: 10,
  },
  container: {
    color: 'black',
    flex: 1,
    backgroundColor: 'white',
  },
  addSection: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    borderTopColor: '#E5E5EA',
    borderTopWidth: 0.5,
    marginVertical: 1,
  },
  arrow: {
    marginRight: 10,
  },
  addSectionText: {
    fontFamily: font.regular,
    fontSize: 16,
    width: wp(40),
    color: '#434279',
  },
  headerText: {
    fontFamily: font.semibold,
    color: '#434279',
    fontSize: 20,
  },
});
