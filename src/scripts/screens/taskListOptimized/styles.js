import { StyleSheet } from 'react-native';
import { font } from '../../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    marginVertical: 10,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  projectName: {
    flexDirection: 'row',
    marginLeft: 10,
    alignItems: 'center',
  },
  projectText: {
    marginLeft: 10,
    textAlign: 'center',
    fontFamily: font.bold,
    fontStyle: 'italic',
    fontSize: 20,
    color: '#434279'
  },
  arrowBack: {
    padding: 10,
  },
  editButton: {
    color: '#5E63FF',
    marginRight: 20,
    marginLeft: 10,
  },
  addTaskText: {
    marginLeft: 10,
  },
  addTask: {
    marginLeft: 15,
    borderBottomColor: '#E5E5EA',
    borderBottomWidth: 1,
  },
  tbBtn: {
    fontFamily: font.bold,
    fontSize: 18,
  },
  dragPlaceholder: {
    height: 40,
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  outOfBoundary: {
    color: '#fff',
    fontFamily: font.bold,
    fontSize: 16
  }
});
