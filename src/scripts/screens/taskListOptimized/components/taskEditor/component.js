import React, {
  forwardRef, useEffect, useRef, useState
} from 'react';
import {
  View, Text, Pressable, TextInput, TouchableOpacity
} from 'react-native';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import AdaptiveCheckBox from '../../../../components/AdaptableCheckBox';
import styles from './styles';
import {
  setModal,
  toggleModal,
  toggleEditMode,
} from '../../../../state/actions/modal';
import RightSwipeable from '../../../../components/RightSwipeable';
import SharedIcon from '../../../../../assets/icons/shared.svg';
import { canTaskBeDeleted, removeHTMLTags, wp } from '../../../../utils';
import { whiteboardItemsSelectors } from '../../../../state/slices/whiteboardItems/whiteboardItemsSlice';
import RenderHtml from '../../../../components/renderHtml';
// import { memoNextRecurringItem } from '../../../../state/slices/recurrentItems/selectors';

const TaskEditor = (
  {
    id,
    onFocus,
    addTask,
    updateTask,
    deleteTask,
    onDrag,
    isActive,
    store,
    projectExtras,
    currentFocus,
    toggleCollapse,
    shuffleMode
  },
  ref,
) => {
  if (id === 'placeholder') {
    return <View style={[styles.taskContent]} />;
  }

  let item = useSelector((state) => whiteboardItemsSelectors.selectById(state, id));
  let isTemp = false;
  if (store.current[id]) {
    item = store.current[id];
    isTemp = true;
  }
  // const state = useStore().getState();
  const dispatch = useDispatch();
  const [whiteboardItem, setWhiteboardItem] = useState();
  const [localCompleted, setLocalCompleted] = useState();
  const [isSharedProject, setIsSharedProject] = useState();
  const [isSharedItem, setIsSharedItem] = useState();
  const [checkBoxType, setCheckBoxType] = useState();
  const [content, setContent] = useState('');
  const appliedStyle = useRef({});

  useEffect(() => {
    const thisItem = item;
    if (thisItem) {
      setContent(thisItem?.name || '');
      setCheckBoxType(thisItem.recurrencePattern && thisItem.type);
      setLocalCompleted(thisItem.completed);
      setIsSharedItem(thisItem.assignedTo?.length > 0);
      setIsSharedProject(canTaskBeDeleted(thisItem));
      setWhiteboardItem(thisItem);
    }
  }, [item]);

  const changeStyle = (stylize) => {
    appliedStyle.current = stylize;
    setStylizedContent(stylize, content);
  };

  const setStylizedContent = (stylize, text) => {
    let style = 'format';
    if (stylize.bold) {
      style += ' bold';
    }
    if (stylize.italic) {
      style += ' italic';
    }
    if (stylize.heading) {
      style += ` ${stylize.heading}`;
    }
    const contentText = removeHTMLTags(text);
    const newContent = `<span class="${style.trim()}">${contentText}</span>`;
    setContent(newContent);
    if (isTemp) {
      store.current[id] = { ...item, name: newContent };
    }
  };

  const swipeToEdit = () => {
    ref.current[id].ref?.blur();
    toggleModal(true, dispatch);
    let startDate = moment();
    let endDate = moment();
    if (scheduledDate) {
      startDate = moment(scheduledDate);
      endDate = moment(scheduledDate).add(duration, 'hours');
    }
    setModal(
      {
        mode: 'edit',
        type: 'task',
        startDate,
        endDate,
        ...whiteboardItem,
        name: content,
      },
      dispatch,
    );
    toggleEditMode(true, dispatch);
  };

  if (!whiteboardItem) return null;
  if (projectExtras[id]?.isCollapsed) return null;
  const {
    scheduledDate,
    deadline,
    duration,
    completed,
  } = whiteboardItem;
  let { parentTask, subtype } = whiteboardItem;
  parentTask = projectExtras[id]?.parentTask || parentTask;
  subtype = projectExtras[id]?.subtype || subtype;
  const { num, collapseState, isParentInList } = projectExtras[id] || { collapseState: -1 };
  const collapseBtn = collapseState !== -1 && !parentTask;

  const getDeadline = () => {
    if (whiteboardItem.recurrencePattern) {
      // const task = memoNextRecurringItem()(state, id); // moved to state to make it realtime
      return null;
    } else {
      return deadline;
    }
  };
  const deadlineTime = getDeadline();

  let swipeoutBtns = [
    {
      text: 'Edit',
      color: 'white',
      backgroundColor: '#626DFF',
      onPress: swipeToEdit
    },
    {
      text: isSharedProject ? 'Delete' : 'Remove',
      color: 'white',
      backgroundColor: '#FA8072',
      onPress: () => deleteTask(id, false, isSharedProject)
    },
  ];
  const checkStyle = (!subtype || subtype === 'check');
  swipeoutBtns = checkStyle
    ? swipeoutBtns
    : swipeoutBtns.filter((b) => b.text !== 'Edit'); // removing edit button from other type of tasks except check

  const renderCollapseBtn = collapseBtn ? (
    <TouchableOpacity onPress={() => toggleCollapse(id)} style={styles.collapseBtn} hitSlop={{ top: 4, right: 4, bottom: 4 }}>
      <FontAwesome5Icon
        size={wp(3)}
        name={collapseState || isActive ? 'chevron-right' : 'chevron-down'}
      />
    </TouchableOpacity>
  ) : (
    <View style={styles.collapseBtn} />
  );

  const renderSubType = () => {
    switch (subtype) {
      case 'text':
        return (
          // `note` type
          <View style={styles.empty} />
        );
      case 'bullet':
        return (
          // `bullet` type
          <Text style={styles.bullet}>{'\u2B24'}</Text>
        );
      case 'number':
        return (
          // `number` type
          <Text style={styles.number}>
            {`${num && num > 0 ? `${num}.` : ''}`}
          </Text>
        );
      default:
        return (
          // `check` type
          <View style={{ marginVertical: 4 }}>
            <AdaptiveCheckBox
              type={checkBoxType}
              onValueChange={(isComplete) => {
                updateTask(content, subtype, isComplete, id);
                ReactNativeHapticFeedback.trigger('selection');
              }}
              value={localCompleted}
            />
          </View>
        );
    }
  };

  const renderTextInput = () => {
    return (
      <TextInput
        onLayout={() => { setTimeout(() => ref.current[currentFocus.current]?.ref?.focus(), 0); }}
        style={styles.editor}
        onFocus={() => { onFocus(id); }}
        selectTextOnFocus={false}
        autoFocus={false}
        scrollEnabled={false}
        multiline
        showSoftInputOnFocus={false}
        blurOnSubmit={false}
        onSubmitEditing={() => addTask(subtype)}
        onBlur={() => setTimeout(() => updateTask(content, subtype, completed, id, true), 500)}
        onChangeText={(text) => {
          setStylizedContent(appliedStyle.current, text);
        }}
        onKeyPress={(e) => {
          const plainText = removeHTMLTags(content);
          if (e.nativeEvent.key === 'Backspace' && plainText.trim().length === 0) {
            deleteTask(id, true);
          }
        }}
        ref={(r) => {
          ref.current[id] = {
            ref: r, appliedStyle, changeStyle
          };
        }}
      >
        <RenderHtml html={content} appliedStyle={appliedStyle} />
      </TextInput>
    );
  };

  const SelectedView = shuffleMode ? Pressable : View;
  return (
    <View
      style={[
        styles.taskContainer,
        {
          paddingVertical: checkStyle ? 3 : 0,
          marginLeft: isParentInList ? wp(3.6) : 0,
          width: isParentInList ? '96.4%' : '100%',
        },
      ]}
    >
      <RightSwipeable swipeoutBtns={swipeoutBtns}>
        <SelectedView
          style={{
            ...styles.taskContent,
            backgroundColor: isActive ? 'yellow' : 'white',
          }}
          onLongPress={onDrag}
        >
          {renderCollapseBtn}
          {renderSubType()}
          <View style={{ flex: 1 }} pointerEvents={shuffleMode ? 'none' : 'auto'}>
            {renderTextInput()}
          </View>
          <View style={styles.detailsContainer}>
            {isSharedItem && (<View style={styles.sharedIcon}><SharedIcon /></View>)}
            {deadlineTime && (
            <View style={styles.deadlineDisplay}>
              <Text style={[styles.deadline, {
                color: moment().startOf('D').isAfter(deadlineTime)
                  ? 'red' : '#6574FF'
              }]}
              >
                {moment(deadlineTime).format('MMM D')}
              </Text>
            </View>
            )}
          </View>
        </SelectedView>
      </RightSwipeable>
    </View>
  );
};

export default forwardRef(TaskEditor);
