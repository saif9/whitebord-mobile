import { StyleSheet } from 'react-native';
import { font } from '../../../../../constants';
import { hp, wp } from '../../../../utils';

export default StyleSheet.create({
  taskContainer: {
    borderBottomColor: '#E5E5EA',
    borderBottomWidth: 0,
    width: '100%',
  },
  taskContent: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    minHeight: hp(4),
    width: '100%',
  },
  checkbox: {
    width: 25,
    height: 15,
    marginLeft: 0,
  },
  bullet: {
    width: 25,
    fontFamily: font.medium,
    fontSize: 9,
    marginLeft: 9,
    marginVertical: 6,
  },
  number: {
    width: 25,
    fontFamily: font.extrabold,
    fontSize: 14,
    marginLeft: 9,
    marginVertical: 5,
  },
  empty: {
    marginLeft: 4,
  },
  taskEdit: {
    marginLeft: 10,
    fontFamily: font.regular,
    fontSize: 16,
    color: '#434279',
    width: '90%',
    paddingTop: 0,
  },
  dragContainer: {
    minWidth: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'yellow'
  },
  deadline: {
    fontFamily: font.extrabold,
    color: '#6574FF',
    fontSize: 11,
    paddingHorizontal: 10,
  },
  collapseBtn: {
    height: hp(3.6),
    paddingLeft: 5,
    marginHorizontal: 4,
    alignItems: 'center',
    justifyContent: 'center',
    // minHeight: wp(3),
    minWidth: wp(4),
  },
  _input: {
    position: 'absolute',
    width: 1,
    height: 1,
    zIndex: -999,
    bottom: -999,
    left: -999,
  },
  editor: {
    flex: 1,
    fontFamily: font.regular,
    minHeight: hp(4),
    paddingTop: 4,
  },
  detailsContainer: {
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
  },
  sharedIcon: {
    width: wp(8),
    marginHorizontal: 3,
  },
  deadlineDisplay: {
    width: wp(15),
  }
});
