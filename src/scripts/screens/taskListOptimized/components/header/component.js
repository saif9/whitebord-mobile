import React, { useState } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import { useKeyboard } from '@react-native-community/hooks';
import ArrowBack from '../../../../../assets/icons/chevron_left.svg';
import ProjectEditModal from '../../../../components/modals/ProjectEditModal';
import styles from './styles';
import Shuffle from '../../../../../assets/icons/shuffle.svg';

const Header = ({
  navigation, route, projectName, blurCurrent, shuffleMode, setShuffleMode
}) => {
  const { projectId, sectionId } = route.params;
  const keyboard = useKeyboard();
  const [editProject, setEditProject] = useState(false);

  const shuffleStyle = shuffleMode
    ? styles.shuffleIconContainerActive
    : styles.shuffleIconContainerInActive;

  return (
    <>
      <ProjectEditModal
        projectId={projectId}
        sectionId={sectionId}
        editProject={editProject}
        setEditProject={setEditProject}
      />
      <View style={styles.header}>
        <View style={styles.projectName}>
          <TouchableOpacity
            style={styles.arrowBack}
            onPress={navigation.goBack}
          >
            <ArrowBack />
          </TouchableOpacity>
          <Text style={styles.projectText}>{projectName}</Text>
        </View>
        <View style={styles.actionContainer}>
          <TouchableOpacity
            onPress={() => {
              Keyboard.dismiss(); blurCurrent();
              setShuffleMode(!shuffleMode);
            }}
            style={shuffleStyle}
          >
            <Shuffle height={20} width={20} style={shuffleStyle} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={keyboard.keyboardShown
              ? () => { Keyboard.dismiss(); blurCurrent(); }
              : () => setEditProject(true)}
          >
            <Text style={styles.editButton}>{keyboard.keyboardShown ? ' Done ' : ' Edit '}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

export default Header;
