import { StyleSheet } from 'react-native';
import { font } from '../../../../../constants';

export default StyleSheet.create({

  header: {
    marginVertical: 10,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  projectName: {
    flexDirection: 'row',
    marginLeft: 10,
    alignItems: 'center',
  },
  projectText: {
    marginLeft: 10,
    textAlign: 'center',
    fontFamily: font.bold,
    fontStyle: 'italic',
    fontSize: 20,
    color: '#434279'
  },
  arrowBack: {
    padding: 10,
  },
  editButton: {
    color: '#5E63FF',
    marginRight: 20,
    marginLeft: 10,
  },
  shuffleIconContainerActive: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 30 / 2,
    backgroundColor: '#ABA9FF',
  },
  shuffleIconContainerInActive: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 30 / 2,
    backgroundColor: '#F8F9FF',
  },
  shuffleIconActive: {
    color: '#fff',
  },
  shuffleIconInActive: {
    color: '#ABA9FF',
  },
  actionContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }
});
