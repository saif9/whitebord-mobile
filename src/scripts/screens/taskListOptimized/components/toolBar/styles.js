import { StyleSheet } from 'react-native';
import { hp, wp } from '../../../../utils';

export default StyleSheet.create({
  barStyle: {
    width: wp(100),
    height: hp(5),
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 5,
    backgroundColor: '#F5F5F5'
  },
});
