import React, {
  forwardRef, useImperativeHandle, useState
} from 'react';
import { TouchableOpacity, View } from 'react-native';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import { useKeyboard } from '@react-native-community/hooks';
import BulletsIcon from '../../../../../assets/icons/notes_bullets.svg';
import NumbersIcon from '../../../../../assets/icons/notes_numbers.svg';
import ChecksIcon from '../../../../../assets/icons/notes_check.svg';
import HeadingOneIcon from '../../../../../assets/icons/notes_h1.svg';
import HeadingTwoIcon from '../../../../../assets/icons/notes_h2.svg';
import HeadingThreeIcon from '../../../../../assets/icons/notes_h3.svg';
import BoldIcon from '../../../../../assets/icons/notes_bold.svg';
import ItalicIcon from '../../../../../assets/icons/notes_italic.svg';
import styles from './styles';
import { wp } from '../../../../utils';

const CheckList = ({ subtype, onPress }) => (
  <TouchableOpacity onPress={onPress}
    style={{
      padding: 5,
      backgroundColor: subtype === 'check' ? 'yellow' : 'transparent'
    }}
  >
    <ChecksIcon />
  </TouchableOpacity>
);
const BulletList = ({ subtype, onPress }) => (
  <TouchableOpacity onPress={onPress}
    style={{
      padding: 5,
      backgroundColor: subtype === 'bullet' ? 'yellow' : 'transparent'
    }}
  >
    <BulletsIcon />
  </TouchableOpacity>
);
const OrderedList = ({ subtype, onPress }) => (
  <TouchableOpacity onPress={onPress}
    style={{
      padding: 5,
      backgroundColor: subtype === 'number' ? 'yellow' : 'transparent'
    }}
  >
    <NumbersIcon />
  </TouchableOpacity>
);
const HeadingOne = ({ heading, onPress }) => (
  <TouchableOpacity onPress={onPress}
    style={{
      marginTop: 5,
      padding: 3,
      backgroundColor: heading === 'h1' ? 'yellow' : 'transparent'
    }}
  >
    <HeadingOneIcon />
  </TouchableOpacity>
);
const HeadingTwo = ({ heading, onPress }) => (
  <TouchableOpacity onPress={onPress}
    style={{
      marginTop: 3,
      padding: 3,
      backgroundColor: heading === 'h2' ? 'yellow' : 'transparent'
    }}
  >
    <HeadingTwoIcon />
  </TouchableOpacity>
);
const HeadingThree = ({ heading, onPress }) => (
  <TouchableOpacity onPress={onPress}
    style={{
      marginTop: 3,
      padding: 3,
      backgroundColor: heading === 'h3' ? 'yellow' : 'transparent'
    }}
  >
    <HeadingThreeIcon />
  </TouchableOpacity>
);
const Bold = ({ bold, onPress }) => (
  <TouchableOpacity onPress={onPress}
    style={{
      padding: 5,
      backgroundColor: bold ? 'yellow' : 'transparent'
    }}
  >
    <BoldIcon />
  </TouchableOpacity>
);
const Italic = ({ italic, onPress }) => (
  <TouchableOpacity onPress={onPress}
    style={{
      padding: 5,
      backgroundColor: italic ? 'yellow' : 'transparent'
    }}
  >
    <ItalicIcon />
  </TouchableOpacity>
);
const SubtaskMode = ({ taskState, onPress }) => (
  <View>
    <FontAwesome5Icon size={wp(3.5)}
      style={{
        padding: 5,
        color: '#2095F2'
      }}
      name={taskState ? 'chevron-left' : 'chevron-right'}
      onPress={onPress}
    />
  </View>
);

const ToolBar = ({
  subTaskModeRef, toggleTask, subtypeRef, changeSubtype, editors, currentFocus
}, ref) => {
  const keyboard = useKeyboard();
  const [taskState, setTaskState] = useState();
  const [subtype, setSubtype] = useState();
  const [heading, setHeading] = useState(false);
  const [bold, setBold] = useState(false);
  const [italic, setItalic] = useState(false);
  useImperativeHandle(ref, () => ({ // useImperativeHandle fetch the live state of task and update the toolbar UI according to the selected field
    setState: () => {
      setTaskState(subTaskModeRef.current);
    },
    setSubtype: () => {
      setSubtype(subtypeRef.current);
    },
    setAppliedStyle: () => {
      if (editors.current?.[currentFocus.current]?.ref) {
        const { appliedStyle } = editors.current[currentFocus.current];
        if (appliedStyle.current.bold) setBold(true);
        else setBold(false);
        if (appliedStyle.current.italic) setItalic(true);
        else setItalic(false);
        if (appliedStyle.current.heading) setHeading(appliedStyle.current.heading);
        else setHeading(false);
      }
    }
  }));

  const set = (type) => {
    if (subtype === type) {
      setSubtype('text');
      subtypeRef.current = 'text';
    } else {
      setSubtype(type);
      subtypeRef.current = type;
    }
    changeSubtype();
  };

  const setHeader = (selectedHeader) => {
    let header = null;
    if (heading === selectedHeader) {
      setHeading(null);
    } else {
      setHeading(selectedHeader);
      header = selectedHeader;
    }
    if (editors.current?.[currentFocus.current]?.ref) {
      editors.current[currentFocus.current].changeStyle({ bold, italic, heading: header });
    }
  };

  const setBolding = () => {
    if (editors.current?.[currentFocus.current]?.ref) {
      editors.current[currentFocus.current].changeStyle({ bold: !bold, italic, heading });
    }
    setBold(!bold);
  };

  const setItalicise = () => {
    if (editors.current?.[currentFocus.current]?.ref) {
      editors.current[currentFocus.current].changeStyle({ bold, italic: !italic, heading });
    }
    setItalic(!italic);
  };

  return !keyboard.keyboardShown ? null : (
    <View style={styles.barStyle}>
      <CheckList subtype={subtype} onPress={() => set('check')} />
      <BulletList subtype={subtype} onPress={() => set('bullet')} />
      <OrderedList subtype={subtype} onPress={() => set('number')} />
      <View />
      <HeadingOne heading={heading} onPress={() => setHeader('h1')} />
      <HeadingTwo heading={heading} onPress={() => setHeader('h2')} />
      <HeadingThree heading={heading} onPress={() => setHeader('h3')} />
      <View />
      <Bold bold={bold} onPress={() => setBolding()} />
      <Italic italic={italic} onPress={() => setItalicise()} />
      <View />
      <SubtaskMode taskState={taskState}
        onPress={() => {
          subTaskModeRef.current = !taskState;
          setTaskState(!taskState);
          toggleTask();
        }}
      />
    </View>
  );
};

export default forwardRef(ToolBar);
