import React, {
  useState, useRef, useEffect, useMemo, useCallback
} from 'react';
import {
  Text,
  View,
} from 'react-native';
import { useDispatch, useSelector, useStore } from 'react-redux';
import DraggableFlatList from 'react-native-draggable-flatlist';
import { useInteractionManager, useKeyboard } from '@react-native-community/hooks';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { uniq } from 'lodash';
import {
  KeyboardAccessoryView
} from '@flyerhq/react-native-keyboard-accessory-view';
import TaskEditor from './components/taskEditor';
import styles from './styles';
import {
  createNewWhiteboardItem, deleteWhiteboardItem, manageSubItemIdInItem, upsertWhiteboardItem, whiteboardItemsSelectors
} from '../../state/slices/whiteboardItems/whiteboardItemsSlice';
import { memoProjectTasksById } from '../../state/slices/projects/selectors';
import { manageItemIdInProject, projectsSelectors } from '../../state/slices/projects/projectsSlice';
import { hp, useDebouncedEffect } from '../../utils';
import ToolBar from './components/toolBar';
import Header from './components/header';

const placeholder = 'placeholder';
let numb = 0;
let subNumb = 0;
let lastId = ''; // It is just to assign numbering to subtasks, last id is lastParentID for continuous numbering.

// We are using many tricks to optimize this screen. First of all, we are maintaining local state in this component
// that persists for 2 seconds or less before getting refreshed with firestore state. It is to save dispatching and listener
// callback delays along with several other unwanted glitches that we don't want.

const TaskListScreen = ({
  route, navigation
}) => {
  const { projectId } = route.params;
  const dispatch = useDispatch();
  const store = useStore(); // Live redux store which has live information
  const selectTasks = useMemo(memoProjectTasksById, []); // memoized tasks
  const listTasks = useSelector((state) => selectTasks(state, projectId)); // To trigger any update in live firestore

  const [projectTasks, _setProjectTasks] = useState([]); // tasks of this project
  const projectTasksRef = useRef([]); // same thing as "projectTasks" but to avoid rendering delays for state updates
  const projectState = useRef({}); // live project state updating after every 2 seconds
  const whiteboardItems = useRef({}); // live items state updating after every 2 seconds
  const currentFocus = useRef(); // current focus field
  const subtaskModeRef = useRef(1); // To change subtask mode
  const subtypeRef = useRef(); // To change subtype in local state.
  const tempStore = useRef({}); // To store local updates for 2 seconds before accepting the forestore state.
  const toolbarRef = useRef({}); // To update toolbar component and to not render this whole screen for toolbar UI update
  const [projectExtras, setProjectExtras] = useState({}); // It is important to store numberings and other metadata about tasks

  const [shuffleMode, setShuffleMode] = useState(false);

  const editors = useRef({}); // saving editors ids to focus on previous inputs
  const listRef = useRef();
  const deletedIds = useRef([]); // deleted ids to avoid glitches

  const autoFocus = useRef(false);
  const interactionReady = useInteractionManager(); // For smooth navigation transition
  const keyboard = useKeyboard();
  const keyboardRef = useRef(false);

  const setProjectTasks = (list) => {
    projectTasksRef.current = list;
    _setProjectTasks(list);
  };

  useEffect(() => { // keyboardRef to enable/disable click events on TextInput
    keyboardRef.current = keyboard.keyboardShown;
  }, [keyboard]);

  useEffect(() => { // it will execute to add a local task if there are no tasks in the list.
    if (listTasks?.length === 0 && projectTasks.length === 0) {
      autoFocus.current = false;
      currentFocus.current = null;
      addTask('check');
    }
  }, [projectTasks, listTasks]);

  const getItem = (id) => {
    return tempStore.current[id] || whiteboardItems.current[id];
  };
  const getSubtasks = (parentId) => {
    return projectTasksRef.current.filter((id) => {
      const item = getItem(id);
      return item.parentTask === parentId;
    });
  };
  const getTasks = () => {
    return projectTasksRef.current.filter((id) => !getItem(id)?.parentTask);
  };
  const getListTasks = () => {
    return projectState.current?.hideCompleted
      ? projectTasksRef.current.filter((taskId) => getItem(taskId)?.completed === false) : projectTasksRef.current;
  };
  const isSame = (id) => { // Comparing ids to confirm that our local state is in complete agreement with firestore state so that we can remove item from local state.
    if (tempStore.current[id] && whiteboardItems.current[id]) {
      const item1 = tempStore.current[id];
      const item2 = whiteboardItems.current[id];
      // eslint-disable-next-line eqeqeq
      return item1.parentTask == item2.parentTask && item1.subtype === item2.subtype
        && item1.collapsed === item2.collapsed;
    } else if (!tempStore.current[id]) {
      return true;
    } else return false;
  };

  const allProjectTasks = () => { // syncing live firestore state with local state
    projectState.current = projectsSelectors.selectById(store.getState(), projectId);
    whiteboardItems.current = whiteboardItemsSelectors.selectEntities(store.getState());
    let initTasks = [...(listTasks || []).filter((id) => !deletedIds.current.includes(id))];
    Object.keys(tempStore.current).forEach((k) => {
      if (initTasks.includes(k) && isSame(k)) {
        delete tempStore.current[k];
      }
      if (tempStore.current[k]) { // merging local state with firestore/redux state and replace the current local index with current task that is not synced yet
        const ind = projectTasksRef.current.indexOf(k);
        if (ind > -1) {
          initTasks.splice(ind, 0, k);
        }
      }
    });
    initTasks = uniq(initTasks);
    setProjectTasks(initTasks);
  };
  useEffect(allProjectTasks, []); // fetch project tasks for first time without any delay
  useDebouncedEffect(allProjectTasks, [listTasks], 2000); // It will run after the delay of two seconds to batch all updates and avoid unnecessary endering
  useEffect(() => { // Whenever projecttasks list updates, it will assign numbering and other metadata (parents, collapse state, etc) to each task.
    try {
      const bin = {};
      const tasks = getTasks();
      projectTasks.forEach((id) => {
        const whiteboardItem = getItem(id);
        const { parentTask, subtype, collapsed } = whiteboardItem;
        if (parentTask) {
          if (lastId !== parentTask) {
            lastId = parentTask;
            subNumb = 0;
          }
          // eslint-disable-next-line no-sequences
          const num = subtype === 'number' ? ++subNumb : (subNumb = 0, -1);
          const isParentInList = tasks.includes(parentTask);
          let isCollapsed = false;
          if (isParentInList) isCollapsed = getItem(parentTask)?.collapsed;
          bin[id] = {
            id,
            num,
            parentTask,
            subtype,
            isCollapsed,
            isParentInList
          };
        } else {
          const subtasks = getSubtasks(id);
          // eslint-disable-next-line no-sequences
          const num = subtype === 'number' ? ++numb : (numb = 0, -1);
          bin[id] = {
            id,
            subtype,
            num,
            collapseState: subtasks.length === 0 ? -1 : collapsed
          };
        }
      });
      numb = 0;
      subNumb = 0;
      lastId = '';
      setProjectExtras(bin);
    } catch (e) {
      allProjectTasks();
    }
  }, [projectTasks]);

  const collapseAll = () => {
    const colObj = {};
    getTasks().forEach((tID) => { colObj[tID] = true; });
    // setCollapseIds(colObj);
  };

  const [ph, setPh] = useState(false); // placeholder for subtasks dragged outside of their parentTask.
  const [dragID, setDragID] = useState('');

  const renderDragPlaceholder = () => { // placeholder for subtasks dragged outside of their parentTask.
    return (
      <View style={[styles.dragPlaceholder, { backgroundColor: ph ? 'red' : '#fff' }]}>
        <Text style={styles.outOfBoundary}>Cannot Drop Here</Text>
        <Text style={styles.outOfBoundary}>Cannot Drop Here</Text>
      </View>
    );
  };

  // check if subtask is dragged outside of it's parent task or not
  const placeholderIndexChange = (index) => {
    const id = projectTasks[index];
    const indexTask = getItem(id);
    const dragTask = getItem(dragID);
    // one can be null and one can be undefined so === will be false which is not the correct behaviour
    // eslint-disable-next-line eqeqeq
    if (dragTask && indexTask && dragTask.parentTask == indexTask.parentTask) {
      setPh(false);
    } else {
      setPh(true);
    }
  };

  const onDragBegin = (index) => {
    const id = projectTasks[index];
    setDragID(id);
    const task = getItem(id);
    if (!task.parentTask) {
      collapseAll();
    }
  };

  const onDragEnd = ({ data }) => {
    if (!ph) {
      let dat = data;
      const bugIndex = data.indexOf(undefined); // bugIndex is used as a fix when all the tasks collapsed the drag id becomes undefined
      if (bugIndex !== -1) {
        dat = dat.filter((d) => d !== dragID);
        dat.splice(bugIndex, 1, dragID);
      }
      dat = dat.filter((d) => d && d !== placeholder);
      const dragTask = getItem(dragID);
      if (dragTask.parentTask) { // update subtask ordering
        const subtasks = getSubtasks(dragTask.parentTask);
        const fsubtasks = dat.filter((t) => subtasks.includes(t));
        dispatch(manageSubItemIdInItem({ updates: { id: dragTask.parentTask, subtasks: fsubtasks } }));
      } else { // update task ordering
        const subtasks = getSubtasks(dragID);
        dat = dat.filter((d) => !subtasks.includes(d));
        const sortedTasks = getTasks().sort((a, b) => dat.indexOf(a) - dat.indexOf(b));
        const ind = sortedTasks.indexOf(dragID);
        dat.splice(ind + 1, 0, ...subtasks);
        dispatch(manageItemIdInProject({ updates: { projectId, tasks: sortedTasks } }));
      }
      setProjectTasks(dat);
    }
    setDragID('');
  };

  const onFocus = async (id) => {
    try {
      autoFocus.current = false;
      currentFocus.current = id;
      const { parentTask, subtype } = getItem(id);
      subtaskModeRef.current = parentTask;
      subtypeRef.current = subtype;
      toolbarRef.current.setState();
      toolbarRef.current.setSubtype();
      toolbarRef.current.setAppliedStyle();
    } catch (e) {
      console.log(e);
    }
  };

  const changeSubtype = () => {
    const taskId = currentFocus.current;
    const thisItem = getItem(taskId);
    tempStore.current[taskId] = { ...thisItem, subtype: subtypeRef.current };
    setProjectTasks([...projectTasks]);
  };

  const toggleTask = () => {
    const taskId = currentFocus.current;
    if (taskId && getSubtasks(taskId).length === 0) {
      const thisItem = getItem(taskId);
      const { parentTask } = thisItem;
      let index = -1;
      let parent = null;
      let subtasks = [];
      if (parentTask && !subtaskModeRef.current) { // subtaskmode Off
        tempStore.current[taskId] = { ...thisItem, parentTask: parent };
        index = projectTasks.indexOf(parentTask);
        subtasks = getSubtasks(parentTask);
        index += subtasks.length;
      } else if (!parentTask && subtaskModeRef.current) { // it's a parent
        const prevInd = projectTasks.indexOf(taskId) - 1;
        const prevTask = getItem(projectTasks[prevInd]);
        parent = prevTask?.parentTask || prevTask?.id;
        tempStore.current[taskId] = { ...thisItem, parentTask: parent };
        index = projectTasks.indexOf(parent);
        subtasks = getSubtasks(parent);
        index += (subtasks.length - 1);
      }

      if (index > -1) {
        const id = taskId;
        if (whiteboardItems.current[id]) { // subtask toggleMode case for existing task
          dispatch(upsertWhiteboardItem({ updates: { id, parentTask: parent, isManaged: true } }));
          dispatch(manageSubItemIdInItem({ updates: { id: parent || parentTask, subtasks } }));
          dispatch(manageItemIdInProject({ updates: { projectId, tasks: getTasks() } }));
        } else {
          const currState = parent ? getSubtasks(parent) : getTasks();
          tempStore.current[taskId] = { ...thisItem, parentTask: parent, currState };
        }
        const modArr = [...projectTasks.filter((ID) => ID !== taskId)];
        modArr.splice(index + 1, 0, taskId);
        setProjectTasks(modArr);
      }
    }
  };

  const addTask = (subtype) => {
    if (!autoFocus.current) {
      autoFocus.current = true;
      const prevId = currentFocus.current;
      const prevTask = getItem(prevId) || {};
      let index = projectTasksRef.current.indexOf(prevId);
      const task = {
        name: '',
        subtype,
        projectId,
      };
      if (subtaskModeRef.current) { // subtaskmode On
        task.parentTask = prevTask.parentTask || prevId;
        // parent task not avialable in this project so new task won't have any parent
        if (!(getTasks().includes(task.parentTask))) delete task.parentTask;
      } else if (!prevTask.parentTask) { // it's a parent
        const subtasks = getSubtasks(prevId);
        index += subtasks.length;
      } else if (prevTask.parentTask && !subtaskModeRef.current) { // subtaskmode off, but it has a parent, jump outside
        index = projectTasksRef.current.indexOf(prevTask.parentTask);
        const subtasks = getSubtasks(prevTask.parentTask);
        index += subtasks.length;
      }
      const newTask = createNewWhiteboardItem(task);
      currentFocus.current = newTask.id;
      tempStore.current[newTask.id] = { ...newTask }; // This line is important for getSubtasks()
      const modArr = [...projectTasksRef.current];
      modArr.splice(index + 1, 0, newTask.id);
      setProjectTasks(modArr);
      // capture current state
      tempStore.current[newTask.id] = { ...newTask, currState: newTask?.parentTask ? getSubtasks(newTask.parentTask) : getTasks() };
    }
  };

  const addToStore = (newTask, currState) => {
    dispatch(upsertWhiteboardItem({ updates: { ...newTask, isManaged: true } }));
    if (newTask.parentTask) {
      dispatch(manageSubItemIdInItem({ updates: { id: newTask.parentTask, subtasks: currState } }));
    } else {
      dispatch(manageItemIdInProject({ updates: { projectId, tasks: currState } }));
    }
  };

  const updateTask = async (name, subtype, completed, id, isManaged) => {
    if (deletedIds.current.includes(id)) return;
    const newTask = tempStore.current[id];
    if (newTask?.currState) { // in the case of toggle task and new task
      const { currState, ...rest } = newTask;

      const task = {
        ...rest, name, subtype, completed
      };
      addToStore(task, currState);
      tempStore.current[id] = task;
    } else {
      const updatedTask = {
        name, subtype, completed, id
      };
      dispatch(upsertWhiteboardItem({ updates: { ...updatedTask, isManaged } }));
    }
  };

  const deleteTask = async (id, toFocus) => {
    if (toFocus) { // Incase of collapsed ids search to focus back
      let index = projectTasks.indexOf(id) - 1;
      while (!editors.current[projectTasks[index]]?.ref) {
        if (index < 0) break;
        index -= 1;
      }
      editors.current[projectTasks[index]]?.ref?.focus();
    }
    const delIds = [id, ...getSubtasks(id)];
    delIds.forEach((ID) => { delete tempStore.current[ID]; deletedIds.current.push(id); });
    setProjectTasks(projectTasks.filter((ID) => !delIds.includes(ID)));
    dispatch(deleteWhiteboardItem({ updates: { id } }));
  };

  const toggleCollapse = (id) => {
    const item = getItem(id);
    tempStore.current[id] = { ...item, collapsed: !item.collapsed };
    setProjectTasks([...projectTasksRef.current]);
    dispatch(upsertWhiteboardItem({ updates: { id, collapsed: !item.collapsed, isManaged: true } }));
  };

  const blurCurrent = useCallback(() => {
    editors.current[currentFocus.current]?.ref?.blur();
    currentFocus.current = '';
  }, []);

  const renderItem = useCallback(({
    item, index, drag, isActive
  }) => {
    return (
      <TaskEditor id={item}
        index={index}
        store={tempStore}
        projectId={projectId}
        onFocus={onFocus}
        ref={editors}
        onDrag={drag}
        isActive={isActive}
        addTask={addTask}
        updateTask={updateTask}
        deleteTask={deleteTask}
        toggleCollapse={toggleCollapse}
        projectExtras={projectExtras}
        currentFocus={currentFocus}
        shuffleMode={shuffleMode}
      />
    );
  }, [projectExtras, shuffleMode]);

  const scrollList = () => { // Scroll automatically if user is adding task at end
    if (projectTasks.findIndex((s) => s === currentFocus.current) >= projectTasks.length - 2) { listRef.current.current.getNode().scrollToEnd(); }
  };

  const allTasks = (panHandlers) => (
    <DraggableFlatList
      data={[...getListTasks(), placeholder]}
      style={{ flex: 1 }}
      contentContainerStyle={{ flexGrow: 1 }}
      renderItem={renderItem}
      onRef={(ref) => { listRef.current = ref; }}
      keyExtractor={(item, _index) => item} // it is important to mark the keys as ids
      onDragBegin={onDragBegin}
      onDragEnd={onDragEnd}
      keyboardShouldPersistTaps="always"
      keyboardDismissMode="interactive"
      onEndReached={scrollList}
      renderPlaceholder={renderDragPlaceholder}
      onPlaceholderIndexChange={placeholderIndexChange}
      onEndReachedThreshold={50}
      {...panHandlers}
    />
  );

  return !projectState.current?.name && interactionReady ? (
    <View style={{ flex: 1 }} />
  ) : (
    <View style={styles.container}>
      <Header
        navigation={navigation}
        route={route}
        projectName={projectState.current?.name}
        blurCurrent={blurCurrent}
        shuffleMode={shuffleMode}
        setShuffleMode={setShuffleMode}
      />
      <KeyboardAccessoryView // It is native component in ios to keep this view above keyboard.
        style={{ flex: 1 }}
        contentOffsetKeyboardOpened={isIphoneX() ? -hp(4.7) : -hp(7)}
        spaceBetweenKeyboardAndAccessoryView={isIphoneX() ? -hp(4.7) : -hp(7)}
        renderScrollable={allTasks}
      >
        <ToolBar ref={toolbarRef}
          subTaskModeRef={subtaskModeRef}
          subtypeRef={subtypeRef}
          changeSubtype={changeSubtype}
          currentFocus={currentFocus}
          editors={editors}
          toggleTask={toggleTask}
        />
      </KeyboardAccessoryView>
    </View>

  );
};

export default TaskListScreen;
