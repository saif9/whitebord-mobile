import React, { useState, useEffect } from 'react';
import {
  View, SafeAreaView, Text, ActivityIndicator
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { connect, useDispatch, useSelector } from 'react-redux';
import auth from '@react-native-firebase/auth';
import moment from 'moment';
import perf from '@react-native-firebase/perf';
import RNBootSplash from 'react-native-bootsplash';
import Welcome from '../screens/user/auth/Welcome';
import Auth from '../screens/user/auth';

import {
  CalendarScreen,
  HomeScreen,
  UserScreen,
  WorkspaceScreen,
  InboxScreen,
  TaskListScreen,
  NetworkScreen,
} from '../screens';

import HomeActive from '../../assets/icons/home_active.svg';
import HomeInactive from '../../assets/icons/home_inactive.svg';
import WorkspaceActive from '../../assets/icons/workspace_active.svg';
import WorkspaceInactive from '../../assets/icons/workspace_inactive.svg';
import CalendarActive from '../../assets/icons/calendar_active.svg';
import CalendarInactive from '../../assets/icons/calendar_inactive.svg';
import NetworkActive from '../../assets/icons/network_active.svg';
import NetworkInactive from '../../assets/icons/network_inactive.svg';
import { hp } from '../utils';
import { fetchMyProfile } from '../state/slices/publicProfiles/publicProfileSlice';
import {
  fetchandTrackNotifications,
  fetchandTrackReceivedInvites,
  fetchandTrackSentInvites,
} from '../state/slices/publicProfiles/listener';
import { fetchEvents } from '../state/slices/calendars/calendarSlice';
import { fetchandTrackWhiteboardItems } from '../state/slices/whiteboardItems/listener';
import { fetchandTrackProjects } from '../state/slices/projects/listener';
import { fetchandTrackSections } from '../state/slices/sections/listener';
import { fetchandTrackUser } from '../state/slices/user/listener';
import { fetchandTrackQuickTasks } from '../state/slices/quickTasks/listener';
import { makeQuickTasksDoc } from '../state/slices/quickTasks/quickTasks';
import { fetchandTrackExceptions } from '../state/slices/exceptions/listener';
import { font } from '../../constants';

const Tab = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const CalendarStack = createStackNavigator();
const WorkspaceStack = createStackNavigator();
const NetworkStack = createStackNavigator();
const AuthStack = createStackNavigator();

// eslint-disable-next-line no-unused-vars
const MenuTitle = () => {
  // TODO
  return <View />;
};

const HomeStackScreen = () => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="Home"
        component={HomeScreen}
        options={headerStyle}
      />
      <HomeStack.Screen
        name="Inbox"
        component={InboxScreen}
        options={headerStyle}
      />
      <HomeStack.Screen
        name="User"
        component={UserScreen}
        options={headerStyle}
      />
    </HomeStack.Navigator>
  );
};

const CalendarStackScreen = () => {
  return (
    <CalendarStack.Navigator>
      <CalendarStack.Screen
        name="Calendar"
        component={CalendarScreen}
        options={headerStyle}
      />
      <CalendarStack.Screen
        name="Inbox"
        component={InboxScreen}
        options={headerStyle}
      />
    </CalendarStack.Navigator>
  );
};

const NetworkStackScreen = () => {
  return (
    <NetworkStack.Navigator>
      <NetworkStack.Screen
        name="Network"
        component={NetworkScreen}
        options={headerStyle}
      />
    </NetworkStack.Navigator>
  );
};

const WorkspaceStackScreen = () => {
  return (
    <WorkspaceStack.Navigator>
      <WorkspaceStack.Screen
        name="Workspace"
        component={WorkspaceScreen}
        options={headerStyle}
      />
      <WorkspaceStack.Screen
        name="Inbox"
        component={InboxScreen}
        options={headerStyle}
      />
      <WorkspaceStack.Screen
        name="TaskList"
        component={TaskListScreen}
        options={headerStyle}
      />
    </WorkspaceStack.Navigator>
  );
};

const AuthScreen = () => (
  <NavigationContainer>
    <AuthStack.Navigator>
      <AuthStack.Screen
        name="Welcome"
        component={Welcome}
        options={{ headerShown: false }}
      />
      <AuthStack.Screen
        name="Auth"
        component={Auth}
        options={{ headerShown: false }}
      />
    </AuthStack.Navigator>
  </NavigationContainer>
);

const TabNavigator = () => {
  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  const dispatch = useDispatch();

  // Handle user state changes
  async function onAuthStateChanged(userParam) {
    const dateRange = {
      timeMinString: moment()
        .startOf('month')
        .add(1, 'minute')
        .toISOString(),
      timeMaxString: moment().endOf('month').toISOString(),
    };
    if (userParam) {
      setInitializing(true);
      await Promise.all([
        await makeQuickTasksDoc(),
        await dispatch(fetchEvents(dateRange)),
        dispatch(fetchMyProfile()),
      ]);
    } else {
      setInitializing(false);
    }
    setUser(userParam);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  const showIndicator = useSelector((state) => state.publicProfiles.newNotification);

  // eslint-disable-next-line consistent-return
  useEffect(() => {
    if (user) {
      return loadValues();
    } else {
      return () => {};
    }
  }, [user]);

  // called to load values before showing dashboard
  // separate function because useEffect doesn't accept async funcs
  const loadValues = async () => {
    // firebase performance trace
    const trace = perf().newTrace('load_values');
    trace.stop();

    let load = 0;
    const loaded = () => {
      load += 1;
      if (load > 5) { // stop loading after all listeners executed
        setInitializing(false);
        trace.stop();
      }
    };

    // this piece of code will be called everytime user gets changed
    // It subscribes for changes in invites and notifications and other stuff in future.
    // It will get unsubscribe when app is closed or hook executes again.

    // awaits retrieving subscriptions to implement firebase performance
    // does not affect wait times

    const inviteSentSub = await fetchandTrackSentInvites(dispatch);
    const inviteReceiveSub = await fetchandTrackReceivedInvites(dispatch);
    const notificationSub = await fetchandTrackNotifications(dispatch);
    const whiteboardItemsSub = await fetchandTrackWhiteboardItems(dispatch, loaded);
    const projectsSub = await fetchandTrackProjects(dispatch, loaded);
    const sectionsSub = await fetchandTrackSections(dispatch, loaded);
    const userSub = await fetchandTrackUser(dispatch, loaded);
    const quickTaskSub = await fetchandTrackQuickTasks(dispatch, loaded);
    const exceptionsSub = await fetchandTrackExceptions(dispatch, loaded);
    return () => {
      if (inviteSentSub) inviteSentSub();
      if (inviteReceiveSub) inviteReceiveSub();
      if (notificationSub) notificationSub();
      if (whiteboardItemsSub) whiteboardItemsSub();
      if (projectsSub) projectsSub();
      if (sectionsSub) sectionsSub();
      if (userSub) userSub();
      if (quickTaskSub) quickTaskSub();
      if (exceptionsSub) exceptionsSub();
    };
  };

  // dismiss splash screen
  RNBootSplash.hide({ fade: true });
  if (initializing) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator size="large" />
        <Text style={{ marginTop: 10, fontFamily: font.regular }}>
          Loading...
        </Text>
      </View>
    );
  }

  if (!user) return <AuthScreen />;

  const imgDim = hp(4);
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
      <NavigationContainer>
        <Tab.Navigator
          tabBarOptions={{
            activeTintColor: '#8093F1',
            style: {
              backgroundColor: 'white',
              height: hp(10),
            },
          }}
        >
          <Tab.Screen
            name="Home"
            component={HomeStackScreen}
            options={{
              headerStyle,
              tabBarIcon: ({ focused }) => (focused ? (
                <HomeActive height={imgDim} width={imgDim} />
              ) : (
                <HomeInactive height={imgDim} width={imgDim} />
              )),
            }}
          />
          <Tab.Screen
            name="Workspace"
            component={WorkspaceStackScreen}
            options={{
              headerStyle,
              tabBarIcon: ({ focused }) => (focused ? (
                <WorkspaceActive height={imgDim} width={imgDim} />
              ) : (
                <WorkspaceInactive height={imgDim} width={imgDim} />
              )),
            }}
          />
          <Tab.Screen
            name="Calendar"
            component={CalendarStackScreen}
            options={{
              headerStyle,
              tabBarIcon: ({ focused }) => (focused ? (
                <CalendarActive height={imgDim} width={imgDim} />
              ) : (
                <CalendarInactive height={imgDim} width={imgDim} />
              )),
            }}
          />
          <Tab.Screen
            name="Network"
            component={NetworkStackScreen}
            options={{
              headerStyle,
              tabBarIcon: ({ focused }) => (
                <View>
                  {focused ? (
                    <NetworkActive height={imgDim} width={imgDim} />
                  ) : (
                    <NetworkInactive height={imgDim} width={imgDim} />
                  )}
                  {showIndicator && (
                    <Text style={headerStyle.bullet}>{'\u2B24'}</Text>
                  )}
                </View>
              ),
            }}
          />
        </Tab.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  );
};

const headerStyle = {
  headerStyle: {
    backgroundColor: 'white',
    height: hp(10),
    borderColor: 'white',
  },
  headerShown: false,
  headerTintColor: 'white',
  headerTitleStyle: {
    color: '#434279',
  },
  bullet: {
    color: 'red',
    fontSize: 9,
    position: 'absolute',
    right: 0,
    top: 0,
  },
};

const mapStateToProps = (_state) => {
  return {};
};

const mapDispatchToProps = (_dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(TabNavigator);
