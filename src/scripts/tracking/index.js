import { Mixpanel } from 'mixpanel-react-native';
import { AppState } from 'react-native';
import { startCase } from 'lodash';
import { MIXPANEL_TOKEN } from '../../constants';

export const mixpanel = new Mixpanel(MIXPANEL_TOKEN);

/**
 * Unlike the Mixpanel API for the web app, the react-native Mixpanel API's
 * events are queued and sent as a batch instead of being sent immediately after
 * each event. Therefore, we use `mixpanel.flush()` to send the queued events
 * to Mixpanel in order to see them in the Events view
 */

const flush = () => { try { mixpanel.flush(); } catch (e) { console.log(e); } };

const interval = 5 * 60 * 1000; // Interval set in milliseconds
setInterval(flush, interval); // Send queued events to Mixpanel every 5 minutes

// Send queued events to Mixpanel when the app turns active/inactive/background
AppState.addEventListener('change', (state) => {
  if (state === 'active' || state === 'background' || state === 'inactive') flush();
});

/**
 * Trackers that capture events and send to Mixpanel
 */
export const trackSignUp = ({ user }) => {
  const { uid, email, displayName } = user;
  const [firstName, lastName] = displayName.split(' ');
  mixpanel.identify(uid);
  mixpanel.getPeople().setOnce({
    $email: email,
    $first_name: firstName,
    $last_name: lastName,
    $name: displayName,
    $distinct_id: uid,
  });
  mixpanel.track('Sign Up');
};

export const trackLogin = ({ user }) => {
  const {
    uid, email, displayName, network
  } = user;
  const [firstName, lastName] = displayName.split(' ');
  mixpanel.identify(uid);
  mixpanel.getPeople().setOnce({
    $email: email,
    $first_name: firstName,
    $last_name: lastName,
    $name: displayName,
    $distinct_id: uid,
  });
  mixpanel.track('Login', { Network: `${network}` });
};

export const trackSectionCreation = () => {
  mixpanel.track('Section Created');
};

export const trackProjectCreation = () => {
  mixpanel.track('Project Created');
};

export const trackWhiteboardItemCreation = ({ whiteboardItem }) => {
  const { type, parentTask, subtype } = whiteboardItem;
  if (parentTask) mixpanel.track('Subtask Created', { Subtype: subtype || 'check' });
  else if (type === 'task') mixpanel.track('Task Created', { Subtype: subtype || 'check' });
  else if (type === 'habit') mixpanel.track('Habit Created');
};

export const trackCollaboratorInvitation = () => {
  mixpanel.track('Collaborator Invited');
};

export const trackCalendarFilters = ({ filter }) => {
  mixpanel.track('Calendar Filter Toggled', { filterType: filter });
};

export const trackWhiteboardItemCompletion = ({ completed, type }) => {
  const msg = `${startCase(type)} ${completed ? 'Completed' : 'Unchecked'}`;
  mixpanel.track(msg, { type, completed });
};
