import { ActionTypes as modalActionTypes } from './modal';

const ActionTypes = {
  ...modalActionTypes,
  USER_LOGOUT: 'USER_LOGGED_OUT'
};

export { ActionTypes };
