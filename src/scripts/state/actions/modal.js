import { ActionTypes } from './types';

export const setModal = (fields, dispatch) => {
  dispatch({ type: ActionTypes.SET_MODAL, payload: fields });
};

export const toggleModal = (bool, dispatch) => {
  dispatch({ type: ActionTypes.TOGGLE_MODAL, payload: bool });
};

export const toggleEditMode = (bool, dispatch) => {
  dispatch({ type: ActionTypes.TOGGLE_EDIT_MODE, payload: bool });
};

export const resetModal = (_, dispatch) => {
  dispatch({ type: ActionTypes.RESET_MODE, payload: { editing: false, isOpen: false } });
};
