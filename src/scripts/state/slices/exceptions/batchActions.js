import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import perf from '@react-native-firebase/perf';
import { Collections } from '../../../../constants/collections';

export const getAllTaskExceptions = async (taskId) => {
  const trace = await perf().startTrace('fetch_all_task_exceptions'); // measures how long to fetch exceptions

  const me = auth().currentUser;
  const end = taskId.replace(/.$/, (c) => String.fromCharCode(c.charCodeAt(0) + 1));
  const exceptions = await firestore()
    .collection(Collections.RECURRENCE_EXCEPTIONS)
    .where(`roles.${me.uid}`, 'in', ['owner', 'collaborator'])
    .where(firestore.FieldPath.documentId(), '>=', taskId)
    .where(firestore.FieldPath.documentId(), '<', end)
    .get();

  trace.stop(); // stops trace
  return exceptions.docs;
};

export const updateRolesInAllExceptions = async (
  taskId,
  newInehritedRoles
) => {
  const trace = await perf().startTrace('update_roles_all_exceptions'); // measures how long to update

  const exceptions = await getAllTaskExceptions(taskId);
  const updates = exceptions
    .filter((doc) => doc.exists)
    .map((doc) => {
      return firestore()
        .collection(Collections.RECURRENCE_EXCEPTIONS)
        .doc(doc.id)
        .update({
          roles: newInehritedRoles,
        });
    });

  await Promise.all(updates);

  trace.stop(); // stops trace
};

export const deleteAllExceptions = async (
  taskId,
) => {
  const trace = await perf().startTrace('delete_all_exceptions'); // measures how long to delete

  const exceptions = await getAllTaskExceptions(taskId);
  const updates = exceptions
    .filter((doc) => doc.exists)
    .map((doc) => {
      return firestore()
        .collection(Collections.RECURRENCE_EXCEPTIONS)
        .doc(doc.id).delete();
    });

  await Promise.all(updates);

  trace.stop();
};
