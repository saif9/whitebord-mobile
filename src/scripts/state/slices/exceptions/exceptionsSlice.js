import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
} from '@reduxjs/toolkit';
import firestore from '@react-native-firebase/firestore';
import { isEmpty } from 'lodash';
import auth from '@react-native-firebase/auth';
import { Collections } from '../../../../constants/collections';
import { whiteboardItemsSelectors } from '../whiteboardItems/whiteboardItemsSlice';
import { generateRecurrentItems } from '../recurrentItems/recurrentItemsSlice';
import { trackWhiteboardItemCompletion } from '../../../tracking';

export const upsertException = createAsyncThunk('exceptionsSlice/upsertException', async ({ updates, fromListener }, { getState, dispatch }) => {
  let oldItem;
  try {
    const me = auth().currentUser;
    const userId = me.uid;
    if (!userId) return { status: 400, error: 'No User' };
    const { id, ...newItem } = updates;
    if (isEmpty(newItem)) return { status: 400, error: 'No Item' };
    oldItem = exceptionsSelectors.selectById(getState(), id);
    const whiteboardID = id.split('_')[0];
    const { roles, type } = whiteboardItemsSelectors.selectById(getState(), whiteboardID);
    newItem.id = id;
    newItem.roles = roles;
    if (fromListener) {
      dispatch(upsertExceptionsLocal(newItem));
      dispatch(generateRecurrentItems());
    } else if (oldItem) {
      firestore().collection(Collections.RECURRENCE_EXCEPTIONS).doc(id).update({ ...newItem });
      // track item checkbox toggling via Mixpanel
      if (oldItem?.completed !== newItem?.completed) {
        trackWhiteboardItemCompletion({
          type: type === 'task' ? 'recurring task' : type,
          completed: newItem.completed,
        });
      }
    } else firestore().collection(Collections.RECURRENCE_EXCEPTIONS).doc(id).set(newItem, { merge: true });
    return { status: 200 };
  } catch (e) {
    console.log(e);
    return { status: 400, error: e.message, item: oldItem };
  }
});

export const deleteException = createAsyncThunk('exceptionsSlice/deleteException', async ({ updates, fromListener }, { getState, dispatch }) => {
  let oldItem;
  try {
    const me = auth().currentUser;
    const userId = me.uid;
    if (!userId) return { status: 400, error: 'No User' };
    const { id } = updates;
    oldItem = exceptionsSelectors.selectById(getState(), id);
    if (isEmpty(oldItem)) return { status: 400, error: 'No Item' };
    if (fromListener) {
      dispatch(deleteExceptionLocal(id));
      dispatch(generateRecurrentItems());
    } else firestore().collection(Collections.RECURRENCE_EXCEPTIONS).doc(id).delete();
    return { status: 200 };
  } catch (e) {
    console.log(e.message);
    return { status: 400, error: e.message, item: oldItem };
  }
});
const exceptionsAdapter = createEntityAdapter({
  creating: false,
  loading: false,
  error: '',
});

const exceptionsSlice = createSlice({
  name: 'exceptionsSlice',
  initialState: exceptionsAdapter.getInitialState(),
  reducers: {
    upsertExceptionsLocal: exceptionsAdapter.upsertOne,
    addAllExceptions: exceptionsAdapter.addMany,
    deleteExceptionLocal: exceptionsAdapter.removeOne,
  },
  extraReducers: {
    [upsertException.fulfilled]: (state, { payload }) => {
      if (payload.status === 400) {
        upsertExceptionsLocal(payload.item);
        state.error = payload.error;
      }
    },
  }
});
export const { addAllExceptions, upsertExceptionsLocal, deleteExceptionLocal } = exceptionsSlice.actions;
export default exceptionsSlice.reducer;

export const exceptionsSelectors = exceptionsAdapter.getSelectors((state) => state.exceptionsSlice);
