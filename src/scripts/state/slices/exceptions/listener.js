import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import perf from '@react-native-firebase/perf';
import { Collections } from '../../../../constants/collections';
import { addAllExceptions, deleteException, upsertException } from './exceptionsSlice';

export const fetchandTrackExceptions = async (dispatch, loaded) => {
  try {
    const trace = await perf().startTrace('subscribe_exceptions'); // measures how long to fetch
    let initLoading = true;
    const initialItems = [];
    const me = auth().currentUser;
    if (!me.uid) throw new Error('No User');
    const subscriber = firestore().collection(Collections.RECURRENCE_EXCEPTIONS)
      .where(`roles.${me.uid}`, 'in', ['owner', 'collaborator'])
      .onSnapshot((querySnapshot) => {
        querySnapshot?.docChanges().forEach((change) => {
          const item = { id: change.doc.id, ...change.doc.data() };
          if (change.type === 'added') {
            if (initLoading) {
              initialItems.push(item);
            } else {
              setTimeout(() => dispatch(upsertException({ updates: item, fromListener: true })), 0);
            }
          }
          if (change.type === 'modified') {
            setTimeout(() => dispatch(upsertException({ updates: item, fromListener: true })), 0);
          }
          if (change.type === 'removed') {
            setTimeout(() => dispatch(deleteException({ updates: item, fromListener: true })), 0);
          }
        });
        if (initLoading) {
          dispatch(addAllExceptions(initialItems));
          loaded();
          trace.stop();
        }
        initLoading = false;
      });
    return subscriber;
  } catch (e) {
    console.log(e);
    throw e;
  }
};
