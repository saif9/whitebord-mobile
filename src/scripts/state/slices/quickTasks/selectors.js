import { createSelector } from '@reduxjs/toolkit';
import auth from '@react-native-firebase/auth';
import { uniq } from 'lodash';
import { whiteboardItemsSelectors } from '../whiteboardItems/whiteboardItemsSlice';
import { projectsSelectors } from '../projects/projectsSlice';

const memoAllQuickTasks = createSelector(
  (state) => state.quickTaskSlice.quickTask,
  whiteboardItemsSelectors.selectEntities,
  projectsSelectors.selectEntities,
  ({ tasks }, whiteboardItems, projects) => {
    const uuid = auth().currentUser.uid;
    if (tasks && tasks.length > 0) {
      const quictasks = tasks
        .map((id) => {
          if (whiteboardItems[id]) return id;
          else return 'N/A';
        })
        .filter((task) => task !== 'N/A')
        .map((id) => whiteboardItems[id])
        .filter((item) => !(projects[item.projectId]?.roles[uuid]))
        .filter((item) => !(item.backupProjectId?.[uuid]))
        .map((item) => item.id);
      return quictasks;
    }
    return [];
  }
);

const memoSharedInboxTasks = createSelector(
  whiteboardItemsSelectors.selectAll,
  projectsSelectors.selectEntities,
  (whiteboardItems, projects) => {
    const uuid = auth().currentUser.uid;
    const quictasks = whiteboardItems
      .filter((item) => item.type === 'task' && item.roles[uuid] === 'collaborator')
      .filter((item) => !(projects[item.projectId]?.roles[uuid]))
      .filter((item) => !(item.backupProjectId?.[uuid]))
      .map((item) => item.id);
    return quictasks;
  }
);

export const memoAllInboxTasks = createSelector(
  memoAllQuickTasks,
  memoSharedInboxTasks,
  (quickTasks, sharedTasks) => {
    return uniq([...sharedTasks, ...quickTasks]);
  }
);
