import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import perf from '@react-native-firebase/perf';
import { Collections } from '../../../../constants/collections';
import { upsertQuickTask } from './quickTasks';

export const fetchandTrackQuickTasks = async (dispatch, loaded) => {
  try {
    let initLoading = true;
    const trace = await perf().startTrace('subscribe_quick_tasks'); // measures how long to fetch
    const me = auth().currentUser;
    if (!me.uid) throw new Error('No User');
    const subscriber = firestore().collection(Collections.QUICK_TASKS)
      .doc(me.uid)
      .onSnapshot((documentSnapshot) => {
        const quickTask = { id: me.uid, ...documentSnapshot?.data() };
        dispatch(upsertQuickTask({ updates: quickTask, fromListener: true }));
        if (initLoading) {
          initLoading = false;
          loaded();
          trace.stop(); // stops trace
        }
      });
    return subscriber;
  } catch (e) {
    console.log(e.message);
    throw e;
  }
};
