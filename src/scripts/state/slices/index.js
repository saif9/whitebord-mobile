import calendarsSlice from './calendars/calendarSlice';
import publicProfilesSlice from './publicProfiles/publicProfileSlice';
import filterSlice from './filters/filterSlice';
import recurrentItemsSlice from './recurrentItems/recurrentItemsSlice';
import whiteboardItemsSlice from './whiteboardItems/whiteboardItemsSlice';
import projectsSlice from './projects/projectsSlice';
import sectionsSlice from './sections/sectionsSlice';
import userSlice from './user/userSlice';
import quickTaskSlice from './quickTasks/quickTasks';
import exceptionsSlice from './exceptions/exceptionsSlice';

const Slices = {
  calendarsSlice,
  publicProfilesSlice,
  filterSlice,
  recurrentItemsSlice,
  whiteboardItemsSlice,
  exceptionsSlice,
  projectsSlice,
  sectionsSlice,
  userSlice,
  quickTaskSlice
};

export default Slices;
