import axios from 'axios';
import auth from '@react-native-firebase/auth';
import perf from '@react-native-firebase/perf';
import { backendRoot } from '../../../../constants';

export const checkEmail = async (email) => {
  try {
    const trace = await perf().startTrace('check_email'); // measures how long to get a response
    const me = auth().currentUser;
    const token = await me.getIdToken();
    const res = await axios.post(`${backendRoot}/checkEmail`, { email }, { headers: { Authorization: `Bearer ${token}` } });
    trace.stop(); // stops trace
    return res.data;
  } catch (e) {
    return { success: false };
  }
};

export const removeCollaborator = async (uid) => {
  try {
    const trace = await perf().startTrace('remove_collaborator'); // measures how long to get a response
    const me = auth().currentUser;
    const token = await me.getIdToken();
    const res = await axios.post(`${backendRoot}/removeCollaborator`, { uid }, { headers: { Authorization: `Bearer ${token}` } });
    trace.stop(); // stops trace
    return res.data;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

export const acceptInvitation = async (uid) => {
  try {
    const trace = await perf().startTrace('accept_invitation'); // measures how long to get a response
    const me = auth().currentUser;
    const token = await me.getIdToken();
    const res = await axios.post(`${backendRoot}/acceptInvite`, { uid }, { headers: { Authorization: `Bearer ${token}` } });
    trace.stop(); // stops trace
    return res.data;
  } catch (e) {
    console.log(e);
    throw e;
  }
};
