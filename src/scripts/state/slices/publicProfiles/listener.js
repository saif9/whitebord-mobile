import AsyncStorage from '@react-native-async-storage/async-storage';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import moment from 'moment';
import perf from '@react-native-firebase/perf';
import { Collections } from '../../../../constants/collections';
import {
  addReceivedInvite, addSentInvite, removeReceivedInvite, removeSentInvite, removeNotification, addNotificationProxy, addAllNotifications, setNewNotification
} from './publicProfileSlice';

export const fetchandTrackSentInvites = async (dispatch) => {
  try {
    const trace = await perf().startTrace('subscribe_sent_notifications'); // measures how long to get a response
    const me = auth().currentUser;
    const subscriber = firestore().collection(Collections.INVITES).where('from.uid', '==', me.uid)
      .onSnapshot((querySnapshot) => {
        querySnapshot?.docChanges().forEach((change) => {
          if (change.type === 'added') {
            dispatch(addSentInvite({ id: change.doc.id, ...change.doc.data() }));
          }
          if (change.type === 'removed') {
            dispatch(removeSentInvite(change.doc.id));
          }
        });
        trace.stop();
      });
    return subscriber;
  } catch (e) {
    console.log(e.message);
    throw e;
  }
};

export const fetchandTrackReceivedInvites = async (dispatch) => {
  try {
    const trace = await perf().startTrace('subscribe_received_notifications'); // measures how long to get a response
    const me = auth().currentUser;
    const subscriber = firestore().collection(Collections.INVITES).where('to.uid', 'in', [me.uid, me.email])
      .onSnapshot((querySnapshot) => {
        querySnapshot?.docChanges().forEach((change) => {
          if (change.type === 'added') {
            dispatch(addReceivedInvite({ id: change.doc.id, ...change.doc.data() }));
          }
          if (change.type === 'removed') {
            dispatch(removeReceivedInvite(change.doc.id));
          }
        });
        trace.stop(); // stops trace
      });
    return subscriber;
  } catch (e) {
    console.log(e.message);
    throw e;
  }
};

export const fetchandTrackNotifications = async (dispatch) => {
  try {
    const trace = await perf().startTrace('subscribe_notifications'); // measures how long to get a response
    let initLoading = true;
    const initialItems = [];
    const me = auth().currentUser;
    const subscriber = firestore().collection(Collections.NOTIFICATIONS).where('to', '==', me.uid)
      .onSnapshot((querySnapshot) => {
        querySnapshot?.docChanges().forEach((change) => {
          const item = { id: change.doc.id, ...change.doc.data() };
          if (change.type === 'added') {
            if (initLoading) { initialItems.push(item); } else { dispatch(addNotificationProxy(item)); }
          }
          if (change.type === 'removed') {
            dispatch(removeNotification(change.doc.id));
          }
        });
        if (initLoading) {
          const [lastItem] = initialItems.slice(-1);
          if (lastItem) {
            (async () => {
              const lastSeen = await AsyncStorage.getItem('lastTime');
              if (moment(lastItem.createdAt).isAfter(moment(lastSeen))) { dispatch(setNewNotification(true)); }
            })();
          }
          dispatch(addAllNotifications(initialItems));
          trace.stop(); // stops trace
        }
        initLoading = false;
      });
    return subscriber;
  } catch (e) {
    console.log(e.message);
    throw e;
  }
};
