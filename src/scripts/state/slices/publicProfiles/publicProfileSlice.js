import {
  createEntityAdapter,
  createSlice,
  createAsyncThunk,
} from '@reduxjs/toolkit';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import debounce from 'lodash.debounce';
import perf from '@react-native-firebase/perf';
import { Collections } from '../../../../constants/collections';
import {
  acceptInvitation, removeCollaborator
} from './api';
import { sendNotification } from './notifications';
import { site } from '../../../../constants';
import { getSharedSectionId } from '../../../utils';
import { createSection } from '../sections/sectionsSlice';

const getProfiles = async (id) => {
  const trace = await perf().startTrace('fetch_profile'); // measures how long to fetch
  const docRef = await firestore().collection(Collections.PUBLIC_PROFILES).doc(id).get();
  trace.stop(); // stops trace
  return docRef.exists ? { ...docRef.data(), id } : {};
};

// Create shared section on firebase if does not exist already
const createSharingDefaults = async (getState, dispatch) => {
  const trace = await perf().startTrace('create_sharing_defaults'); // measures how long to fetch
  const { userSlice, publicProfiles } = getState();
  const me = auth().currentUser;
  const sharedSectionId = getSharedSectionId();
  const ref = await firestore().collection(Collections.PUBLIC_PROFILES).doc(me.uid).get();
  if (!ref.exists) {
    await dispatch(updateMyProfile(publicProfiles.myProfile));
  }
  if (!userSlice.user?.sections?.includes(sharedSectionId)) {
    createSection({ id: sharedSectionId, name: 'Shared with me' }, dispatch);
  }
  trace.stop(); // stops trace
};

export const fetchProfiles = createAsyncThunk('publicProfiles/fetchProfiles', async (uuid) => {
  const trace = await perf().startTrace('fetch_profiles'); // measures how long to fetch
  const docRef = await firestore().collection(Collections.PUBLIC_PROFILES).doc(uuid).get();
  if (docRef) {
    const networkIds = docRef.data().network;
    const taskData = networkIds.map((id) => getProfiles(id));
    const data = await Promise.all(taskData);
    trace.stop(); // stops trace
    return data;
  }
  trace.stop(); // stops trace
  return [];
});

export const createInvites = createAsyncThunk('publicProfiles/createInvites', async (emails, { getState, dispatch }) => {
  const trace = await perf().startTrace('create_invites'); // measures how long to create
  const createInvite = (data) => {
    const id = `${data.from.uid}-${data.to.uid}`;
    const ref = firestore().collection(Collections.INVITES).doc(id);
    return ref.set({ ...data, site });
  };
  const { publicProfiles } = getState();
  const promises = [];
  const profile = publicProfiles.myProfile;
  await createSharingDefaults(getState, dispatch);
  for (const email of emails) {
    const data = {
      from: {
        uid: profile.uid,
        email: profile.email,
        firstName: profile.firstName,
        lastName: profile.lastName,
        photoURL: profile.photoURL,
      },
      to: email,
      createdAt: new Date().toISOString(),
      createdBy: profile.uid,
    };
    promises.push(createInvite(data));
  }
  await Promise.all(promises);
  trace.stop(); // stops trace
  return [];
});

export const fetchMyProfile = createAsyncThunk('publicProfiles/fetchMyProfile', async (_, { dispatch }) => {
  const trace = await perf().startTrace('fetch_my_profile'); // measures how long to fetch
  const me = auth().currentUser;
  const docRef = await firestore().collection(Collections.PUBLIC_PROFILES).doc(me.uid).get();
  let myProfile;
  if (docRef) {
    myProfile = docRef.data();
  }
  const result = {
    uid: me.uid,
    email: me.email,
    firstName: myProfile?.firstName || null,
    lastName: myProfile?.lastName || null,
    location: myProfile?.location || null,
    jobTitle: myProfile?.jobTitle || null,
    photoURL: myProfile?.photoURL || me.photoURL || null,
    network: myProfile?.network || []
  };
  if (!myProfile) {
    const i = me.displayName.indexOf(' ');
    if (i >= 0) {
      result.firstName = me.displayName.slice(0, i);
      result.lastName = me.displayName.slice(i + 1);
    } else {
      result.firstName = me.displayName;
    }
  }
  dispatch(fetchProfiles(me.uid));
  trace.stop();
  return result;
});

export const updateMyProfile = createAsyncThunk('publicProfiles/updateMyProfile', async (data) => {
  const trace = await perf().startTrace('update_my_profile'); // measures how long to update
  const me = auth().currentUser;
  const docRef = await firestore().collection(Collections.PUBLIC_PROFILES).doc(me.uid);
  const update = {};
  if (data.firstName !== undefined || data.firstName !== undefined) {
    update.displayName = `${data.firstName || ''} ${data.lastName || ''}`.trim();
  }
  if (data.photoURL !== undefined) {
    update.photoURL = data.photoURL;
  }
  await me.updateProfile(update);
  docRef.set(data, { merge: true });
  trace.stop(); // stops trace
  return data;
});

export const removeInvite = createAsyncThunk('publicProfiles/removeInvite', async (invite, { dispatch }) => {
  const trace = await perf().startTrace('delete_invite'); // measures how long to delete
  await firestore().collection(Collections.INVITES).doc(invite.id).delete();
  const me = auth().currentUser;
  const cancel = invite.from.uid === me.uid;
  const type = cancel ? 'invite.cancel' : 'invite.decline';
  const to = cancel ? invite.to.uid : invite.from.uid;
  dispatch(sendNotification({ type, to, data: undefined }));
  trace.stop();
});

export const acceptInvite = createAsyncThunk('publicProfiles/acceptInvite', async (uid, { getState, dispatch }) => {
  const trace = await perf().startTrace('accept_invite'); // measures how long to get a response
  await createSharingDefaults(getState, dispatch);
  await acceptInvitation(uid);
  dispatch(fetchMyProfile());
  trace.stop(); // stops trace
});

export const removeProfile = createAsyncThunk('publicProfiles/removeProfile', async (uid) => {
  const trace = await perf().startTrace('remove_profile'); // measures how long to fetch
  await removeCollaborator(uid);
  trace.stop(); // stops trace
  return uid;
});

const refreshProfile = debounce((dispatch) => {
  dispatch(fetchMyProfile());
}, 1000, { trailing: true });

// This function will be used every time we get a notification and it will perform the related action according to notification.
export const addNotificationProxy = createAsyncThunk('publicProfiles/addNotificationProxy', async (notification, { dispatch, getState }) => {
  switch (notification.type) {
    case 'invite.accept':
      refreshProfile(dispatch);
      break;
    default:
      break;
  }
  dispatch(addNotification(notification));
  dispatch(setNewNotification(true));
});

export const deleteNotification = createAsyncThunk('publicProfiles/deleteNotification', async (id) => {
  const trace = await perf().startTrace('delete_notification'); // measures how long to delete
  await firestore().collection(Collections.NOTIFICATIONS).doc(id).delete();
  trace.stop(); // stops trace
});

const publicProfilesAdapter = createEntityAdapter();
const invitesSentAdapter = createEntityAdapter();
const invitesReceivedAdapter = createEntityAdapter();
const notificationsAdapter = createEntityAdapter();
const INITIAL_STATE = {
  myProfile: {},
  profiles: publicProfilesAdapter.getInitialState(),
  invitesSent: invitesSentAdapter.getInitialState(),
  invitesReceived: invitesReceivedAdapter.getInitialState(),
  notifications: notificationsAdapter.getInitialState(),
  newNotification: false,
};
const publicProfilesSlice = createSlice({
  name: 'publicProfiles',
  initialState: INITIAL_STATE,
  reducers: {
    addOne: publicProfilesAdapter.addOne,
    addSentInvite(state, action) {
      invitesSentAdapter.addOne(state.invitesSent, action.payload);
    },
    removeSentInvite(state, action) {
      invitesSentAdapter.removeOne(state.invitesSent, action.payload);
    },
    addReceivedInvite(state, action) {
      invitesReceivedAdapter.addOne(state.invitesReceived, action.payload);
    },
    removeReceivedInvite(state, action) {
      invitesReceivedAdapter.removeOne(state.invitesReceived, action.payload);
    },
    addNotification(state, action) {
      notificationsAdapter.addOne(state.notifications, action.payload);
    },
    addAllNotifications(state, action) {
      notificationsAdapter.setAll(state.notifications, action.payload);
    },
    removeNotification(state, action) {
      notificationsAdapter.removeOne(state.notifications, action.payload);
    },
    setNewNotification(state, action) {
      state.newNotification = action.payload;
    },
  },
  extraReducers: {
    [fetchProfiles.fulfilled]: (state, { payload }) => {
      publicProfilesAdapter.setAll(state.profiles, payload);
    },
    [removeProfile.fulfilled]: (state, { payload }) => {
      publicProfilesAdapter.removeOne(state.profiles, payload);
    },
    [fetchMyProfile.fulfilled]: (state, { payload }) => {
      state.myProfile = payload;
    },
    [updateMyProfile.fulfilled]: (state, { payload }) => {
      state.myProfile = { ...state.myProfile, ...payload };
    }
  }
});
export const {
  addOne, addSentInvite, addReceivedInvite, removeSentInvite, removeReceivedInvite,
  addAllNotifications, addNotification, removeNotification, setNewNotification
} = publicProfilesSlice.actions;
export default publicProfilesSlice.reducer;

export const publicProfilesSelectors = publicProfilesAdapter.getSelectors((state) => state.publicProfiles.profiles);

export const invitesSentSelectors = invitesSentAdapter.getSelectors((state) => state.publicProfiles.invitesSent);

export const invitesReceivedSelectors = invitesReceivedAdapter.getSelectors((state) => state.publicProfiles.invitesReceived);

export const notificationsSelectors = notificationsAdapter.getSelectors((state) => state.publicProfiles.notifications);
