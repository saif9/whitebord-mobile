import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { createAsyncThunk } from '@reduxjs/toolkit';
import perf from '@react-native-firebase/perf';
import { site } from '../../../../constants';
import { getOwner } from '../../../utils';
import { Collections } from '../../../../constants/collections';

export const sendNotification = createAsyncThunk('publicProfiles/sendNotification', async ({ type, to, data }, { getState }) => {
  const me = getState().publicProfiles.myProfile;
  if (to === me.uid) {
    return;
  }
  const trace = await perf().startTrace(); // measures how long to create doc

  await firestore().collection(Collections.NOTIFICATIONS).add({
    data,
    type,
    to,
    site,
    createdAt: new Date().toISOString(),
    user: {
      displayName: `${me.firstName} ${me.lastName}`,
      photoURL: me.photoURL,
      uid: me.uid,
    },
  });

  trace.stop();
});

export const sendShareNotifications = createAsyncThunk('publicProfiles/sendShareNotifications',
  async ({
    collection, itemId, item, prevRoles, newRoles
  }, { dispatch }) => {
    const data = { id: itemId, name: item?.name };
    const { uid } = auth().currentUser;
    const owner = getOwner(newRoles);
    for (const k in newRoles) {
      if (k !== uid && !prevRoles[k]) {
        dispatch(sendNotification({ type: `share.start.${collection}`, to: k, data }));
      }
    }

    for (const k in prevRoles) {
      if (!newRoles[k]) {
        if (k !== uid) {
          dispatch(sendNotification({ type: `share.stop.${collection}`, to: k, data }));
        } else if (k !== owner) {
          dispatch(sendNotification({ type: `share.decline.${collection}`, to: owner, data }));
        }
      }
    }
  });
