import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import perf from '@react-native-firebase/perf';
import { Collections } from '../../../../constants/collections';
import { addAllProjects, deleteProject, upsertProject } from './projectsSlice';

export const fetchandTrackProjects = async (dispatch, loaded) => {
  try {
    let initLoading = true;
    const initialItems = [];
    const me = auth().currentUser;
    if (!me.uid) throw new Error('No User');
    const trace = await perf().startTrace('subscribe_projects'); // measures how long before response

    const subscriber = firestore().collection(Collections.PROJECTS)
      .where(`roles.${me.uid}`, 'in', ['owner', 'collaborator'])
      .onSnapshot((querySnapshot) => {
        querySnapshot?.docChanges().forEach((change) => {
          const item = { id: change.doc.id, ...change.doc.data() };
          if (change.type === 'added') {
            if (initLoading) {
              initialItems.push(item);
            } else {
              setTimeout(() => dispatch(upsertProject({ updates: item, fromListener: true })), 0);
            }
          }
          if (change.type === 'modified') {
            setTimeout(() => dispatch(upsertProject({ updates: item, fromListener: true })), 0);
          }
          if (change.type === 'removed') {
            dispatch(deleteProject({ updates: item, fromListener: true }));
          }
        });
        if (initLoading) {
          dispatch(addAllProjects(initialItems));
          loaded();
          trace.stop(); // stops trace
        }
        initLoading = false;
      });
    return subscriber;
  } catch (e) {
    console.log(e);
    throw e;
  }
};
