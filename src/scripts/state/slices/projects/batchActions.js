import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import perf from '@react-native-firebase/perf';
import { Collections } from '../../../../constants/collections';
import { updateRolesInAllExceptions } from '../exceptions/batchActions';

export const getAllProjectTasks = async (projectID) => {
  const trace = await perf().startTrace('fetch_all_project_tasks'); // measures how long to fetch

  const me = auth().currentUser;
  const tasks = await firestore()
    .collection(Collections.WHITEBOARD_ITEMS)
    .where(`roles.${me.uid}`, 'in', ['owner', 'collaborator'])
    .where('projectId', '==', projectID)
    .where('type', '==', 'task')
    .get();

  trace.stop(); // stops trace
  return tasks.docs;
};

export const updateRolesInAllTasks = async (
  projectID,
  newInehritedRoles
) => {
  const trace = await perf().startTrace('update_roles_all_tasks');

  const tasks = await getAllProjectTasks(projectID);
  const taskData = tasks
    .filter((doc) => doc.exists)
    .map((doc) => ({ id: doc.id, ...doc.data() }))
    .reduce((json, doc) => ({ ...json, [doc.id]: doc }), {});

  const updates = tasks
    .filter((doc) => doc.exists)
    .map((doc) => {
      const oldData = taskData[doc.id];
      const newRoles = { ...newInehritedRoles };
      (oldData.assignedTo || []).forEach((user) => {
        if (newRoles[user] !== 'owner') {
          newRoles[user] = 'collaborator';
        }
      });
      taskData[doc.id] = {
        ...oldData,
        inheritedRoles: newInehritedRoles,
        roles: newRoles,
      };
      if (oldData.recurrencePattern) updateRolesInAllExceptions(doc.id, newRoles);
      return firestore()
        .collection(Collections.WHITEBOARD_ITEMS)
        .doc(doc.id)
        .update({
          inheritedRoles: newInehritedRoles,
          roles: newRoles,
        });
    });

  await Promise.all(updates);
  trace.stop();
};

export const deleteProjectFromFirestore = async (projectID) => {
  const trace = await perf().startTrace('delete_project');

  const tasks = await getAllProjectTasks(projectID);
  firestore().collection(Collections.PROJECTS).doc(projectID).delete();
  const deletions = tasks
    .filter((doc) => doc.exists)
    .map((doc) => firestore().collection(Collections.WHITEBOARD_ITEMS).doc(doc.id).delete());

  await Promise.all(deletions);
  trace.stop();
};
