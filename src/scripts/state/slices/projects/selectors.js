import { createSelector } from '@reduxjs/toolkit';
import auth from '@react-native-firebase/auth';
import { uniq } from 'lodash';
import { whiteboardItemsSelectors } from '../whiteboardItems/whiteboardItemsSlice';
import { projectsSelectors } from './projectsSlice';

const calculateItems = (project, whiteboardItems) => {
  if (!project) return null;
  const userId = auth().currentUser.uid;
  const { id: projId } = project || {};
  const { tasks = [] } = project || {};
  let tasksList = [];
  tasks.forEach((id) => {
    const whiteboardItem = whiteboardItems[id];
    if (!whiteboardItem?.roles[userId]) {
      return;
    }
    const subTasks = (whiteboardItem?.subtasks || []);
    tasksList = [...tasksList, id, ...subTasks];
  });
  Object.values(whiteboardItems).forEach((item) => {
    const {
      id, projectId, backupProjectId = {}
    } = item;
    const belongsTo = projectId && (projId === projectId || backupProjectId[userId] === projId);
    if (belongsTo && !tasksList.includes(id)) {
      tasksList = [...tasksList, id];
    }
  });
  tasksList = uniq(tasksList.filter((tId) => whiteboardItems[tId]?.roles[userId]));
  return tasksList;
};

export const memoProjectTasksById = () => createSelector(
  (state, projectId) => projectsSelectors.selectById(state, projectId),
  whiteboardItemsSelectors.selectEntities,
  (project, whiteboardItems) => {
    return calculateItems(project, whiteboardItems);
  }
);
