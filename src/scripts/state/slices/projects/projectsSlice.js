import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
} from '@reduxjs/toolkit';
import firestore from '@react-native-firebase/firestore';
import { isEmpty, isEqual } from 'lodash';
import auth from '@react-native-firebase/auth';
import { Collections } from '../../../../constants/collections';
import { sendShareNotifications } from '../publicProfiles/notifications';
import { manageItemIdInSection, sectionsSelectors } from '../sections/sectionsSlice';
import { deleteProjectFromFirestore, updateRolesInAllTasks } from './batchActions';
import { trackProjectCreation } from '../../../tracking';

export const createProjectID = () => {
  const docRef = firestore().collection(Collections.PROJECTS).doc();
  return docRef.id;
};

// This function is complex as same function is called for local update and listener update
export const upsertProject = createAsyncThunk('projectsSlice/upsertProject', async ({ updates, fromListener }, { getState, dispatch }) => {
  let oldItem;
  try {
    const me = auth().currentUser;
    const userId = me.uid;
    if (!userId) return { status: 400, error: 'No User' };
    const {
      id, isAssignedNewSection, toSectionId, fromSectionId, ...newItem
    } = updates;
    if (isEmpty(newItem)) return { status: 400, error: 'No Item' };
    oldItem = projectsSelectors.selectById(getState(), id);

    if (fromListener) { // called from listener whhich will be the case even update is generated from else statements
      newItem.id = id;
      if (newItem.roles[userId]) { // update project if my roles is available
        dispatch(upsertProjectLocal(newItem));
      } else { // delete project if my role is not available
        dispatch(deleteProject({ updates: newItem, fromListener }));
      }
    } else if (oldItem) { // for updating project
      firestore().collection(Collections.PROJECTS).doc(id).update({ ...newItem });
    } else { // for creating project
      firestore().collection(Collections.PROJECTS).doc(id).set(newItem, { merge: true });
      trackProjectCreation();
    }

    // If a new section is assigned to a project
    if (isAssignedNewSection) {
      if (toSectionId) {
        dispatch(manageItemIdInSection({ updates: { sectionId: toSectionId, projectId: id }, fromListener }));
      }
      if (fromSectionId) {
        dispatch(manageItemIdInSection({ updates: { sectionId: fromSectionId, projectId: id, isDeleted: true }, fromListener }));
      }
    }

    if (!fromListener) {
      // sending collaborators shared notification
      if (!isEqual(oldItem?.roles, newItem.roles)) {
        updateRolesInAllTasks(id, newItem.roles); // Just update the roles, listener will do their work
        dispatch(sendShareNotifications({
          collection: Collections.PROJECTS,
          itemId: id,
          item: { id, name: (newItem.name || oldItem?.name) },
          prevRoles: oldItem?.roles || {},
          newRoles: newItem.roles
        }));
      }
    }
    return { status: 200 };
  } catch (e) {
    console.log(e.message);
    return { status: 400, error: e.message, item: oldItem };
  }
});

export const manageItemIdInProject = createAsyncThunk('projectsSlice/manageItemIdInProject', async ({ updates, fromListener }, { getState, dispatch }) => {
  let item;
  try {
    const {
      projectId, taskId, tasks, isDeleted
    } = updates; // fromListener will ignore firestore update
    item = projectsSelectors.selectById(getState(), projectId);
    if (!item) return { status: 400, error: 'No Item' };
    const projectRef = firestore().collection(Collections.PROJECTS).doc(projectId);
    if (!fromListener) {
      if (tasks?.length) {
        projectRef.update({ tasks });
      } else if (isDeleted) {
        projectRef.update({ tasks: firestore.FieldValue.arrayRemove(taskId) });
      } else if (!(item.tasks || []).includes(taskId)) {
        projectRef.update({ tasks: firestore.FieldValue.arrayUnion(taskId) });
      }
    }
    return { status: 200 };
  } catch (e) {
    console.log(e.message);
    return { status: 400, error: e.message, item };
  }
});

export const deleteProject = createAsyncThunk('projectsSlice/deleteProject', async ({ updates, fromListener }, { getState, dispatch }) => {
  let oldItem;
  try {
    const me = auth().currentUser;
    const userId = me.uid;
    if (!userId) return { status: 400, error: 'No User' };
    const { id } = updates;
    oldItem = projectsSelectors.selectById(getState(), id);
    const section = sectionsSelectors.selectAll(getState()).find((sec) => (sec.projects || []).includes(id));
    if (isEmpty(oldItem)) return { status: 400, error: 'No Item' };
    const { roles, name } = oldItem;
    const { [userId]: value, ...newRoles } = roles;
    const isOwner = value === 'owner';
    if (section) {
      dispatch(manageItemIdInSection({ updates: { sectionId: section.id, projectId: id, isDeleted: true }, fromListener }));
    }
    if (fromListener) dispatch(deleteProjectLocal(id)); // always delete if delete is called from listener, or role update from listner
    else if (isOwner) {
      deleteProjectFromFirestore(id);
      dispatch(sendShareNotifications({
        collection: Collections.PROJECTS,
        itemId: id,
        item: { id, name },
        prevRoles: roles,
        newRoles: isOwner ? { [userId]: value } : newRoles
      }));
    } else {
      dispatch(upsertProject({ updates: { id, roles: newRoles } })); // if this person removes himself from project
      // then it should just update and on from listener it will get deleted
    }

    return { status: 200 };
  } catch (e) {
    console.log(e.message);
    return { status: 400, error: e.message, item: oldItem };
  }
});

const projectsAdapter = createEntityAdapter({
  creating: false,
  loading: false,
  error: '',
});

const projectsSlice = createSlice({
  name: 'projectsSlice',
  initialState: projectsAdapter.getInitialState(),
  reducers: {
    upsertProjectLocal: projectsAdapter.upsertOne,
    addAllProjects: projectsAdapter.addMany,
    deleteProjectLocal: projectsAdapter.removeOne,
  },
  extraReducers: {
    [upsertProject.fulfilled]: (state, { payload }) => {
      if (payload.status === 400) {
        upsertProjectLocal(payload.item);
        state.error = payload.error;
      }
    },
    [manageItemIdInProject.fulfilled]: (state, { payload }) => {
      if (payload.status === 400) {
        upsertProjectLocal(payload.item);
        state.error = payload.error;
      }
    },
  }
});
export const { addAllProjects, upsertProjectLocal, deleteProjectLocal } = projectsSlice.actions;
export default projectsSlice.reducer;

export const projectsSelectors = projectsAdapter.getSelectors((state) => state.projectsSlice);
