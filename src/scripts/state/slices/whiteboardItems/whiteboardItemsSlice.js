import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
} from '@reduxjs/toolkit';
import firestore from '@react-native-firebase/firestore';
import { isEmpty, isEqual } from 'lodash';
import auth from '@react-native-firebase/auth';
import perf from '@react-native-firebase/perf';
import { Collections } from '../../../../constants/collections';
import { canTaskBeDeleted, prepareRoles, removeHTMLTags } from '../../../utils';
import { sendNotification, sendShareNotifications } from '../publicProfiles/notifications';
import { manageItemIdInProject } from '../projects/projectsSlice';
import { manageItemIdInQuickTask } from '../quickTasks/quickTasks';
import { deleteAllExceptions, updateRolesInAllExceptions } from '../exceptions/batchActions';
import { generateRecurrentItems } from '../recurrentItems/recurrentItemsSlice';
import { INBOX_ID } from '../../../../constants';
import { trackWhiteboardItemCompletion, trackWhiteboardItemCreation } from '../../../tracking';

export const createWhiteboardId = () => {
  const docRef = firestore().collection(Collections.WHITEBOARD_ITEMS).doc();
  return docRef.id;
};

const deleteSubtasksFromFirestore = async (taskId, subtasks = []) => {
  const trace = await perf().startTrace('delete_subtasks'); // measure how long to delete
  const deletions = subtasks.map((tId) => firestore().collection(Collections.WHITEBOARD_ITEMS).doc(tId).delete());
  await Promise.all([
    firestore().collection(Collections.WHITEBOARD_ITEMS).doc(taskId).delete(),
    ...deletions,
  ]);
  trace.stop(); // stops trace
};

export const createNewWhiteboardItem = (item) => { // for taskList only
  const me = auth().currentUser.uid;
  const newItem = {
    id: createWhiteboardId(),
    userId: me,
    projectId: item.projectId,
    name: item.name,
    completed: false,
    type: 'task',
    subtype: item.subtype || 'check',
    roles: { [me]: 'owner' },
    parentTask: item.parentTask,
    allDay: true,
  };
  trackWhiteboardItemCreation({ whiteboardItem: item });
  return newItem;
  // dispatch(upsertWhiteboardItem({ updates: { ...newItem } }));
};

/**
 * update or insert updates of whiteboarditems
 * @param {*} updates actual content of item
 * @param {*} fromListener updating from listener, so disable updates to firestore and collaboration notifications
 * @returns add item to redux store and firestore(if fromListener is false).
 */
export const upsertWhiteboardItem = createAsyncThunk('whiteboardItems/upsertWhiteboardItem', async ({ updates, fromListener }, { getState, dispatch }) => {
  let oldItem;
  try {
    const me = auth().currentUser;
    const userId = me.uid;
    if (!userId) return { status: 400, error: 'No User' };
    const { id, isManaged, ...newItem } = updates;
    if (isEmpty(newItem)) return { status: 400, error: 'No Item' };
    oldItem = whiteboardItemsSelectors.selectById(getState(), id);
    const date = newItem.scheduledDate || newItem.deadline;
    if (date) {
      const dateField = newItem.type === 'event' ? 'date' : 'scheduledDate';
      newItem[dateField] = date;
    }
    newItem.id = id;
    const finalItem = prepareRoles(getState(), userId, oldItem, newItem);
    if (fromListener) { // called from listener whhich will be the case even update is generated from else statements
      if (finalItem.roles[userId]) { // update whiteboardItem if my role is available
        dispatch(upsertWhiteboardItemLocal(finalItem));
      } else { // delete whiteboardItem if my role is not available
        dispatch(deleteWhiteboardItem({ updates: finalItem, fromListener }));
      }
      dispatch(generateRecurrentItems());
    } else if (oldItem) { // update if old Item
      firestore().collection(Collections.WHITEBOARD_ITEMS).doc(id).update({ ...finalItem });
      // track item checkbox toggling via Mixpanel
      if (oldItem?.completed !== finalItem?.completed) {
        trackWhiteboardItemCompletion({ type: oldItem.type, completed: finalItem.completed });
      }
    } else { // create new Item
      firestore().collection(Collections.WHITEBOARD_ITEMS).doc(id).set(finalItem, { merge: true });
      trackWhiteboardItemCreation({ whiteboardItem: finalItem });
    }
    if (!isManaged && !fromListener) { // for performance improvement in task List
      // first condition checking if project should be updated or ignored
      // eslint-disable-next-line eqeqeq
      if (('parentTask' in finalItem) && finalItem.parentTask != oldItem?.parentTask) { // undefined == nulll
        if (finalItem.parentTask) {
          dispatch(manageSubItemIdInItem({ updates: { id: finalItem.parentTask, taskId: id }, fromListener }));
        }
        if (oldItem?.parentTask) {
          dispatch(manageSubItemIdInItem({ updates: { id: finalItem.parentTask, taskId: id, isDeleted: true }, fromListener }));
        }
      } else if (('projectId' in finalItem) && finalItem.projectId !== oldItem?.projectId) { // If owner of task assigned the task to other project?
        if (finalItem.projectId) {
          if (finalItem.projectId === INBOX_ID) {
            dispatch(manageItemIdInQuickTask({ updates: { taskId: id }, fromListener }));
          } else {
            dispatch(manageItemIdInProject({ updates: { projectId: finalItem.projectId, taskId: id }, fromListener }));
          }
        }
        if (oldItem?.projectId) {
          if (oldItem?.projectId === INBOX_ID) {
            dispatch(manageItemIdInQuickTask({ updates: { taskId: id, isDeleted: true }, fromListener }));
          } else {
            dispatch(manageItemIdInProject({ updates: { projectId: oldItem.projectId, taskId: id, isDeleted: true }, fromListener }));
          }
        }
      }
      // If shared task is moved to other project?
      if (('backupProjectId' in finalItem) && finalItem.backupProjectId?.[userId] !== oldItem?.backupProjectId?.[userId]) {
        if (finalItem.backupProjectId?.[userId]) {
          dispatch(manageItemIdInProject({ updates: { projectId: finalItem.backupProjectId[userId], taskId: id }, fromListener }));
        }
        if (oldItem?.backupProjectId?.[userId]) {
          dispatch(manageItemIdInProject({ updates: { projectId: oldItem.backupProjectId[userId], taskId: id, isDeleted: true }, fromListener }));
        }
      }

      if (!fromListener) {
        // sending collaborators shared notification
        if (!isEqual(oldItem?.roles, finalItem.roles)) {
          if (finalItem.recurrencePattern) updateRolesInAllExceptions(id, finalItem.roles); // update roles in all exceptions
          dispatch(
            sendShareNotifications({
              collection: Collections.WHITEBOARD_ITEMS,
              itemId: id,
              item: { id, name: removeHTMLTags(finalItem.name || oldItem?.name) },
              prevRoles: oldItem?.roles || {},
              newRoles: finalItem.roles,
            }),
          );
        }
      }

      // sending collaborators complete notification
      if (oldItem?.completed === false && finalItem.completed === true) {
        (oldItem.assignedTo || []).forEach((k) => {
          dispatch(
            sendNotification({
              type: 'task.complete',
              to: k,
              data: { id, name: removeHTMLTags(finalItem.name || oldItem?.name) },
            }),
          );
        });
      }
    }

    return { status: 200 };
  } catch (e) {
    console.log(e.message);
    return { status: 400, error: e.message, item: oldItem };
  }
});

// else if (prevId) {
//   const subtasksArr = [...(item.subtasks || [])];
//   const index = subtasksArr.indexOf(prevId);
//   const found = subtasksArr.indexOf(taskId);
//   if (found < 0) {
//     subtasksArr.splice(index + 1, 0, taskId);
//     itemRef.update({ subtasks: subtasksArr });
//   }
// }
export const manageSubItemIdInItem = createAsyncThunk('whiteboardItems/manageSubItemIdInItem', async ({ updates, fromListener }, { getState, dispatch }) => {
  let item;
  try {
    const {
      id, taskId, subtasks, isDeleted
    } = updates; // fromListener will ignore firestore update
    item = whiteboardItemsSelectors.selectById(getState(), id);
    if (!item) return { status: 400, error: 'No Item' };
    const itemRef = firestore().collection(Collections.WHITEBOARD_ITEMS).doc(id);
    if (!fromListener) {
      if (subtasks?.length) {
        itemRef.update({ subtasks });
      } else if (isDeleted) {
        itemRef.update({ subtasks: firestore.FieldValue.arrayRemove(taskId) });
      } else if (!(item.subtasks || []).includes(taskId)) {
        itemRef.update({ subtasks: firestore.FieldValue.arrayUnion(taskId) });
      }
    }
    return { status: 200 };
  } catch (e) {
    console.log(e.message);
    return { status: 400, error: e.message, item };
  }
});

export const deleteWhiteboardItem = createAsyncThunk('whiteboardItems/deleteWhiteboardItem', async ({ updates, fromListener }, { getState, dispatch }) => {
  let oldItem;
  try {
    const me = auth().currentUser;
    const userId = me.uid;
    if (!userId) return { status: 400, error: 'No User' };
    const { id } = updates;
    oldItem = whiteboardItemsSelectors.selectById(getState(), id);
    if (isEmpty(oldItem)) return { status: 400, error: 'No Item' };

    if (fromListener) dispatch(deleteWhiteboardItemLocal(id));
    else if (canTaskBeDeleted(oldItem)) {
      firestore().collection(Collections.WHITEBOARD_ITEMS).doc(id).delete();
      dispatch(
        sendShareNotifications({
          collection: Collections.WHITEBOARD_ITEMS,
          itemId: id,
          item: { id, name: removeHTMLTags(oldItem?.name) },
          prevRoles: oldItem?.roles || {},
          newRoles: {},
        }),
      );

      if (oldItem?.projectId) {
        if (oldItem?.projectId === 'INBOX_ID') {
          dispatch(manageItemIdInQuickTask({ updates: { taskId: id, isDeleted: true }, fromListener }));
        } else {
          dispatch(manageItemIdInProject({ updates: { projectId: oldItem.projectId, taskId: id, isDeleted: true }, fromListener }));
        }
      }
      if (oldItem?.parentTask) {
        dispatch(manageSubItemIdInItem({ updates: { id: oldItem?.parentTask, taskId: id, isDeleted: true }, fromListener }));
      }
      if (oldItem?.subtasks?.length) {
        deleteSubtasksFromFirestore(id, oldItem?.subtasks);
      }
      if (oldItem.recurrencePattern) {
        deleteAllExceptions(id);
      }
    } else {
      // eslint-disable-next-line no-unused-vars
      const { [userId]: value, ...newRoles } = oldItem.roles;
      const updated = {
        id,
        projectId: undefined,
        roles: newRoles,
        assignedTo: (oldItem.assignedTo || []).filter((user) => user !== userId)
      };
      dispatch(upsertWhiteboardItem({ updates: updated, fromListener }));
    }

    return { status: 200 };
  } catch (e) {
    console.log(e.message);
    return { status: 400, error: e.message, item: oldItem };
  }
});

export const moveToWorkspace = createAsyncThunk('whiteboardItems/moveToWorkspace', async ({ updates }, { dispatch }) => {
  try {
    const {
      taskIds, projectId
    } = updates;
    taskIds.forEach((id) => {
      dispatch(upsertWhiteboardItem({ updates: { id, projectId, origin: '' } }));
    });
  } catch (e) {
    console.log(e.message);
  }
});

const whiteboardItemsAdapter = createEntityAdapter({
  creating: false,
  loading: false,
  error: '',
  newTask: undefined
});

const whiteboardItemsSlice = createSlice({
  name: 'whiteboardItemsSlice',
  initialState: whiteboardItemsAdapter.getInitialState(),
  reducers: {
    upsertWhiteboardItemLocal: whiteboardItemsAdapter.upsertOne,
    addAllWhiteboardItems: whiteboardItemsAdapter.addMany,
    deleteWhiteboardItemLocal: whiteboardItemsAdapter.removeOne,
    upsertNewTask(state, action) {
      state.newTask = { ...state.newTask, ...action.payload };
    },
    clearNewTask(state, action) {
      state.newTask = undefined;
    }
  },
  extraReducers: {
    [upsertWhiteboardItem.fulfilled]: (state, { payload }) => {
      if (payload.status === 400) {
        upsertWhiteboardItemLocal(payload.item);
        state.error = payload.error;
      }
    },
    [deleteWhiteboardItem.fulfilled]: (state, { payload }) => {
      if (payload.status === 400) {
        upsertWhiteboardItemLocal(payload.item);
        state.error = payload.error;
      }
    },
  }
});
export const {
  upsertNewTask, clearNewTask, addAllWhiteboardItems,
  upsertWhiteboardItemLocal, deleteWhiteboardItemLocal
} = whiteboardItemsSlice.actions;
export default whiteboardItemsSlice.reducer;

export const whiteboardItemsSelectors = whiteboardItemsAdapter.getSelectors((state) => state.whiteboardItemsSlice);
