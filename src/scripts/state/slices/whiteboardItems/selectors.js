import { createSelector } from '@reduxjs/toolkit';
import moment from 'moment';
import auth from '@react-native-firebase/auth';
import { whiteboardItemsSelectors } from './whiteboardItemsSlice';
import { memoAllRecurringItems, memoCalendarRecurringItems, memoDateRecurringItems } from '../recurrentItems/selectors';
import { memoAllCalendarEvents, memoDateCalendarEvents } from '../calendars/selectors';
import { EVENT_COLORS, EVENT_COLORS_W_BODY } from '../../../../constants/event_colors';

const memoTodaysTasks = createSelector(
  (_, date) => date,
  whiteboardItemsSelectors.selectAll,
  (date, whiteboardItems) => {
    const me = auth().currentUser.uid;
    return whiteboardItems
      .filter((item) => {
        return item.id && item.type === 'task' && !item.recurrencePattern;
      })
      .filter((item) => {
        const roles = Object.keys(item.roles || {});
        if (roles.length > 1) return ((item.assignedTo?.length === 0 || item.assignedTo?.includes(me)) && item.deadline);
        else return true;
      })
      .filter(
        (task) => !(
          moment(task.scheduledDate || null).isBefore(date, 'day')
          && task.completed
        ),
      )
      .filter((task) => moment(task.scheduledDate || null).isSame(date, 'day'))
      .map((task) => ({
        ...task,
        scheduledDate:
          task.scheduledDate || task.deadline || task.finalDeadline || null,
      }))
      .map((task) => ({
        ...task,
        startDate: moment(task.scheduledDate),
        endDate: moment(task.scheduledDate).add(task.duration, 'hours'),
      }))
      .filter((task) => {
        return moment(date).isSame(task.endDate, 'day');
      })
      .sort(
        (a, b) => -b.startDate.diff(a.startDate),
      );
  }

);

const memoEvents = createSelector(
  (_, date) => date,
  whiteboardItemsSelectors.selectAll,
  (date, whiteboardItems) => {
    return whiteboardItems
      .map((item) => ({
        ...item,
        scheduledDate: item.scheduledDate ?? item.date,
      }))
      .filter((item) => item.type === 'event' && !item.recurrencePattern)
      .filter((item) => moment(item.scheduledDate).isSame(date, 'day'))
      .map((item) => ({
        ...item,
        startDate: moment(item.scheduledDate),
        endDate: moment(item.scheduledDate).add(item.duration, 'hours'),
      }));
  }
);

export const memoAllRecItems = createSelector(
  whiteboardItemsSelectors.selectAll,
  (whiteboardItems) => {
    return whiteboardItems
      .map((item) => ({
        ...item,
        scheduledDate: item.scheduledDate ?? item.date,
      }))
      .filter((task) => task.recurrencePattern);
  }
);

const memoActivityTasks = createSelector(
  (_, date) => date,
  whiteboardItemsSelectors.selectAll,
  (date, whiteboardItems) => {
    const me = auth().currentUser.uid;
    return (
      whiteboardItems
        .filter((item) => {
          const roles = Object.keys(item.roles || {});
          if (roles.length > 1) return item.assignedTo?.includes(me);
          else return true;
        })
        .map((item) => ({
          ...item,
          scheduledDate:
              item.scheduledDate || item.deadline || item.finalDeadline || null,
        }))
        .filter(
          (item) => item.type === 'task' && !item.recurrencePattern && item.allDay,
        )
        .filter((task) => moment(task.scheduledDate).isSame(date, 'day'))
        .map((task) => ({
          ...task,
          startDate: moment(task.scheduledDate),
          endDate: moment(task.scheduledDate).add(task.duration, 'hours'),
        }))
        .sort((a, b) => a.completed - b.completed)
    );
  }
);

export const memoActivityItems = createSelector(
  memoCalendarRecurringItems,
  memoActivityTasks,
  (state) => state.calFilter.filter,
  (recurringItems, activityTasks, filter) => {
    return [...recurringItems, ...activityTasks]
      .filter((eve) => {
        if (eve.calId) {
          return filter.event && (filter[eve.calId] !== undefined ? filter[eve.calId] : true);
        } else return filter[eve.type];
      }).sort(
        (a, b) => a.completed - b.completed,
      );
  }
);
const memoAllWhiteboardItems = createSelector(
  whiteboardItemsSelectors.selectAll,
  (whiteboardItems) => {
    const me = auth().currentUser.uid;
    return whiteboardItems.filter((item) => {
      const roles = Object.keys(item.roles || {});
      if (roles.length > 1) return item.assignedTo?.includes(me);
      else return true;
    })
      .map((item) => ({
        ...item,
        date: item.type === 'task' ? (item.scheduledDate || item.deadline || item.finalDeadline || null) : item.date
      }))
      .filter((item) => item.date && !item.recurrencePattern)
      .map((item) => ({
        ...item,
        color: EVENT_COLORS_W_BODY[item.color] ? item.color : EVENT_COLORS.GREEN,
        startDate: moment(item.date),
        endDate: moment(item.date).add(item.duration, 'hours'),
      }));
  }
);

export const memoAllEvents = createSelector(
  memoAllRecurringItems,
  memoAllWhiteboardItems,
  memoAllCalendarEvents,
  (state) => state.calFilter.filter,
  (recurringItems, whiteboardItems, calEvents, filter) => [...recurringItems, ...whiteboardItems, ...calEvents].filter((eve) => {
    if (eve.calId) {
      return filter.event && (filter[eve.calId] !== undefined ? filter[eve.calId] : true);
    } else return filter[eve.type];
  })
);

export const memoAllDayItems = createSelector(
  memoAllEvents,
  (allEvents) => allEvents.filter((item) => item.allDay && item.type === 'event')
);

export const memoAllSmallItems = createSelector(
  memoAllEvents,
  (allEvents) => allEvents.filter((item) => !item.allDay)
);

const memoAllHomeItems = createSelector(
  memoDateRecurringItems,
  memoEvents,
  memoDateCalendarEvents,
  (state) => state.calFilter.filter,
  (recurringItems, events, calEvents, filter) => {
    const recurringEvents = recurringItems.filter((e) => e.type === 'event');
    return [...recurringEvents, ...events, ...calEvents].filter(
      (item) => {
        if (item.calId) {
          return filter.event && (filter[item.calId] !== undefined ? filter[item.calId] : true);
        } else return filter[item.type];
      },
    );
  }
);

export const memoFormatAllEvents = createSelector(
  memoAllHomeItems,
  (allEvents) => [
    ...allEvents.filter((item) => item.allDay),
    ...allEvents
      .filter((item) => !item.allDay)
      .sort((a, b) => -b.startDate.diff(a.startDate)),
  ]
);

export const memoFormatAllHomeTasks = createSelector(
  memoDateRecurringItems,
  memoTodaysTasks,
  (state) => state.calFilter.filter,
  (recurringItems, todaysTasks, filter) => {
    const recurringTasks = recurringItems.filter((e) => e.type !== 'event');
    return [...recurringTasks, ...todaysTasks].filter((t) => filter[t.type]);
  }
);
