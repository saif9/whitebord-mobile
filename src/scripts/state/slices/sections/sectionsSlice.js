import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
} from '@reduxjs/toolkit';
import firestore from '@react-native-firebase/firestore';
import { isEmpty } from 'lodash';
import auth from '@react-native-firebase/auth';
import perf from '@react-native-firebase/perf';
import { Collections } from '../../../../constants/collections';
import { manageItemIdInUser } from '../user/userSlice';
import { deleteProjectFromFirestore } from '../projects/batchActions';
import { trackSectionCreation } from '../../../tracking';

export const createSection = (section, dispatch) => {
  const userId = auth().currentUser.uid;
  const { id, name, projects = [] } = section;
  if (name) {
    const docRef = firestore().collection(Collections.SECTIONS).doc();
    const newSection = {
      id: id || docRef.id,
      name,
      projects,
      roles: { [userId]: 'owner' }
    };
    dispatch(upsertSection({ updates: newSection }));
    trackSectionCreation();
    return docRef.id;
  }
  return null;
};

const deleteSectionFromFirestore = async (sectionId, projects = []) => {
  const trace = await perf().startTrace('delete_section'); // measures how long to delete
  firestore().collection(Collections.SECTIONS).doc(sectionId).delete();
  const deletions = projects.map((pId) => deleteProjectFromFirestore(pId));
  await Promise.all(deletions);
  trace.stop();
};

export const upsertSection = createAsyncThunk('sectionsSlice/upsertSection', async ({ updates, fromListener }, { getState, dispatch }) => {
  let oldItem;
  try {
    const me = auth().currentUser;
    const userId = me.uid;
    if (!userId) return { status: 400, error: 'No User' };
    const { id, ...newItem } = updates;
    if (isEmpty(newItem)) return { status: 400, error: 'No Item' };
    oldItem = sectionsSelectors.selectById(getState(), id);
    newItem.id = id;
    if (fromListener) dispatch(upsertSectionLocal(newItem));
    else firestore().collection(Collections.SECTIONS).doc(id).set(newItem, { merge: true });
    dispatch(manageItemIdInUser({ updates: { sectionId: id }, fromListener }));
    return { status: 200 };
  } catch (e) {
    console.log(e);
    return { status: 400, error: e.message, item: oldItem };
  }
});

export const manageItemIdInSection = createAsyncThunk('sectionsSlice/manageItemIdInSection', async ({ updates, fromListener }, { getState, dispatch }) => {
  let item;
  try {
    const {
      sectionId, projectId, isDeleted
    } = updates; // fromListener will ignore firestore update
    item = sectionsSelectors.selectById(getState(), sectionId);
    if (!item) return { status: 400, error: 'No Item' };
    const sectionRef = firestore().collection(Collections.SECTIONS).doc(sectionId);
    if (!fromListener) {
      if (isDeleted) {
        sectionRef.update({ projects: firestore.FieldValue.arrayRemove(projectId) });
      } else if (!(item.projects || []).includes(projectId)) {
        sectionRef.update({ projects: firestore.FieldValue.arrayUnion(projectId) });
      }
    }
    return { status: 200, item };
  } catch (e) {
    console.log(e);
    return { status: 400, error: e.message, item };
  }
});

export const deleteSection = createAsyncThunk('sectionsSlice/deleteSection', async ({ updates, fromListener }, { getState, dispatch }) => {
  let oldItem;
  try {
    const me = auth().currentUser;
    const userId = me.uid;
    if (!userId) return { status: 400, error: 'No User' };
    const { id } = updates;
    oldItem = sectionsSelectors.selectById(getState(), id);
    if (isEmpty(oldItem)) return { status: 400, error: 'No Item' };
    dispatch(manageItemIdInUser({ updates: { sectionId: id, isDeleted: true }, fromListener }));
    if (fromListener) dispatch(deleteSectionLocal(id));
    else deleteSectionFromFirestore(id, oldItem.projects);
    return { status: 200 };
  } catch (e) {
    console.log(e.message);
    return { status: 400, error: e.message, item: oldItem };
  }
});

const sectionsAdapter = createEntityAdapter({
  creating: false,
  loading: false,
  error: '',
});

const sectionsSlice = createSlice({
  name: 'sectionsSlice',
  initialState: sectionsAdapter.getInitialState(),
  reducers: {
    upsertSectionLocal: sectionsAdapter.upsertOne,
    addAllSections: sectionsAdapter.addMany,
    deleteSectionLocal: sectionsAdapter.removeOne,
  },
  extraReducers: {
    [upsertSection.fulfilled]: (state, { payload }) => {
      if (payload.status === 400) {
        upsertSectionLocal(payload.item);
        state.error = payload.error;
      }
    },
    [manageItemIdInSection.fulfilled]: (state, { payload }) => {
      if (payload.status === 400) {
        upsertSectionLocal(payload.item);
        state.error = payload.error;
      }
    },
  }
});
export const { addAllSections, upsertSectionLocal, deleteSectionLocal } = sectionsSlice.actions;
export default sectionsSlice.reducer;

export const sectionsSelectors = sectionsAdapter.getSelectors((state) => state.sectionsSlice);
