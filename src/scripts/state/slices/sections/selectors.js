import { createSelector } from '@reduxjs/toolkit';
import auth from '@react-native-firebase/auth';
import { uniq, uniqBy } from 'lodash';
import { sectionsSelectors } from './sectionsSlice';
import { projectsSelectors } from '../projects/projectsSlice';

export const memoAllSections = createSelector(
  sectionsSelectors.selectEntities,
  (state) => state.userSlice.user.sections,
  (sections, sortedIds = []) => {
    const userId = auth().currentUser.uid;
    if (!sections[`${userId}shared`]) {
      sections[`${userId}shared`] = {
        name: 'Shared with me',
        projects: [],
        roles: { [userId]: 'owner' }
      };
    }
    return Object.values(sections).sort((secA, secB) => sortedIds.indexOf(secA.id) - sortedIds.indexOf(secB.id));
  }
);

export const memoSectionById = () => createSelector(
  (state, { sectionId }) => sectionsSelectors.selectById(state, sectionId),
  projectsSelectors.selectEntities,
  memoAllSections,
  (_, { collapseIds }) => collapseIds,
  (section, projectsObj, sections, collapseIds) => {
    const userId = auth().currentUser.uid;
    const { id: secId } = section;
    let { projects = [] } = section;
    projects = uniq(projects.filter((pId) => projectsObj[pId]?.roles[userId]));
    if (secId === `${userId}shared`) {
      Object.keys(projectsObj).forEach((projId) => {
        const inOtherSection = sections.find((sec) => sec.projects.includes(projId));
        if (!inOtherSection) {
          projects = [...projects, projId];
        }
      });
    }
    if (collapseIds[secId]) { // remove projects from list in case of collapsed state
      projects = [];
    } else projects = projects.map((pId) => projectsObj[pId]); // add projects from list in case of non-collapsed state
    return [
      section,
      ...projects
    ];
  }
);

export const memoWorkspaceList = createSelector(
  memoAllSections,
  (state) => state,
  (_, collapseIds) => collapseIds, // Passed Single variable so no destructing required.
  (sections, state, collapseIds) => {
    const sectionSelector = memoSectionById();
    // flatMap will change the 2d Array to 1d Array, => Sections and projects in same array for single draggable list
    const sortedFilledSections = sections.flatMap((sec) => sectionSelector(state, { sectionId: sec.id, collapseIds }));
    return uniqBy(sortedFilledSections, 'id');
  }
);
