import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import perf from '@react-native-firebase/perf';
import { Collections } from '../../../../constants/collections';
import { addAllSections, deleteSection, upsertSection } from './sectionsSlice';

export const fetchandTrackSections = async (dispatch, loaded) => {
  try {
    const trace = await perf().startTrace('subscribe_sections'); // measures how long to fetch
    let initLoading = true;
    const initialItems = [];
    const me = auth().currentUser;
    if (!me.uid) throw new Error('No User');
    const subscriber = firestore().collection(Collections.SECTIONS)
      .where(`roles.${me.uid}`, 'in', ['owner', 'collaborator'])
      .onSnapshot((querySnapshot) => {
        querySnapshot?.docChanges().forEach((change) => {
          const item = { id: change.doc.id, ...change.doc.data() };
          if (change.type === 'added') {
            if (initLoading) {
              initialItems.push(item);
            } else {
              dispatch(upsertSection({ updates: item, fromListener: true }));
            }
          }
          if (change.type === 'modified') {
            dispatch(upsertSection({ updates: item, fromListener: true }));
          }
          if (change.type === 'removed') {
            dispatch(deleteSection({ updates: item, fromListener: true }));
          }
        });
        if (initLoading) {
          dispatch(addAllSections(initialItems));
          loaded();
        }

        trace.stop(); // stops trace
        initLoading = false;
      });
    return subscriber;
  } catch (e) {
    console.log(e);
    throw e;
  }
};
