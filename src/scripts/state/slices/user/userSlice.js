import {
  createAsyncThunk,
  createSlice,
} from '@reduxjs/toolkit';
import firestore from '@react-native-firebase/firestore';
import { isEmpty } from 'lodash';
import auth from '@react-native-firebase/auth';
import { Collections } from '../../../../constants/collections';

export const upsertUser = createAsyncThunk('usersSlice/upsertUser', async ({ updates, fromListener }, { getState, dispatch }) => {
  let oldItem;
  try {
    const me = auth().currentUser;
    const userId = me.uid;
    if (!userId) return { status: 400, error: 'No User' };
    const { ...newItem } = updates;
    if (isEmpty(newItem)) return { status: 400, error: 'No Item' };
    oldItem = getState().userSlice.user;
    newItem.id = userId;
    if (fromListener) dispatch(upsertUserLocal(newItem));
    else firestore().collection(Collections.USERS).doc(userId).set(newItem, { merge: true });
    return { status: 200 };
  } catch (e) {
    console.log(e.message);
    return { status: 400, error: e.message, item: oldItem };
  }
});

export const manageItemIdInUser = createAsyncThunk('usersSlice/manageItemIdInUser', async ({ updates, fromListener }, { getState, dispatch }) => {
  let item;
  try {
    const me = auth().currentUser;
    const userId = me.uid;
    const {
      sectionId, isDeleted
    } = updates; // fromListener will ignore firestore update
    item = getState().userSlice.user;
    if (!item) return { status: 400, error: 'No Item' };
    const userRef = firestore().collection(Collections.USERS).doc(userId);
    if (isDeleted) {
      item.sections = (item.sections || []).filter((id) => id !== sectionId);
      if (fromListener) dispatch(upsertUserLocal(item));
      else userRef.update({ sections: firestore.FieldValue.arrayRemove(sectionId) });
    } else {
      if (!(item.sections || []).includes(sectionId)) { item.sections = [...item.sections, sectionId]; }
      if (fromListener) dispatch(upsertUserLocal(item));
      else userRef.update({ sections: firestore.FieldValue.arrayUnion(sectionId) });
    }
    return { status: 200, item };
  } catch (e) {
    console.log(e.message);
    return { status: 400, error: e.message, item };
  }
});

const usersSlice = createSlice({
  name: 'usersSlice',
  initialState: {
    user: {},
    loading: false,
    error: ''
  },
  reducers: {
    upsertUserLocal(state, { payload }) {
      state.user = { ...state.user, ...payload };
    },
  },
  extraReducers: {
    [upsertUser.fulfilled]: (state, { payload }) => {
      upsertUserLocal(payload.item);
      if (payload.status === 400) {
        state.error = payload.error;
      }
    },
    [manageItemIdInUser.fulfilled]: (state, { payload }) => {
      upsertUserLocal(payload.item);
      if (payload.status === 400) {
        state.error = payload.error;
      }
    },
  }
});
export const { addAllUsers, upsertUserLocal, deleteUserLocal } = usersSlice.actions;
export default usersSlice.reducer;
