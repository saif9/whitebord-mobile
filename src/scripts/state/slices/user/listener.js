import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import perf from '@react-native-firebase/perf';
import { Collections } from '../../../../constants/collections';
import { upsertUser } from './userSlice';

export const fetchandTrackUser = async (dispatch, loaded) => {
  try {
    const trace = await perf().startTrace('subscribe_user');
    let initLoading = true;
    const me = auth().currentUser;
    if (!me.uid) throw new Error('No User');
    const subscriber = firestore().collection(Collections.USERS)
      .doc(me.uid)
      .onSnapshot((documentSnapshot) => {
        const user = { id: me.uid, ...documentSnapshot?.data() };
        dispatch(upsertUser({ updates: user, fromListener: true }));
        if (initLoading) {
          initLoading = false;
          loaded();
          trace.stop();
        }
      });
    return subscriber;
  } catch (e) {
    console.log(e);
    throw e;
  }
};
