import { createSelector } from '@reduxjs/toolkit';
import moment from 'moment';
import { eventsSelectors } from './calendarSlice';

export const memoDateCalendarEvents = createSelector(
  (_, date) => date,
  eventsSelectors.selectAll,
  (date, events) => events.map((eve) => ({
    ...eve,
    startDate: moment(eve.startDate),
    endDate: moment(eve.endDate),
  })).filter((item) => item.startDate.isSame(date, 'day'))
);

export const memoAllCalendarEvents = createSelector(
  eventsSelectors.selectAll,
  (events) => events.map((eve) => ({
    ...eve,
    startDate: moment(eve.startDate),
    endDate: moment(eve.endDate),
    duration: moment(eve.endDate).diff(moment(eve.startDate), 'm') / 60
  }))
);
