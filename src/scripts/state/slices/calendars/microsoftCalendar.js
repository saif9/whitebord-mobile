import axios from 'axios';
import perf from '@react-native-firebase/perf';
import { EVENT_COLORS } from '../../../../constants/event_colors';
import { getMicrosoftToken } from '../../../screens/user/auth/Microsoft';

const MS_ROOT = 'https://graph.microsoft.com/v1.0/me';

export const fetchMicrosoftCalendar = async (timeMinString, timeMaxString) => {
  const data = {};
  const mToken = await getMicrosoftToken();
  if (!mToken) throw new Error('Invalid Microsoft token');
  const trace = await perf().startTrace('fetch_microsoft'); // measures how long to fetch microsoft

  const headers = { headers: { Authorization: `Bearer ${mToken}`, Prefer: 'outlook.body-content-type="text"' } };
  const res = await axios.get(`${MS_ROOT}/calendars`, headers);
  data.calendars = res.data.value;
  const eveRes = await Promise.all(data.calendars.map((cal) => axios.get(`${MS_ROOT}/calendars/${encodeURIComponent(cal.id)}/calendarview?top=1000&startDateTime=${encodeURIComponent(timeMinString)}&endDateTime=${encodeURIComponent(timeMaxString)}`, headers)));
  const events = eveRes.map((calEvents, i) => {
    return calEvents.data.value.map((eve) => ({
      ...eve,
      calId: data.calendars[i].id,
      name: eve.subject,
      startDate: (eve.start.dateTime),
      endDate: (eve.end.dateTime),
      type: 'event',
      provider: 'microsoft',
      color: EVENT_COLORS.GREEN,
      allDay: eve.isAllDay,
    }));
  });
  data.events = events.flat();

  trace.stop(); // stops trace
  return data;
};
