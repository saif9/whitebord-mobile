import axios from 'axios';
import perf from '@react-native-firebase/perf';
import { EVENT_COLORS } from '../../../../constants/event_colors';
import { getGoogleToken } from '../../../screens/user/auth/Google';

const GCAL_ROOT = 'https://www.googleapis.com/calendar/v3';

export const fetchGoogleCalendar = async (timeMinString, timeMaxString) => {
  const data = {};
  const gToken = await getGoogleToken();
  if (!gToken) throw new Error('Invalid Google token');
  const trace = await perf().startTrace('fetch_google'); // measures how long to fetch google

  const headers = { headers: { Authorization: `Bearer ${gToken}` } };
  const res = await axios.get(`${GCAL_ROOT}/users/me/calendarList`, headers);
  data.calendars = res.data.items.map((cal) => ({
    ...cal,
    name: cal.summary
  }));
  const calls = data.calendars.map((cal) => axios.get(`${GCAL_ROOT}/calendars/${encodeURIComponent(cal.id)}/events?timeMin=${encodeURIComponent(timeMinString)}`
  + `&timeMax=${encodeURIComponent(timeMaxString)}&singleEvents=true`, headers));
  const eveRes = await Promise.all(calls);
  const events = eveRes.map((calEvents, i) => {
    return calEvents.data.items.map((eve) => ({
      ...eve,
      calId: data.calendars[i].id,
      name: eve.summary,
      startDate: (eve.start.dateTime || eve.start.date),
      endDate: (eve.end.dateTime || eve.end.date),
      type: 'event',
      provider: 'google',
      color: EVENT_COLORS.GREEN,
      allDay: !!eve.start.date,
    }));
  });
  data.events = events.flat();

  trace.stop(); // stops trace
  return data;
};
