import {
  createEntityAdapter,
  createSlice,
  createAsyncThunk,
  current,
} from '@reduxjs/toolkit';
import moment from 'moment';
// import moment from 'moment';
import perf from '@react-native-firebase/perf';
import { fetchGoogleCalendar } from './googleCalendar';
import { fetchMicrosoftCalendar } from './microsoftCalendar';

export const fetchEvents = createAsyncThunk('calendars/fetchEvents', async (dateRange, { getState, dispatch }) => {
  const trace = await perf().startTrace('fetch_events');
  // console.log(moment(timeMinString).toLocaleString(), moment(timeMaxString).toLocaleString());
  let { timeMinString, timeMaxString } = dateRange;
  const { minTime, maxTime } = getState().calendars.dateRange;

  if (moment(timeMinString).isBefore(moment(minTime))) {
    timeMinString = moment(timeMinString).startOf('month').toISOString();
    timeMaxString = moment(maxTime).toISOString();
    dispatch(updateDateRange({ minTime: timeMinString, maxTime: timeMaxString }));
  }
  if (moment(timeMaxString).isAfter(moment(maxTime))) {
    timeMinString = moment(minTime).toISOString();
    timeMaxString = moment(timeMaxString).endOf('month').toISOString();
    dispatch(updateDateRange({ minTime: timeMinString, maxTime: timeMaxString }));
  }

  const res = await Promise.allSettled([fetchGoogleCalendar(timeMinString, timeMaxString), fetchMicrosoftCalendar(timeMinString, timeMaxString)]);
  const data = {};
  if (res[0].status === 'fulfilled') {
    data.google = res[0].value;
  }
  if (res[1].status === 'fulfilled') {
    data.microsoft = res[1].value;
  }

  trace.stop();
  return { data, dateRange: { timeMinString, timeMaxString } };
});

const calendarsAdapter = createEntityAdapter();

const eventsAdapter = createEntityAdapter();

const INITIAL_STATE = {
  dateRange: { minTime: moment().toISOString(), maxTime: moment().toISOString() },
  loading: false,
  events: eventsAdapter.getInitialState()
};
const calendarsSlice = createSlice({
  name: 'calendars',
  initialState: calendarsAdapter.getInitialState(INITIAL_STATE),
  reducers: {
    addMany: calendarsAdapter.addMany,
    updateDateRange(state, action) {
      state.dateRange = action.payload;
    },
    removeAllCalendars: calendarsAdapter.removeAll,
    removeAllEvents(state, action) {
      eventsAdapter.removeAll(state.events);
      state.dateRange = { minTime: moment().toISOString(), maxTime: moment().toISOString() };
    }
  },
  extraReducers: {
    [fetchEvents.pending]: (state) => {
      state.loading = true;
    },
    [fetchEvents.fulfilled]: (state, { payload }) => {
      const { data, dateRange } = payload;
      const { timeMinString, timeMaxString } = dateRange;
      const queryStartTime = moment(timeMinString);
      const queryEndTime = moment(timeMaxString);
      const { google, microsoft } = data;
      const calendars = [];
      const events = [];
      if (google) {
        calendars.push(google.calendars);
        events.push(google.events);
      }
      if (microsoft) {
        calendars.push(microsoft.calendars);
        events.push(microsoft.events);
      }
      const newCalendars = calendars.flat();
      const newEvents = events.flat();
      const prevCalendars = state;
      const prevEvents = state.events;
      const deletedCalendars = current(prevCalendars.ids).filter(
        (x) => !newCalendars.map((it) => it.id).includes(x),
      );
      let deletedCalendarEvents = [];

      deletedCalendarEvents = current(prevEvents.ids).filter(
        (x) => {
          const eventData = current(prevEvents.entities[x]);
          return !deletedCalendars.includes(eventData.calId) // is event's calendar deleted
              && moment(eventData.startDate).isBetween(queryStartTime, queryEndTime) // Is this event exists between update range. if true,
              && !newEvents.map((it) => it.id).includes(x); // then, newEvents must include this event, otherwise delete it.
        },
      );

      state.loading = false;
      calendarsAdapter.upsertMany(state, newCalendars);
      eventsAdapter.upsertMany(state.events, newEvents);

      calendarsAdapter.removeMany(state, deletedCalendars);
      eventsAdapter.removeMany(state.events, deletedCalendarEvents);
    },
    [fetchEvents.rejected]: (state, { error }) => {
      state.loading = false;
      console.log(error);
    }
  }
});
export const {
  addMany, updateDateRange, removeAllCalendars, removeAllEvents
} = calendarsSlice.actions;
export default calendarsSlice.reducer;

export const eventsSelectors = eventsAdapter.getSelectors((state) => state.calendars.events);

export const calendarsSelectors = eventsAdapter.getSelectors((state) => state.calendars);
