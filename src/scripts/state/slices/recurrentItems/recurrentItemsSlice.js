import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
} from '@reduxjs/toolkit';
import debounce from 'lodash.debounce';
import moment from 'moment';
import { generateRecurrences } from '../../../utils';

const handler = async (payload, { getState, dispatch }) => {
  const {
    dateRange,
    forceReload,
  } = payload || {};
  const state = getState();
  let { minTime, maxTime } = state.recurrentItems.dateRange;
  if (dateRange) {
    // dateRange won't be available if called from createItem or updateItem actions in whiteboard actions
    const { timeMinString, timeMaxString } = dateRange;
    if (
      !forceReload
      && moment(timeMinString).isBetween(minTime, maxTime)
      && moment(timeMaxString).isBetween(minTime, maxTime)
    ) {
      return;
    }
    minTime = moment(timeMinString).startOf('month');
    maxTime = moment(timeMaxString).endOf('month');
  }
  const items = Object.values(state.whiteboardItemsSlice.entities).map((item) => ({
    ...item,
    scheduledDate: item.scheduledDate ?? item.date,
  })).filter((task) => task.recurrencePattern);
  const exceptionsData = state.exceptionsSlice.entities;
  const recs = items.map((item) => generateRecurrences(
    item.id,
    item,
    minTime.toISOString(),
    moment(maxTime).add(1, 'd').toISOString(),
    exceptionsData,
  ));
  const updateItems = recs.flat().filter((i) => i);
  dispatch(setDateRange({ minTime, maxTime }));
  dispatch(setAllRecurrentItems(updateItems));
};

const debouncedHandler = debounce(handler, 500, { leading: false });
export const generateRecurrentItems = createAsyncThunk(
  'recurrentItems/generateRecurrentItems',
  debouncedHandler
);

const recurrentItemsAdapter = createEntityAdapter();
const INITIAL_STATE = {
  dateRange: { minTime: moment(), maxTime: moment() },
  items: recurrentItemsAdapter.getInitialState()
};

const recurrentItemsSlice = createSlice({
  name: 'recurrentItems',
  initialState: INITIAL_STATE,
  reducers: {
    setDateRange(state, action) {
      state.dateRange = action.payload;
    },
    setAllRecurrentItems(state, action) {
      recurrentItemsAdapter.setAll(state.items, action.payload);
    },
  },
  extraReducers: {},
});
export const { setDateRange, setAllRecurrentItems } = recurrentItemsSlice.actions;
export default recurrentItemsSlice.reducer;
export const recurringSelectors = recurrentItemsAdapter.getSelectors(
  (state) => state.recurrentItems.items,
);
