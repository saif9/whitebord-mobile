import { createSelector } from '@reduxjs/toolkit';
import moment from 'moment';
import { recurringSelectors } from './recurrentItemsSlice';

export const memoDateRecurringItems = createSelector(
  (_, date) => date,
  recurringSelectors.selectAll,
  (date, recurrings) => recurrings.map((task) => ({
    ...task,
    startDate: moment(task.scheduledDate),
    endDate: moment(task.scheduledDate).add(task.duration, 'hours'),
  }))
    .filter((task) => moment(date).isSame(task.endDate, 'day'))
    .map((task) => ({
      ...task,
      itemToEdit: {
        ...task.itemToEdit,
        startDate: moment(task.itemToEdit.scheduledDate),
        endDate: moment(task.itemToEdit.scheduledDate).add(
          task.itemToEdit?.duration,
          'hours',
        ),
      },
    }))
    .sort((a, b) => a.type.localeCompare(b.type))
);

export const memoCalendarRecurringItems = createSelector(
  (_, date) => date,
  recurringSelectors.selectAll,
  (date, recurrings) => {
    return recurrings.map((task) => ({
      ...task,
      startDate: moment(task.scheduledDate),
      endDate: moment(task.scheduledDate).add(task.duration, 'hours'),
    }))
      .filter(
        (task) => task.allDay && moment(task.scheduledDate).isSame(date, 'day'),
      )
      .map((task) => ({
        ...task,
        itemToEdit: {
          ...task.itemToEdit,
          startDate: moment(task.itemToEdit.scheduledDate),
          endDate: moment(task.itemToEdit.scheduledDate).add(
            task.itemToEdit?.duration,
            'hours',
          ),
        },
      }))
      .sort((a, b) => a.type.localeCompare(b.type));
  }
);

export const memoAllRecurringItems = createSelector(
  recurringSelectors.selectAll,
  (recurrings) => recurrings
    .map((task) => ({
      ...task,
      startDate: moment(task.scheduledDate),
      endDate: moment(task.scheduledDate).add(task.duration, 'hours'),
    }))
    .map((task) => ({
      ...task,
      itemToEdit: {
        ...task.itemToEdit,
        startDate: moment(task.itemToEdit.scheduledDate),
        endDate: moment(task.itemToEdit.scheduledDate).add(task.itemToEdit?.duration, 'hours'),
      }
    }))
    .sort((a, b) => a.type.localeCompare(b.type))
);

export const memoNextRecurringItem = () => createSelector(
  (_, id) => id,
  recurringSelectors.selectAll,
  (id, recurrings) => {
    const recurringsList = recurrings
      .filter((item) => item.id.startsWith(id))
      .map((task) => ({
        ...task,
        startDate: moment(task.scheduledDate),
        endDate: moment(task.scheduledDate).add(task.duration, 'hours'),
      }))
      .map((task) => ({
        ...task,
        itemToEdit: {
          ...task.itemToEdit,
          startDate: moment(task.itemToEdit.scheduledDate),
          endDate: moment(task.itemToEdit.scheduledDate).add(task.itemToEdit?.duration, 'hours'),
        }
      }))
      .filter((task) => !task.completed)
      .sort((a, b) => moment(a.startDate).diff(moment(b.startDate)));
    return recurringsList[0];
  }
);
