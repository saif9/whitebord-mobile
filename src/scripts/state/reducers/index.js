import modal from './modal';

const rootReducer = {
  modal
};

export default rootReducer;
