import { ActionTypes } from '../actions/types';

// initial state
const modal = {
  isOpen: false,
  editing: false,
};

// reducer
const ModalReducer = (state = modal, action) => {
  switch (action.type) {
    case ActionTypes.SET_MODAL:
      return action.payload;

    case ActionTypes.TOGGLE_MODAL:
      return { ...state, isOpen: action.payload };

    case ActionTypes.TOGGLE_EDIT_MODE:
      return { ...state, editing: action.payload };

    case ActionTypes.RESET_MODE:
      return { ...action.payload };

    default:
      return state;
  }
};

export default ModalReducer;
