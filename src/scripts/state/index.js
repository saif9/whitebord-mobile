import { combineReducers, configureStore } from '@reduxjs/toolkit';
import reducers from './reducers';
import slices from './slices';

const appReducer = combineReducers({
  ...reducers,
  calendars: slices.calendarsSlice,
  publicProfiles: slices.publicProfilesSlice,
  calFilter: slices.filterSlice,
  recurrentItems: slices.recurrentItemsSlice,
  whiteboardItemsSlice: slices.whiteboardItemsSlice,
  projectsSlice: slices.projectsSlice,
  sectionsSlice: slices.sectionsSlice,
  userSlice: slices.userSlice,
  quickTaskSlice: slices.quickTaskSlice,
  exceptionsSlice: slices.exceptionsSlice
});

const rootReducer = (state, action) => {
  // when a logout action is dispatched it will reset redux state
  if (action.type === 'USER_LOGGED_OUT') {
    state = undefined;
  }

  return appReducer(state, action);
};

const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: false,
  }),
  devTools: true
});

export default store;
