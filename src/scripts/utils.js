import moment from 'moment';
import { PixelRatio } from 'react-native';
import auth from '@react-native-firebase/auth';
import { isEmpty } from 'lodash';
import { useEffect } from 'react';
import { layout } from '../constants';

const { width, height } = layout.window;

export const wp = (widthPercent) => {
  // Parse string percentage input and convert it to number.
  const elemWidth = typeof widthPercent === 'number' ? widthPercent : parseFloat(widthPercent);

  // Use PixelRatio.roundToNearestPixel method in order to round the layout
  // size (dp) to the nearest one that corresponds to an integer number of pixels.
  return PixelRatio.roundToNearestPixel(width * (elemWidth / 100));
};
export const hp = (heightPercent) => {
  // Parse string percentage input and convert it to number.
  const elemHeight = typeof heightPercent === 'number' ? heightPercent : parseFloat(heightPercent);

  // Use PixelRatio.roundToNearestPixel method in order to round the layout
  // size (dp) to the nearest one that corresponds to an integer number of pixels.
  return PixelRatio.roundToNearestPixel(height * (elemHeight / 100));
};

export const removeHTMLTags = (text) => { return text ? text.replace(/<(.|\n)*?>/g, '').replace('&nbsp;', ' ') : text; };

export const getSharedSectionId = () => {
  const me = auth().currentUser;
  return `${me.uid}shared`;
};

export function delay(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export function getOwner(roles) {
  if (roles) {
    for (const k in roles) {
      if (roles[k] === 'owner') return k;
    }
  }
  return undefined;
}

export function rolesChanged(roles1, roles2) {
  if (!roles1 || !roles2) {
    return !roles1 !== !roles2;
  }
  for (const key in roles1) {
    if (roles1[key] !== roles2[key]) return true;
  }

  for (const key in roles2) {
    if (roles1[key] !== roles2[key]) return true;
  }

  return false;
}

export function getInheritedRoles(state, projectId, parentTaskId) {
  let inheritedRoles;
  if (parentTaskId) {
    const parentTask = state.whiteboardItemsSlice.entities[parentTaskId];
    if (!parentTask) return undefined;
    inheritedRoles = { ...parentTask.roles };
  } else if (projectId) {
    const projectState = state.projectsSlice.entities[projectId];
    if (!projectState) return undefined;
    inheritedRoles = { ...projectState.roles };
  } else {
    return null;
  }

  for (const uid of Object.keys(inheritedRoles)) {
    if (!inheritedRoles[uid] === 'owner') {
      inheritedRoles[uid] = 'collaborator';
    }
  }
  return inheritedRoles;
}
// It will prepare roles as a whole whenever some role is changed, or some project is added to backup project
export function prepareRoles(state, currentUserUid, whiteboardItem, data) {
  const result = { ...data };
  const existingItem = whiteboardItem;
  const taskOwner = getOwner(existingItem?.roles) || getOwner(data?.roles);
  const isTaskOwner = taskOwner === currentUserUid;
  let projectID = result.projectId;
  result.backupProjectId = { ...(existingItem?.backupProjectId || {}) };
  if (!isTaskOwner && existingItem && result.projectId !== undefined && result.projectId !== existingItem.projectId) {
    result.backupProjectId = { ...existingItem?.backupProjectId, [currentUserUid]: result.projectId };
    result.projectId = existingItem.projectId;
    projectID = existingItem.projectId;
  }

  if (result.roles) {
    const parentTaskId = result.parentTask !== undefined ? result.parentTask : existingItem?.parentTask;
    result.inheritedRoles = getInheritedRoles(state, projectID, parentTaskId) || existingItem?.inheritedRoles || {};
    result.roles = { ...result.inheritedRoles, ...result.roles };
  }

  if (existingItem && result.roles) {
    for (const k in existingItem.roles) {
      if (existingItem.roles[k] === 'owner') {
        result.roles[k] = 'owner';
      }
    }
  }

  return result;
}

function parseDate(input, defaultValue = {}) {
  if (!input) {
    return defaultValue === {} ? new Date() : defaultValue;
  }

  if (typeof input === 'string' && input.length === 10) {
    const d = input.split('-');
    if (d.length === 3) {
      return new Date(d[0], d[1] - 1, d[2]);
    }
  }
  return new Date(input);
}

function formatDate(date) {
  if (!date) return date;
  const d = date instanceof Date ? date : parseDate(date);
  let month = d.getMonth() + 1;
  let day = d.getDate();
  if (month < 10) month = `0${month}`;
  if (day < 10) day = `0${day}`;
  return [d.getFullYear(), month, day].join('-');
}

const millisecondsInDay = 1000 * 60 * 60 * 24;
/**
 * It is calculating the recurrent instances based on recurrent pattern.
 * Instances will be created in memory and get replaced if corresponding exception exists.
 * @param {*} masterItemId it is recurrent item id
 * @param {*} masterItem it is recurrent item
 * @param {*} start start date of instance calculation
 * @param {*} end end date of instance calculation
 * @param {*} exceptions checking for exceptions already present (updating instance will create corresponding exception to store changes)
 * @returns recurrent item instances between start date and end date
 */

export const generateRecurrences = (masterItemId, masterItem, start, end, exceptions) => {
  const { allDay, recurrencePattern } = masterItem;
  const dates = generateRecurrentDates(recurrencePattern, start, end);
  if (!dates?.length) return undefined;
  const result = [];
  for (const occurenceId of dates) {
    let date = occurenceId;
    const itemId = `${masterItemId}_${occurenceId}`;
    const exception = exceptions?.[itemId];
    if (!exception?.deleted) {
      const allDay2 = exception?.allDay;
      const allDay3 = allDay2 !== undefined ? allDay2 : allDay;
      if (!allDay3 && !exception?.date) {
        const time = parseDate(masterItem.recurrenceTime || recurrencePattern.recurrenceStartDate);
        const dt = parseDate(date);
        dt.setHours(time.getHours());
        dt.setMinutes(time.getMinutes());
        date = dt.toISOString();
      }

      const item = {
        ...masterItem,
        completed: false,
        date,
        itemToEdit: { ...masterItem },
        scheduledDate: exception?.date || date,
        ...exception,
        parentItemId: masterItemId,
        id: itemId,
        isRecurrenceItem: true,
      };

      result.push(item);
    }
  }

  return result;
};
// returning dates according to the pattern
const generateRecurrentDates = (pattern, start, end, callback) => {
  if (!pattern) return undefined;
  const sFirst = formatDate(pattern.recurrenceStartDate);
  let sFinish;
  if (pattern.recurrenceEndType === 'never') { // pattern is repeating forever
    sFinish = formatDate(end);
  } else {
    sFinish = parseDate(pattern.recurrenceEndDate);
    sFinish.setDate(sFinish.getDate() + 1);
    sFinish = formatDate(sFinish);
  }
  const sFinish2 = end ? formatDate(end) : sFinish;
  if (sFinish2 < sFinish) sFinish = sFinish2;
  const finishAsDate = parseDate(sFinish);
  const firstAsDate = parseDate(sFirst);
  const frequency = pattern.frequency || 1;
  let sStart = start ? formatDate(start) : sFirst;
  if (sStart < sFirst) sStart = sFirst;
  if (sFinish < sStart) return undefined;
  const result = [];
  if (pattern.recurrenceType === 'day') { // pattern is repeating for each day
    if (sFirst < sStart) {
      const periods = Math.ceil((parseDate(sStart).getTime() - firstAsDate.getTime()) / millisecondsInDay / frequency);
      firstAsDate.setDate(firstAsDate.getDate() + (periods * frequency));
    }

    while (firstAsDate < finishAsDate) {
      const ds = formatDate(firstAsDate);
      if (callback) {
        if (callback(ds)) return [ds];
      } else {
        result.push(ds);
      }

      firstAsDate.setDate(firstAsDate.getDate() + frequency);
    }
  } else if (pattern.recurrenceType === 'month') { // pattern is repeating for each month
    let delta = 0;
    if (sFirst < sStart) {
      const startAsDate = parseDate(sStart);
      const firstMonth = firstAsDate.getFullYear() * 12 + firstAsDate.getMonth();
      const startMonth = startAsDate.getFullYear() * 12 + startAsDate.getMonth();
      delta = (Math.floor((startMonth - firstMonth) / frequency) * frequency);
    }

    const year = firstAsDate.getFullYear();
    const month = firstAsDate.getMonth();
    const date = firstAsDate.getDate();

    let dt = new Date(year, month + delta, date);
    while (dt < finishAsDate) {
      const d = formatDate(dt);
      if (sStart <= d) {
        if (callback) {
          if (callback(d)) return [d];
        } else {
          result.push(d);
        }
      }
      delta += frequency;
      dt = new Date(year, month + delta, date);
    }
  } else if (pattern.recurrenceType === 'week') { // pattern is repeating weekly
    const { daysOfWeek } = pattern;
    if (!daysOfWeek?.length) return [];
    const days = new Array(7);
    for (const i of daysOfWeek) {
      days[i] = true;
    }

    if (sFirst < sStart) {
      const periods = Math.floor((parseDate(sStart).getTime() - firstAsDate.getTime()) / millisecondsInDay / frequency / 7);
      firstAsDate.setDate(firstAsDate.getDate() + (periods * frequency * 7));
    }

    const year = firstAsDate.getFullYear();
    const month = firstAsDate.getMonth();

    let dow = firstAsDate.getDay();
    const dom = firstAsDate.getDate();
    const count = Math.ceil((finishAsDate.getTime() - firstAsDate.getTime()) / millisecondsInDay);
    const lastDom = dom + count - 1;
    let i = dom;
    let dow2 = 0;
    const blankDays = Math.max(0, frequency - 1) * 7;
    while (i <= lastDom) {
      if (days[dow]) {
        const dt = new Date(year, month, i);
        const d = formatDate(dt);
        if (sStart <= d && dt < finishAsDate) {
          if (callback) {
            if (callback(d)) return [d];
          } else {
            result.push(d);
          }
        }
      }

      i += 1;
      dow += 1;
      dow2 += 1;
      if (dow >= 7) dow = 0;
      if (dow2 >= 7) {
        dow2 = 0;
        i += blankDays;
      }
    }
  }

  return result;
};
export const toISOFormat = (date) => { return moment(date).toISOString(); };

export const canTaskBeDeleted = (task) => {
  const uuid = auth().currentUser.uid;
  if (isEmpty(task)) return false;
  return Object.keys(task.inheritedRoles || {}).includes(uuid) || task.roles[uuid] === 'owner';
};

export const useDebouncedEffect = (effect, deps, delays) => { // delays the execution even if dependency changed (to batch the updates for single rendering)
  useEffect(() => {
    const handler = setTimeout(() => effect(), delays);
    return () => clearTimeout(handler);
  }, [...deps || [], delays]);
};
