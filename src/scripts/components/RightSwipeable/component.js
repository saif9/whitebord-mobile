import React, { memo } from 'react';
import {
  Pressable, Text, View
} from 'react-native';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import styles from './styles';

export const RightSwipeable = memo((props) => {
  const { swipeoutBtns, padding } = props;
  const swipeableRef = React.createRef();

  const renderRightActions = () => {
    return (
      <View style={styles.rightAction}>
        {
          swipeoutBtns.map((btn, idx) => (
            <Pressable key={`${btn + idx}`}
              style={{ ...styles.btnStyle, backgroundColor: btn.backgroundColor, padding }}
              onPress={() => {
                btn.onPress();
                swipeableRef.current?.close();
              }}
            >
              <Text style={styles.actionText}>{btn.text}</Text>
            </Pressable>
          ))
        }
      </View>
    );
  };

  return (
    <Swipeable ref={swipeableRef}
      renderRightActions={renderRightActions}
      rightThreshold={100}
      onSwipeableWillOpen={() => {
        if (props.setSwipeableOpen) props.setSwipeableOpen(true);
      }}
      onSwipeableWillClose={() => {
        if (props.setSwipeableOpen) props.setSwipeableOpen(false);
      }}
    >
      {props.children}
    </Swipeable>
  );
});
