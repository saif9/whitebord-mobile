import { StyleSheet } from 'react-native';
import { font } from '../../../constants';

const styles = StyleSheet.create({
  actionText: {
    color: 'white',
    fontSize: 18,
    fontFamily: font.regular,
    fontWeight: '600',
    textTransform: 'capitalize'
  },
  rightAction: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'flex-end',
  },
  btnStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 5
  },
});

export default styles;
