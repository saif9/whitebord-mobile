import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  format: {
    color: '#434279',
    fontSize: 15,
  },
  bold: {
    fontWeight: '800',
  },
  italic: {
    fontStyle: 'italic'
  },
  h1: {
    fontSize: 18,
  },
  h2: {
    fontSize: 17,
  },
  h3: {
    fontSize: 16,
  },
});

export default styles;
