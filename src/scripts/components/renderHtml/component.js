import { Text, View } from 'react-native';
import { parseDocument, ElementType } from 'htmlparser2';
import React, { PureComponent } from 'react';
import styles from './styles';
import { removeHTMLTags } from '../../utils';

export class RenderHtml extends PureComponent {
  ignoredTags = ['head'];

  textTags = ['span'];

  getStyle(attributes, appliedStyle) {
    const { class: attribs = '' } = attributes || {};
    let style = {};
    if (attribs.includes('bold')) {
      style = { ...style, ...styles.bold };
      appliedStyle.bold = true;
    }
    if (attribs.includes('italic')) {
      style = { ...style, ...styles.italic };
      appliedStyle.italic = true;
    }
    if (attribs.includes('h1')) {
      style = { ...style, ...styles.h1 };
      appliedStyle.heading = 'h1';
    }
    if (attribs.includes('h2')) {
      style = { ...style, ...styles.h2 };
      appliedStyle.heading = 'h2';
    }
    if (attribs.includes('h3')) {
      style = { ...style, ...styles.h3 };
      appliedStyle.heading = 'h3';
    }
    return style;
  }

  renderTextNode(textNode, index, appliedStyle) {
    const style = this.getStyle(textNode?.parent?.attribs, appliedStyle);
    return <Text style={[styles.format, style]} key={index}>{textNode.data}</Text>;
  }

  renderElement(element, index, appliedStyle) {
    if (this.ignoredTags.indexOf(element.name) > -1) {
      return null;
    }
    const Wrapper = this.textTags.indexOf(element.name) > -1 ? Text : View;
    return (
      <Wrapper key={index}>
        {element.children.map((c, i) => this.renderNode(c, i, appliedStyle))}
      </Wrapper>
    );
  }

  // It is dependent on the parsed nodes in the HTML string. So It won't loop infinitely.
  renderNode(node, index, appliedStyle) {
    switch (node.type) {
      case ElementType.Text:
        return this.renderTextNode(node, index, appliedStyle);
      case ElementType.Tag:
        return this.renderElement(node, index, appliedStyle);
      default:
        return null;
    }
  }

  render() {
    const { html, appliedStyle } = this.props;
    const document = parseDocument(html);
    const style = {};
    const children = document.children.map((c, i) => this.renderNode(c, i, style));
    if (removeHTMLTags(html).trim()?.length > 0) {
      appliedStyle.current = style;
    }
    return <Text>{children}</Text>;
  }
}
