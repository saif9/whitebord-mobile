import { StyleSheet } from 'react-native';
import { font } from '../../../../constants';

const styles = StyleSheet.create({
  cancel: { color: '#C6C6C6' },
  done: { color: '#6574FF' },
  modal: { backgroundColor: 'white', borderRadius: 20 },
  calendarContainer: {
    paddingHorizontal: 15,
    paddingTop: 10,
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  buttonsContainer: { flexDirection: 'row', height: 40, width: '100%' },
  text: { fontSize: 16, fontFamily: font.medium },
});

export default styles;
