import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Modal from 'react-native-modal';
import styles from './styles';

function DTPickerModal({
  visible, dismissModal, value, mode, onChange, hideButtons
}) {
  const [date, setDate] = useState(value);
  useEffect(() => {
    setDate(value);
  }, [value]);

  const handleOnChange = (_event, newDate) => {
    setDate(newDate);
    if (hideButtons) {
      onChange(newDate);
      setTimeout(dismissModal, 500);
    }
  };

  const done = () => {
    onChange(date);
    dismissModal();
  };

  const heightStyle = () => {
    let height;
    if (mode === 'datetime') height = 370;
    else if (mode === 'date') height = 330;
    else height = 80;
    return { height };
  };

  return visible ? (
    <Modal isVisible={visible} onBackdropPress={dismissModal}>
      <View style={styles.modal}>
        <View style={styles.calendarContainer}>
          <DateTimePicker
            testID="dateTimePicker"
            value={date ?? value}
            mode={mode}
            is24Hour
            display="inline"
            style={heightStyle()}
            onChange={handleOnChange}
          />
        </View>
        {!hideButtons && (
          <View style={styles.buttonsContainer}>
            <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={dismissModal}>
                <Text style={[styles.text, styles.cancel]}>Cancel</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={done}>
                <Text style={[styles.text, styles.done]}>Done</Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </View>
    </Modal>
  ) : (
    <View />
  );
}

export default DTPickerModal;
