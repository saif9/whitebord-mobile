import React, { useState, useEffect } from 'react';
import {
  View,
  TextInput,
  Text,
  TouchableOpacity,
} from 'react-native';
import auth from '@react-native-firebase/auth';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Modal from 'react-native-modal';
import CheckBox from '@react-native-community/checkbox';
import { useDispatch, useSelector } from 'react-redux';
import DatePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import styles from './styles';
import { SectionDropDown, CollaboratorsDropDown } from './DropDowns';
import NoteIcon from '../../../../assets/icons/note_icon.svg';
import DeadlineIcon from '../../../../assets/icons/deadline_icon.svg';
import SectionIcon from '../../../../assets/icons/section_projectmodal.svg';
import NetworkIcon from '../../../../assets/icons/network_projectmodal.svg';
import { createProjectID, projectsSelectors, upsertProject } from '../../../state/slices/projects/projectsSlice';

const ProjectEditModal = ({
  projectId,
  sectionId,
  editProject,
  setEditProject,
}) => {
  const uuid = auth().currentUser.uid;
  const projectState = useSelector((state) => projectsSelectors.selectById(state, projectId));
  const dispatch = useDispatch();

  const [name, setName] = useState(projectState?.name || 'New Project');
  const [description, setDescription] = useState(projectState?.description);
  const initialDeadline = projectState?.deadline
    ? new Date(projectState?.deadline)
    : null;
  const [deadlineAsDateObject, setDeadlineAsDateObject] = useState(initialDeadline);
  const [editingDeadline, setEditingDeadline] = useState(false);
  const deadline = deadlineAsDateObject?.toISOString
    ? deadlineAsDateObject.toISOString()
    : null;
  const initialHideCompleted = projectState?.hideCompleted || false;
  const [hideCompleted, setHideCompleted] = useState(initialHideCompleted);
  const [currentPicker, setCurrentPicker] = useState();
  const [sectionValue, setSectionValue] = useState(sectionId);
  const [roles, setRoles] = useState(projectState?.roles || { [uuid]: 'owner' });

  const modalMount = () => {
    setName(projectState?.name || 'New Project');
    setDeadlineAsDateObject(initialDeadline);
    setDescription(projectState?.description);
    setHideCompleted(initialHideCompleted);
    setSectionValue(sectionId);
    setRoles(projectState?.roles || { [uuid]: 'owner' });
  };

  useEffect(modalMount, [editProject]);

  const handleCancel = () => { setEditProject(false); };

  const handleSave = async () => {
    if (projectId) {
      const isAssignedNewSection = sectionValue !== sectionId;
      const toSectionId = sectionValue;
      const fromSectionId = sectionId;
      const updates = {
        id: projectId,
        name,
        description,
        deadline,
        hideCompleted,
        isAssignedNewSection,
        toSectionId,
        fromSectionId,
        roles,
      };
      dispatch(upsertProject({ updates }));
    } else {
      const updates = {
        id: createProjectID(),
        isAssignedNewSection: true,
        toSectionId: sectionId,
        name,
        description,
        deadline,
        hideCompleted,
        tasks: [],
        roles
      };
      dispatch(upsertProject({ updates }));
    }
    setEditProject(false);
  };

  const DTPickerOnChange = (_event, date) => {
    setDeadlineAsDateObject(date);
    setEditingDeadline(false);
  };

  return (
    <Modal
      animationType="slide"
      coverScreen
      visible={editProject}
      style={styles.modal}
      onBackdropPress={handleCancel}
    >
      <View style={styles.modalView}>
        <KeyboardAwareScrollView keyboardDismissMode="interactive" keyboardShouldPersistTaps="always">
          <View style={styles.modalContent}>
            {/* Modal Header */}
            <View style={styles.header}>
              <TouchableOpacity onPress={handleCancel}>
                <Text style={[styles.headerButton, styles.cancel]}>cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={handleSave}>
                <Text style={[styles.headerButton, styles.save]}>save</Text>
              </TouchableOpacity>
            </View>
            {/* Project Name */}
            <TextInput
              style={styles.projectName}
              value={name}
              onChangeText={setName}
              autoFocus
              selectTextOnFocus
              onSubmitEditing={handleSave}
            />
            {/* Deadline Picker */}
            <View style={styles.deadline}>
              <TouchableWithoutFeedback onPress={() => setEditingDeadline(!editingDeadline)}>
                <View style={styles.row}>
                  <DeadlineIcon />
                  {!deadlineAsDateObject ? (
                    <Text style={styles.setDeadlineText}>Set Deadline</Text>
                  ) : (
                    <Text style={styles.deadlineText}>
                      {moment(deadlineAsDateObject).format('MMMM D')}
                    </Text>
                  )}
                  <TouchableOpacity
                    onPress={() => setEditingDeadline(!editingDeadline)}
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}
                  />
                </View>
              </TouchableWithoutFeedback>
              {editingDeadline && (
              <DatePicker
                value={deadlineAsDateObject || new Date()}
                mode="date"
                display="inline"
                onChange={DTPickerOnChange}
              />
              )}
            </View>
            {/* Section Dropdown */}
            <View style={{ ...styles.note, zIndex: 2 }}>
              <SectionIcon marginRight={2} />
              <SectionDropDown
                currentPicker={currentPicker}
                setCurrentPicker={setCurrentPicker}
                sectionValue={sectionValue}
                setSectionValue={setSectionValue}
              />
            </View>
            {/* Collaborator Dropdown */}
            <View style={{ ...styles.note, zIndex: 1 }}>
              <NetworkIcon />
              <CollaboratorsDropDown
                currentPicker={currentPicker}
                setCurrentPicker={setCurrentPicker}
                roles={roles}
                setRoles={setRoles}
              />
            </View>
            {/* Project Description */}
            <View style={styles.note}>
              <NoteIcon marginRight={7} />
              <TextInput
                multiline
                placeholder="Add Description"
                value={description}
                onChangeText={setDescription}
                style={styles.noteText}
              />
            </View>
            {/* Show Completed */}
            <View style={styles.row}>
              <CheckBox
                value={hideCompleted}
                onValueChange={(value) => {
                  setHideCompleted(value);
                  ReactNativeHapticFeedback.trigger('selection');
                }}
                style={styles.checkbox}
                boxType="square"
                onCheckColor="white"
                onFillColor="#9D9DBA"
                tintColor="#9D9DBA"
                onTintColor="#9D9DBA"
                animationDuration={0}
              />
              <Text style={styles.checkboxText}>Hide Completed Tasks</Text>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    </Modal>
  );
};

export default ProjectEditModal;
