import { StyleSheet } from 'react-native';
import { font, layout } from '../../../../constants';

const { width } = layout.window;
export default StyleSheet.create({
  modalView: {
    alignSelf: 'center',
    flex: 1,
    margin: 0,
    backgroundColor: 'white',
  },
  modalContent: {
    marginHorizontal: 10,
    marginBottom: 40
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 15,
  },
  headerButton: {
    fontFamily: font.medium,
    fontSize: 15,
  },
  cancel: {
    color: '#ABA9FF',
  },
  save: {
    color: '#6574FF',
  },
  projectName: {
    alignSelf: 'center',
    fontFamily: font.semibold,
    color: '#434279',
    fontSize: 17,
  },
  note: {
    flexDirection: 'row',
    marginHorizontal: 10,
    marginVertical: 5,
    alignItems: 'center',
  },
  noteText: {
    height: 40,
    width: width / 1.25,
    marginLeft: 15,
    color: '#9D9DBA',
    fontSize: 16,
    fontFamily: font.regular,
    paddingHorizontal: 15,
    paddingTop: 10,
    borderRadius: 5,
    borderStyle: 'solid',
    borderColor: '#DEDDFF',
    borderWidth: 1
  },
  deadline: {
    margin: 10,
  },
  row: {
    marginTop: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  deadlineText: {
    marginLeft: 20,
    fontSize: 16,
    color: '#6574FF',
    fontFamily: font.bold,
  },
  setDeadlineText: {
    marginLeft: 20,
    fontSize: 16,
    color: '#434279',
    fontFamily: font.regular,
  },
  checkbox: {
    width: 18,
    height: 18,
    marginLeft: 10,
  },
  checkboxText: {
    marginLeft: 20,
    fontFamily: font.medium,
    fontSize: 16,
    color: '#9D9DBA',
  },
  modal: {
    marginHorizontal: 0,
    marginBottom: 0,
    marginTop: 25,
    flex: 1,
    backgroundColor: 'white',
  },
});
