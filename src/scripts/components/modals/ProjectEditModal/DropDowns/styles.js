import { StyleSheet } from 'react-native';
import { font, layout } from '../../../../../constants';

const { width } = layout.window;

const styles = StyleSheet.create({
  dropDownText: {
    fontFamily: font.light,
    fontSize: 16,
  },
  sectionContainerStyle: {
    marginLeft: 17,
    width: width / 1.25,
  },
  sectionDropDownContainerStyle: {
    borderColor: '#DEDDFF',
  },
  sectionDropDownPickerStyle: {
    height: 40,
    borderColor: '#DEDDFF',
    borderRadius: 5,
  },
  sectionPlaceholderStyle: {
    color: '#9D9DBA',
    fontSize: 16,
    paddingLeft: 5
  },
});

export default styles;
