import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import DropDownPicker from 'react-native-dropdown-picker';
import { publicProfilesSelectors } from '../../../../state/slices/publicProfiles/publicProfileSlice';

import styles from './styles';
import { getOwner } from '../../../../utils';

const NAME = 'Collaborator';
export default ({
  currentPicker, setCurrentPicker, roles, setRoles
}) => {
  const owner = getOwner(roles);
  const { [owner]: userId, ...rest } = roles;
  const currentCollaborators = Object.keys(rest);
  const collaborators = useSelector(publicProfilesSelectors.selectAll)
    .map(({ id, firstName, lastName }) => ({
      label: `${firstName} ${lastName}`,
      value: id,
      selectable: true
    }));

  const [values, setValues] = useState(currentCollaborators);
  const [items, setItems] = useState(collaborators);

  useEffect(() => {
    const cols = values.reduce((collabs, val) => {
      return { ...collabs, [val]: 'collaborator' };
    }, {});
    cols[owner] = userId;
    setRoles(cols);
  }, [values]);

  return (
    <DropDownPicker
      open={currentPicker === NAME}
      value={values}
      setOpen={(open) => setCurrentPicker(open ? NAME : '')}
      setValue={setValues}
      multiple
      items={items}
      setItems={setItems}
      showArrowIcon={false}
      showTickIcon
      maxHeight={300}
      dropDownDirection="BOTTOM"
      placeholder="Select Collaborator"
      placeholderStyle={styles.sectionPlaceholderStyle}
      containerStyle={styles.sectionContainerStyle}
      dropDownContainerStyle={styles.sectionDropDownContainerStyle}
      style={styles.sectionDropDownPickerStyle}
      textStyle={styles.dropDownText}
      listMode="SCROLLVIEW"
      zIndex={2000}
      zIndexInverse={1000}
      mode="SIMPLE"
      // onChangeItem={() => {}}
    />
  );
};
