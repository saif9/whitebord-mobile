import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import DropDownPicker from 'react-native-dropdown-picker';
import styles from './styles';
import { memoAllSections } from '../../../../state/slices/sections/selectors';

const NAME = 'Section';
export default ({
  currentPicker, setCurrentPicker, sectionValue, setSectionValue,
}) => {
  const sections = useSelector(memoAllSections).map(
    ({ id, name, projects }) => ({
      label: name,
      value: id,
      projects
    }),
  );

  const [items, setItems] = useState(sections);

  return (
    <DropDownPicker
      open={currentPicker === NAME}
      value={sectionValue}
      setOpen={(open) => setCurrentPicker(open ? NAME : '')}
      setValue={setSectionValue}
      items={items}
      setItems={setItems}
      showArrowIcon={false}
      showTickIcon={false}
      maxHeight={300}
      dropDownDirection="BOTTOM"
      placeholder="Select Sections"
      placeholderStyle={styles.sectionPlaceholderStyle}
      containerStyle={styles.sectionContainerStyle}
      dropDownContainerStyle={styles.sectionDropDownContainerStyle}
      style={styles.sectionDropDownPickerStyle}
      textStyle={styles.dropDownText}
      listMode="SCROLLVIEW"
      zIndex={2000}
      zIndexInverse={1000}
    />
  );
};
