import React, { useState, useRef, useEffect } from 'react';
import auth from '@react-native-firebase/auth';
import { useDispatch, useSelector } from 'react-redux';
import {
  View,
  SafeAreaView,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Modal from 'react-native-modal';
import moment from 'moment';
import { Header, Body, ActionBar } from './modalParts';
import { getRecurrencePatternError } from './util';
import styles from './styles';
import { resetModal, toggleEditMode, toggleModal } from '../../../state/actions/modal';
import { canTaskBeDeleted, getOwner, toISOFormat } from '../../../utils';
import {
  upsertWhiteboardItem, deleteWhiteboardItem, createWhiteboardId
} from '../../../state/slices/whiteboardItems/whiteboardItemsSlice';

export default () => {
  const uuid = auth().currentUser.uid;
  const modal = useSelector((state) => ({
    ...state.modal,
    isRecurrenceItem: !!state.modal.recurrencePattern,
    roles: state.modal?.roles || { [uuid]: 'owner' },
    assignedTo: state.modal?.assignedTo || []
  }));

  const { isOpen, editing } = modal;
  const [modalDetail, setModalDetail] = useState(modal);
  useEffect(() => {
    if (isOpen === false) { // undefined and false due to some reason
      resetModal('', dispatch);
    } else {
      setModalDetail(modal);
    }
  }, [isOpen]);

  const [error, setError] = useState(null);
  const modalScrollViewRef = useRef();
  const dispatch = useDispatch();
  useEffect(() => {
    toggleEditMode(modal.editing, dispatch);
  }, [modal.editing]);

  const submitChanges = async () => {
    const {
      id,
      mode,
      name,
      note,
      startDate,
      endDate,
      color,
      completed,
      projectId,
      type,
      allDay,
      place,
      isRecurrenceItem,
      roles: eRoles, // Previous roles, usser will edit assignedTo list, members from whhich will be added in these roles.
      assignedTo,
    } = modalDetail;
    let { recurrencePattern, deadline } = modalDetail;

    if (!name) {
      setError(`Please enter a name for this ${type}!`);
      modalScrollViewRef.current.scrollToEnd({ animated: true });
      return;
    }

    const _startDate = moment(startDate);
    let scheduledDate = allDay ? _startDate.startOf('day') : _startDate;
    scheduledDate = toISOFormat(scheduledDate);

    const duration = moment(endDate).diff(moment(startDate), 'minutes') / 60;

    const recurrencePatternError = getRecurrencePatternError(recurrencePattern);
    if (recurrencePatternError) {
      recurrencePattern = null;
      setError(recurrencePatternError);
      modalScrollViewRef.current.scrollToEnd({ animated: true });
      return;
    }
    if (!isRecurrenceItem && type !== 'habit') { recurrencePattern = null; }
    if (recurrencePattern) {
      recurrencePattern = {
        ...recurrencePattern,
        recurrenceStartDate: toISOFormat(recurrencePattern.recurrenceStartDate),
        recurrenceEndDate: toISOFormat(recurrencePattern.recurrenceEndDate),
      };
    }

    if (allDay) {
      scheduledDate = toISOFormat(deadline);
    }
    const owner = getOwner(eRoles) || uuid;
    const roles = { [owner]: 'owner' };
    assignedTo.forEach((a) => {
      if (a !== owner) { roles[a] = 'collaborator'; }
    });
    setError(null);
    toggleEditMode(false, dispatch);
    toggleModal(false, dispatch);

    // assign deadline to null if it is undefined since `toISOFormat(undefined)`
    // returns a valid date
    if (deadline === undefined) deadline = null;

    if (mode === 'edit') {
      const fields = {
        id,
        name,
        note,
        date: scheduledDate,
        scheduledDate,
        color,
        completed,
        duration,
        projectId,
        type,
        subtype: projectId ? 'check' : undefined,
        allDay,
        place,
        roles,
        recurrencePattern,
        assignedTo,
        deadline: type === 'event' ? null : toISOFormat(deadline)
      };
      await dispatch(upsertWhiteboardItem({ updates: fields }));
    } else if (mode === 'create') {
      const fields = {
        id: createWhiteboardId(),
        name,
        projectId,
        allDay,
        completed,
        deadline: toISOFormat(deadline),
        duration,
        note,
        onCal: true,
        origin: null,
        parentTask: null,
        date: scheduledDate,
        scheduledDate,
        color,
        place,
        temp: null,
        recurrencePattern,
        type,
        roles,
        assignedTo,
        subtype: 'check',
      };
      dispatch(upsertWhiteboardItem({ updates: fields }));
    }
  };

  const deleteEntry = async () => {
    toggleModal(false, dispatch);
    toggleEditMode(false, dispatch);
    dispatch(deleteWhiteboardItem({ updates: { id: modalDetail.id } }));
  };

  return (
    <>
      {isOpen === undefined
        && (
          <Modal
            animationType="slide"
            coverScreen
            visible={isOpen}
            style={styles.modal}
            onBackdropPress={() => toggleModal(false, dispatch)}
          >
            <SafeAreaView style={styles.modalView}>
              <Header editing={editing} submitChanges={submitChanges} modalDetail={modalDetail} />
              <View style={styles.modalContent}>
                <KeyboardAwareScrollView ref={modalScrollViewRef} keyboardDismissMode="interactive">
                  <Body editing={editing} error={error} modalDetail={modalDetail} setModalDetail={setModalDetail} submitChanges={submitChanges} />
                  <ActionBar
                    editing={editing}
                    submitChanges={submitChanges}
                    deleteEntry={deleteEntry}
                    modalDetail={modalDetail}
                    toggleModal={toggleModal}
                    canDelete={canTaskBeDeleted(modalDetail)}
                  />
                </KeyboardAwareScrollView>
              </View>
            </SafeAreaView>
          </Modal>
        )}
    </>
  );
};
