import React from 'react';
import { View } from 'react-native';
import { EVENT_COLORS } from '../../../../constants/event_colors';

export const colorPickerItems = (
  Object.entries({
    Violet: EVENT_COLORS.VIOLET,
    Lavender: EVENT_COLORS.LAVENDER,
    Magenta: EVENT_COLORS.MAGENTA,
    Pink: EVENT_COLORS.PINK,
    Peach: EVENT_COLORS.PEACH,
    Apple: EVENT_COLORS.APPLE,
    Blue: EVENT_COLORS.BLUE,
    Ocean: EVENT_COLORS.OCEAN,
    Cyan: EVENT_COLORS.CYAN,
    Turquoise: EVENT_COLORS.TURQUOISE,
    Midori: EVENT_COLORS.MIDORI,
    Green: EVENT_COLORS.GREEN,
    Matcha: EVENT_COLORS.MATCHA,
    Marsh: EVENT_COLORS.MARSH,
    Mustard: EVENT_COLORS.MUSTARD,
    Terracotta: EVENT_COLORS.TERRACOTTA
  }).map(([name, hex]) => {
    return {
      label: name,
      value: hex,
      icon: () => (
        <View style={{
          width: 20,
          height: 20,
          backgroundColor: hex,
          borderRadius: 5
        }}
        />
      )
    };
  })
);

export const durationItems = [
  { label: '15 min', value: 0.25 },
  { label: '30 min', value: 0.5 },
  { label: '1 hr', value: 1 },
  { label: '1h 30m', value: 1.5 },
];

export const recurrenceOptions = [
  { label: 'Day(s)', value: 'day' },
  { label: 'Week(s)', value: 'week' },
  { label: 'Month(s)', value: 'month' }
];

export const frequencyOptions = [
  { label: '1', value: 1 },
  { label: '2', value: 2 },
  { label: '3', value: 3 },
  { label: '4', value: 4 },
  { label: '5', value: 5 },
  { label: '6', value: 6 },
  { label: '7', value: 7 },
  { label: '8', value: 8 },
  { label: '9', value: 9 },
  { label: '10', value: 10 },
  { label: '11', value: 11 },
  { label: '12', value: 12 },
];

export const daysOfWeek = {
  U: 0,
  M: 1,
  T: 2,
  W: 3,
  R: 4,
  F: 5,
  S: 6
};
