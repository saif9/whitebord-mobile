import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  modal: {
    marginHorizontal: 0,
    marginBottom: 0,
    marginTop: 25,
    flex: 1,
    backgroundColor: 'white',
  },
  modalView: {
    flex: 1,
    alignSelf: 'center',
    backgroundColor: 'white',
    width: '100%',
    borderRadius: 20,
    borderStyle: 'solid',
    borderColor: '#DEDDFF',
    borderWidth: 0,
  },
  modalContent: {
    flex: 1,
    marginHorizontal: 10,
  },
});
