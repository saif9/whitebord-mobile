export const getRecurrencePatternError = (pattern) => {
  if (!pattern) { return null; }

  if (!pattern.recurrenceStartDate || !pattern.recurrenceEndDate || !pattern.frequency) {
    return 'You must provide a start and end date for recurring tasks!';
  }

  if (pattern.recurrenceType !== 'week' && pattern.daysOfWeek?.length > 0) {
    return 'You cannot indicate days of week unless task is weekly';
  }

  return null;
};

export const isValidHttpUrl = (string) => {
  try {
    return !!new URL(string);
  } catch (_) {
    return false;
  }
};
