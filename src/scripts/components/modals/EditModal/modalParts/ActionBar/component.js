import React from 'react';
import { useDispatch } from 'react-redux';
import {
  View,
  Text,
  TouchableHighlight,
} from 'react-native';
import { upsertWhiteboardItem } from '../../../../../state/slices/whiteboardItems/whiteboardItemsSlice';

import styles from './styles';

export default (props) => {
  const {
    modalDetail,
    editing,
    deleteEntry,
    toggleModal,
    canDelete
  } = props;

  const UnscheduleButton = () => {
    const dispatch = useDispatch();
    const { id } = modalDetail;
    const buttonFunc = () => {
      toggleModal(false, dispatch);
      dispatch(upsertWhiteboardItem({
        updates: {
          id, recurrenceTime: null, scheduledDate: null, onCal: false, deadline: null
        }
      }));
    };

    return (
      <TouchableHighlight
        style={{
          ...styles.actionButton,
          backgroundColor: '#ABA9FF'
        }}
        onPress={buttonFunc}
      >
        <Text style={styles.actionButtonText}>
          Unschedule
        </Text>
      </TouchableHighlight>
    );
  };

  return (
    !editing
      ? (
        <View />
      ) : (
        <View style={styles.bottomRow}>
          <UnscheduleButton />
          <TouchableHighlight
            style={{
              ...styles.actionButton,
              backgroundColor: '#FA8072'
            }}
            onPress={deleteEntry}
          >
            <Text style={styles.actionButtonText}>
              { canDelete ? 'Delete' : 'Remove' }
            </Text>
          </TouchableHighlight>
        </View>
      )
  );
};
