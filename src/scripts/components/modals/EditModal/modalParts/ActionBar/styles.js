import { StyleSheet } from 'react-native';
import { font } from '../../../../../../constants';

const styles = StyleSheet.create({
  bottomRow: {
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  actionButton: {
    height: 35,
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginVertical: 5,
  },
  actionButtonText: {
    color: 'white',
    fontFamily: font.bold
  },
});

export default styles;
