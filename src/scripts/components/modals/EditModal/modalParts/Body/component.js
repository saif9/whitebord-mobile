import React from 'react';
import { View } from 'react-native';
import {
  EventDisplayMode, TaskDisplayMode, EditMode, HabitDisplayMode
} from './components';

export default ({
  modalDetail, setModalDetail, editing, error, submitChanges
}) => {
  let Component;
  switch (modalDetail.type) {
    case 'task':
      Component = <TaskDisplayMode modalDetail={modalDetail} />;
      break;
    case 'event':
      Component = <EventDisplayMode modalDetail={modalDetail} />;
      break;
    case 'habit':
      Component = <HabitDisplayMode modalDetail={modalDetail} />;
      break;
    default:
      Component = <View />;
  }

  return editing
    ? (
      <>
        <EditMode
          modalDetail={modalDetail}
          setModalDetail={setModalDetail}
          error={error}
          submitChanges={submitChanges}
        />
      </>
    )
    : Component;
};
