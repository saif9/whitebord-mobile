import { StyleSheet } from 'react-native';
import { font, layout } from '../../../../../../constants';

const { width } = layout.window;

export default StyleSheet.create({
  colorDropDownItem: {
    justifyContent: 'flex-start'
  },
  deadline: {
    margin: 10,
  },
  deadlineRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  deadlineText: {
    marginLeft: 20,
    fontSize: 16,
    color: '#6574FF',
    fontFamily: font.medium,
  },
  detailBar: {
    marginVertical: 5,
    borderBottomWidth: 0.5,
    borderBottomColor: '#DEDDFF',
    width: '96%',
    alignSelf: 'center'
  },
  dropDownContainer: {
    height: 40,
    width: width / 1.3,
    zIndex: 1
  },
  DOWPickerContainer: {
    marginVertical: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  DOWItem: {
    height: 40,
    width: 40,
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  DOWItemText: {
    fontSize: 13
  },
  dropDownPicker: {
    borderColor: '#DEDDFF',
    borderWidth: 1,
    marginLeft: 10,
  },
  dropDownPlaceholder: {
    color: '#9d9dba',
  },
  dropDownText: {
    fontFamily: font.regular,
    fontSize: 16,
    color: '#9d9dba'
  },
  project: {
    flexDirection: 'row',
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 10,
  },
  collaborator: {
    flexDirection: 'row',
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 5,
  },
  descriptionView: {
    flex: 1,
  },
  durationDropDownItem: {
    justifyContent: 'flex-start',
  },
  durationText: {
    marginLeft: 20,
    fontSize: 16,
    color: '#52575C',
    fontFamily: font.regular,
    paddingVertical: 10,
  },
  errorText: {
    fontFamily: font.semibold,
    fontSize: 15,
    color: 'red',
    alignSelf: 'center'
  },
  frequencyDropDown: {
    width: 60,
  },
  htmlName: {
    flexDirection: 'column',
    marginTop: 5,
    marginBottom: 15,
    marginHorizontal: 15,
    minHeight: 45
  },
  name: {
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 15,
  },
  nameText: {
    marginTop: 10,
    color: '#434279',
    fontSize: 18,
    fontFamily: font.semibold,
    textAlign: 'center'
  },
  note: {
    flexDirection: 'row',
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  noteText: {
    width: width / 1.3,
    marginLeft: 15,
    fontSize: 16,
    fontFamily: font.regular,
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderRadius: 5,
    borderStyle: 'solid',
    borderColor: '#DEDDFF',
    borderWidth: 1
  },
  origin: {
    margin: 10,
    flexDirection: 'row',
    alignSelf: 'center',
    borderRadius: 10
  },
  originColorSquare: {
    height: 35,
    width: 35,
    marginRight: 20,
    borderRadius: 5
  },
  originText: {
    fontFamily: font.bold,
  },
  projectDropDownItem: {
    height: 45,
    justifyContent: 'flex-start',
  },
  projectText: {
    fontFamily: font.bold,
    fontSize: 16,
    paddingHorizontal: 15
  },
  recurrenceBox: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    zIndex: 1
  },
  recurrenceText: {
    fontFamily: font.regular,
    color: '#52575C',
    fontSize: 13,
  },
  setDeadlineText: {
    marginLeft: 20,
    fontSize: 16,
    color: '#9D9DBA',
    fontFamily: font.medium,
  },
  startEndText: {
    marginRight: 30,
    color: '#6574FF',
    fontSize: 14,
    fontFamily: font.bold,
  },
  switch: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  switchButton: {
    transform: [{ scaleX: 0.7 }, { scaleY: 0.7 }],
    marginRight: 5
  },
  time: {
    marginHorizontal: 10,
    marginVertical: 20,
    zIndex: 2,
  },
  timeContainer: {
    flexDirection: 'row'
  },
  timeDisplay: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  timeText: {
    fontSize: 14,
    color: '#52575C',
    fontFamily: font.regular,
  },
  typeSwitcher: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  unitDropDown: {
    width: 105
  },
  viewDeadline: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginHorizontal: 10,
  },
  viewDetailBar: {
    borderBottomWidth: 0.5,
    borderBottomColor: '#DEDDFF',
    width: '96%',
    alignSelf: 'center'
  },
  viewDuration: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10,
  },
  viewNote: {
    flexDirection: 'row',
    marginHorizontal: 10,
    alignItems: 'flex-start',
    marginVertical: 10,
  },
  viewNoteText: {
    marginLeft: 20,
    fontSize: 16,
    color: '#52575C',
    fontFamily: font.regular,
  },
  linkText: {
    color: '#386bc6',
    textDecorationLine: 'underline'
  },
  showLessText: {
    fontSize: 14,
    fontFamily: font.semibold,
    alignSelf: 'flex-end',
    marginRight: 20,
    color: '#6574FF',
    marginTop: 5,
  },
  viewProject: {
    flexDirection: 'row',
    marginHorizontal: 5,
    marginVertical: 15,
    borderColor: '#DEDDFF',
    borderWidth: 1,
    borderRadius: 10,
    height: 50,
    alignItems: 'center',
    padding: 10
  },
});
