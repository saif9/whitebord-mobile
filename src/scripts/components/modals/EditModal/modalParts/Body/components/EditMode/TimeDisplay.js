import React from 'react';
import { Text, View } from 'react-native';
import moment from 'moment';
import styles from '../../styles';

export default (props) => {
  const {
    text, momentTime, style
  } = props;

  return (
    <View style={{ ...styles.timeDisplay, ...style }}>
      <View style={{ flex: 1.2 }}>
        {text && <Text style={styles.startEndText}>{text}</Text>}
      </View>
      <View style={{ flex: 2.5 }}>
        {momentTime && (
        <Text style={styles.timeText}>
          {moment(momentTime).format('ddd, MMM Do')}
        </Text>
        )}
      </View>
      <View style={{ flex: 2 }}>
        <Text style={styles.timeText}>
          {moment(momentTime).format('h:mma')}
        </Text>
      </View>
    </View>
  );
};
