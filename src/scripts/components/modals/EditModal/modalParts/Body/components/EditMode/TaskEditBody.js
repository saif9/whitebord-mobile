import React, { useState } from 'react';
import { Text, TextInput, View } from 'react-native';
import moment from 'moment';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import TimeDisplay from './TimeDisplay';
import TimeDisplayOneRow from './TimeDisplayOneRow';
import NoteIcon from '../../../../../../../../assets/icons/note_icon.svg';
import DeadlineIcon from '../../../../../../../../assets/icons/deadline_icon.svg';
import ProjectIcon from '../../../../../../../../assets/icons/project_icon.svg';
import PeopleIcon from '../../../../../../../../assets/icons/people.svg';
import RecurrenceSelection from './RecurrenceSelection';
import { ProjectDropDown } from './DropDowns';
import styles from '../../styles';
import CollaboratorsDropDown from './DropDowns/CollaboratorsDropDown';
import DTPickerModal from '../../../../../DTPickerModal';

const TaskEditMode = (props) => {
  const {
    error, modalDetail, setModalDetail, isRecurrenceItem
  } = props;

  const [deadline, setDeadline] = useState(new Date());
  const [currentPicker, setCurrentPicker] = useState('');
  const [recurrencePattern, setRecurrencePattern] = useState(
    modalDetail.recurrencePattern || {
      daysOfWeek: [],
      frequency: 1,
      recurrenceStartDate: moment(),
      recurrenceEndDate: moment(),
      recurrenceType: 'day',
      recurrenceEndType: 'never',
    },
  );

  const { allDay } = modalDetail;

  const [dTPickerModalProps, setDTPickerModalProps] = useState({
    visible: false,
  });

  const onChangeRecurrenceField = (fieldName, data) => {
    let updatedState = {
      ...recurrencePattern,
      [fieldName]: data,
    };
    if (updatedState.recurrenceStartDate && updatedState.recurrenceEndDate) {
      const sDate = moment(updatedState.recurrenceStartDate);
      const eDate = moment(updatedState.recurrenceEndDate);
      if (sDate.isAfter(eDate)) {
        updatedState = { ...updatedState, recurrenceEndDate: sDate };
      }
    }
    setRecurrencePattern(updatedState);
    onChangeField('recurrencePattern', updatedState);
  };

  const onChangeField = (fieldName, data) => {
    let updatedState = {
      ...modalDetail,
      [fieldName]: data,
    };
    if (fieldName === 'recurrencePattern') {
      const rSDate = moment(data.recurrenceStartDate);
      const [year, month, date] = [
        rSDate.get('year'),
        rSDate.get('month'),
        rSDate.get('date'),
      ];
      const startDate = moment(updatedState.startDate).set({
        year,
        month,
        date,
      });
      const endDate = moment(updatedState.endDate).set({ year, month, date });
      updatedState = { ...updatedState, startDate, endDate };
    }

    if (updatedState.startDate && updatedState.endDate) {
      const sDate = moment(updatedState.startDate);
      const eDate = moment(updatedState.endDate);
      if (sDate.isAfter(eDate)) {
        updatedState = { ...updatedState, endDate: sDate };
      }
    }
    setModalDetail(updatedState);
    if (fieldName === 'deadline') {
      setDeadline(data);
    }
  };

  const twoRowsTimeDisplay = () => (
    <>
      <TouchableWithoutFeedback
        onPress={() => {
          setDTPickerModalProps({
            visible: true,
            mode: modalDetail.allDay || isRecurrenceItem ? 'date' : 'datetime',
            value: new Date(modalDetail.startDate),
            onChange: (date) => onChangeField('startDate', moment(date)),
          });
        }}
      >
        <TimeDisplay text="Starts" momentTime={modalDetail.startDate} />
        <View style={{ marginBottom: 15 }} />
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback
        onPress={() => {
          setDTPickerModalProps({
            visible: true,
            mode: modalDetail.allDay || isRecurrenceItem ? 'date' : 'datetime',
            value: new Date(modalDetail.endDate),
            onChange: (date) => onChangeField('endDate', moment(date)),
          });
        }}
      >
        <TimeDisplay text="Ends" momentTime={modalDetail.endDate} />
      </TouchableWithoutFeedback>
    </>
  );

  const oneRowTimeDisplay = () => (
    <TimeDisplayOneRow
      startTime={modalDetail.startDate}
      endTime={modalDetail.endDate}
      setDTPickerModalProps={setDTPickerModalProps}
      onChangeField={onChangeField}
    />
  );

  const deadlineTimeDisplay = () => (
    <View style={styles.deadline}>
      <View style={styles.deadlineRow}>
        <DeadlineIcon />
        <TouchableWithoutFeedback
          onPress={() => setDTPickerModalProps({
            visible: true,
            mode: 'date',
            value: modalDetail.deadline
              ? new Date(modalDetail.deadline)
              : deadline,
            onChange: (date) => onChangeField('deadline', moment(date)),
          })}
        >
          {!modalDetail.deadline ? (
            <Text style={styles.setDeadlineText}>Set Deadline</Text>
          ) : (
            <Text style={styles.deadlineText}>
              {moment(modalDetail.deadline).format('MMMM D')}
            </Text>
          )}
        </TouchableWithoutFeedback>
      </View>
    </View>
  );

  const CustomTimeDisplay = () => {
    if (!allDay && !isRecurrenceItem) {
      return twoRowsTimeDisplay();
    } else if (!allDay && isRecurrenceItem) {
      return oneRowTimeDisplay();
    } else if (allDay && !isRecurrenceItem) {
      return deadlineTimeDisplay();
    } else {
      return <View />;
    }
  };

  return (
    <>
      <DTPickerModal
        visible={dTPickerModalProps.visible}
        dismissModal={() => setDTPickerModalProps({ visible: false })}
        mode={dTPickerModalProps.mode}
        value={dTPickerModalProps.value}
        onChange={dTPickerModalProps.onChange}
      />
      <View style={styles.time}>{CustomTimeDisplay()}</View>
      {isRecurrenceItem && (
        <RecurrenceSelection
          currentPicker={currentPicker}
          setCurrentPicker={setCurrentPicker}
          recurrencePattern={recurrencePattern}
          onChangeField={onChangeRecurrenceField}
        />
      )}
      <View style={styles.project}>
        <ProjectIcon width={20} height={20} />
        <ProjectDropDown
          currentPicker={currentPicker}
          setCurrentPicker={setCurrentPicker}
          onChangeField={onChangeField}
          modalDetail={modalDetail}
        />
      </View>
      <View style={styles.collaborator}>
        <PeopleIcon width={20} height={20} />
        <CollaboratorsDropDown
          currentPicker={currentPicker}
          setCurrentPicker={setCurrentPicker}
          onChangeField={onChangeField}
          modalDetail={modalDetail}
        />
      </View>
      <View style={styles.note}>
        <NoteIcon width={20} height={20} />
        <TextInput
          multiline
          placeholder="Add Notes"
          onChangeText={(note) => onChangeField('note', note)}
          style={styles.noteText}
          placeholderTextColor="#9d9dba"
        >
          {modalDetail.note}
        </TextInput>
      </View>
      <View style={styles.detailBar} />
      {error && <Text style={styles.errorText}>{error}</Text>}
    </>
  );
};

export default TaskEditMode;
