import React, { useState, useEffect } from 'react';
import {
  Pressable,
  Text,
  View
} from 'react-native';
import { concat, includes } from 'lodash';
import { daysOfWeek } from '../../../../constants';
import styles from '../../styles';

export default ({ onChangeDaysOfWeek, daysArray, style }) => {
  const [selected, setSelected] = useState(daysArray);
  useEffect(() => {
    setSelected(daysArray);
  }, [daysArray]);

  useEffect(() => {
    onChangeDaysOfWeek(selected);
  }, [selected]);
  return (
    <View style={{ ...styles.DOWPickerContainer, ...style }}>
      {Object.entries(daysOfWeek).map(([day, value]) => {
        return (
          <Pressable
            key={value}
            onPress={() => {
              setSelected((prev) => {
                return includes(prev, value)
                  ? prev.filter((val) => (val !== value)).sort()
                  : concat(prev, value).sort();
              });
            }}
          >
            <View style={{
              ...styles.DOWItem,
              borderColor: includes(selected, value) ? '#6574FF' : '#9D9DBA',
              backgroundColor: includes(selected, value) ? '#6574FF' : 'white',
            }}
            >
              <Text style={{
                ...styles.DOWItemText,
                color: includes(selected, value) ? 'white' : '#9D9DBA'
              }}
              >
                {day}
              </Text>
            </View>
          </Pressable>
        );
      })}
    </View>
  );
};
