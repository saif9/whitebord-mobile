import React, { useState } from 'react';
import {
  Text, View, TouchableOpacity, Linking
} from 'react-native';
import moment from 'moment';
import RenderHtml from 'react-native-render-html';
import PeopleIcon from '../../../../../../../assets/icons/people.svg';
import NoteIcon from '../../../../../../../assets/icons/note_icon.svg';
import DurationIcon from '../../../../../../../assets/icons/duration_icon.svg';
import LocationIcon from '../../../../../../../assets/icons/location.svg';
import styles from '../styles';
import { font } from '../../../../../../../constants';
import { isValidHttpUrl } from '../../../util';
import { removeHTMLTags } from '../../../../../../utils';

const EventDisplayMode = ({ modalDetail }) => {
  const {
    name,
    description,
    startDate,
    endDate,
    allDay,
    // color,
  } = modalDetail;

  const [showLess, setShowLess] = useState(true);
  const systemFonts = [...Object.values(font)];
  const MAX_DESC_LENGTH = 300;
  const toggleDescriptionLength = () => {
    if (description && description.length >= MAX_DESC_LENGTH) {
      return (
        <TouchableOpacity onPress={() => setShowLess(!showLess)}>
          <Text style={styles.showLessText}>
            {`${!showLess ? 'show less' : 'show more'}...`}
          </Text>
        </TouchableOpacity>
      );
    }
    return null;
  };
  const formatNote = () => {
    if (!description) return 'No Description';
    if (description.length < MAX_DESC_LENGTH) return description;
    return showLess ? description.slice(0, MAX_DESC_LENGTH) : description;
  };
  const formatDateTime = (date) => moment(date).format('h:mma, ddd, MMM D, YYYY');
  const formatDate = (date) => moment(date).format('MMM D, YYYY');
  return (
    <View>
      <View style={styles.name}>
        <Text style={styles.nameText}>
          {removeHTMLTags(name) || ''}
        </Text>
      </View>
      <View style={styles.viewDuration}>
        <DurationIcon width={20} height={20} />
        <Text style={styles.durationText}>
          {allDay
            ? formatDate(startDate)
            : `From: ${formatDateTime(startDate)}\nTo: ${formatDateTime(
              endDate,
            )}`}
        </Text>
      </View>
      <View style={styles.viewDetailBar} />
      <View style={styles.viewNote}>
        <LocationIcon width={20} height={20} />
        <TouchableOpacity
          onPress={() => Linking.openURL(modalDetail.location)}
          disabled={!isValidHttpUrl(modalDetail.location)}
          style={{ flex: 1 }}
        >
          <Text
            style={[
              styles.viewNoteText,
              isValidHttpUrl(modalDetail.location) && styles.linkText,
            ]}
          >
            {modalDetail.location || 'No Location'}
          </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.viewDetailBar} />
      <View style={styles.viewNote}>
        <PeopleIcon width={20} height={20} />
        <Text style={styles.viewNoteText}>
          {modalDetail.collaborator || 'No Collaborator'}
        </Text>
      </View>
      <View style={styles.viewDetailBar} />
      <View style={styles.viewNote}>
        <NoteIcon width={20} height={20} />
        <View style={styles.descriptionView}>
          <RenderHtml
            contentWidth={50}
            source={{ html: formatNote() }}
            baseStyle={styles.viewNoteText}
            systemFonts={systemFonts}
          />
          {toggleDescriptionLength()}
        </View>
      </View>
      {/* <View style={styles.detailBar} />
      <View style={styles.origin}>
        <TouchableHighlight
          style={{
            ...styles.originColorSquare,
            backgroundColor: color,
          }}
        />
      </View> */}
    </View>
  );
};

export default EventDisplayMode;
