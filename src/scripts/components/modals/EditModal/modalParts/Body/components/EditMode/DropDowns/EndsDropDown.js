import React, { useState, useEffect } from 'react';
import DropDownPicker from 'react-native-dropdown-picker';
import { endDropDownItems } from '../constants';
import styles from './styles';

const LABEL = 'Ends';
export default ({
  currentPicker, setCurrentPicker, selected, onChangeField
}) => {
  const [value, setValue] = useState(selected || 'on');
  const [items, setItems] = useState(endDropDownItems);
  useEffect(() => {
    onChangeField('recurrenceEndType', value);
  }, [value]);
  return (
    <DropDownPicker
      open={currentPicker === LABEL}
      value={value}
      setOpen={(open) => setCurrentPicker(open ? LABEL : '')}
      setValue={setValue}
      items={items}
      setItems={setItems}
      showArrowIcon={false}
      showTickIcon={false}
      maxHeight={300}
      containerStyle={styles.endContainerStyle}
      dropDownContainerStyle={styles.endDropDownContainerStyle}
      style={styles.endDropDownPickerStyle}
      textStyle={styles.dropDownText}
      listMode="SCROLLVIEW"
    />
  );
};
