import { StyleSheet } from 'react-native';
import { font } from '../../../../../../../../constants';

const styles = StyleSheet.create({
  defaultFont: {
    fontFamily: font.regular,
    textTransform: 'capitalize',
  },
  disabledFont: {
    fontFamily: font.regular,
    textTransform: 'capitalize',
    color: '#bcbcbc',
  },
  defaultStyle: {
    backgroundColor: 'white',
    padding: 5,
    paddingVertical: 8,
    paddingHorizontal: 8,
    borderRadius: 5,
  },
  dateContainer: {
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#DEDDFF',
    marginHorizontal: 20,
    paddingHorizontal: 10,
  },
  dateText: {
    fontFamily: font.regular,
    fontSize: 16,
    color: '#6574FF',
  },
  selectedFont: {
    color: '#5754FF',
    fontFamily: font.semibold,
    textTransform: 'capitalize',
  },
  selectedStyle: {
    backgroundColor: '#C6C6EC38',
    paddingVertical: 8,
    paddingHorizontal: 8,
    borderRadius: 5,
  },
  switcherContainer: {
    flexDirection: 'row',
  },
  recurrenceContainer: {
    backgroundColor: '#F8F9FF',
    borderRadius: 10,
    width: '93%',
    alignSelf: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
    zIndex: 20,
    marginBottom: 20,
  },
  recurrenceFirstRow: {
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 5,
    zIndex: 1,
  },
  recurrenceSecondRow: {
    marginVertical: 5,
    zIndex: 0,
  },
  row: {
    flexDirection: 'row',
    marginVertical: 10,
    zIndex: 0,
    alignItems: 'center',
  },
  daysRow: {
    flexDirection: 'row',
    marginVertical: 10,
    zIndex: 0,
    justifyContent: 'space-evenly',
  },
  recurrenceText: {
    fontFamily: font.regular,
    color: '#434279',
  },
  datePicker: {
    marginLeft: 20,
    width: 100,
    zIndex: -1000,
  },
});

export default styles;
