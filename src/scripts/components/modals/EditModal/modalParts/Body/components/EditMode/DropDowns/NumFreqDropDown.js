import React, { useState, useEffect } from 'react';
import DropDownPicker from 'react-native-dropdown-picker';
import { numFreqDropDownItems } from '../constants';
import styles from './styles';

const LABEL = 'NumFreq';
export default ({
  currentPicker, setCurrentPicker, onChangeField, selected
}) => {
  const [value, setValue] = useState(selected);
  const [items, setItems] = useState(numFreqDropDownItems);

  useEffect(() => {
    onChangeField('frequency', value);
  }, [value]);

  return (
    <DropDownPicker
      open={currentPicker === LABEL}
      value={value}
      setOpen={(open) => setCurrentPicker(open ? LABEL : '')}
      setValue={setValue}
      items={items}
      setItems={setItems}
      showArrowIcon={false}
      showTickIcon={false}
      maxHeight={300}
      containerStyle={styles.numFreqContainerStyle}
      dropDownContainerStyle={styles.numFreqDropDownContainerStyle}
      style={styles.numFreqDropDownPickerStyle}
      textStyle={styles.dropDownText}
      listMode="SCROLLVIEW"
    />
  );
};
