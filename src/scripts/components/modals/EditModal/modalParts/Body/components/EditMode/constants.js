import Monday from '../../../../../../../../assets/icons/monday.svg';
import Tuesday from '../../../../../../../../assets/icons/tuesday.svg';
import Wednesday from '../../../../../../../../assets/icons/wednesday.svg';
import Thursday from '../../../../../../../../assets/icons/thursday.svg';
import Friday from '../../../../../../../../assets/icons/friday.svg';
import Saturday from '../../../../../../../../assets/icons/saturday.svg';
import Sunday from '../../../../../../../../assets/icons/sunday.svg';
import MondaySelected from '../../../../../../../../assets/icons/monday_selected.svg';
import TuesdaySelected from '../../../../../../../../assets/icons/tuesday_selected.svg';
import WednesdaySelected from '../../../../../../../../assets/icons/wednesday_selected.svg';
import ThursdaySelected from '../../../../../../../../assets/icons/thursday_selected.svg';
import FridaySelected from '../../../../../../../../assets/icons/friday_selected.svg';
import SaturdaySelected from '../../../../../../../../assets/icons/saturday_selected.svg';
import SundaySelected from '../../../../../../../../assets/icons/sunday_selected.svg';

export const Components = {
  SundaySelected,
  MondaySelected,
  TuesdaySelected,
  WednesdaySelected,
  ThursdaySelected,
  FridaySelected,
  SaturdaySelected,
  Sunday,
  Monday,
  Tuesday,
  Wednesday,
  Thursday,
  Friday,
  Saturday,
};

export const dayFreqDropDownItems = [
  { label: 'day', value: 'day' },
  { label: 'week', value: 'week' },
  { label: 'month', value: 'month' },
];

export const numFreqDropDownItems = [
  { label: '1', value: 1 },
  { label: '2', value: 2 },
  { label: '3', value: 3 },
  { label: '4', value: 4 },
  { label: '5', value: 5 },
  { label: '6', value: 6 },
  { label: '7', value: 7 },
];

export const endDropDownItems = [
  { label: 'On', value: 'on' },
  { label: 'Never', value: 'never' },
];

export const DAYS = {
  Sunday: 0,
  Monday: 1,
  Tuesday: 2,
  Wednesday: 3,
  Thursday: 4,
  Friday: 5,
  Saturday: 6
};
