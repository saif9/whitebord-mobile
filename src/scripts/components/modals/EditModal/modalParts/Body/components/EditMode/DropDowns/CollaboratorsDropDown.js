import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import DropDownPicker from 'react-native-dropdown-picker';
import { publicProfilesSelectors } from '../../../../../../../../state/slices/publicProfiles/publicProfileSlice';
import styles from './styles';

const LABEL = 'Collaborator';
export default ({
  currentPicker, setCurrentPicker, onChangeField, modalDetail
}) => {
  const { assignedTo } = modalDetail;
  // adding myprofile to assignees so that task could be assigned to me
  const currentProfile = useSelector((state) => state.publicProfiles.myProfile);
  const assignees = useSelector(publicProfilesSelectors.selectAll)
    .concat({ ...currentProfile, id: currentProfile.uid })
    .map(({ id, firstName, lastName }) => ({
      label: `${firstName} ${lastName}`,
      value: id,
      selectable: true
    }));

  const [values, setValues] = useState(assignedTo);
  const [items, setItems] = useState(assignees);

  useEffect(() => { // adding this hook to refresh collaborators each time modal is opened.
    setValues(assignedTo);
    setItems(assignees);
  }, [modalDetail]);

  useEffect(() => {
    onChangeField('assignedTo', values);
  }, [values]);

  return (
    <DropDownPicker
      open={currentPicker === LABEL}
      value={values}
      setOpen={(open) => setCurrentPicker(open ? LABEL : '')}
      setValue={setValues}
      items={items}
      multiple
      setItems={setItems}
      showArrowIcon={false}
      showTickIcon
      maxHeight={300}
      dropDownDirection="BOTTOM"
      placeholder="Add Collaborators"
      placeholderStyle={styles.projectPlaceholderStyle}
      containerStyle={styles.projectContainerStyle}
      dropDownContainerStyle={styles.projectDropDownContainerStyle}
      style={styles.projectDropDownPickerStyle}
      listParentLabelStyle={styles.projectParentLabelStyle}
      textStyle={styles.dropDownText}
      listMode="SCROLLVIEW"
      zIndex={1000}
      zIndexInverse={2000}
      mode="SIMPLE"
    />
  );
};
