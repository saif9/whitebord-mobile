import { StyleSheet } from 'react-native';
import { font, layout } from '../../../../../../../../../constants';

const { width } = layout.window;

const styles = StyleSheet.create({
  dropDownText: {
    fontFamily: font.regular,
    fontSize: 16,
  },
  numFreqContainerStyle: {
    marginLeft: 10,
    width: 30,
  },
  numFreqDropDownContainerStyle: {
    borderColor: '#DEDDFF',
  },
  numFreqDropDownPickerStyle: {
    height: 30,
    borderColor: '#DEDDFF',
  },
  dayFreqContainerStyle: {
    marginLeft: 10,
    width: 70,
  },
  dayFreqDropDownContainerStyle: {
    borderColor: '#DEDDFF',
  },
  dayFreqDropDownPickerStyle: {
    height: 30,
    borderColor: '#DEDDFF',
  },
  endContainerStyle: {
    marginLeft: 25,
    width: 70,
  },
  endDropDownContainerStyle: {
    borderColor: '#DEDDFF',
  },
  endDropDownPickerStyle: {
    height: 30,
    borderColor: '#DEDDFF',
  },
  projectContainerStyle: {
    marginLeft: 12,
    width: width / 1.3,
  },
  projectDropDownContainerStyle: {
    borderColor: '#DEDDFF',
  },
  projectDropDownPickerStyle: {
    height: 40,
    borderColor: '#DEDDFF',
    borderRadius: 5,
  },
  projectPlaceholderStyle: {
    color: '#9D9DBA',
    fontSize: 16,
    paddingLeft: 5
  },
  projectParentLabelStyle: {
    color: '#9D9DBA',
    fontFamily: font.extrabold,
  }
});

export default styles;
