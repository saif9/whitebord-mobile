import React, { useState } from 'react';
import { Text, View } from 'react-native';
import moment from 'moment';
import styles from '../../styles';
import RecurrenceSelection from './RecurrenceSelection';
import TimeDisplayOneRow from './TimeDisplayOneRow';
import DTPickerModal from '../../../../../DTPickerModal';

const HabitEditMode = (props) => {
  const {
    error, modalDetail, setModalDetail
  } = props;

  const [recurrencePattern, setRecurrencePattern] = useState(
    modalDetail.recurrencePattern || {
      daysOfWeek: [],
      frequency: 1,
      recurrenceStartDate: moment(),
      recurrenceEndDate: moment(),
      recurrenceType: 'day',
      recurrenceEndType: 'never',
    },
  );
  const [currentPicker, setCurrentPicker] = useState('');
  const [dTPickerModalProps, setDTPickerModalProps] = useState({
    visible: false,
  });

  const { allDay } = modalDetail;

  const onChangeRecurrenceField = (fieldName, data) => {
    let updatedState = {
      ...recurrencePattern,
      [fieldName]: data,
    };
    if (updatedState.recurrenceStartDate && updatedState.recurrenceEndDate) {
      const sDate = moment(updatedState.recurrenceStartDate);
      const eDate = moment(updatedState.recurrenceEndDate);
      if (sDate.isAfter(eDate)) {
        updatedState = { ...updatedState, recurrenceEndDate: sDate };
      }
    }
    setRecurrencePattern(updatedState);
    onChangeField('recurrencePattern', updatedState);
  };

  const onChangeField = (fieldName, data) => {
    let updatedState = {
      ...modalDetail,
      [fieldName]: data,
    };
    if (fieldName === 'recurrencePattern') {
      // update startDate to recurrenceStartDate but don't change time
      const rSDate = moment(data.recurrenceStartDate);
      const [year, month, date] = [
        rSDate.get('year'),
        rSDate.get('month'),
        rSDate.get('date'),
      ];
      const startDate = moment(updatedState.startDate).set({
        year,
        month,
        date,
      });
      const endDate = moment(updatedState.endDate).set({ year, month, date });
      updatedState = { ...updatedState, startDate, endDate };
    }

    if (updatedState.startDate && updatedState.endDate) {
      const sDate = moment(updatedState.startDate);
      const eDate = moment(updatedState.endDate);
      if (sDate.isAfter(eDate)) {
        updatedState = { ...updatedState, endDate: sDate };
      }
    }
    setModalDetail(updatedState);
  };

  const oneRowTimeDisplay = () => (
    <TimeDisplayOneRow
      startTime={modalDetail.startDate}
      endTime={modalDetail.endDate}
      setDTPickerModalProps={setDTPickerModalProps}
      onChangeField={onChangeField}
    />
  );

  const CustomTimeDisplay = () => {
    if (!allDay) {
      return oneRowTimeDisplay();
    } else {
      return <View />;
    }
  };

  return (
    <>
      <DTPickerModal
        visible={dTPickerModalProps.visible}
        dismissModal={() => setDTPickerModalProps({ visible: false })}
        mode={dTPickerModalProps.mode}
        value={dTPickerModalProps.value}
        onChange={dTPickerModalProps.onChange}
      />
      <View style={styles.time}>{CustomTimeDisplay()}</View>
      <RecurrenceSelection
        currentPicker={currentPicker}
        setCurrentPicker={setCurrentPicker}
        recurrencePattern={recurrencePattern}
        onChangeField={onChangeRecurrenceField}
      />
      <View style={styles.detailBar} />
      {error && <Text style={styles.errorText}>{error}</Text>}
    </>
  );
};

export default HabitEditMode;
