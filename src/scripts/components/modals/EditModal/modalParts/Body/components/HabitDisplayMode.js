import React from 'react';
import {
  Text,
  View,
} from 'react-native';
import DurationIcon from '../../../../../../../assets/icons/duration_icon.svg';
import styles from '../styles';
import { removeHTMLTags } from '../../../../../../utils';

const HabitDisplayMode = ({ modalDetail }) => {
  const {
    name, duration,
  } = modalDetail;
  return (
    <View>
      <View style={styles.name}>
        <Text style={styles.nameText}>{ removeHTMLTags(name) || ''}</Text>
      </View>
      <View style={styles.viewDetailBar} />
      <View style={styles.viewDuration}>
        <DurationIcon />
        <Text style={styles.durationText}>
          {duration ? `${duration * 60} minutes` : 'No assigned duration'}
        </Text>
      </View>
    </View>
  );
};

export default HabitDisplayMode;
