import React, { useState } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import moment from 'moment';
import { NumFreqDropDown, DayFreqDropDown, EndsDropDown } from './DropDowns';
import styles from './styles';
import { Components, DAYS } from './constants';
import DTPickerModal from '../../../../../DTPickerModal';

export default ({
  currentPicker,
  setCurrentPicker,
  recurrencePattern: rp,
  onChangeField,
}) => {
  const [dTPickerModalProps, setDTPickerModalProps] = useState({
    visible: false,
  });

  const handlePress = (day) => {
    if (!rp.daysOfWeek.includes(DAYS[day])) {
      onChangeField('daysOfWeek', [...rp.daysOfWeek, DAYS[day]]);
    } else {
      onChangeField(
        'daysOfWeek',
        rp.daysOfWeek.filter((el) => el !== DAYS[day]),
      );
    }
  };

  const dayToggle = (day) => {
    if (rp.daysOfWeek.includes(DAYS[day])) {
      const SelectedDay = Components[`${day}Selected`];
      return <SelectedDay />;
    } else {
      const Day = Components[day];
      return <Day />;
    }
  };

  const dayButtons = Object.keys(DAYS).map((day) => (
    <TouchableOpacity onPress={() => handlePress(day)} key={day}>
      {dayToggle(day)}
    </TouchableOpacity>
  ));

  return (
    <>
      <DTPickerModal
        visible={dTPickerModalProps.visible}
        dismissModal={() => setDTPickerModalProps({ visible: false })}
        mode={dTPickerModalProps.mode}
        value={dTPickerModalProps.value}
        onChange={dTPickerModalProps.onChange}
      />
      <View style={styles.recurrenceContainer}>
        <View style={styles.recurrenceFirstRow}>
          <Text style={styles.recurrenceText}>Repeats every</Text>
          <NumFreqDropDown
            currentPicker={currentPicker}
            setCurrentPicker={setCurrentPicker}
            selected={rp.frequency}
            onChangeField={onChangeField}
          />
          <DayFreqDropDown
            currentPicker={currentPicker}
            setCurrentPicker={setCurrentPicker}
            selected={rp.recurrenceType}
            onChangeField={onChangeField}
          />
        </View>
        {rp.recurrenceType === 'week' && (
          <>
            <Text style={styles.recurrenceText}>Repeat on</Text>
            <View style={styles.daysRow}>{dayButtons}</View>
          </>
        )}
        <View style={styles.recurrenceSecondRow}>
          <View style={styles.row}>
            <Text style={styles.recurrenceText}>Starts</Text>
            <TouchableWithoutFeedback
              onPress={() => {
                setDTPickerModalProps({
                  visible: true,
                  mode: 'date',
                  value: new Date(moment(rp.recurrenceStartDate)),
                  onChange: (date) => onChangeField('recurrenceStartDate', date),
                });
              }}
            >
              <View style={styles.dateContainer}>
                <Text style={styles.dateText}>
                  {moment(rp.recurrenceStartDate).format('M/D/YY')}
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View style={styles.row}>
            <Text style={styles.recurrenceText}>Ends</Text>
            <EndsDropDown
              currentPicker={currentPicker}
              setCurrentPicker={setCurrentPicker}
              selected={rp.recurrenceEndType}
              onChangeField={onChangeField}
            />
            {!rp.recurrenceEndType
              || (rp.recurrenceEndType === 'on' && (
                <TouchableWithoutFeedback
                  onPress={() => {
                    setDTPickerModalProps({
                      visible: true,
                      mode: 'date',
                      value: new Date(moment(rp.recurrenceEndDate)),
                      onChange: (date) => onChangeField('recurrenceEndDate', date),
                    });
                  }}
                >
                  <View
                    style={[styles.dateContainer, { marginHorizontal: 10 }]}
                  >
                    <Text style={styles.dateText}>
                      {moment(rp.recurrenceEndDate).format('M/D/YY')}
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              ))}
          </View>
        </View>
      </View>
    </>
  );
};
