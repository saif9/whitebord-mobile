import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import moment from 'moment';
import styles from '../../styles';
import DeadlineIcon from '../../../../../../../../assets/icons/deadline_icon.svg';

export default ({
  startTime, endTime, setDTPickerModalProps, onChangeField
}) => {
  return (
    <View style={styles.timeDisplay}>
      <DeadlineIcon margin={5} />
      <View style={{ ...styles.timeDisplay, justifyContent: 'space-evenly', flex: 1 }}>
        <TouchableOpacity onPress={() => {
          setDTPickerModalProps({
            visible: true,
            mode: 'time',
            value: new Date(startTime),
            onChange: (date) => onChangeField('startDate', moment(date)),
          });
        }}
        >
          <Text style={styles.timeText}>
            {moment(startTime).format('h:mma')}
          </Text>
        </TouchableOpacity>
        <Text style={styles.timeText}>—</Text>
        <TouchableOpacity onPress={() => {
          setDTPickerModalProps({
            visible: true,
            mode: 'time',
            value: new Date(endTime),
            onChange: (date) => onChangeField('endDate', moment(date)),
          });
        }}
        >
          <Text style={styles.timeText}>
            {moment(endTime).format('h:mma')}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
