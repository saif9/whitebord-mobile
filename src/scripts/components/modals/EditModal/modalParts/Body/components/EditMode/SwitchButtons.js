import React from 'react';
import { View, Text, Switch } from 'react-native';
import styles from '../../styles';

export default ({
  recurring, setRecurring, modalDetail, onChangeField
}) => {
  return (
    <View style={styles.switch}>
      <Text style={styles.recurrenceText}>All Day</Text>
      <Switch
        trackColor={{ false: '#9d9dba', true: '#6574FF' }}
        thumbColor="#FFFFFF"
        ios_backgroundColor="#9d9dba"
        onValueChange={(value) => onChangeField('allDay', value)}
        value={modalDetail.allDay}
        style={styles.switchButton}
      />
      {modalDetail.type !== 'habit' && (
      <>
        <Text style={styles.recurrenceText}>Repeats</Text>
        <Switch
          trackColor={{ false: '#9d9dba', true: '#6574FF' }}
          thumbColor="#FFFFFF"
          ios_backgroundColor="#9d9dba"
          onValueChange={(value) => setRecurring(value)}
          value={recurring}
          style={styles.switchButton}
        />
      </>
      )}

    </View>
  );
};
