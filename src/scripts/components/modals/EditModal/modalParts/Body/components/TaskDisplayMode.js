import React from 'react';
import {
  Text,
  View,
} from 'react-native';
import moment from 'moment';
import { useSelector } from 'react-redux';
import DeadlineIcon from '../../../../../../../assets/icons/deadline_icon.svg';
import NoteIcon from '../../../../../../../assets/icons/note_icon.svg';
import ProjectIcon from '../../../../../../../assets/icons/project_icon.svg';
import PeopleIcon from '../../../../../../../assets/icons/people.svg';
import styles from '../styles';
import { removeHTMLTags } from '../../../../../../utils';
import { projectsSelectors } from '../../../../../../state/slices/projects/projectsSlice';

const TaskDisplayMode = ({ modalDetail }) => {
  const {
    name, note, deadline,
  } = modalDetail;
  const { name: projectName } = useSelector((state) => projectsSelectors.selectById(state, modalDetail.projectId)) || {};
  return (
    <View>
      <View style={styles.name}>
        <Text style={styles.nameText}>{ removeHTMLTags(name) || 'New Task'}</Text>
      </View>
      <View style={styles.viewDeadline}>
        <DeadlineIcon width={20} height={20} />
        <Text style={{ ...styles.deadlineText, paddingBottom: 10 }}>
          {!deadline
            ? 'No assigned deadline'
            : moment(deadline).format('dddd, MMM D, YYYY')}
        </Text>
      </View>
      <View style={styles.viewDetailBar} />
      <View style={styles.viewNote}>
        <ProjectIcon width={20} height={20} />
        <Text style={styles.viewNoteText}>{projectName || 'No Project'}</Text>
      </View>
      <View style={styles.viewDetailBar} />
      <View style={styles.viewNote}>
        <PeopleIcon width={20} height={20} />
        <Text style={styles.viewNoteText}>{modalDetail.collaborator || 'No Collaborator'}</Text>
      </View>
      <View style={styles.viewDetailBar} />
      <View style={styles.viewNote}>
        <NoteIcon width={20} height={20} />
        <Text style={styles.viewNoteText}>{note || 'No Notes'}</Text>
      </View>
    </View>
  );
};

export default TaskDisplayMode;
