import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import DropDownPicker from 'react-native-dropdown-picker';
import styles from './styles';
import { INBOX_ID } from '../../../../../../../../../constants';
import { memoAllSections } from '../../../../../../../../state/slices/sections/selectors';
import { projectsSelectors } from '../../../../../../../../state/slices/projects/projectsSlice';

const LABEL = 'Project';
export default ({
  currentPicker, setCurrentPicker, onChangeField, modalDetail
}) => {
  const projectItems = [];
  const sectionsArr = useSelector(memoAllSections);
  const projectsObj = useSelector(projectsSelectors.selectEntities);

  const sections = sectionsArr.map(
    ({ id, name, projects }) => ({
      label: name,
      value: id,
      projects,
      disabled: true
    }),
  );

  for (const section of sections) {
    projectItems.push(section);
    for (const project of [...section.projects].reverse()) {
      if (projectsObj[project]) {
        projectItems.push({
          label: projectsObj[project]?.name,
          value: project,
          parent: section.value,
        });
      }
    }
  }

  // Setting `disabled` prop to false enables the selection of the "Inbox" dropdown item
  projectItems.unshift({ label: 'Inbox', value: INBOX_ID, disabled: false });

  const [value, setValue] = useState(modalDetail.projectId);
  const [items, setItems] = useState(projectItems);

  useEffect(() => {
    onChangeField('projectId', value);
  }, [value]);

  useEffect(() => {
    setValue(modalDetail.projectId);
  }, [modalDetail.projectId]);
  return (
    <DropDownPicker
      open={currentPicker === LABEL}
      value={value}
      setOpen={(open) => setCurrentPicker(open ? LABEL : '')}
      setValue={setValue}
      items={items}
      setItems={setItems}
      showArrowIcon={false}
      showTickIcon={false}
      maxHeight={300}
      dropDownDirection="BOTTOM"
      placeholder="Select Project"
      placeholderStyle={styles.projectPlaceholderStyle}
      containerStyle={styles.projectContainerStyle}
      dropDownContainerStyle={styles.projectDropDownContainerStyle}
      style={styles.projectDropDownPickerStyle}
      listParentLabelStyle={styles.projectParentLabelStyle}
      textStyle={styles.dropDownText}
      listMode="SCROLLVIEW"
      zIndex={2000}
      zIndexInverse={1000}
    />
  );
};
