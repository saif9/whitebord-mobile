/* eslint-disable no-nested-ternary */
import React from 'react';
import { View, Pressable, Text } from 'react-native';
import styles from './styles';

export default (props) => {
  const { modalDetail, onChangeField } = props;
  const { type, mode } = modalDetail;
  const switchTypes = ['event', 'task', 'habit'];

  return (
    <View style={styles.switcherContainer}>
      {switchTypes.map((switchType) => (
        <Pressable
          onPress={
            mode === 'create' ? () => onChangeField('type', switchType) : null
          }
          key={switchType}
        >
          <View
            style={
              type === switchType ? styles.selectedStyle : styles.defaultStyle
            }
          >
            <Text
              style={
                type === switchType
                  ? styles.selectedFont
                  : mode === 'create'
                    ? styles.defaultFont
                    : styles.disabledFont
              }
            >
              {switchType}
            </Text>
          </View>
        </Pressable>
      ))}
    </View>
  );
};
