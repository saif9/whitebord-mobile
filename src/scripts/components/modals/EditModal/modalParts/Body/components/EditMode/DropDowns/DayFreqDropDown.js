import React, { useState, useEffect } from 'react';
import DropDownPicker from 'react-native-dropdown-picker';
import { dayFreqDropDownItems } from '../constants';
import styles from './styles';

const LABEL = 'Day';
export default ({
  currentPicker, setCurrentPicker, selected, onChangeField
}) => {
  const [value, setValue] = useState(selected);
  const [items, setItems] = useState(dayFreqDropDownItems);

  useEffect(() => {
    onChangeField('recurrenceType', value);
  }, [value]);

  return (
    <DropDownPicker
      open={currentPicker === LABEL}
      value={value}
      setOpen={(open) => setCurrentPicker(open ? LABEL : '')}
      setValue={setValue}
      items={items}
      setItems={setItems}
      showArrowIcon={false}
      showTickIcon={false}
      maxHeight={300}
      containerStyle={styles.dayFreqContainerStyle}
      dropDownContainerStyle={styles.dayFreqDropDownContainerStyle}
      style={styles.dayFreqDropDownPickerStyle}
      textStyle={styles.dropDownText}
      listMode="SCROLLVIEW"
    />
  );
};
