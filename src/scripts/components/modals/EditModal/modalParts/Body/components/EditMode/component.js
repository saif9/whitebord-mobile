import React from 'react';
import {
  TextInput,
  View
} from 'react-native';
// import { RichEditor } from 'react-native-pell-rich-editor';
import TaskEditBody from './TaskEditBody';
import EventEditBody from './EventEditBody';
import HabitEditBody from './HabitEditBody';
import TypeSwitcher from './TypeSwitcher';
import SwitchButtons from './SwitchButtons';
import styles from '../../styles';
import { removeHTMLTags } from '../../../../../../../utils';

// const dict = {
//   '<div>': '<div@1234>',
//   '<h1>': '<h1@1234>',
//   '<h2>': '<h2@1234>',
//   '<h3>': '<h3@1234>',
//   '<b>': '<b@1234>',
//   '<i>': '<i@1234>',
//   '<span>': '<span@1234>',
//   '<p>': '<p@1234>',
// };
// const replaceTags = (str, mapObj) => {
//   const re = new RegExp(Object.keys(mapObj).join('|'), 'gi');
//   return str?.replace(re, (matched) => {
//     return mapObj[matched.toLowerCase()];
//   });
// };

const EditMode = ({
  modalDetail, setModalDetail, error, submitChanges
}) => {
  // const richEditor = useRef();
  // const modeName = replaceTags(modalDetail.name, dict);

  let Body;
  switch (modalDetail.type) {
    case 'task': {
      Body = TaskEditBody;
      break;
    }
    case 'event': {
      Body = EventEditBody;
      break;
    }
    case 'habit': {
      Body = HabitEditBody;
      break;
    }
    default:
      return <View />;
  }

  const onChangeField = (fieldName, data) => {
    setModalDetail({
      ...modalDetail,
      [fieldName]: data
    });
  };

  // const onKeyDown = (e) => {
  //   if (e.keyCode === 13) {
  //     richEditor.current.blurContentEditor();
  //     setTimeout(submitChanges, 1);
  //   }
  // };
  return (
    <>
      <View style={styles.htmlName}>
        {/* <RichEditor
          ref={richEditor}
          onKeyDown={onKeyDown}
          onChange={(name) => onChangeField('name', name?.replace(/@1234/g, ''))}
          initialContentHTML={modeName}
          // placeholder={`Think of a name for this ${modalDetail.type}!`}
          initialFocus
          editorStyle={{
            contentCSSText: 'text-align: center; font-size: 1.3rem;'
          }}
        /> */}
        <TextInput
          style={styles.nameText}
          value={removeHTMLTags(modalDetail.name)}
          placeholder={`Think of a name for this ${modalDetail.type}!`}
          onChangeText={(name) => onChangeField('name', name)}
          autoFocus
          selectTextOnFocus
          onSubmitEditing={submitChanges}
        />
      </View>
      <View style={styles.typeSwitcher}>
        <TypeSwitcher onChangeField={onChangeField} modalDetail={modalDetail} />
        <SwitchButtons
          recurring={modalDetail.isRecurrenceItem}
          setRecurring={() => onChangeField('isRecurrenceItem', !modalDetail.isRecurrenceItem)}
          modalDetail={modalDetail}
          onChangeField={onChangeField}
        />
      </View>
      {Body ? (
        <Body
          modalDetail={modalDetail}
          setModalDetail={setModalDetail}
          error={error}
          isRecurrenceItem={modalDetail.isRecurrenceItem}
        />
      ) : (
        <View />
      )}
    </>
  );
};

export default EditMode;
