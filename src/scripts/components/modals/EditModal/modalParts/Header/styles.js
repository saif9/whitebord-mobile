import { StyleSheet } from 'react-native';
import { font } from '../../../../../../constants';

export default StyleSheet.create({
  topRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 20,
  },
  closeModal: {
    width: 50,
  },
  cancelText: {
    color: '#ABA9FF',
    fontFamily: font.regular,
  },
  editSaveText: {
    color: '#6574FF',
    fontFamily: font.semibold,
  },
});
