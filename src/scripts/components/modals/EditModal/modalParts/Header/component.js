import React from 'react';
import {
  Text, View, TouchableOpacity
} from 'react-native';
import { useDispatch } from 'react-redux';
import { toggleEditMode, toggleModal } from '../../../../../state/actions/modal';

import styles from './styles';

export default ({ editing, submitChanges, modalDetail }) => {
  const dispatch = useDispatch();
  const { editDisabled } = modalDetail;
  const saveEditPress = () => {
    if (editing) {
      submitChanges();
    } else {
      toggleEditMode(true, dispatch);
    }
  };

  const closeModal = () => {
    toggleEditMode(false, dispatch);
    toggleModal(false, dispatch);
  };

  return (
    <View style={styles.topRow}>
      <TouchableOpacity
        style={styles.closeModal}
        onPress={closeModal}
      >
        <Text style={styles.editSaveText}>{editing ? 'cancel' : 'X'}</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          ...styles.closeModal,
          justifyContent: 'flex-end',
          flexDirection: 'row'
        }}
        onPress={saveEditPress}
        disabled={editDisabled}
      >
        <Text style={styles.editSaveText}>{editing ? 'save' : !editDisabled && 'edit'}</Text>
      </TouchableOpacity>
    </View>
  );
};
