import { StyleSheet } from 'react-native';

const bluClr = '#2F57E9';
export default StyleSheet.create({
  containerStyle: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: bluClr,
  },
  textStyle: {
    color: 'white',
    fontWeight: '500'
  }
});
