import React, { memo } from 'react';
import FastImage from 'react-native-fast-image';
import { Text, View } from 'react-native';
import styles from './styles';

const CusAvatar = ({ fName, lName, size }) => {
  const textContainerStyle = {
    ...styles.containerStyle,
    borderRadius: size / 2,
    marginTop: -(size / 20),
    height: size,
    width: size,
  };
  return (
    <View style={textContainerStyle}>
      <Text
        style={{
          ...styles.textStyle,
          fontSize: size / 2.5,
        }}
        adjustsFontSizeToFit
      >
        {(fName?.[0] || '') + (lName?.[0] || '')}
      </Text>
    </View>
  );
};

const UserAvatar = ({
  size, name, src, style
}) => {
  const [fName, lName] = name?.toUpperCase().split(' ');

  return (
    <View style={style}>
      {src ? (
        <FastImage
          style={{ width: size, height: size, borderRadius: size / 2, }}
          source={{
            uri: src
          }}
          resizeMode={FastImage.resizeMode.cover}
        />
      )
        : <CusAvatar fName={fName} lName={lName} size={size} />}
    </View>
  );
};

export default memo(UserAvatar);
