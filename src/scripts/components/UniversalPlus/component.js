import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import moment from 'moment';
import { useDispatch } from 'react-redux';
import styles from './styles';
import PlusButton from '../../../assets/icons/plus.svg';
import { setModal, toggleModal } from '../../state/actions/modal';

const UniversalPlus = ({ bottomMargin }) => {
  const dispatch = useDispatch();

  const onTaskPress = () => {
    toggleModal(true, dispatch);
    const startDate = moment();
    const endDate = moment();
    setModal({
      name: '',
      startDate,
      endDate,
      completed: false,
      id: null,
      mode: 'create',
      type: 'task',
      subtype: 'check',
      duration: null,
      deadline: moment(),
      allDay: true,
      editing: true,
      projectId: null
    }, dispatch);
  };

  return (
    <View style={[styles.buttonView, bottomMargin]}>
      <TouchableOpacity onPress={onTaskPress}>
        <PlusButton width={70} height={70} />
      </TouchableOpacity>
    </View>
  );
};

export default UniversalPlus;
