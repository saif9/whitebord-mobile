import { StyleSheet } from 'react-native';
import layout from '../../../constants/layout';

const windowWidth = layout.window.width;
const windowHeight = layout.window.height;

export default StyleSheet.create({
  buttonView: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
  taskButtonView: {
    position: 'absolute',
    bottom: windowHeight - (windowHeight / 1.05),
    right: windowWidth - (windowWidth / 1.2),
  },
  eventButtonView: {
    position: 'absolute',
    bottom: windowHeight - (windowHeight / 1.1),
    right: windowWidth - (windowWidth / 1.02),
  },
  overlay: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    backgroundColor: 'rgba(255,255,255,0.6)',
  },
});
