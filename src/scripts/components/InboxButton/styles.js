import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  inbox: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 100,
    backgroundColor: '#F8F9FF',
    marginLeft: 10,
  }
});
