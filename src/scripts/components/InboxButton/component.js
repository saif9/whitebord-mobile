import React from 'react';
import { TouchableOpacity } from 'react-native';
import styles from './styles';
import InboxIcon from '../../../assets/icons/inbox.svg';

const InboxButton = ({ navigation }) => {
  return (
    <TouchableOpacity
      style={[styles.navButton, styles.inbox]}
      onPress={() => navigation.navigate('Inbox')}
    >
      <InboxIcon />
    </TouchableOpacity>
  );
};

export default InboxButton;
