import React from 'react';
import { TouchableOpacity } from 'react-native';
import styles from './styles';

const MenuButton = ({ onPress, children }) => {
  return (
    <TouchableOpacity style={[styles.navButton, styles.menuBtnBg]} onPress={onPress}>
      {children}
    </TouchableOpacity>

  );
};

export default MenuButton;
