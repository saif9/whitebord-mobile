import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  menuBtnBg: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 40 / 2,
    backgroundColor: '#F8F9FF',
    marginLeft: 10,
  }
});
