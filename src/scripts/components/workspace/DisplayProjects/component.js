import React from 'react';
import {
  View,
  Text,
  Pressable,
} from 'react-native';
import { useSelector } from 'react-redux';
import styles from './styles';
import projectStyles from '../../../screens/workspace/components/Project/styles';
import Checkmark from '../../../../assets/icons/checkmark.svg';
import { projectsSelectors } from '../../../state/slices/projects/projectsSlice';

const DisplayProjects = ({ id, selectedProjectId, setSelectedProjectId }) => {
  const project = useSelector((state) => projectsSelectors.selectById(state, id));
  return !project ? <View /> : (
    <Pressable style={styles.projectContainer} onPress={() => setSelectedProjectId(id)}>
      <View style={styles.projectNameContainer}>
        <View style={styles.checkmarkContainer}>{id === selectedProjectId && <Checkmark />}</View>
        <Text style={id === selectedProjectId
          ? [projectStyles.projectFont, styles.selectedProject]
          : projectStyles.projectFont}
        >
          {project?.name}
        </Text>
      </View>
    </Pressable>
  );
};

export default DisplayProjects;
