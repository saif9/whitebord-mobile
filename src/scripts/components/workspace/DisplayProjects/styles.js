import { StyleSheet } from 'react-native';
import { font } from '../../../../constants';
import { wp } from '../../../utils';

export default StyleSheet.create({
  checkmarkContainer: {
    width: wp(7),
    alignItems: 'center',
    justifyContent: 'center',
  },
  selectedProject: {
    color: '#6574FF',
  },
  projectText: {
    fontFamily: font.medium,
    fontSize: 16,
    color: '#434279',
    paddingHorizontal: wp(2),
  },
  projectContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    paddingVertical: wp(3.5)
  },
  projectNameContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
});
