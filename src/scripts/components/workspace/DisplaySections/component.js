import React from 'react';
import {
  View,
  Text,
} from 'react-native';
import { useSelector } from 'react-redux';
import styles from './styles';
import AccordionListItem from '../../../screens/workspace/components/AccordianListItem';
import DisplayProjects from '../DisplayProjects';
import { sectionsSelectors } from '../../../state/slices/sections/sectionsSlice';

const DisplaySections = ({ selectedProjectId, setSelectedProjectId }) => {
  const sections = useSelector(sectionsSelectors.selectAll);

  return (
    sections.length === 0
      ? <Text>Looks like you don&apos;t have any sections right now!</Text>
      : sections.map((section) => {
        return (
          // Don't show sections that have no projects.
          section.projects.length === 0
            ? <View key={section.id} />
            : (
              <View key={section.id} style={styles.AccordionListWrapper}>
                <AccordionListItem key={section.id} title={section.name}>
                  {section.projects?.map((id) => (
                    <DisplayProjects
                      key={id}
                      id={id}
                      selectedProjectId={selectedProjectId}
                      setSelectedProjectId={setSelectedProjectId}
                    />
                  ))}
                </AccordionListItem>
              </View>
            )
        );
      })
  );
};

export default DisplaySections;
