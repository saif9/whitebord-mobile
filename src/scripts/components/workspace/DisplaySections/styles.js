import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  selectedProject: {
    color: '#6574FF',
  },
});
