import React from 'react';
import { TouchableOpacity } from 'react-native';
import styles from './styles';
import HabitUnchecked from '../../../assets/icons/habit-unchecked.svg';
import HabitChecked from '../../../assets/icons/habit-checked.svg';
import RecurrenceUnchecked from '../../../assets/icons/recurring-unchecked.svg';
import RegularUnchecked from '../../../assets/icons/regular-unchecked.svg';
import RegularChecked from '../../../assets/icons/regular-checked.svg';

class AdaptableCheckBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: props.value };
  }

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value) this.setState({ value: nextProps.value });
  }

  render() {
    const toggleValue = async () => {
      const newValue = !this.state.value;
      this.setState({ value: newValue });
      setTimeout(() => this.props.onValueChange(newValue), 300);
    };

    // `duration` is measured in hours
    const sideLength = (this.props.duration * 60 <= 20) ? 9 : 20;

    switch (this.props.type) {
      case 'habit':
        return (
          <TouchableOpacity style={styles.checkbox} onPress={toggleValue}>
            {this.state.value ? (
              <HabitChecked height={sideLength} width={sideLength} />
            ) : (
              <HabitUnchecked height={sideLength} width={sideLength} />
            )}
          </TouchableOpacity>
        );
      case 'task':
        return (
          <TouchableOpacity style={styles.checkbox} onPress={toggleValue}>
            {this.state.value ? (
              <RegularChecked height={sideLength} width={sideLength} />
            ) : (
              <RecurrenceUnchecked height={sideLength} width={sideLength} />
            )}
          </TouchableOpacity>
        );
      default:
        return (
          <TouchableOpacity style={styles.checkbox} onPress={toggleValue}>
            {this.state.value ? (
              <RegularChecked height={sideLength} width={sideLength} />
            ) : (
              <RegularUnchecked height={sideLength} width={sideLength} />
            )}
          </TouchableOpacity>
        );
    }
  }
}

export default AdaptableCheckBox;
