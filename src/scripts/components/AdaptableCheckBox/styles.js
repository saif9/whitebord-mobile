import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  checkbox: {
    width: 20,
    height: 20,
    marginRight: 10, // for tasklist
    marginLeft: 4, // for tasklist
    zIndex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
