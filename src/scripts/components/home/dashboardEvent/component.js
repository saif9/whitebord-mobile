import React from 'react';
import { useDispatch } from 'react-redux';
import { Text, View, Pressable } from 'react-native';
import moment from 'moment';
import { setModal, toggleModal } from '../../../state/actions/modal';
import { EVENT_COLORS_W_BODY } from '../../../../constants/event_colors';
import styles from './styles';
import { removeHTMLTags } from '../../../utils';

const DashboardEvent = (props) => {
  // itemToEdit is the master item of recurrence item instances
  const item = props.item.itemToEdit || props.item;
  const {
    id, name, startDate, endDate, color, allDay
  } = item;
  const dispatch = useDispatch();

  const onEventPress = () => {
    // Google events include a key called "summary"
    if (!props.item.summary) {
      toggleModal(true, dispatch);
      setModal({ ...item, mode: 'edit' }, dispatch);
    } else {
      toggleModal(true, dispatch);
      setModal({ ...props.item, mode: 'edit', editDisabled: true }, dispatch);
    }
  };
  const start = moment(startDate).format('hh:mmA');
  const end = moment(endDate).format('hh:mmA');
  const duration = allDay ? 'All Day' : `${start} - ${end}`;
  const maxLength = 60;
  const dynamicName = name.length >= maxLength ? `${name.slice(0, maxLength)}...` : name;
  return (
    <Pressable
      style={[
        styles.eventBlock,
        {
          backgroundColor: EVENT_COLORS_W_BODY[color]?.body || '#C7C1F0',
          height: 40,
        },
      ]}
      key={id}
      onPress={onEventPress}
    >
      <View style={styles.eventContent}>
        <View style={styles.timeSlotView}>
          <Text style={styles.timeSlotText}>{duration}</Text>
        </View>
        <View style={styles.eventView}>
          <Text style={styles.eventText}>{removeHTMLTags(dynamicName)}</Text>
        </View>
      </View>
    </Pressable>
  );
};

export default DashboardEvent;
