import { StyleSheet } from 'react-native';
import { font } from '../../../../constants';

export default StyleSheet.create({
  eventBlock: {
    marginVertical: 5,
    paddingHorizontal: 5,
    justifyContent: 'center',
    borderRadius: 5,
  },
  eventContent: {
    flexDirection: 'row',
    marginHorizontal: 10,
  },
  eventText: {
    fontFamily: font.regular,
  },
  eventView: {
    width: 0,
    flex: 1,
  },
  timeSlotText: {
    fontFamily: font.light,
    fontSize: 13,
  },
  timeSlotView: {
    justifyContent: 'center',
    marginRight: 15,
  },
});
