import React from 'react';
import { View } from 'react-native';
import Weekday from './weekday';
import styles from './styles';
import { layout } from '../../../../constants';

const { width } = layout.window;

const addMargin = (index) => {
  if (index === 0) {
    return { marginLeft: width / 30 };
  } else if (index === 6) {
    return { marginRight: width / 30 };
  } else {
    return {};
  }
};

function Train({ weekdays, date, setDate }) {
  return (
    <View style={styles.train}>
      {weekdays.map((w, index) => (
        <Weekday
          key={w}
          date={w}
          addMargin={addMargin(index)}
          selectedDate={date}
          setDate={setDate}
        />
      ))}
    </View>
  );
}

export default Train;
