import { StyleSheet } from 'react-native';
import { font } from '../../../../constants';

export default StyleSheet.create({
  horizCal: { height: 60, marginTop: 10 },
  train: {
    flexDirection: 'row',
    height: 60,
    alignItems: 'center',
  },
  weekday: {
    flex: 1,
    height: 55,
    paddingVertical: 5,
    marginRight: 5,
    backgroundColor: '#FFEEBB',
    alignItems: 'center',
    borderRadius: 5,
  },
  dateView: { flex: 1, justifyContent: 'center' },
  dateText: { fontSize: 16, fontFamily: font.bold },
  weekdayText: { fontSize: 15, fontFamily: font.semibold },
});
