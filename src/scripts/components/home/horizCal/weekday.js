import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import moment from 'moment';
import styles from './styles';

const formatWeekday = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

const additionalStyles = (date, selectedDate) => {
  const isSelectedDate = moment(date).isSame(selectedDate, 'day');
  const isToday = moment(date).isSame(new Date(), 'day');

  if (isSelectedDate) {
    return {
      paddingVertical: 7.5,
      paddingHorizontal: 2.5,
      height: 60,
      backgroundColor: '#FDCA29',
    };
  } else if (isToday) {
    return {
      backgroundColor: '#FEE390',
      height: 58,
      paddingHorizontal: 1,
      paddingVertical: 6,
    };
  } else {
    return {};
  }
};

function Weekday({
  date, addMargin, selectedDate, setDate
}) {
  return (
    <TouchableOpacity
      style={[styles.weekday, addMargin, additionalStyles(date, selectedDate)]}
      onPress={() => setDate(moment(date))}
    >
      <View style={styles.dateView}>
        <Text style={styles.dateText}>{date.getDate()}</Text>
      </View>
      <Text style={styles.weekdayText}>{formatWeekday[date.getDay()]}</Text>
    </TouchableOpacity>
  );
}

export default Weekday;
