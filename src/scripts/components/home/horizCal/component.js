import React, { useEffect, useRef, useState } from 'react';
import { View } from 'react-native';
import PagerView from 'react-native-pager-view';
import moment from 'moment';
import styles from './styles';
import Train from './train';

/**
 * grabs an array of 11 weeks worth of `Date` objects around the given `date`
 * @param {Date} date date used to grab `trains`
 * @returns an array of 11 weeks based on `date`
 */

const getTrains = (date) => {
  // sets the sunday of first week in the array
  const firstSunday = new Date(
    date.getFullYear(),
    date.getMonth(),
    date.getDate() - date.getDay() - 7 * 5,
  );

  const trains = [];

  for (let i = 0; i <= 11; i++) {
    // sets the sunday of a certain week
    const sunday = new Date(
      firstSunday.getFullYear(),
      firstSunday.getMonth(),
      firstSunday.getDate() + i * 7,
    );

    const weekdays = [];

    // cycles thru the days of the week
    for (let days = 0; days < 7; days++) {
      weekdays.push(
        new Date(
          sunday.getFullYear(),
          sunday.getMonth(),
          sunday.getDate() + days,
        ),
      );
    }

    const train = { key: i, weekdays };

    trains.push(train);
  }

  return trains;
};

const HorizCal = ({
  date, setDate, displayMonth, setDisplayMonth
}) => {
  const [trains, setTrains] = useState(getTrains(new Date()));
  const [position, setPosition] = useState(5);
  const pagerView = useRef();

  useEffect(() => {
    date = new Date(date.startOf('day'));
    navToPosition();
  }, [date]);

  const isDateDisplayed = (pos) => {
    // position is passed and a boolean is returned, indicating whether the selectedDate is currently being shown
    return date >= trains[pos].weekdays[0] && date <= trains[pos].weekdays[6];
  };

  const updateDisplayMonth = (d) => {
    const newDisplayMonth = moment(d).format('MMMM YYYY');
    if (newDisplayMonth !== displayMonth) {
      setDisplayMonth(newDisplayMonth);
    }
  };

  const navToPosition = () => {
    // checks if new selected date is being displayed - if so, do nothing
    if (isDateDisplayed(position)) return updateDisplayMonth(date);

    // otherwise, that means the user selected a date using the calendar modal from dashboard - therefore, we need to rebuild the trains to show that date
    return rebuildTrains(date);
  };

  // trains needs to rebuild whenever a user wants to see weeks past the 11 weeks in the current train
  const rebuildTrains = (d) => {
    updateDisplayMonth(d);
    setTrains(getTrains(d));
    // changes the position of the page view where the selected date is displayed
    setPosition(5);
    pagerView.current.setPageWithoutAnimation(5);
  };

  const onPageSelected = (event) => {
    const p = event.nativeEvent.position;
    setPosition(p);

    // rebuilds the trains if swiping to see weeks outside of the 11 weeks in the current train
    if (p === 0) {
      rebuildTrains(trains[0].weekdays[0]);
    } else if (p === trains.length - 1) {
      rebuildTrains(trains[trains.length - 1].weekdays[0]);
    } else if (isDateDisplayed(p)) {
      // if the date is showing in the current train, then have the display month be based on the selected date
      updateDisplayMonth(date);
    } else {
      // otherwise, the display month will be based on the first day in the train
      updateDisplayMonth(trains[p].weekdays[0]);
    }
  };

  return (
    <View style={styles.horizCal}>
      <PagerView
        initialPage={5}
        ref={pagerView}
        onPageSelected={onPageSelected}
      >
        {trains.map((train) => (
          <Train
            key={train.key}
            weekdays={train.weekdays}
            date={date}
            setDate={setDate}
          />
        ))}
      </PagerView>
    </View>
  );
};

export default HorizCal;
