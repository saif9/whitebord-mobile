// An enum with all the firebase collections.

export const Collections = {
  USERS: 'users',
  SECTIONS: 'sections',
  PROJECTS: 'projects',
  WHITEBOARD_ITEMS: 'whiteboardItems',
  RECURRENCE_EXCEPTIONS: 'recurrenceExceptions',
  QUICK_TASKS: 'quickTasks',
  PUBLIC_PROFILES: 'publicProfiles',
  INVITES: 'invites',
  NOTIFICATIONS: 'notifications'
};
