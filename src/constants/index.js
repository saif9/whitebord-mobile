import layout from './layout';

const SERVER_ENDPOINTS = {
  PROD: 'TODO',
  QA: 'TODO',
  LOCAL: 'TODO'
};

// eslint-disable-next-line no-undef
const API_URL = __DEV__ ? SERVER_ENDPOINTS.LOCAL : SERVER_ENDPOINTS.PROD;

export {
  API_URL,
  layout,
  SERVER_ENDPOINTS,
};

// endpoints for main db
export const backendRoot = 'https://us-central1-whiteboard-backup.cloudfunctions.net';
export const site = 'https://whiteboard-backup.web.app';
// // endpoints for mock db
// export const backendRoot = 'https://us-central1-whiteboard-test-21w.cloudfunctions.net';
// export const site = 'https://whiteboard-test-21w.web.app';

export const rfc3339FormatString = 'YYYY-MM-DDTHH:mm:ssZ';

export const calPrefix = '@@calendar';
export const CalProviders = {
  native: 'native',
  google: 'google',
  microsoft: 'microsoft',
};

export const font = {
  light: 'HKGrotesk-Light',
  regular: 'HKGrotesk-Regular',
  medium: 'HKGrotesk-Medium',
  semibold: 'HKGrotesk-SemiBold',
  bold: 'HKGrotesk-Bold',
  extrabold: 'HKGrotesk-ExtraBold',
};

export const INBOX_ID = 'INBOX_ID';

export const MIXPANEL_TOKEN = 'd075bbd08a8fc676c3e583d61cffeaf7';

export const FEEDBACK_URL = 'https://docs.google.com/forms/d/e/1FAIpQLScwhR6Edhdt76Q1kA9QNbLdWg1MSh0DV_sXuacgsIPXzgmlLg/viewform?usp=sf_link';
// eslint-disable-next-line no-useless-escape
export const emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const cssText = `
span 
.h1 {font-size: 2rem; line-height: 23px; font-weight: normal; font-style: normal; padding: 0 0 0 0; margin: 0 0 0 0}
.h2 {font-size: 1.8rem; line-height: 23px; font-weight: normal; font-style: normal; padding: 0 0 0 0; margin: 0 0 0 0}
.h3 {font-size: 1.4rem; line-height: 23px; font-weight: normal; font-style: normal; padding: 0 0 0 0; margin: 0 0 0 0}
.bold { font-weight: bold; }
.italic {font-style: italic; }
h1 { font-size: 2rem; line-height: 23px; font-weight: normal; font-style: normal; padding: 0 0 0 0; margin: 0 0 0 0}
h2 {font-size: 1.8rem; line-height: 23px; font-weight: normal; font-style: normal; padding: 0 0 0 0; margin: 0 0 0 0}
h3 {font-size: 1.4rem; line-height: 23px; font-weight: normal; font-style: normal; padding: 0 0 0 0; margin: 0 0 0 0}
p {font-size: 1rem; line-height: 23px; font-weight: normal; font-style: normal; padding: 0 0 0 0; margin: 0 0 0 0}
b { font-weight: bold; }
i { font-style: italic; }
`;
