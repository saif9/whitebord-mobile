# Whiteboard Mobile App

React Native application for the Whiteboard mobile app

## Architecture

- [React Native](https://reactnative.dev/)
- [Redux](https://redux.js.org/)
- [Xcode](https://developer.apple.com/xcode/)

## Setup Steps
### Installation: Public Packages

1. Xcode:
```
xcode-select --install
```
2. Homebrew:
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```
3. Node:
```
brew install node@14.4.0
```
4. Watchman:
```
brew install watchman
```
5. React Native CLI:
```
npm install -g react-native-cli
```
6. CocoaPods:
```
sudo gem install cocoapods -v 1.10.1
```

### Installation: Private Package

1. Request and obtain access to the [repository of the private package](https://github.com/whiteboard-llc/react-native-week-view)
2. Follow instructions [here](https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token) to create a GitHub Personal Access token with the following scopes:
   - repo
   - write:packages
   - delete:packages
3. Create or edit `~/.npmrc` file to include the following line:
```
//npm.pkg.github.com/:_authToken=YOUR GITHUB ACCESS TOKEN
```


remove this dummy character and save it
### Setting Up Project

- `git clone https://github.com/whiteboard-llc/whiteboard-mobile.git`
- `cd whiteboard-mobile`
- `yarn && cd ios && pod install && cd .. && yarn ios`

## Authors
Designers:
- Emma Kallman
- Jennifer Xu
- Cindy Yuan
  
Developers:
- Ian Hou
- Jack Keane
- Janvi Kalra
- Mike Lee

## Addtional information on the private package
### Setting up symlink

One of the libraries used in this app, `@whiteboard-llc/react-native-week-view`, is forked from [here](https://github.com/hoangnm/react-native-week-view) in order to better fit its functionality to the needs of this app.

However, using `yarn link` is not possible with React Native projects, so the npm package `wml` is used to allow developers to further shape this library while developing this app.

Usage:

1. Install wml: `yarn global add wml`
2. Link repo to node module. Note that this command only needs to be run once: `wml add <path/to/@whiteboard-llc/react-native-week-view> ./node_modules/@whiteboard-llc/react-native-week-view`
3. Start watching all links added: `wml start`

After running `wml start`, the changes you make in `<path/to/@whiteboard-llc/react-native-week-view>` will be reflected on the simulator. `wml start` works like `yarn start` for the `react-native-week-view` library once configured by the command in step 2.
